import { HTMLAttributes, ReactNode } from "react";

type CollapseTitleProps = {
    children: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const CollapseTitle = ({ children, className, ...args }: CollapseTitleProps) => {
    const classes = ["collapse-title"];
    if (className) {
        classes.push(...className?.split(" "));
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
