import { ReactNode, SelectHTMLAttributes } from "react";

export type SelectProps = {
    children?: ReactNode;
    className?: string;
} & SelectHTMLAttributes<HTMLSelectElement>;

export const Select = ({ children, className, ...args }: SelectProps) => {
    const classes = ["select"];
    if (className) {
        classes.push(...className.split(" "));
    }

    return (
        <select className={classes.join(" ")} {...args}>
            {children}
        </select>
    );
};
