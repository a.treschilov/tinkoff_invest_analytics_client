import AsyncSelect, { AsyncProps } from "react-select/async";
import { GroupBase } from "react-select/dist/declarations/src/types";

// @ts-expect-error unknown
type AsyncSelectWrapperProps = AsyncProps<Option, IsMulti, GroupBase<Option>>;

export const AsyncSelectWrapper = ({ ...args }: AsyncSelectWrapperProps) => {
    return (
        <AsyncSelect
            {...args}
            classNames={{
                control: () => "py-[1px] rounded-r-lg"
            }}
            theme={theme => ({
                ...theme,
                borderRadius: 0,
                colors: {
                    ...theme.colors,
                    neutral0: "var(--color-base-100)",
                    neutral10: "color-mix(in oklab, var(--color-base-content) 20%, #0000)",
                    neutral20: "color-mix(in oklab, var(--color-base-content) 20%, #0000)",
                    neutral80: "var(--color-base-content)",
                    primary: "var(--color-primary)",
                    primary25: "color-mix(in oklab, var(--btn-color, var(--color-base-200)), #000 7%)",
                    primary50: "color-mix(in oklab, var(--btn-color, var(--color-base-200)), #000 7%)"
                }
            })}
        />
    );
};
