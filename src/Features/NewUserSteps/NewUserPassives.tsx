import { StepComponentProps } from "./NewUserSteps";
import { NewUserActivesMarketStepProps } from "./NewUserActivesMarket";
import { LoanManage } from "../Loan/LoanManage";
import { useState } from "react";
import { Trans } from "react-i18next";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

export const NewUserPassives = ({ onComplete, active }: StepComponentProps) => {
    const [passivesStep, setPassivesStep] = useState(1);

    if (!active) {
        return <></>;
    }
    return (
        <>
            <NewUserPassivesStep1
                show={passivesStep === 1}
                onNegative={() => onComplete()}
                onPositive={() => {
                    setPassivesStep(2);
                }}
            />
            <NewUserPassivesStep2
                show={passivesStep === 2}
                onNegative={() => onComplete()}
                onPositive={() => {
                    setPassivesStep(3);
                }}
            />
            <NewUserPassivesStep3 show={passivesStep === 3} onPositive={() => onComplete()} />
        </>
    );
};

export const NewUserPassivesStep1 = ({
    show,
    onNegative = () => {},
    onPositive = () => {}
}: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <div className={"text-lg mb-2"}>
                <Trans>NewUserWizard.Passives.PassivesExistQuestion</Trans>
            </div>
            <div>
                <Button
                    size="sm"
                    color={"primary"}
                    onClick={() => {
                        onNegative();
                    }}
                >
                    <Trans>NewUserWizard.Passives.PassivesExistNegativeAnswer</Trans>
                </Button>
                &nbsp;&nbsp;
                <Button
                    size="sm"
                    color={"primary"}
                    onClick={() => {
                        onPositive();
                    }}
                >
                    <Trans>NewUserWizard.Passives.PassivesExistPositiveAnswer</Trans>
                </Button>
            </div>
        </Card>
    );
};

const NewUserPassivesStep2 = ({
    show,
    onNegative = () => {},
    onPositive = () => {}
}: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <div className={"text-lg mb-2"}>
                <Trans>NewUserWizard.Passives.PassivesSetupIntegrationQuestion</Trans>
            </div>
            <div>
                <span className={"link"} onClick={onNegative}>
                    <Trans>NewUserWizard.Passives.PassivesSetupIntegrationNegativeAnswer</Trans>
                </span>
                &nbsp;&nbsp;
                <Button size="sm" color={"primary"} onClick={onPositive}>
                    <Trans>NewUserWizard.Passives.PassivesSetupIntegrationPositiveAnswer</Trans>
                </Button>
            </div>
        </Card>
    );
};

const NewUserPassivesStep3 = ({ show, onPositive = () => {} }: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <>
            <LoanManage />
            <div className="text-end">
                <Button color={"success"} onClick={onPositive}>
                    <Trans>NewUserWizard.Passives.PassivesCompleteIntegration</Trans>
                </Button>
            </div>
        </>
    );
};
