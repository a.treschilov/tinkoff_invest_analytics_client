import { FC, ReactNode } from "react";
import { Select as DaisyUISelect, SelectProps as DaisyUISelectProps } from "../../Common/Select/Select";

type SelectExtensions = {
    Label: typeof Label;
    Select: typeof Options;
    Description: typeof Description;
};

type SelectProps = {
    children: ReactNode;
    containerClassName?: string;
};
export const Select: FC<SelectProps> & SelectExtensions = ({ children, containerClassName = "" }: SelectProps) => {
    return <div className={`form-control w-full ` + containerClassName}>{children}</div>;
};

const Label = ({ children }: { children: ReactNode }) => {
    return (
        <label className="label">
            <span className={"label-text text-base-content"}>{children}</span>
        </label>
    );
};
Select.Label = Label;

const Description = ({ children }: { children: ReactNode }) => {
    return <div className={"text-xs mt-1"}>{children}</div>;
};

Select.Description = Description;

interface IOptionsLabel {
    children: ReactNode;
}

type OptionLabelProps = IOptionsLabel & DaisyUISelectProps;
const Options = ({ children, ...args }: OptionLabelProps) => {
    return <DaisyUISelect {...args}>{children}</DaisyUISelect>;
};
Select.Select = Options;

export default Select;
