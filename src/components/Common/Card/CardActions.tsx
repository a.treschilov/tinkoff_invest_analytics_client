import { ReactNode } from "react";

type CardActionsProps = {
    children?: ReactNode;
    className?: string;
};

export const CardActions = ({ children, className }: CardActionsProps) => {
    return <div className={"card-actions justify-end" + (className ? " " + className : "")}>{children}</div>;
};
