import { redirect } from "next/navigation";
import { UserService } from "../../src/services/UserSerice";
import { deleteCookie } from "cookies-next";

export default async function RootLayout(props: {
    params: Promise<{ lng: string }>;
    searchParams: Promise<{ [key: string]: string | string[] | undefined }>;
}) {
    const params = await props.params;
    const user = await UserService.getUser();
    const { auth_redirect = 0 } = await props.searchParams;

    if (user.userId !== null) {
        redirect("/" + params.lng + "/user");
    } else {
        deleteCookie("jwt");
        redirect("/" + params.lng + "/login?auth_redirect=" + auth_redirect);
    }
}
