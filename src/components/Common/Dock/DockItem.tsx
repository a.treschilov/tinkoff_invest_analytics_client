import { ButtonHTMLAttributes, FC, ReactNode } from "react";
import { DockItemLabel } from "./DockItemLabel";

type DockItemProps = {
    children?: ReactNode;
    active?: boolean;
} & ButtonHTMLAttributes<HTMLButtonElement>;

type DockItemPropsExtension = {
    Label: typeof DockItemLabel;
};

export const DockItem: FC<DockItemProps> & DockItemPropsExtension = ({
    children,
    className,
    active = false,
    ...args
}: DockItemProps) => {
    const classes = [];
    if (className) {
        classes.push(...className?.split(" "));
    }
    if (active) {
        classes.push("dock-active");
    }

    return (
        <button className={classes.join(" ")} {...args}>
            {children}
        </button>
    );
};

DockItem.Label = DockItemLabel;
