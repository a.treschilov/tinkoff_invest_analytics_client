"use client";

import { ChangeEvent, useEffect, useState } from "react";
import { ActionButtonText, ActionButtonTextProps } from "../../components/Common/ActionButtonText";
import { Trans } from "react-i18next";
import { CredentialBreadcrumb } from "./CredentialBreadcrumb";
import { ApiService } from "../../services/ApiService";
import { DefaultUserCredential } from "./CredentialStaticData";
import { CredentialFormBody } from "./CredentialFormBody";
import { Modal } from "../../components/Common/Modal/Modal";
import { Alert } from "../../components/Common/Alert/Alert";
import { PageLoader } from "../../components/Loader/Loaders";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

type CredentialFormProps = {
    userCredentialId: number;
    isShow: boolean;
    hideModal: () => void;
    successAction: (userCredentialId: number) => void;
};

export type UserCredentialType = {
    userCredentialId: number;
    brokerId: string;
    brokerName: string;
    apiKey: string;
    isActive: number;
};

export const CredentialForm = (props: CredentialFormProps) => {
    const [isLoaded, setIsLoaded] = useState(false);
    const [isNewCredential, setIsNewCredential] = useState(false);
    const [alertMessage, setAlertMessage] = useState<string | null>(null);
    const [userCredential, setUserCredential] = useState<UserCredentialType>(DefaultUserCredential);
    const [buttonHint, setButtonHint] = useState<ActionButtonTextProps>({
        status: "",
        children: <Trans>CredentialForm.EmptyText</Trans>
    });

    useEffect(() => {
        if (props.isShow) {
            if (props.userCredentialId === 0) {
                setIsNewCredential(true);
                setAlertMessage(null);
                setIsLoaded(false);
                setTimeout(() => {
                    setUserCredential(DefaultUserCredential);
                    setIsLoaded(true);
                }, 50);
            } else {
                setIsNewCredential(false);
                setAlertMessage(null);

                setIsLoaded(false);
                ApiService.fetch("/v1/user/credential/" + props.userCredentialId)
                    .then(response => {
                        setUserCredential(response);
                        setIsLoaded(true);
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
        }
    }, [props.isShow, props.userCredentialId]);

    const goTo = (name: string) => {
        if (name === "account" && props.userCredentialId !== 0) {
            props.successAction(props.userCredentialId);
        }
    };

    const showError = (message: string | null) => {
        setButtonHint({
            status: "error",
            children: <Trans>CredentialForm.ErrorText</Trans>
        });
        setAlertMessage(message);

        setTimeout(() => {
            setButtonHint({
                status: "",
                children: <Trans>CredentialForm.ErrorText</Trans>
            });
        }, 2000);
    };

    const handleHideModal = () => {
        props.hideModal();
    };

    const handleFormSubmit = (event: ChangeEvent<HTMLFormElement>) => {
        const newUserCredential: UserCredentialType = userCredential;

        if (isNewCredential) {
            addCredential(newUserCredential);
        } else {
            editCredential(newUserCredential);
        }

        event.preventDefault();
    };

    const addCredential = (userCredential: UserCredentialType) => {
        setIsLoaded(false);

        ApiService.post("/v1/user/credential", {
            brokerId: userCredential.brokerId,
            apiKey: userCredential.apiKey,
            isActive: userCredential.isActive
        })
            .then(response => {
                props.successAction(response.userCredentialId);
            })
            .catch(error => {
                showError(error.data.error);
            })
            .then(() => {
                setIsLoaded(true);
            });
    };

    const editCredential = (userCredential: UserCredentialType) => {
        setIsLoaded(false);

        ApiService.put("/v1/user/credential/" + userCredential.userCredentialId, {
            brokerId: userCredential.brokerId,
            apiKey: userCredential.apiKey,
            isActive: userCredential.isActive
        })
            .then(() => {
                props.successAction(props.userCredentialId);
            })
            .catch(error => {
                showError(error.response.data.error);
            })
            .then(() => {
                setIsLoaded(true);
            });
    };

    const renderAlert = () => {
        return (
            <Alert
                status="warning"
                show={alertMessage !== null}
                dismissible
                onClose={() => {
                    setAlertMessage(null);
                }}
            >
                {alertMessage}
            </Alert>
        );
    };

    const title = (
        <Trans>
            CredentialForm.
            {isNewCredential ? "HeaderAdd" : "HeaderEdit"}
        </Trans>
    );

    const changeForm = (credential: UserCredentialType) => {
        setUserCredential(credential);
    };

    return (
        <Modal show={props.isShow} onClose={() => handleHideModal()}>
            <form onSubmit={(event: ChangeEvent<HTMLFormElement>) => handleFormSubmit(event)}>
                <Modal.Header>{title}</Modal.Header>
                <Modal.Body>
                    {renderAlert()}
                    <Card className={"bg-base-300 p-2 pl-6 mb-4"} border={true}>
                        <CredentialBreadcrumb
                            credentialId={props.userCredentialId}
                            navClickHandler={tabName => goTo(tabName)}
                            activeTab="credential"
                        />
                    </Card>
                    <Card className={"bg-base-300 p-4 mb-6"} border={true}>
                        <PageLoader show={!isLoaded} />
                        <CredentialFormBody show={isLoaded} changeForm={changeForm} credential={userCredential} />
                    </Card>
                </Modal.Body>
                <Modal.Actions>
                    <ActionButtonText status={buttonHint.status}>{buttonHint.children}</ActionButtonText>
                    <Button color={"primary"} type="submit">
                        <Trans>CredentialForm.FormSaveButton</Trans>
                    </Button>
                </Modal.Actions>
            </form>
        </Modal>
    );
};
