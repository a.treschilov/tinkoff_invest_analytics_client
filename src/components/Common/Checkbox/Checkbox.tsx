import { InputHTMLAttributes } from "react";
import { Color } from "../Types";
import { daisyUIColorClass } from "../utils";

type CheckboxProps = {
    color?: Color;
} & InputHTMLAttributes<HTMLInputElement>;

export const Checkbox = ({ color, className, type = "checkbox", ...args }: CheckboxProps) => {
    const classes = ["checkbox"];
    if (className) {
        classes.push(...className.split(" "));
    }
    classes.push(daisyUIColorClass("checkbox", color));

    return <input type={type} className={classes.join(" ")} {...args} />;
};
