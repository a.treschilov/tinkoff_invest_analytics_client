import { Trans } from "react-i18next";
import { BsCalendarWeek, BsFillBagCheckFill, BsFillPencilFill, BsFillPieChartFill, BsReceipt } from "react-icons/bs";
import { AiFillCreditCard, AiOutlineLineChart } from "react-icons/ai";
import { LiaTachometerAltSolid } from "react-icons/lia";
import { RiLuggageDepositLine } from "react-icons/ri";
import { MdContactSupport, MdOutbond, MdPrivacyTip, MdRealEstateAgent } from "react-icons/md";
import { SiCrowdsource, SiFuturelearn, SiMarketo } from "react-icons/si";
import { GiDivergence } from "react-icons/gi";
import Link from "next/link";
import { makeLink } from "../../utils/url";
import Image from "next/image";
import { FaTelegramPlane } from "react-icons/fa";
import { PiSignatureFill } from "react-icons/pi";
import { IoMdAlert } from "react-icons/io";
import { Menu } from "../Common/Menu/Menu";

type MenuProps = {
    lng: string;
    onMenuClick?: () => void;
};
const date = new Date();
export const MenuComponent = ({ lng, onMenuClick = () => {} }: MenuProps) => {
    return (
        <>
            <Menu>
                <Menu.Item>
                    <Link href="/" className={"py-4"}>
                        <Image
                            src="/images/logo_white.webp"
                            width={113}
                            height={18}
                            alt="Hakkes - онлайн помошник долгосрочного инвестора"
                        />
                    </Link>
                </Menu.Item>
            </Menu>
            <MenuPortfolio lng={lng} onMenuClick={onMenuClick} />
            <MenuAssets lng={lng} onMenuClick={onMenuClick} />
            <MenuMarket lng={lng} onMenuClick={onMenuClick} />
        </>
    );
};

type MenuComponentProps = {
    show?: boolean;
    title?: boolean;
    lng: string;
    onMenuClick: () => void;
};
export const MenuPortfolio = ({ lng, title = true, show = true, onMenuClick }: MenuComponentProps) => {
    return (
        <Menu className={show ? "" : "hidden"}>
            <Menu.Title className={title ? "" : "hidden"}>
                <Trans>Menu.PortfolioHeader</Trans>
            </Menu.Title>
            <Menu.Item>
                <Link href={makeLink("/user/", lng)} onClick={() => onMenuClick()}>
                    <BsFillPieChartFill /> <Trans>Menu.PortfolioDashboard</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/portfolio", lng)} onClick={() => onMenuClick()}>
                    <BsFillBagCheckFill /> <Trans>Menu.UserPortfolio</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/analytics", lng)} onClick={() => onMenuClick()}>
                    <AiOutlineLineChart /> <Trans>Menu.Analytics</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/operations/list", lng)} onClick={() => onMenuClick()}>
                    <BsReceipt /> <Trans>Menu.OperationsOperations</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/eventCalendar", lng)} onClick={() => onMenuClick()}>
                    <BsCalendarWeek /> <Trans>Menu.EventCalendar</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/market/portfolio", lng)} onClick={() => onMenuClick()}>
                    <LiaTachometerAltSolid /> <Trans>Menu.PortfolioStrategy</Trans>
                </Link>
            </Menu.Item>
        </Menu>
    );
};

export const MenuAssets = ({ lng, title = true, show = true, onMenuClick }: MenuComponentProps) => {
    return (
        <Menu className={show ? "" : "hidden"}>
            <Menu.Title className={title ? "" : "hidden"}>
                <Trans>Menu.SettingsHeader</Trans>
            </Menu.Title>
            <Menu.Item>
                <Link href={makeLink("/user/strategy/portfolio", lng)} onClick={() => onMenuClick()}>
                    <BsFillPencilFill /> <Trans>Menu.SettingsPortfolio</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/deposit", lng)} onClick={() => onMenuClick()}>
                    <RiLuggageDepositLine /> <Trans>Menu.SettingsDeposit</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/loan", lng)} onClick={() => onMenuClick()}>
                    <AiFillCreditCard /> <Trans>Menu.SettingsLoan</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/realEstate", lng)} onClick={() => onMenuClick()}>
                    <MdRealEstateAgent /> <Trans>Menu.SettingsRealEstate</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/crowdlanding", lng)} onClick={() => onMenuClick()}>
                    <SiCrowdsource /> <Trans>Menu.SettingsCrowdfunding</Trans>
                </Link>
            </Menu.Item>
        </Menu>
    );
};

export const MenuMarket = ({ lng, title = true, show = true, onMenuClick }: MenuComponentProps) => {
    return (
        <Menu className={show ? "" : "hidden"}>
            <Menu.Title className={title ? "" : "hidden"}>
                <Trans>Menu.MarketHeader</Trans>
            </Menu.Title>
            <Menu.Item>
                <Link href={makeLink("/user/market/signals", lng)} onClick={() => onMenuClick()}>
                    <IoMdAlert /> <Trans>Menu.MarketStockAlert</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/market/strategy", lng)} onClick={() => onMenuClick()}>
                    <GiDivergence /> <Trans>Menu.MarketStockDiversification</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/market/stock", lng)} onClick={() => onMenuClick()}>
                    <SiMarketo /> <Trans>Menu.MarketStock</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/market/bond", lng)} onClick={() => onMenuClick()}>
                    <MdOutbond /> <Trans>Menu.MarketBond</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={makeLink("/user/market/future", lng)} onClick={() => onMenuClick()}>
                    <SiFuturelearn /> <Trans>Menu.MarketFuture</Trans>
                </Link>
            </Menu.Item>
        </Menu>
    );
};

export const MenuLinks = ({ show = true, onMenuClick }: MenuComponentProps) => {
    return (
        <Menu className={show ? "" : "hidden"}>
            <Menu.Item>
                <Link
                    href={process.env.NEXT_PUBLIC_LANDING_PAGE_URL + "/help"}
                    target={"_blank"}
                    onClick={() => onMenuClick()}
                >
                    <MdContactSupport className={"inline"} /> <Trans>Footer.HelpCenter</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={"https://t.me/hakkes_invest"} target={"_blank"} onClick={() => onMenuClick()}>
                    <FaTelegramPlane className={"inline"} /> <Trans>Footer.TgChannel</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href={"https://t.me/hakkesChat"} target={"_blank"} onClick={() => onMenuClick()}>
                    <FaTelegramPlane className={"inline"} /> <Trans>Footer.TgChat</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href="https://hakkes.com/legal/privacyPolicy" target={"_blank"} onClick={() => onMenuClick()}>
                    <MdPrivacyTip /> <Trans>Footer.PrivacyPolicy</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link href="https://hakkes.com/legal/userAgreement" target={"_blank"} onClick={() => onMenuClick()}>
                    <PiSignatureFill /> <Trans>Footer.UserAgreement</Trans>
                </Link>
            </Menu.Item>
            <Menu.Item>
                <div className={"px-4 py-2"}>
                    Hakkes &copy; 2022-{date.getFullYear()} <Trans>Footer.Copyright</Trans>
                </div>
            </Menu.Item>
        </Menu>
    );
};
