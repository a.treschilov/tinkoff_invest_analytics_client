import { DetailedHTMLProps, InputHTMLAttributes, ReactNode } from "react";

export interface InputLabel {
    label?: ReactNode | null;
    topDescription?: ReactNode | null;
    containerClassName?: string;
}

type InputTextProps = InputLabel & DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

export const InputText = ({ label, topDescription, containerClassName = "", ...args }: InputTextProps) => {
    const labelBlock =
        label === null || label === undefined || args.hidden ? (
            <></>
        ) : (
            <label className="label">
                <span className={"label-text text-base-content"}>{label}</span>
            </label>
        );

    const topDescriptionBlock =
        topDescription === null || topDescription === undefined ? (
            <></>
        ) : (
            <small>
                <span>{topDescription}</span>
            </small>
        );
    const className = "input input-bordered w-full " + args.className;
    delete args.className;
    return (
        <div className={`form-control w-full ` + containerClassName}>
            {labelBlock}
            {topDescriptionBlock}
            <input className={className} {...args} />
        </div>
    );
};
