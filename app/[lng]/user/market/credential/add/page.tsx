"use client";

import { BrokerListManage } from "../../../../../../src/Features/Credential/BrokerListManage";
import { Breadcrumbs, Card } from "react-daisyui";
import Link from "next/link";
import { useContext } from "react";
import { ThemeContext } from "../../../../../../src/providers/ThemeProvider";
import { makeLink } from "../../../../../../src/utils/url";
import { useTranslation } from "../../../../../i18n/client";

const CredentialAddPage = () => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "broker");

    return (
        <>
            <Card className={"bg-base-100 p-2 pl-6 mb-2"} bordered={true}>
                <Breadcrumbs>
                    <Breadcrumbs.Item>
                        <Link href={makeLink("/", lng)} className={"link"}>
                            {t("Breadcrumbs.Dashboard")}
                        </Link>
                    </Breadcrumbs.Item>
                    <Breadcrumbs.Item>
                        <Link href={makeLink("/user/market/credential", lng)} className={"link"}>
                            {t("Breadcrumbs.MyAccounts")}
                        </Link>
                    </Breadcrumbs.Item>
                    <Breadcrumbs.Item>{t("Breadcrumbs.UploadReport")}</Breadcrumbs.Item>
                </Breadcrumbs>
            </Card>
            <BrokerListManage />
        </>
    );
};

export default CredentialAddPage;
