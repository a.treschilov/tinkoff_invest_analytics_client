import { Skeleton } from "../../components/Common/Skeleton/Skeleton";

const rowNumbers = 8;

export const BondListSkeleton = () => {
    const renderRow = (key: number) => {
        return (
            <tr id={"bond-list-skeleton-row" + key}>
                <td className={"hidden sm:table-cell"}>
                    <Skeleton className={"h-4 w-2/3"} />
                </td>
                <td className={"sm:hidden"}>
                    <Skeleton className={"h-4 w-2/3 mb-1"} />
                    <Skeleton className={"h-4 w-1/2 mb-1"} />
                    <Skeleton className={"h-4 w-1/3 mb-1"} />
                </td>
                <td className={"hidden sm:table-cell"}>
                    <Skeleton className={"h-4 w-2/3"} />
                </td>
                <td className={"hidden lg:table-cell"}>
                    <Skeleton className={"h-4 w-2/3"} />
                </td>
                <td className={"hidden sm:table-cell"}>
                    <Skeleton className={"h-4 w-3/4"} />
                </td>
            </tr>
        );
    };

    return (
        <>
            {[...Array(rowNumbers)].map((value: number, key: number) => {
                return renderRow(key);
            })}
        </>
    );
};
