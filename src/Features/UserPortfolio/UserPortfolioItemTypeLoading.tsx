import { Skeleton } from "../../components/Common/Skeleton/Skeleton";

const tableRowCount = 5;

export const UserPortfolioItemTypeLoading = () => {
    const renderRow = (num: number) => {
        return (
            <tr key={"userPortfolioItemTypeLoading-" + num}>
                <td>
                    <Skeleton className={"w-[100px] h-4"} />
                </td>
                <td className={"sm:hidden"}>
                    <Skeleton className={"w-[160px] h-4 inline-block"} />
                </td>
                <td className={"hidden sm:table-cell"}>
                    <Skeleton className={"w-[120px] h-4 inline-block"} />
                </td>
                <td className={"hidden sm:table-cell"}>
                    <Skeleton className={"w-[60px] h-4 inline-block"} />
                </td>
            </tr>
        );
    };

    return (
        <table className={"table w-full"}>
            <thead>
                <tr>
                    <th></th>
                    <th className={"sm:hidden"}>
                        <Skeleton className={"w-[130px] h-4 inline-block"} />
                    </th>
                    <th className={"hidden sm:table-cell"}>
                        <Skeleton className={"w-[100px] h-4"} />
                    </th>
                    <th className={"hidden sm:table-cell"}>
                        <Skeleton className={"w-[50px] h-4"} />
                    </th>
                </tr>
            </thead>
            <tbody>
                {[...Array(tableRowCount)].map((x, i) => {
                    return renderRow(i);
                })}
            </tbody>
        </table>
    );
};
