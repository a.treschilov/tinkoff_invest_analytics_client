import { InputHTMLAttributes } from "react";
import { Color, Size } from "../Types";
import { daisyUIColorClass, daisyUISizeClass } from "../utils";

export type ToggleProps = {
    color?: Color;
    size?: Size;
} & InputHTMLAttributes<HTMLInputElement>;

export const Toggle = ({ className, color, size, ...args }: ToggleProps) => {
    const classes = ["toggle"];
    if (className) {
        classes.push(...className.split(" "));
    }
    classes.push(daisyUIColorClass("toggle", color));
    classes.push(daisyUISizeClass("toggle", size));

    return <input type={"checkbox"} className={classes.join(" ")} {...args} />;
};
