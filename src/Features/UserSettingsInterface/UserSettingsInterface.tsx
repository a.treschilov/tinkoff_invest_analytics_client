import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../providers/AuthProvider";
import { useRouter, useSearchParams } from "next/navigation";
import { ApiService } from "../../services/ApiService";
import { Alert } from "../../components/Common/Alert/Alert";
import { PageState } from "../../components/Common/Types";
import Link from "next/link";
import { FaTelegram } from "react-icons/fa6";
import { useTranslation } from "../../../app/i18n/client";
import { ThemeContext } from "../../providers/ThemeProvider";
import { UserSocialAccount } from "../UserSocialAccount/UserSocialAccount";
import { Card } from "../../components/Common/Card/Card";
import { Divider } from "../../components/Common/Divider/Divider";
import { Button } from "../../components/Common/Button/Button";
import { Radio } from "../../components/Common/Form/Radio/Radio";

export const UserSettingsInterface = () => {
    const searchParams = useSearchParams();
    const { language } = useContext(AuthContext);
    const [userLng, setUserLng] = useState(language);
    const [errorMessage, setErrorMessage] = useState<string | null>(null);
    const [pageState, setPageState] = useState(
        searchParams.get("save") === "1" ? PageState.SUCCESS : PageState.EDIT_FORM
    );
    const [userCode, setUserCode] = useState<string | null>(null);
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userSettings");
    const router = useRouter();

    useEffect(() => {
        ApiService.post("/v1/user/code/release").then(response => {
            setUserCode(response.code);
        });
    }, []);

    const onSaveSettings = () => {
        setPageState(PageState.LOADING);
        ApiService.put("/v1/user/data", { language: userLng })
            .then(() => {
                router.push("/" + userLng + "/user/settings/interface?save=1");
            })
            .catch(response => {
                const error = response.response.data;
                setErrorMessage(error.error_code + ": " + error.error_message);
            })
            .finally(() => {
                setPageState(PageState.EDIT_FORM);
            });
    };

    return (
        <>
            <UserSocialAccount />
            <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                <Card.Title className="mb-6">{t("Notifications.Header")}</Card.Title>
                <p className={"inline"}>
                    {t("Notifications.TgNotificationsDescription")}{" "}
                    <Link
                        href={"https://t.me/HakkesBot?start=" + userCode}
                        target={"_blank"}
                        className={"text-info inline"}
                    >
                        {t("Notifications.TgNotificationsLink")} <FaTelegram className={"inline  text-info w-4 h-4"} />
                    </Link>
                </p>
            </Card>
            <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                <Card.Title className={errorMessage !== null || pageState === PageState.SUCCESS ? "mb-6" : ""}>
                    {t("Interface.PageHeader")}
                </Card.Title>
                <Alert
                    dismissible={true}
                    status={"success"}
                    show={pageState === PageState.SUCCESS}
                    onClose={() => {
                        setPageState(PageState.EDIT_FORM);
                    }}
                >
                    {t("Interface.SuccessMessage")}
                </Alert>
                <Alert
                    dismissible={true}
                    status={"error"}
                    show={errorMessage !== null}
                    onClose={() => {
                        setErrorMessage(null);
                    }}
                >
                    {errorMessage}
                </Alert>
                <Divider />
                <form>
                    <div className={"max-w-40"}>
                        <label className={"flex"}>{t("Interface.LanguageHeader")}</label>
                        <Radio className={"flex px-1 pt-2 justify-between"}>
                            <Radio.Label>English</Radio.Label>
                            <Radio.Input
                                name={"language"}
                                value={"en"}
                                defaultChecked={language === "en"}
                                onChange={event => {
                                    setUserLng(event.target.value);
                                }}
                            />
                        </Radio>
                        <Radio className={"flex px-1 pt-2 justify-between"}>
                            <Radio.Label>Русский</Radio.Label>
                            <Radio.Input
                                name={"language"}
                                value={"ru"}
                                defaultChecked={language === "ru"}
                                onChange={event => {
                                    setUserLng(event.target.value);
                                }}
                            />
                        </Radio>
                    </div>
                    <Divider />
                    <div className={"text-start"}>
                        <Button
                            type="button"
                            color={pageState === PageState.LOADING ? "neutral" : "primary"}
                            onClick={onSaveSettings}
                        >
                            Save
                        </Button>
                    </div>
                </form>
            </Card>
        </>
    );
};
