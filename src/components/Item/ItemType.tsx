import { ItemType } from "../Common/Types";
import { useTranslation } from "react-i18next";

type ItemTypeTranslationProps = {
    type: ItemType;
};

export const ItemTypeTranslation = ({ type }: ItemTypeTranslationProps) => {
    const { t } = useTranslation("item");
    let translation = "";
    switch (type) {
        case "loan":
            translation = "ItemType.Loan";
            break;
        case "deposit":
            translation = "ItemType.Deposit";
            break;
        case "real_estate":
            translation = "ItemType.RealEstate";
            break;
        case "market":
            translation = "ItemType.Market";
            break;
        case "crowdfunding":
            translation = "ItemType.Crowdfunding";
            break;
    }

    return <>{t(translation)}</>;
};
