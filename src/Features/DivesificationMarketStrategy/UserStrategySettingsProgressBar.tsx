import { useEffect, useState } from "react";
import { Progress } from "../../components/Common/Progress/Progress";
import { Color } from "../../components/Common/Types";

type UserStrategySettingsProgressBarProps = {
    progress: number;
};
export const UserStrategySettingsProgressBar = ({ progress }: UserStrategySettingsProgressBarProps) => {
    const [variant, setVariant] = useState<Color>("success");

    useEffect(() => {
        if (progress < 100) {
            setVariant("info");
        } else if (progress > 100) {
            setVariant("warning");
        } else if (progress === 100) {
            setVariant("success");
        }
    }, [progress]);

    return <Progress value={progress} max={100} color={variant} />;
};
