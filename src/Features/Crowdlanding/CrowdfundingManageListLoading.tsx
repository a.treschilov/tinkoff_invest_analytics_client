import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Divider } from "../../components/Common/Divider/Divider";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Card } from "../../components/Common/Card/Card";

type CrowdfundingManageListLoadingProps = {
    show: boolean;
};

const tableRowCount = 10;

export const CrowdfundingManageListLoading = ({ show }: CrowdfundingManageListLoadingProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/crowdlanding");

    if (!show) {
        return <></>;
    }

    const renderRow = (i: number) => {
        return (
            <tr key={"crowdfunding-loading-row-" + i}>
                <td className={"w-5/12"}>
                    <Skeleton className={"w-5/6 h-4"} />
                    <span className={"sm:hidden"}>
                        <Skeleton className={"w-2/6 h-4 my-1"} />
                        <Skeleton className={"w-3/6 h-4"} />
                    </span>
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-5/6 h-4"} />
                </td>
                <td className="hidden sm:table-cell">
                    <Skeleton className={"w-5/6 h-4"} />
                </td>
                <td className="hidden sm:table-cell">
                    <Skeleton className={"w-5/6 h-4"} />
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-5/6 h-4"} />
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-5/6 h-4"} />
                </td>
            </tr>
        );
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>{t("Manage.PageHeader")}</Card.Title>
            <Divider />
            <table className="table w-full">
                <thead>
                    <tr>
                        <th className={"w-4/12"}>
                            <Skeleton className={"w-2/6 h-4"} />
                        </th>
                        <th className="hidden lg:table-cell w-1/12">
                            <Skeleton className={"w-5/6 h-4"} />
                        </th>
                        <th className={"w-2/12 hidden sm:table-cell"}>
                            <Skeleton className={"w-5/6 h-4"} />
                        </th>
                        <th className={"w-1/12 hidden sm:table-cell"}>
                            <Skeleton className={"w-5/6 h-4"} />
                        </th>
                        <th className="hidden lg:table-cell w-1/12">
                            <Skeleton className={"w-5/6 h-4"} />
                        </th>
                        <th className="hidden lg:table-cell w-1/12">
                            <Skeleton className={"w-5/6 h-4"} />
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {[...Array(tableRowCount)].map((x, i) => {
                        return renderRow(i);
                    })}
                </tbody>
            </table>
        </Card>
    );
};
