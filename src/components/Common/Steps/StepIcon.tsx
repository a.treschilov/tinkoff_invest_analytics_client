import { HTMLAttributes, ReactNode } from "react";

type StepIconProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLSpanElement>;

export const StepIcon = ({ children, className }: StepIconProps) => {
    const classes = ["step-icon"];
    if (className) {
        classes.push(...className.split(" "));
    }
    return <span className={classes.join(" ")}>{children}</span>;
};
