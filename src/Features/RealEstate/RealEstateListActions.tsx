import { FiMoreHorizontal } from "react-icons/fi";
import { BsFillPencilFill, BsFillTrashFill, BsListUl } from "react-icons/bs";
import { Trans } from "react-i18next";
import { useState } from "react";
import { MdOutlineSell } from "react-icons/md";
import { RealEstate } from "../../components/Item/Item";
import { ItemDelete } from "../Item/Delete/ItemDelete";
import { ItemClose } from "../Item/Close/ItemClose";
import { Dropdown } from "../../components/Common/Dropdown/Dropdown";

type RealEstateListActionsProps = {
    realEstate: RealEstate;
    handleOperationManage: (itemId: number) => void;
    handleEditRealEstate: (realEstateId: number) => void;
    handleActionComplete: (action: RealEstateActionType) => void;
};

export type RealEstateActionType = "deleteAsset" | "addAsset" | "sellAsset";

export const RealEstateListActions = ({
    realEstate,
    handleOperationManage,
    handleEditRealEstate,
    handleActionComplete
}: RealEstateListActionsProps) => {
    const [isShowDeleteConfirmation, setIsShowDeleteConfirmation] = useState(false);
    const [isShowSellConfirmation, setIsShowSellConfirmation] = useState(false);

    return (
        <>
            <Dropdown key={"loanActions-" + realEstate.realEstateId} placement={"left"}>
                <Dropdown.Toggle>
                    <FiMoreHorizontal></FiMoreHorizontal>
                </Dropdown.Toggle>
                <Dropdown.Menu className={"z-50 w-44"}>
                    <Dropdown.Item
                        onClick={() => {
                            handleOperationManage(realEstate.itemId === null ? 0 : realEstate.itemId);
                        }}
                    >
                        <BsListUl className="me-2" />
                        <Trans>RealEstateManage.ItemOperations</Trans>
                    </Dropdown.Item>
                    <Dropdown.Item
                        onClick={e => {
                            handleEditRealEstate(realEstate.realEstateId === null ? 0 : realEstate.realEstateId);
                            e.preventDefault();
                        }}
                    >
                        <BsFillPencilFill className="me-2" />
                        <Trans>RealEstateManage.ItemEdit</Trans>
                    </Dropdown.Item>
                    <Dropdown.Item
                        onClick={() => {
                            setIsShowSellConfirmation(true);
                        }}
                    >
                        <MdOutlineSell className="me-2" />
                        <Trans>RealEstateManage.ItemSell</Trans>
                    </Dropdown.Item>
                    <Dropdown.Item
                        onClick={() => {
                            setIsShowDeleteConfirmation(true);
                        }}
                    >
                        <BsFillTrashFill
                            style={{ cursor: "pointer" }}
                            title="Delete deposit"
                            className="me-2 text-danger"
                        />
                        <span className="text-warning">
                            <Trans>RealEstateManage.ItemDelete</Trans>
                        </span>
                    </Dropdown.Item>
                </Dropdown.Menu>
                <ItemDelete
                    itemId={realEstate.itemId ?? 0}
                    onSuccess={() => {
                        handleActionComplete("deleteAsset");
                    }}
                    onDiscard={() => {
                        setIsShowDeleteConfirmation(false);
                    }}
                    show={isShowDeleteConfirmation}
                />
                <ItemClose
                    itemId={realEstate.itemId ?? 0}
                    onSuccess={() => {
                        handleActionComplete("sellAsset");
                    }}
                    onDiscard={() => {
                        setIsShowSellConfirmation(false);
                    }}
                    show={isShowSellConfirmation}
                />
            </Dropdown>
        </>
    );
};
