import { ApexOptions } from "apexcharts";

const compactCurrencyFormatter = Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB",
    notation: "compact"
});

const currencyFormatter = Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB"
});

export const defaultChartOptions: ApexOptions = {
    colors: ["#00aff5", "#fea135", "#adb5bd", "rgb(22, 202, 175)"],
    grid: {
        borderColor: "rgba(173, 181, 189, 0.3)",
        strokeDashArray: 3
    },
    stroke: {
        width: 3,
        curve: "smooth"
    },
    xaxis: {
        type: "datetime",
        labels: {
            datetimeFormatter: {
                year: "yyyy",
                month: "MMM 'yy",
                day: "dd MMM"
            }
        }
    },
    yaxis: [
        {
            opposite: false,
            labels: {
                formatter: (value: number) => {
                    return compactCurrencyFormatter.format(value);
                }
            },
            forceNiceScale: true
        }
    ],
    tooltip: {
        y: {
            formatter(value: number): string {
                return currencyFormatter.format(value);
            }
        }
    },
    dataLabels: {
        enabled: false
    },
    theme: {
        mode: "dark"
    },
    chart: {
        background: "transparent",
        type: "line"
    },
    noData: {
        text: "Data not found",
        style: {
            fontSize: "26px"
        }
    }
};
