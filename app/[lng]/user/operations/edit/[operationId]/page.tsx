"use client";

import { useContext, useEffect, useState, use } from "react";
import { OperationEdit } from "../../../../../../src/Features/Operation/OperationEdit";
import { makeLink } from "../../../../../../src/utils/url";
import { ThemeContext } from "../../../../../../src/providers/ThemeProvider";
import { useRouter } from "next/navigation";
import { useTranslation } from "react-i18next";
import { ApiService } from "../../../../../../src/services/ApiService";
import { useErrorBoundary } from "react-error-boundary";
import { Divider } from "../../../../../../src/components/Common/Divider/Divider";
import { Card } from "../../../../../../src/components/Common/Card/Card";

const OperationEditPage = (props: { params: Promise<{ operationId: string }> }) => {
    const params = use(props.params);
    const [operationId] = useState<number>(parseInt(params.operationId || "0"));
    const [itemId, setItemId] = useState<number | null>();
    const { lng } = useContext(ThemeContext);
    const router = useRouter();
    const { t } = useTranslation("pages");
    const { showBoundary } = useErrorBoundary();

    useEffect(() => {
        ApiService.fetch("/v1/operations/operation/" + operationId)
            .then(data => {
                setItemId(data.itemId);
            })
            .catch(error => {
                showBoundary(error);
            });
    }, [operationId, showBoundary]);

    const moveToPage = () => {
        if (itemId !== null) {
            router.push(makeLink("/user/item/" + itemId, lng));
        }
    };
    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>{t("UserOperationEdit.PageTitle")}</Card.Title>
            <Divider />
            <OperationEdit
                itemId={itemId ? itemId : 0}
                operationId={operationId}
                onCancel={() => {
                    moveToPage();
                }}
                onChangeData={() => {
                    moveToPage();
                }}
            />
        </Card>
    );
};

export default OperationEditPage;
