import { useContext, useEffect, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { ItemType, PageState, Price } from "../../components/Common/Types";
import { UserPortfolioStock } from "./UserPortfolioStock";
import { UserPortfolioDeposit } from "./UserPortfolioDeposit";
import { UserPortfolioItemType } from "./UserPortfolioItemType";
import { ErrorContent } from "../../components/ErrorContent/ErrorContent";
import { RiLuggageDepositLine } from "react-icons/ri";
import { AiFillCreditCard } from "react-icons/ai";
import { MdRealEstateAgent } from "react-icons/md";
import { SiCoinmarketcap, SiCrowdsource } from "react-icons/si";
import { useTranslation } from "../../../app/i18n/client";
import { ThemeContext } from "../../providers/ThemeProvider";
import { Card } from "../../components/Common/Card/Card";
import { Tabs } from "../../components/Common/Tabs/Tabs";

type PortfolioItemStock = {
    type: "Stock" | "Etf" | "Currency" | "Bond" | "Futures";
};

type PortfolioItemItem = {
    externalId: string;
    name: string;
    logo: string | null;
    type: ItemType;
    stock: PortfolioItemStock | null;
};

export type PortfolioItem = {
    itemId: number;
    quantity: number;
    amount: number;
    currencyIso: string;
    baseAmount: Price;
    price: Price | null;
    item: PortfolioItemItem;
    itemTypeShare: number;
    image: string;
};

export type ItemTypeRatio = {
    market: ItemRatio;
    deposit: ItemRatio;
    realEstate: ItemRatio;
    loan: ItemRatio;
    crowdfunding: ItemRatio;
};

type ItemRatio = {
    amount: Price;
    ratio: number;
};

const itemRationDefault: ItemRatio = {
    amount: {
        amount: 0,
        currency: "RUB"
    },
    ratio: 0
};

const itemTypeDefaultRatio: ItemTypeRatio = {
    market: itemRationDefault,
    deposit: itemRationDefault,
    realEstate: itemRationDefault,
    loan: itemRationDefault,
    crowdfunding: itemRationDefault
};

export const UserPortfolio = () => {
    const [pageState, setPageState] = useState<string>(PageState.LOADING);
    const [items, setItems] = useState<PortfolioItem[]>([]);
    const [itemTypeRatio, setItemTypeRatio] = useState<ItemTypeRatio>(itemTypeDefaultRatio);
    const [activeTab, setActiveTab] = useState<ItemType>("market");
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userPortfolio");

    useEffect(() => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/item/portfolio")
            .then(response => {
                setItems(
                    response.items.map((responseItem: PortfolioItem) => {
                        let stock = null;
                        if (responseItem.item.stock !== null) {
                            stock = {
                                type: responseItem.item.stock.type
                            };
                        }
                        return {
                            itemId: responseItem.itemId,
                            quantity: responseItem.quantity,
                            amount: responseItem.amount,
                            currencyIso: responseItem.currencyIso,
                            baseAmount: {
                                amount: responseItem.baseAmount.amount,
                                currency: responseItem.baseAmount.currency
                            },
                            price:
                                responseItem.price === null
                                    ? null
                                    : {
                                          amount: responseItem.price.amount,
                                          currency: responseItem.price.currency
                                      },
                            item: {
                                externalId: responseItem.item.externalId,
                                name: responseItem.item.name,
                                logo: responseItem.item.logo,
                                type: responseItem.item.type,
                                stock: stock
                            },
                            itemTypeShare: responseItem.itemTypeShare
                        };
                    })
                );
                setItemTypeRatio({
                    market: response.itemTypeRatio.market,
                    deposit: response.itemTypeRatio.deposit,
                    realEstate: response.itemTypeRatio.real_estate,
                    loan: response.itemTypeRatio.loan,
                    crowdfunding: response.itemTypeRatio.crowdfunding
                });
                setPageState(PageState.SUCCESS);
            })
            .catch(() => {
                setPageState(PageState.ERROR);
            });
    }, []);

    const portfolio = () => {
        const stocks = items.filter((item: PortfolioItem) => {
            return item.item.type === "market";
        });
        const loans = items.filter((item: PortfolioItem) => {
            return item.item.type === "loan" && item.baseAmount.amount < -0.001;
        });
        const deposit = items.filter((item: PortfolioItem) => {
            return item.item.type === "deposit" && item.baseAmount.amount > 0.001;
        });
        const realEstate = items.filter((item: PortfolioItem) => {
            return item.item.type === "real_estate" && item.quantity > 0;
        });
        const crowdfunding = items.filter((item: PortfolioItem) => {
            return item.item.type === "crowdfunding" && item.baseAmount.amount > 0.001;
        });

        return (
            <>
                <UserPortfolioItemType
                    ratio={itemTypeRatio}
                    state={pageState === PageState.LOADING ? "loading" : "success"}
                />
                <Card className={"bg-base-100 p-2 lg:p-6 mb-6"} border={true}>
                    <Tabs decorate={"lift"} className="overflow-x-auto">
                        <Tabs.Tab
                            active={activeTab === "market"}
                            onClick={() => {
                                setActiveTab("market");
                            }}
                            className={"flex-row"}
                        >
                            <SiCoinmarketcap className={"inline sm:hidden"}></SiCoinmarketcap>
                            <span className={"sm:inline sm:p-0 px-2" + (activeTab !== "market" ? " hidden" : "")}>
                                {t("Tab.NameStock")}
                            </span>
                        </Tabs.Tab>
                        <Tabs.Tab
                            active={activeTab === "deposit"}
                            onClick={() => {
                                setActiveTab("deposit");
                            }}
                            className={"flex-row"}
                        >
                            <RiLuggageDepositLine className={"inline sm:hidden"} />
                            <span className={"sm:inline sm:p-0 px-2" + (activeTab !== "deposit" ? " hidden" : "")}>
                                {t("Tab.NameDeposit")}
                            </span>
                        </Tabs.Tab>
                        <Tabs.Tab
                            active={activeTab === "loan"}
                            onClick={() => {
                                setActiveTab("loan");
                            }}
                            className={"flex-row"}
                        >
                            <AiFillCreditCard className={"inline sm:hidden"} />
                            <span className={"sm:inline sm:p-0 px-2" + (activeTab !== "loan" ? " hidden" : "")}>
                                {t("Tab.NameLoan")}
                            </span>
                        </Tabs.Tab>
                        <Tabs.Tab
                            active={activeTab === "real_estate"}
                            onClick={() => {
                                setActiveTab("real_estate");
                            }}
                            className={"flex-row"}
                        >
                            <MdRealEstateAgent className={"inline sm:hidden"} />
                            <span className={"sm:inline sm:p-0 px-2" + (activeTab !== "real_estate" ? " hidden" : "")}>
                                {t("Tab.NameRealEstate")}
                            </span>
                        </Tabs.Tab>
                        <Tabs.Tab
                            active={activeTab === "crowdfunding"}
                            onClick={() => {
                                setActiveTab("crowdfunding");
                            }}
                            className={"flex-row"}
                        >
                            <SiCrowdsource className={"inline sm:hidden"} />
                            <span className={"sm:inline sm:p-0 px-2" + (activeTab !== "crowdfunding" ? " hidden" : "")}>
                                {t("Tab.NameCrowdFunding")}
                            </span>
                        </Tabs.Tab>
                        <div
                            className={
                                "tab-content bg-base-100 border-base-300 rounded-box p-2 lg:p-6 border block mb-[-1px]" +
                                (activeTab === "market" ? " rounded-s-none" : "")
                            }
                        >
                            <UserPortfolioStock
                                items={stocks}
                                state={pageState === PageState.LOADING ? "loading" : "success"}
                                show={activeTab === "market"}
                            />
                            <UserPortfolioDeposit
                                items={deposit}
                                state={pageState === PageState.LOADING ? "loading" : "success"}
                                show={activeTab === "deposit"}
                            />
                            <UserPortfolioDeposit
                                items={loans}
                                state={pageState === PageState.LOADING ? "loading" : "success"}
                                show={activeTab === "loan"}
                            />
                            <UserPortfolioDeposit
                                items={realEstate}
                                state={pageState === PageState.LOADING ? "loading" : "success"}
                                show={activeTab === "real_estate"}
                            />
                            <UserPortfolioDeposit
                                items={crowdfunding}
                                state={pageState === PageState.LOADING ? "loading" : "success"}
                                show={activeTab === "crowdfunding"}
                            />
                        </div>
                    </Tabs>
                </Card>
            </>
        );
    };

    let content;
    switch (pageState) {
        case PageState.ERROR:
            content = <ErrorContent />;
            break;
        default:
            content = portfolio();
    }
    return <>{content}</>;
};
