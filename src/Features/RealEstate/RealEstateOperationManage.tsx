"use client";

import { useEffect, useState } from "react";
import { Modal } from "../../components/Common/Modal/Modal";
import { RealEstateOperationList } from "./RealEstateOperationList";
import { RealEstateOperationAdd } from "./RealEstateOperationAdd";
import { ItemType } from "../../components/Common/Types";

interface IRealEstateOperationManageProps {
    isShow: boolean;
    hideModal: (action: string | null) => void;
    itemId: number;
    itemType: ItemType;
    onChange?: () => void;
}

type OperationPageType = "operationList" | "operationAdd";

export const RealEstateOperationManage = ({
    isShow,
    hideModal,
    itemId,
    itemType,
    onChange
}: IRealEstateOperationManageProps) => {
    const [page, setPage] = useState<OperationPageType>("operationList");
    const [operationId, setOperationId] = useState(0);
    const [infoText, setInfoText] = useState<string | null>(null);
    const [isChangedOperation, setIsChangedOperation] = useState(false);

    useEffect(() => {
        setPage("operationList");
        setOperationId(0);
        setIsChangedOperation(false);
    }, [isShow]);

    const handleHideModal = () => {
        setInfoText(null);
        const action = isChangedOperation ? "changeOperations" : null;

        hideModal(action);
    };

    const handleOperationList = (text: string | null) => {
        setPage("operationList");
        setInfoText(text);
    };

    const handleEditOperation = (operationId: number) => {
        setOperationId(operationId);
        setPage("operationAdd");
    };

    const handleChangeOperation = () => {
        setIsChangedOperation(true);
        if (onChange !== undefined) {
            onChange();
        }
    };

    let content;
    switch (page) {
        case "operationList":
            content = (
                <RealEstateOperationList
                    itemId={itemId}
                    infoTextProp={infoText}
                    itemType={itemType}
                    handleOperationPage={handleEditOperation}
                />
            );
            break;
        case "operationAdd":
            content = (
                <RealEstateOperationAdd
                    operationId={operationId}
                    itemId={itemId}
                    handleChangeOperation={handleChangeOperation}
                    handleOperationList={handleOperationList}
                />
            );
            break;
    }

    return (
        <Modal show={isShow} onClose={handleHideModal} aria-labelledby="contained-modal-title-vcenter">
            {content}
        </Modal>
    );
};
