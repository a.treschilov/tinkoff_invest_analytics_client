import { IUserSettings } from "./UserSettings";

export const DefaultUserSettingsData: IUserSettings = {
    desiredPension: {
        amount: 0,
        currency: "RUB"
    },
    userSettingsId: null,
    retirementAge: 60,
    expenses: {
        amount: 0,
        currency: "RUB"
    },
    birthday: null
};
