import { ReactNode } from "react";
import { redirect } from "next/navigation";
import { UserService } from "../../../src/services/UserSerice";

const LoginLayout = async (props: { children: ReactNode; params: Promise<{ lng: string }> }) => {
    const params = await props.params;

    const { children } = props;

    const user = await UserService.getUser();

    if (user.userId !== null) {
        redirect("/" + params.lng);
    }

    return <>{children}</>;
};

export default LoginLayout;
