"use client";

import { useCallback, useContext, useEffect, useState } from "react";
import { CrowdfundingImport } from "./CrowdfundingImport";
import { ApiService } from "../../services/ApiService";
import { PageState } from "../../components/Common/Types";
import { CrowdfundingManageList } from "./CrowdfundingManageList";
import { CrowdfundingManageListLoading } from "./CrowdfundingManageListLoading";
import { useErrorBoundary, withErrorBoundary } from "react-error-boundary";
import { ErrorComponent } from "../../components/ErrorContent/ErrorComponent";
import { EmptyContent } from "../../components/Common/EmptyContent/EmptyContent";
import { CiFaceMeh } from "react-icons/ci";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Button } from "../../components/Common/Button/Button";

type CrowdfundingManageProps = {
    onChange?: () => void;
};

export interface IAssetCrowdfundingItem {
    assetId: number;
    itemId: number;
    name: string;
    amount: number;
    duration: number;
    durationPeriod: string;
    interestPercent: number;
    dealDate: {
        date: string;
        timezone: string;
    };
    payoutFrequency: number;
    payoutFrequencyPeriod: string;
}

export interface IAssetPortfolio {
    amount: number;
    currencyIso: string;
    itemId: number;
    quantity: number;
}

export const CrowdfundingManage = ({ onChange }: CrowdfundingManageProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/crowdlanding");

    const [pageState, setPageState] = useState(PageState.LOADING);
    const [isShowImportForm, setIsShowImportForm] = useState(false);
    const [assetList, setAssetList] = useState<IAssetCrowdfundingItem[]>([]);
    const [portfolio, setPortfolio] = useState<IAssetPortfolio[]>([]);
    const { showBoundary } = useErrorBoundary();

    const updateCrowdfundingList = useCallback(() => {
        setPageState(PageState.LOADING);

        ApiService.fetch("/v1/asset/list/crowdfunding")
            .then(response => {
                setPageState(PageState.EDIT_FORM);
                setAssetList(response.items);
                setPortfolio(response.portfolio);
            })
            .catch(error => {
                showBoundary(error);
            });
    }, [showBoundary]);

    useEffect(() => {
        updateCrowdfundingList();
    }, [updateCrowdfundingList]);

    const handleChangeList = () => {
        updateCrowdfundingList();
        if (onChange !== undefined) {
            onChange();
        }
    };

    const handleOpenImportForm = () => {
        setIsShowImportForm(true);
    };

    const handleHideModal = () => {
        setIsShowImportForm(false);
    };

    const renderManageData = () => {
        return (
            <div className="text-end mb-6">
                <Button color="primary" onClick={handleOpenImportForm}>
                    {t("Manage.ImportButtonText")}
                </Button>
            </div>
        );
    };

    return (
        <div>
            {renderManageData()}
            <CrowdfundingManageListLoading show={pageState === PageState.LOADING} />
            <EmptyContent show={pageState !== PageState.LOADING && (assetList.length === 0 || portfolio.length === 0)}>
                {t("Manage.DataNotFound")} <CiFaceMeh className={"inline"} />
            </EmptyContent>
            <CrowdfundingManageList
                show={pageState !== PageState.LOADING && assetList.length !== 0 && portfolio.length !== 0}
                assets={assetList}
                portfolio={portfolio}
            />
            <CrowdfundingImport
                isShow={isShowImportForm}
                hideModal={handleHideModal}
                changeDataHandler={handleChangeList}
            ></CrowdfundingImport>
        </div>
    );
};

export const CrowdfundingManageWithBoundaryErrors = withErrorBoundary(CrowdfundingManage, {
    FallbackComponent: ErrorComponent
});
