export const UserStrategyPredefinedCurrency = [
    {
        id: "ru_qualified",
        name: "UserStrategySettings.Strategies.CurrencyRuQualifiedName",
        description: "UserStrategySettings.Strategies.CurrencyRuQualifiedDescription",
        strategy: [
            {
                value: 0.4,
                id: "RUB"
            },
            {
                value: 0.3,
                id: "USD"
            },
            {
                value: 0,
                id: "EUR"
            },
            {
                value: 0.15,
                id: "CNY"
            },
            {
                value: 0.15,
                id: "HKD"
            },
            {
                value: 0,
                id: "JPY"
            }
        ]
    },
    {
        id: "ru_unqualified",
        name: "UserStrategySettings.Strategies.CurrencyRuUnQualifiedName",
        description: "UserStrategySettings.Strategies.CurrencyRuUnQualifiedDescription",
        strategy: [
            {
                value: 0.6,
                id: "RUB"
            },
            {
                value: 0,
                id: "USD"
            },
            {
                value: 0,
                id: "EUR"
            },
            {
                value: 0.15,
                id: "CNY"
            },
            {
                value: 0.25,
                id: "HKD"
            },
            {
                value: 0,
                id: "JPY"
            }
        ]
    }
];
