import { PortfolioTable } from "./PortfolioTable";
import { Trans } from "react-i18next";
import { ComponentState } from "../../components/Common/Types";
import { IPortfolio } from "../../../app/[lng]/user/market/portfolio/page";

type PortfolioContentGridProps = {
    portfolio: IPortfolio;
    state: ComponentState;
};
export const PortfolioContentGrid = ({ portfolio, state }: PortfolioContentGridProps) => {
    return (
        <div className={"grid grid-cols-1 xl:grid-cols-2 gap-6"}>
            <PortfolioTable
                state={state}
                name={<Trans>Portfolio.InstrumentsTableTitle</Trans>}
                data={portfolio.shares}
            />
            <PortfolioTable
                state={state}
                name={<Trans>Portfolio.CurrencyBalanceTableTitle</Trans>}
                data={portfolio.shareCurrencyBalance}
            />
            <PortfolioTable
                state={state}
                name={<Trans>Portfolio.InstrumentsSectorTableTitle</Trans>}
                data={portfolio.shareSector}
                strategyLink="https://www.spglobal.com/spdji/en/documents/additional-material/gics-500-scorecard.pdf"
            />
            <PortfolioTable
                state={state}
                name={<Trans>Portfolio.InstrumentsCountryTableTitle</Trans>}
                data={portfolio.shareCountry}
                strategyLink="https://bcs-express.ru/novosti-i-analitika/pochemu-vash-portfel-tak-chasto-triaset"
            />
            <PortfolioTable
                state={state}
                name={<Trans>Portfolio.InstrumentsCurrencyTableTitle</Trans>}
                data={portfolio.sharesCurrency}
            />
        </div>
    );
};
