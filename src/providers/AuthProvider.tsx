"use client";

import { createContext, ReactNode, useEffect, useState } from "react";
import { UserType } from "../services/UserSerice";
import { deleteCookie } from "cookies-next";

export type AuthContextType = {
    userId: number | null;
    userLogin: string | null;
    isNewUser: boolean;
    language: string;
    refreshUser: () => void;
};

export const AuthContext = createContext<AuthContextType>(null!);

export const AuthProvider = ({ user, children }: { user: UserType; children: ReactNode }) => {
    const [userId] = useState<number | null>(user.userId);
    const [userLogin] = useState<string | null>(user.userLogin);
    const [isNewUser, setIsNewUser] = useState<boolean>(user.isNewUser);
    const [language] = useState(user.language);

    useEffect(() => {
        if (userId === null) {
            deleteCookie("jwt");
        }
    }, [isNewUser, userId]);

    const refreshUser = () => {
        setIsNewUser(false);
    };

    const value = { userId, userLogin, language, isNewUser, refreshUser };
    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
