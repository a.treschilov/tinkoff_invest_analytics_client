import { IAssetCrowdfundingItem, IAssetPortfolio } from "./CrowdfundingManage";
import Link from "next/link";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { SessionContext } from "../../providers/SessionProvider";
import { makeLink } from "../../utils/url";
import { useTranslation } from "../../../app/i18n/client";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

type CrowdfundingManageListProps = {
    show: boolean;
    assets: IAssetCrowdfundingItem[];
    portfolio: IAssetPortfolio[];
};
export const CrowdfundingManageList = ({ show, assets, portfolio }: CrowdfundingManageListProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/crowdlanding");

    const { updatePreviousPage } = useContext(SessionContext);
    const findItemById = (itemId: number) => {
        return assets.find(el => {
            return el.itemId === itemId;
        });
    };

    const renderAssetList = (assets: IAssetCrowdfundingItem[], portfolio: IAssetPortfolio[]) => {
        const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
            year: "numeric",
            month: "numeric",
            day: "numeric"
        });
        const currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
            style: "currency",
            currency: "RUB"
        });

        return portfolio.map((portfolioItem: IAssetPortfolio) => {
            const asset = findItemById(portfolioItem.itemId);

            if (portfolioItem.quantity === 0) {
                return;
            }

            if (asset === undefined) {
                return;
            }

            return (
                <tr key={"asset-" + asset.assetId}>
                    <td>
                        <Link
                            href={makeLink("/user/item/" + asset.itemId, lng)}
                            onClick={() => updatePreviousPage("crowdlanding")}
                            className={"link"}
                        >
                            {asset.name}
                        </Link>
                        <span className={"sm:hidden"}>
                            <br />
                            {t("Manage.TableHeaderAmount")}: {currencyFormatter.format(portfolioItem.amount)}
                            <br />
                            {t("Manage.TableHeaderInterest")} {(asset.interestPercent * 100).toFixed(2) + "%"}
                        </span>
                    </td>
                    <td className="hidden lg:table-cell">{currencyFormatter.format(portfolioItem.quantity)}</td>
                    <td className={"hidden sm:table-cell"}>{currencyFormatter.format(portfolioItem.amount)}</td>
                    <td className={"hidden sm:table-cell"}>{(asset.interestPercent * 100).toFixed(2) + "%"}</td>
                    <td className="hidden lg:table-cell">
                        {asset.dealDate ? dateFormatter.format(new Date(asset.dealDate.date.replace(" ", "T"))) : ""}
                    </td>
                    <td className="hidden lg:table-cell">
                        {asset.duration && asset.durationPeriod ? (
                            asset.duration + " " + asset.durationPeriod
                        ) : (
                            <span>&mdash;</span>
                        )}
                    </td>
                </tr>
            );
        });
    };

    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-3 sm:p-6 mb-6"} border={true}>
            <Card.Title>{t("Manage.PageHeader")}</Card.Title>
            <Divider />
            <table className="table w-full">
                <thead>
                    <tr>
                        <th>{t("Manage.TableHeaderName")}</th>
                        <th className="hidden lg:table-cell">{t("Manage.TableHeaderAmount")}</th>
                        <th className={"hidden sm:table-cell"}>{t("Manage.TableHeaderRemaining")}</th>
                        <th className={"hidden sm:table-cell"}>{t("Manage.TableHeaderInterest")}</th>
                        <th className="hidden lg:table-cell">{t("Manage.TableHeaderDealDate")}</th>
                        <th className="hidden lg:table-cell">{t("Manage.TableHeaderDuration")}</th>
                    </tr>
                </thead>
                <tbody>{renderAssetList(assets, portfolio)}</tbody>
            </table>
        </Card>
    );
};
