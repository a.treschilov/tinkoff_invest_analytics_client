export const getSocialAccountUrl = (code: string, redirectPage: string): string => {
    const redirectUrl = process.env.NEXT_PUBLIC_BASE_URL + redirectPage;
    const tgRedirectURL = process.env.NEXT_PUBLIC_TELEGRAM_BOT_DOMAIN + redirectPage;

    switch (code) {
        case "GOOGLE":
            return (
                "https://accounts.google.com/o/oauth2/v2/auth?client_id=242488269772-70sjk5a8dh2f94m26cnbjajn3achldvi.apps.googleusercontent.com" +
                "&redirect_uri=" +
                redirectUrl +
                "/google&response_type=token&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/user.birthday.read"
            );
        case "YANDEX":
            return (
                "https://oauth.yandex.ru/authorize?response_type=token&client_id=2d07ef70ea0248c3b0f8069aaffb12dc&redirect_uri=" +
                redirectUrl +
                "/yandex"
            );
        case "TELEGRAM":
            return (
                "https://oauth.telegram.org/auth?bot_id=" +
                process.env.NEXT_PUBLIC_TELEGRAM_BOT_ID +
                "&origin=" +
                process.env.NEXT_PUBLIC_TELEGRAM_BOT_DOMAIN +
                "&embed=0&request_access=write&lang=en&return_to=" +
                tgRedirectURL +
                "/telegram"
            );
        default:
            return "" + process.env.NEXT_PUBLIC_BASE_URL;
    }
};

export const parseToken = (socialCode: string, hash: string): string | null => {
    let regExp;
    switch (socialCode) {
        case "TELEGRAM":
        case "TELEGRAM_WEB":
            regExp = /tgAuthResult=([^&]+)/;
            break;
        default:
            regExp = /access_token=([^&]+)/;
    }

    const tokenParam = regExp.exec(hash);
    return tokenParam ? tokenParam[1] : null;
};
