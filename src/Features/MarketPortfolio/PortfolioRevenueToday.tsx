import { useCallback, useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { IPrice } from "../../interfaces/IPrice";
import { ApiService } from "../../services/ApiService";
import { Trans } from "react-i18next";
import { DemoRevenueTodayData } from "./PortfolioStaticData";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Stats } from "../../components/Common/Stats/Stats";

const DefaultCurrency = "RUB";

type PortfolioRevenueTodayProps = {
    demoData?: boolean;
};

export const PortfolioRevenueToday = ({ demoData }: PortfolioRevenueTodayProps) => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [currency, setCurrency] = useState(DefaultCurrency);
    const [amount, setAmount] = useState<IPrice>({ amount: 0, currency: DefaultCurrency });

    const getData = useCallback(() => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/analytics/summary/today", {
            currency: currency
        })
            .then(response => {
                setAmount({
                    amount: response.revenueToday.amount,
                    currency: response.revenueToday.currency
                });
                setPageState(PageState.SUCCESS);
            })
            .catch(error => {
                if (error.response) {
                    setPageState(PageState.ERROR);
                }
            });
    }, [currency]);

    useEffect(() => {
        if (demoData) {
            const data = DemoRevenueTodayData.find(el => {
                return el.currency === currency;
            });
            if (data !== undefined) {
                setAmount(data);
            } else {
                setAmount({ currency: "RUB", amount: 0 });
            }
            setPageState(PageState.SUCCESS);
        } else {
            getData();
        }
    }, [currency, demoData, getData]);

    const currencyFormatter = new Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: amount.currency
    });

    const renderSwitcherItem = (currency: string) => {
        return (
            <span
                className={"link"}
                style={{ cursor: "pointer" }}
                onClick={() => {
                    setCurrency(currency);
                }}
            >
                {currency}
            </span>
        );
    };

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = <Skeleton className={"w-[200px] h-8 my-1"} />;
            break;
        case PageState.ERROR:
            content = <span>&mdash;</span>;
            break;
        default:
            content = <span>{currencyFormatter.format(amount.amount)}</span>;
    }

    return (
        <Stats.Stat className={"place-items-center"}>
            <Stats.Stat.Title>
                <Trans>Portfolio.RevenueToday</Trans>
            </Stats.Stat.Title>
            <Stats.Stat.Value>{content}</Stats.Stat.Value>
            <Stats.Stat.Desc>
                {renderSwitcherItem("USD")}
                ,&nbsp;
                {renderSwitcherItem("RUB")}
                ,&nbsp;
                {renderSwitcherItem("CNY")}
                ,&nbsp;
                {renderSwitcherItem("HKD")}
                ,&nbsp;
                {renderSwitcherItem("EUR")}
            </Stats.Stat.Desc>
        </Stats.Stat>
    );
};
