import { FC, ReactNode } from "react";
import { Textarea as DaisyUITextarea, TextareaProps as DaisyUITextareaProps } from "../../Common/Textarea/Textarea";

type TextareaExtensions = {
    Label: typeof Label;
    Textarea: typeof InputField;
    Description: typeof Description;
};

type TextareaProps = {
    children: ReactNode;
    containerClassName?: string;
};

export const Textarea: FC<TextareaProps> & TextareaExtensions = ({
    children,
    containerClassName = ""
}: TextareaProps) => {
    return <div className={`form-control w-full ` + containerClassName}>{children}</div>;
};

const Label = ({ children }: { children: ReactNode }) => {
    return (
        <label className="label">
            <span className={"label-text text-base-content"}>{children}</span>
        </label>
    );
};
Textarea.Label = Label;

const InputField = ({ ...args }: DaisyUITextareaProps) => {
    return <DaisyUITextarea {...args} />;
};
Textarea.Textarea = InputField;

const Description = ({ children }: { children: ReactNode }) => {
    return <small>{children}</small>;
};
Textarea.Description = Description;
