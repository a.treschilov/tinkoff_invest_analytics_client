"use client";

import { ChangeEvent, useEffect, useState } from "react";
import { ActionButtonText, ActionButtonTextProps } from "../../components/Common/ActionButtonText";
import { PageState } from "../../components/Common/Types";
import { Trans } from "react-i18next";
import { CredentialBreadcrumb } from "./CredentialBreadcrumb";
import { ApiService } from "../../services/ApiService";
import { Modal } from "../../components/Common/Modal/Modal";
import { PageLoader } from "../../components/Loader/Loaders";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";
import { Checkbox } from "../../components/Common/Checkbox/Checkbox";

type UserAccountListProps = {
    userCredentialId: number;
    isShow: boolean;
    hideModal: () => void;
    handleChangeTab: (name: string, userCredentialId: number) => void;
};

interface IBrokerAccount {
    id: number | null;
    externalId: string;
    isActive: number;
    name: string | null;
    type: number;
}
interface IBrokerAccountRequest {
    externalId: string;
    isActive: number;
}

export const UserAccountList = (props: UserAccountListProps) => {
    const [accountList, setAccountList] = useState<IBrokerAccount[] | null>(null);
    const [updateStatus, setUpdateStatus] = useState<ActionButtonTextProps>({
        status: "",
        children: <Trans>UserAccountList.EmptyText</Trans>
    });
    const [pageState, setPageState] = useState(PageState.LOADING);

    useEffect(() => {
        if (props.userCredentialId !== 0 && props.isShow) {
            setUpdateStatus({
                status: "",
                children: <Trans>UserAccountList.EmptyText</Trans>
            });
            setPageState(PageState.LOADING);

            ApiService.fetch("/v1/user/credential/" + props.userCredentialId + "/account")
                .then(response => {
                    setAccountList(response);
                    setPageState(PageState.EDIT_FORM);
                })
                .catch(() => {
                    setPageState(PageState.ERROR);
                });
        }
    }, [props.userCredentialId, props.isShow]);

    const updateUserAccount = (credentialId: number, accountList: IBrokerAccount[] | null) => {
        const accounts: IBrokerAccountRequest[] = [];
        accountList?.forEach(account => {
            accounts.push({
                externalId: account.externalId,
                isActive: account.isActive
            });
        });

        ApiService.post("/v1/user/credential/" + credentialId + "/account", {
            credentialId: credentialId,
            userAccount: accounts
        })
            .then(() => {
                setUpdateStatus({
                    status: "success",
                    children: <Trans>UserAccountList.SuccessText</Trans>
                });
            })
            .catch(() => {
                setUpdateStatus({
                    status: "error",
                    children: <Trans>UserAccountList.ErrorText</Trans>
                });
            });
    };

    const goTo = (tabName: string) => {
        props.handleChangeTab(tabName, props.userCredentialId);
    };

    const handleHideModal = () => {
        props.hideModal();
    };

    const handleFormSubmit = (event: ChangeEvent<HTMLFormElement>) => {
        updateUserAccount(props.userCredentialId, accountList);

        event.preventDefault();
    };

    const handleIsActiveChange = (event: ChangeEvent<HTMLInputElement>) => {
        const externalId: string = event.target.name;

        const newAccountList: IBrokerAccount[] = accountList || [];
        for (const i in newAccountList) {
            if (newAccountList[i].externalId === externalId) {
                newAccountList[i].isActive = newAccountList[i].isActive ? 0 : 1;
            }
        }

        setAccountList(newAccountList);
    };

    const renderForm = () => {
        const tableBody = accountList?.map((account: IBrokerAccount) => {
            return (
                <tr key={"user-account-row" + account.externalId}>
                    <td>
                        <Checkbox
                            name={account.externalId}
                            defaultChecked={!!account.isActive}
                            onChange={handleIsActiveChange}
                            id={"userAccount-" + account.externalId}
                        />
                    </td>
                    <td className={"hidden sm:table-cell"}>
                        <label htmlFor={"userAccount-" + account.externalId}>{account.externalId}</label>
                    </td>
                    <td>
                        <label htmlFor={"userAccount-" + account.externalId}>{account.name}</label>
                    </td>
                    <td className={"hidden lg:table-cell"}>
                        <label htmlFor={"userAccount-" + account.externalId}>{account.type}</label>
                    </td>
                </tr>
            );
        });
        return (
            <table className="table w-full">
                <thead>
                    <tr>
                        <th></th>
                        <th className={"hidden sm:table-cell"}>
                            <Trans>UserAccountList.TableExternalId</Trans>
                        </th>
                        <th>
                            <Trans>UserAccountList.TableName</Trans>
                        </th>
                        <th className={"hidden lg:table-cell"}>
                            <Trans>UserAccountList.TableType</Trans>
                        </th>
                    </tr>
                </thead>
                <tbody>{tableBody}</tbody>
            </table>
        );
    };

    const renderError = () => {
        return (
            <div className="text-center">
                <Trans>UserAccountList.EmptyAccountList</Trans>
            </div>
        );
    };

    const renderContent = () => {
        switch (pageState) {
            case PageState.LOADING:
                return <PageLoader />;
            case PageState.EDIT_FORM:
                return renderForm();
            case PageState.ERROR:
                return renderError();
            default:
                return <PageLoader />;
        }
    };

    return (
        <Modal show={props.isShow} onClose={handleHideModal}>
            <form onSubmit={handleFormSubmit}>
                <Modal.Header>
                    <Trans>UserAccountList.Header</Trans>
                </Modal.Header>
                <Modal.Body>
                    <Card className={"bg-base-300 p-2 pl-6 mb-4"} border={true}>
                        <CredentialBreadcrumb
                            credentialId={props.userCredentialId}
                            navClickHandler={tabName => goTo(tabName)}
                            activeTab="account"
                        />
                    </Card>
                    <Card className={"bg-base-300 p-4 mb-6"} border={true}>
                        {renderContent()}
                    </Card>
                </Modal.Body>
                <Modal.Actions>
                    <ActionButtonText status={updateStatus.status}>{updateStatus.children}</ActionButtonText>
                    <Button type="submit" color={"primary"}>
                        <Trans>UserAccountList.ButtonTextSave</Trans>
                    </Button>
                </Modal.Actions>
            </form>
        </Modal>
    );
};
