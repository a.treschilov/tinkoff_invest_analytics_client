export const WealthHistoryTypesDemoData = {
    date: {
        date: "2023-05-01 23:59:59.000000",
        timezone: "UTC"
    },
    currency: "RUB",
    stock: 7300000,
    realEstate: 15400000,
    loan: 12000000,
    assets: 900000,
    deposit: 4500000,
    expenses: 400000,
    wealthRate: 3.54
};
