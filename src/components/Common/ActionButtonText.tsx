import { ReactNode } from "react";

export type ActionButtonTextProps = {
    children?: ReactNode;
    status: string;
    disabled?: boolean | false;
};

export const ActionButtonText = ({ children, status }: ActionButtonTextProps) => {
    switch (status) {
        case "success":
            return <span className="text-success me-2">{children}</span>;
        case "error":
            return <span className="text-danger me-2">{children}</span>;
        case "processing":
            return <span className="text-neutral me-2">{children}</span>;
        default:
            return <span />;
    }
};
