"use client";

import { useContext, useEffect, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { IResponseOperations } from "./DepositManage";
import { PageState } from "../../components/Common/Types";
import { IOperation } from "../Operation/OperationEdit";
import { OperationSuggestion } from "../Operation/Suggestion/OperationSuggestion";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

type DepositOperationSuggestionProps = {
    handleOperationsAdded: () => void;
    time: number;
};

export const DepositOperationSuggestion = ({ handleOperationsAdded, time }: DepositOperationSuggestionProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/deposit");

    const [operations, setOperations] = useState<IOperation[]>([]);
    const [pageState, setPageState] = useState(PageState.LOADING);

    useEffect(() => {
        setPageState(PageState.LOADING);
        setOperations([]);
        ApiService.fetch("/v1/deposit/payouts").then(response => {
            const possibleOperations = response.map((operation: IResponseOperations) => {
                return {
                    itemOperationId: operation.id,
                    brokerId: operation.broker,
                    itemId: operation.itemId,
                    itemName: operation.itemName,
                    operationType: operation.type,
                    externalId: operation.externalId,
                    quantity: operation.quantity,
                    date: {
                        date: operation.date.date,
                        timezone: operation.date.timezone
                    },
                    amount: operation.amount,
                    currencyIso: operation.currency,
                    status: operation.status
                };
            });
            setOperations(possibleOperations);
            setPageState(PageState.SUCCESS);
        });
    }, [time]);

    const handleAddBatchOperations = () => {
        setPageState(PageState.SAVING_DATA);
        ApiService.post("/v1/operations/addBatch", operations).then(() => {
            setOperations([]);
            setPageState(PageState.SUCCESS);
            handleOperationsAdded();
        });
    };

    if (operations.length === 0) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>{t("Deposit.SuggestionOperation.Title")}</Card.Title>
            <Divider />
            <div className={"mb-4"}>{t("Deposit.SuggestionOperation.Description")}</div>
            <div className={"grid grid-cols-12 gap-4"}>
                <div className={"col-span-12 sm:col-span-3 text-gray-500 font-bold text-xs"}>
                    {t("Deposit.SuggestionOperation.TableHeaderName")}
                </div>
                <div className={"col-span-12 sm:col-span-9 text-gray-500 font-bold text-xs"}>
                    {t("Deposit.SuggestionOperation.TableHeaderDate")} /{" "}
                    {t("Deposit.SuggestionOperation.TableHeaderAmount")}
                </div>
                <div className={"col-span-12 h-0.25 divider m-0"}></div>
                <OperationSuggestion operations={operations} onChange={ops => setOperations(ops)} state={pageState} />
            </div>
            <Card.Actions className={"mt-4"}>
                <Button
                    onClick={handleAddBatchOperations}
                    disabled={pageState === PageState.SAVING_DATA}
                    color={"primary"}
                >
                    {t("Deposit.SuggestionOperation.AddButton")}
                </Button>
            </Card.Actions>
        </Card>
    );
};
