"use client";

import { ChangeEvent, ReactNode, useEffect, useState } from "react";
import { Color, DateType, PageState } from "../../components/Common/Types";
import { Trans } from "react-i18next";
import { DefaultUserSettingsData } from "./UserSettingsStaticData";
import { ApiService } from "../../services/ApiService";
import { formatInputDayDate } from "../../utils/date";
import { IPrice } from "../../interfaces/IPrice";
import { ErrorContent } from "../../components/ErrorContent/ErrorContent";
import { UserSettingsLoading } from "./UserSettingsLoading";
import { InputText } from "../../components/Common/Form/InputText";
import { InputCurrencyAmount } from "../../components/Common/Form/InputCurrencyAmount";
import { Alert } from "../../components/Common/Alert/Alert";
import { Button } from "../../components/Common/Button/Button";

type UserSettingsProp = {
    onChange?: () => void;
};

type ButtonState = {
    status: Color;
    text: ReactNode;
    disabled?: boolean | false;
};

export interface IUserSettings {
    desiredPension: IPrice;
    userSettingsId: number | null;
    retirementAge: number;
    expenses: IPrice;
    birthday: DateType | null;
}

export const UserSettings = ({ onChange }: UserSettingsProp) => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [saveError, setSaveError] = useState<string | null>(null);
    const [userSettings, setUserSettings] = useState(DefaultUserSettingsData);
    const [dataSaveStatus, setDataSaveStatus] = useState<ButtonState>({
        status: "primary",
        text: <Trans>PortfolioSettings.ButtonTextSave</Trans>,
        disabled: false
    });

    useEffect(() => {
        getData()
            .then(response => {
                setPageState(PageState.EDIT_FORM);
                setUserSettings({
                    desiredPension: {
                        amount: response.desiredPension.amount || 0,
                        currency: response.desiredPension.currency || "RUB"
                    },
                    userSettingsId: response.userSettingsId,
                    retirementAge: response.retirementAge,
                    expenses: {
                        amount: response.expenses.amount || 0,
                        currency: response.expenses.currency || "RUB"
                    },
                    birthday: response.birthday
                });
            })
            .catch(() => {
                setPageState(PageState.ERROR);
            });
    }, []);

    const handleChange = () => {
        if (onChange !== undefined) {
            onChange();
        }
    };

    const getData = () => {
        return ApiService.fetch("/v1/user/settings");
    };

    const changeElement = (name: string, value: string, inputType: string) => {
        let settings = userSettings;

        let typedValue: number | string | object;
        switch (inputType) {
            case "number":
                typedValue = parseFloat(value);
                break;
            default:
                typedValue = value;
        }

        let data;
        switch (name) {
            case "birthday":
                data = {
                    birthday: {
                        date: value,
                        timezone: settings.birthday?.timezone || "UTC",
                        timezone_type: settings.birthday?.timezone_type || 3
                    }
                };
                break;
            case "expensesAmount":
                data = {
                    expenses: {
                        amount: parseFloat(value),
                        currency: settings.expenses.currency
                    }
                };
                break;
            case "expensesCurrency":
                data = {
                    expenses: {
                        amount: settings.expenses.amount,
                        currency: value
                    }
                };
                break;
            case "desiredPensionAmount":
                data = {
                    desiredPension: {
                        amount: parseFloat(value),
                        currency: settings.desiredPension.currency
                    }
                };
                break;
            case "desiredPensionCurrency":
                data = {
                    desiredPension: {
                        amount: settings.desiredPension.amount,
                        currency: value
                    }
                };
                break;
            default:
                data = {
                    [name]: typedValue
                };
        }

        settings = { ...settings, ...data };

        setUserSettings(settings);
    };

    const handleChangeFormData = (event: ChangeEvent<HTMLInputElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleSubmitForm = (event: ChangeEvent<HTMLFormElement>) => {
        setDataSaveStatus({
            status: "primary",
            text: <Trans>PortfolioSettings.ButtonTextSaving</Trans>,
            disabled: true
        });
        ApiService.post("/v1/user/settings", userSettings)
            .then(response => {
                const userSettingsData: IUserSettings = userSettings;
                userSettingsData.userSettingsId = response.userSettingsId;
                setUserSettings(userSettingsData);

                setDataSaveStatus({
                    status: "success",
                    text: <Trans>PortfolioSettings.ButtonTextSuccessSave</Trans>,
                    disabled: false
                });
                setTimeout(() => {
                    setDataSaveStatus({
                        status: "primary",
                        text: <Trans>PortfolioSettings.ButtonTextSave</Trans>,
                        disabled: false
                    });
                }, 2000);

                handleChange();
            })
            .catch(error => {
                setPageState(PageState.EDIT_FORM_ERROR);
                setSaveError(error.response.data.error_message);
                setDataSaveStatus({
                    status: "primary",
                    text: <Trans>PortfolioSettings.ButtonTextSave</Trans>,
                    disabled: false
                });
            });

        event.preventDefault();
    };

    if (pageState === PageState.ERROR) {
        return <ErrorContent />;
    }
    if (pageState === PageState.LOADING) {
        return <UserSettingsLoading />;
    }

    return (
        <div>
            <Alert
                show={pageState === PageState.EDIT_FORM_ERROR}
                className={"mb-6"}
                onClose={() => {
                    setPageState(PageState.EDIT_FORM);
                    setSaveError(null);
                }}
                status={"warning"}
            >
                {saveError}
            </Alert>
            <form onSubmit={handleSubmitForm} className={"grid"}>
                <div className="grid grid-cols-1 md:grid-cols-2 gap-6 mb-6">
                    <InputCurrencyAmount
                        amountInputName="expensesAmount"
                        currencyInputName="expensesCurrency"
                        amount={userSettings.expenses.amount}
                        onChange={changeElement}
                        currency={userSettings.expenses.currency}
                        id={"expenses"}
                        label={<Trans>PortfolioSettings.Expenses</Trans>}
                        topDescription={<Trans>PortfolioSettings.ExpensesDescription</Trans>}
                    />
                    <InputCurrencyAmount
                        amount={userSettings.desiredPension.amount}
                        onChange={changeElement}
                        currency={userSettings.desiredPension.currency}
                        id={"desiredPension"}
                        currencyInputName={"desiredPensionCurrency"}
                        amountInputName={"desiredPensionAmount"}
                        label={<Trans>PortfolioSettings.DesiredIncomeTitle</Trans>}
                        topDescription={<Trans>PortfolioSettings.DesiredPensionDescription</Trans>}
                    />
                    <InputText
                        label={<Trans>PortfolioSettings.RetirementAge</Trans>}
                        type={"number"}
                        step="1"
                        min="0"
                        name="retirementAge"
                        defaultValue={userSettings.retirementAge}
                        onChange={handleChangeFormData}
                    />
                    <InputText
                        label={<Trans>PortfolioSettings.BirthdayTitle</Trans>}
                        type="date"
                        name="birthday"
                        defaultValue={
                            userSettings.birthday !== null
                                ? (formatInputDayDate(new Date(userSettings.birthday.date)) ?? undefined)
                                : undefined
                        }
                        onChange={handleChangeFormData}
                    />
                </div>
                <div>
                    <Button
                        type="submit"
                        disabled={dataSaveStatus.disabled}
                        loading={dataSaveStatus.disabled}
                        color={dataSaveStatus.status}
                        className={"float-right"}
                    >
                        {dataSaveStatus.text}
                    </Button>
                </div>
            </form>
        </div>
    );
};
