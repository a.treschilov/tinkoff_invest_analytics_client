import { FiMoreHorizontal } from "react-icons/fi";
import { Card, Divider, Dropdown, Skeleton } from "react-daisyui";
import { Trans } from "react-i18next";

type RealEstateListLoadingProps = {
    show: boolean;
};

const tableRowCount = 5;

export const RealEstateListLoading = ({ show }: RealEstateListLoadingProps) => {
    if (!show) {
        return <></>;
    }

    const renderRow = (rowNumber: number) => {
        return (
            <tr key={"deposit-loading-row" + rowNumber}>
                <td>
                    <Skeleton className={"w-5/6 h-4"} />
                    <Skeleton className={"sm:hidden w-2/6 h-4 mt-1"} />
                </td>
                <td className="hidden sm:table-cell">
                    <Skeleton className={"w-3/6 h-4"} />
                </td>
                <td className="hidden md:table-cell">
                    <Skeleton className={"w-2/6 h-4"} />
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-2/6 h-4"} />
                </td>
                <td>
                    <Dropdown>
                        <Dropdown.Toggle disabled={true}>
                            <FiMoreHorizontal></FiMoreHorizontal>
                        </Dropdown.Toggle>
                    </Dropdown>
                </td>
            </tr>
        );
    };

    const header = () => {
        return (
            <thead>
                <tr>
                    <th>
                        <Skeleton className={"w-4/6 h-4"} />
                    </th>
                    <th className="hidden sm:table-cell">
                        <Skeleton className={"w-2/6 h-4"} />
                    </th>
                    <th className="hidden md:table-cell">
                        <Skeleton className={"w-1/6 h-4"} />
                    </th>
                    <th className="hidden lg:table-cell">
                        <Skeleton className={"w-4/6 h-4"} />
                    </th>
                    <th className={"w-12"}></th>
                </tr>
            </thead>
        );
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
            <Card.Title tag="h2">
                <Trans>RealEstateManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="w-full">
                <table className="table w-full">
                    {header()}
                    <tbody>
                        {[...Array(tableRowCount)].map((value: number, key: number) => {
                            return renderRow(key);
                        })}
                    </tbody>
                </table>
            </div>
        </Card>
    );
};
