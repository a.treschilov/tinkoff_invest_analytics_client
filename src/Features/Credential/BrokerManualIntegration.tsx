import { BrokerType } from "./CredentialFormBody";
import { FileInput } from "../../components/Common/Form/FileInput";
import { useContext, useEffect, useRef, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { PageState } from "../../components/Common/Types";
import { Alert } from "../../components/Common/Alert/Alert";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import Link from "next/link";
import { Divider } from "../../components/Common/Divider/Divider";
import { Stats } from "../../components/Common/Stats/Stats";

type BrokerManualIntegrationProps = {
    show: boolean;
    broker?: BrokerType | null;
    onUpload: () => void;
};

type UploadedDataStatisticType = {
    added: number;
    skipped: number;
    updated: number;
};

export const BrokerManualIntegration = ({ show, broker = null, onUpload }: BrokerManualIntegrationProps) => {
    const [pageState, setPageState] = useState(PageState.EDIT_FORM);
    const [importStatistic, setImportStatistic] = useState<UploadedDataStatisticType | null>(null);
    const form = useRef<HTMLFormElement>(null);
    const file = useRef<HTMLInputElement>(null);
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "broker");

    useEffect(() => {
        clearState();
    }, [broker]);

    const clearState = () => {
        setImportStatistic(null);
        setPageState(PageState.EDIT_FORM);
    };

    const loadReport = () => {
        clearState();
        setPageState(PageState.SAVING_DATA);
        const data = form.current !== null ? new FormData(form.current) : new FormData();
        const headers = {
            "Content-Type": "multipart/form-data"
        };
        ApiService.post("/v1/broker/manualImport/" + broker?.id, data, headers)
            .then((response: UploadedDataStatisticType) => {
                setImportStatistic({
                    added: response.added,
                    updated: response.updated,
                    skipped: response.skipped
                });
                setPageState(PageState.EDIT_FORM);
                onUpload();
            })
            .catch(() => {
                setPageState(PageState.ERROR);
                setImportStatistic(null);
            })
            .finally(() => {
                if (file.current !== null) {
                    file.current.value = "";
                }
            });
    };

    if (!show) {
        return <></>;
    }
    return (
        <form ref={form}>
            <Divider />
            <h3 className={"text-xl mb-4"}>{broker?.name}</h3>
            <Alert status={"error"} dismissible={true} className={"mb-4"} show={pageState === PageState.ERROR}>
                <h4 className={"text-lg"}>{t("ManualImport.UploadErrorTitle")}</h4>
                <Divider className={"my-1 text-error-content"} />
                <span className={"text-sm"}>{t("ManualImport.UploadErrorDescription")}</span>
            </Alert>
            <Alert
                dismissible={true}
                status={"success"}
                show={importStatistic !== null}
                onClose={() => setImportStatistic(null)}
                className={"mb-4"}
            >
                <Stats className="bg-success grid-flow-row sm:grid-flow-col" vertical={true}>
                    <Stats.Stat className="place-items-center">
                        <Stats.Stat.Title className={"text-success-content"}>
                            {t("ManualImport.UploadSuccessAdded")}
                        </Stats.Stat.Title>
                        <Stats.Stat.Value className={"text-success-content"}>{importStatistic?.added}</Stats.Stat.Value>
                        <Stats.Stat.Desc className={"text-success-content"}>
                            {t("ManualImport.UploadSuccessDescription")}
                        </Stats.Stat.Desc>
                    </Stats.Stat>
                    <Stats.Stat className="place-items-center">
                        <Stats.Stat.Title className={"text-success-content"}>
                            {t("ManualImport.UploadSuccessUpdated")}
                        </Stats.Stat.Title>
                        <Stats.Stat.Value className={"text-success-content"}>
                            {importStatistic?.updated}
                        </Stats.Stat.Value>
                        <Stats.Stat.Desc className={"text-success-content"}>
                            {t("ManualImport.UploadSuccessDescription")}
                        </Stats.Stat.Desc>
                    </Stats.Stat>
                    <Stats.Stat className="place-items-center">
                        <Stats.Stat.Title className={"text-success-content"}>
                            {t("ManualImport.UploadSuccessSkipped")}
                        </Stats.Stat.Title>
                        <Stats.Stat.Value className={"text-success-content"}>
                            {importStatistic?.skipped}
                        </Stats.Stat.Value>
                        <Stats.Stat.Desc className={"text-success-content"}>
                            {t("ManualImport.UploadSuccessDescription")}
                        </Stats.Stat.Desc>
                    </Stats.Stat>
                </Stats>
            </Alert>
            <FileInput>
                <FileInput.Label>
                    {t("ManualImport.ReportFileInputTitle")}
                    {broker?.instructionLink ? (
                        <>
                            &nbsp;
                            <Link
                                href={process.env.NEXT_PUBLIC_LANDING_PAGE_URL + broker?.instructionLink}
                                target={"_blank"}
                                className={"text-primary"}
                            >
                                ({t("ManualImport.ReportFileInputInstruction")})
                            </Link>
                        </>
                    ) : (
                        <></>
                    )}
                </FileInput.Label>
                <FileInput.File
                    disabled={pageState === PageState.SAVING_DATA}
                    ref={file}
                    name={"report"}
                    accept={".xlsx, .xls"}
                    onInput={() => {
                        loadReport();
                    }}
                />
                <FileInput.Description>{t("ManualImport.ReportFileInputDescription")}</FileInput.Description>
            </FileInput>
        </form>
    );
};
