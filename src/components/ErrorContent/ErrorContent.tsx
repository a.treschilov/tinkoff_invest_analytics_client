"use client";

import { BsArrowLeft } from "react-icons/bs";
import { AxiosError } from "axios";
import { Trans } from "react-i18next";
import { useEffect, useState } from "react";

export type ErrorContentProps = {
    error?: AxiosError | null;
};

export type AxiosErrorData = {
    error_code: number;
    error_message: string;
};

export const ErrorContent = ({ error = null }: ErrorContentProps) => {
    const [status] = useState(error?.response?.status);
    const [statusText, setStatusText] = useState(error?.response?.statusText);

    useEffect(() => {
        if (error?.response !== undefined) {
            const data: unknown = error.response.data;
            if (error?.response?.status >= 500) {
                setStatusText(error?.response?.statusText);
            } else if (error?.response?.status >= 400) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                if (data?.error_code !== undefined && data?.error_message !== undefined) {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    setStatusText(error?.response?.data?.error_code + ": " + error?.response?.data?.error_message);
                } else {
                    setStatusText(error?.response?.statusText);
                }
            }
        }
    }, [error]);

    return (
        <div className="text-center mt-4">
            <h1>{status}</h1>
            <p className="lead">{statusText}</p>
            {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
            <a href="/user">
                <BsArrowLeft className="me-1 inline" />
                <span className={"link"}>
                    <Trans>ErrorContent.BackToDashboard</Trans>
                </span>
            </a>
        </div>
    );
};
