import { Trans } from "react-i18next";
import { DashboardPopover } from "./DashboardPopover";
import { BsQuestionCircle } from "react-icons/bs";
import { Popover } from "../../components/Common/Popover/Popover";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Kbd } from "../../components/Common/Kbd/Kbd";

interface DashboardCardRow {
    label: string;
    amount: number | null;
    isLoaded: boolean;
    helperValue?: number | null;
    isTitle?: boolean;
    popoverBody?: string | null;
    emptyValueDescription?: PopoverMessageType | null;
}

type PopoverMessageType = {
    title?: string | null;
    body?: string | null;
};

const currencyFormatter = new Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB",
    minimumFractionDigits: 0,
    notation: "compact"
});

const fullCurrencyFormatter = new Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB",
    minimumFractionDigits: 0
});

export const DashboardCardMoneyRow = ({ label, amount, isLoaded, isTitle }: DashboardCardRow) => {
    if (!isLoaded) {
        return <Skeleton className={"h-4"} />;
    }
    const colorClass = isTitle === true ? "text-xl font-semibold" : "";
    return (
        <div className={"grid grid-cols-3"}>
            <div className={colorClass + " col-span-2"}>
                <Trans>{label}</Trans>:
            </div>
            <div className={colorClass + " text-end"} title={fullCurrencyFormatter.format(amount || 0)}>
                {currencyFormatter.format(amount || 0)}
            </div>
        </div>
    );
};

export const DashboardCardMoneyPeriodRow = ({ label, amount, isLoaded }: DashboardCardRow) => {
    if (!isLoaded) {
        return <Skeleton className={"h-4"} />;
    }

    return (
        <div className={"grid grid-cols-3"}>
            <div className="text-black-50 col-span-2">
                <Trans>{label}</Trans>:{" "}
            </div>
            <div className="text-end" title={fullCurrencyFormatter.format(amount || 0)}>
                <Trans
                    i18nKey="Dashboard.PerMonth"
                    values={{
                        amount: currencyFormatter.format(amount || 0)
                    }}
                ></Trans>
            </div>
        </div>
    );
};

export const DashboardCardNumberRow = ({ label, amount, isLoaded, helperValue, isTitle }: DashboardCardRow) => {
    if (!isLoaded) {
        return <Skeleton className={"h-4"} />;
    }

    const colorClass = isTitle === true ? "text-xl font-semibold" : "";

    return (
        <div className={"grid grid-cols-3"}>
            <div className={colorClass + " col-span-2 flex"}>
                <Trans>{label}</Trans>{" "}
                {helperValue !== undefined ? <DashboardPopover wealthRate={helperValue} /> : <></>}
            </div>
            <div className={colorClass + " text-end"}>
                <Kbd size="md" className={"bg-success text-white"}>
                    {amount?.toFixed(1) || "N/A"}
                </Kbd>
            </div>
        </div>
    );
};

export const DashboardCardPercentRow = ({
    label,
    amount,
    isLoaded,
    isTitle,
    popoverBody,
    emptyValueDescription
}: DashboardCardRow) => {
    if (!isLoaded) {
        return <Skeleton className={"h-4"} />;
    }

    const colorClass = isTitle === true ? "text-xl font-semibold" : "";
    const percentFormatter = Intl.NumberFormat("en-US", { style: "percent" });

    const popoverEmptyValue = () => {
        if (emptyValueDescription === null || amount !== null) {
            return <></>;
        }

        const description = <Trans>{emptyValueDescription?.body}</Trans>;

        return (
            <Popover content={description}>
                <BsQuestionCircle style={{ height: "16px", cursor: "pointer" }} />
            </Popover>
        );
    };

    const popover = () => {
        if (popoverBody === undefined) {
            return <></>;
        }
        const popover = <Trans>{popoverBody}</Trans>;

        return (
            <Popover content={popover}>
                <BsQuestionCircle style={{ height: "16px", cursor: "pointer" }} />
            </Popover>
        );
    };

    return (
        <div className={"grid grid-cols-3"}>
            <div className={colorClass + " col-span-2 flex"}>
                <Trans>{label}</Trans>
                {popover()}
            </div>
            <div className={colorClass + " text-end flex justify-end"}>
                {amount === null ? "N/A" : percentFormatter.format(amount)} {popoverEmptyValue()}
            </div>
        </div>
    );
};
