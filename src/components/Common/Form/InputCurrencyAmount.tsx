import { InputLabel } from "./InputText";
import { CurrencyAmount, CurrencyAmountProps } from "../CurrencyAmount/CurrencyAmount";

type InputCurrencyAmountProps = InputLabel & CurrencyAmountProps;
export const InputCurrencyAmount = ({
    label,
    topDescription = null,
    containerClassName = "",
    ...args
}: InputCurrencyAmountProps) => {
    const topDescriptionBlock =
        topDescription === null ? (
            <></>
        ) : (
            <small className={"mb-2"}>
                <span>{topDescription}</span>
            </small>
        );

    return (
        <div className={`form-control w-full ` + containerClassName}>
            <label className="label flex">
                <span className={"label-text text-base-content"}>{label}</span>
            </label>
            {topDescriptionBlock}
            <CurrencyAmount {...args} />
        </div>
    );
};
