"use client";

import { Trans } from "react-i18next";
import { Breadcrumbs } from "react-daisyui";

type CredentialBreadCrumbTabs = "credential" | "account";

interface ICredentialBreadCrumbProps {
    credentialId: number;
    activeTab: CredentialBreadCrumbTabs;
    navClickHandler: (tabName: CredentialBreadCrumbTabs) => void;
}

export const CredentialBreadcrumb = (props: ICredentialBreadCrumbProps) => {
    const isActiveCredential = props.activeTab === "credential";
    const isActiveAccount = props.activeTab === "account" || props.credentialId === 0;

    return (
        <Breadcrumbs>
            <Breadcrumbs.Item>
                <a
                    href={"#"}
                    onClick={event => {
                        props.navClickHandler("credential");
                        event.preventDefault();
                    }}
                    className={"link " + (!isActiveCredential ? "link-primary" : "")}
                >
                    <Trans>CredentialBreadCrumbs.CredentialTab</Trans>
                </a>
            </Breadcrumbs.Item>
            <Breadcrumbs.Item>
                <a
                    href={"#"}
                    onClick={event => {
                        props.navClickHandler("account");
                        event.preventDefault();
                    }}
                    className={"link " + (!isActiveAccount ? "link-primary" : "")}
                >
                    <Trans>CredentialBreadCrumbs.AccountTab</Trans>
                </a>
            </Breadcrumbs.Item>
        </Breadcrumbs>
    );
};
