import { FiMoreHorizontal } from "react-icons/fi";
import { Skeleton, Dropdown, Card, Divider } from "react-daisyui";
import { Trans } from "react-i18next";

type LoanLoadingProps = {
    show: boolean;
};

const tableRowCount = 3;

export const LoanLoading = ({ show }: LoanLoadingProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
            <Card.Title tag="h2">
                <Trans>LoanManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="overflow-x-auto w-full">
                <table className="table w-full">
                    <thead>
                        <tr>
                            <th>
                                <Skeleton className={"w-5/6 h-4"} />
                            </th>
                            <th className={"hidden sm:table-cell"}>
                                <Skeleton className={"w-5/6 h-4"} />
                            </th>
                            <th className="hidden sm:table-cell">
                                <Skeleton className={"w-5/6 h-4"} />
                            </th>
                            <th className="hidden lg:table-cell">
                                <Skeleton className={"w-5/6 h-4"} />
                            </th>
                            <th className="hidden md:table-cell">
                                <Skeleton className={"w-5/6 h-4"} />
                            </th>
                            <th className={"w-12"}></th>
                        </tr>
                    </thead>
                    <tbody>
                        {[...Array(tableRowCount)].map((x, i) => {
                            return (
                                <tr key={"load-loading-row" + i}>
                                    <td>
                                        <Skeleton className={"w-5/6 h-4"} />
                                        <Skeleton className={"w-4/6 h-4 mt-1 sm:hidden"} />
                                    </td>
                                    <td className={"hidden sm:table-cell"}>
                                        <Skeleton className={"w-5/6 h-4"} />
                                    </td>
                                    <td className="hidden sm:table-cell">
                                        <Skeleton className={"w-5/6 h-4"} />
                                    </td>
                                    <td className="hidden lg:table-cell">
                                        <Skeleton className={"w-5/6 h-4"} />
                                    </td>
                                    <td className="hidden md:table-cell">
                                        <Skeleton className={"w-5/6 h-4"} />
                                    </td>
                                    <td style={{ width: "55px" }}>
                                        <Dropdown>
                                            <Dropdown.Toggle disabled={true}>
                                                <FiMoreHorizontal></FiMoreHorizontal>
                                            </Dropdown.Toggle>
                                        </Dropdown>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </Card>
    );
};
