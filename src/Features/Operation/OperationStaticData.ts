import { formatInputDateTime } from "../../utils/date";
import { IOperation } from "./OperationEdit";

export const DefaultOperationData: IOperation = {
    itemOperationId: null,
    brokerId: null,
    itemId: 0,
    itemName: "",
    itemType: "real_estate",
    operationType: "DividendCard",
    externalId: null,
    quantity: null,
    date: {
        date: formatInputDateTime(new Date()),
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
    },
    amount: 0,
    currencyIso: "RUB",
    status: "Done"
};
