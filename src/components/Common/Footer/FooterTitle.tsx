import { HTMLAttributes, ReactNode } from "react";

type FooterTitleProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const FooterTitle = ({ children, className, ...args }: FooterTitleProps) => {
    const classes = ["footer-title"];
    if (className) {
        classes.push(...className.split(" "));
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
