import { FC, ReactNode } from "react";
import { CardTitle } from "./CardTitle";
import { CardBody } from "./CardBody";
import { CardActions } from "./CardActions";

type CardExtensions = {
    Title: typeof CardTitle;
    Body: typeof CardBody;
    Actions: typeof CardActions;
};

type CardProps = {
    children: ReactNode;
    className?: string;
    border?: boolean;
};

export const Card: FC<CardProps> & CardExtensions = ({ children, className, border = false }: CardProps) => {
    const classes = ["card"];
    if (className) {
        classes.push(...className?.split(" "));
    }
    if (border) {
        classes.push("card-border");
    }

    return <div className={classes.join(" ")}>{children}</div>;
};

Card.Title = CardTitle;
Card.Body = CardBody;
Card.Actions = CardActions;
