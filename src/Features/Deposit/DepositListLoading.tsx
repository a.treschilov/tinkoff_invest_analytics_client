import { FiMoreHorizontal } from "react-icons/fi";
import { Trans } from "react-i18next";
import { Divider } from "../../components/Common/Divider/Divider";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Card } from "../../components/Common/Card/Card";
import { Dropdown } from "../../components/Common/Dropdown/Dropdown";

type DepositListLoadingProps = {
    show: boolean;
};

const tableRowCount = 5;

export const DepositListLoading = ({ show }: DepositListLoadingProps) => {
    if (!show) {
        return <></>;
    }

    const renderRow = (rowNumber: number) => {
        return (
            <tr key={"deposit-loading-row" + rowNumber}>
                <td className={"sm:hidden"}>
                    <Skeleton className={"w-3/6 h-4 mb-1"} />
                    <Skeleton className={"w-5/6 h-4"} />
                </td>
                <td className={"hidden sm:table-cell"}>
                    <Skeleton className={"w-5/6 h-4"} />
                </td>
                <td className="hidden md:table-cell">
                    <Skeleton className={"w-2/6 h-4"} />
                </td>
                <td className={"hidden sm:table-cell"}>
                    <Skeleton className={"w-4/6 h-4"} />
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-3/5 h-4"} />
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-2/6 h-4"} />
                </td>
                <td>
                    <Dropdown>
                        <Dropdown.Toggle disabled={true}>
                            <FiMoreHorizontal></FiMoreHorizontal>
                        </Dropdown.Toggle>
                    </Dropdown>
                </td>
            </tr>
        );
    };

    const header = () => {
        return (
            <thead>
                <tr>
                    <th scope="col" className={"sm:hidden"}>
                        <Skeleton className={"w-5/6 h-4"} />
                    </th>
                    <th scope="col" className={"hidden sm:table-cell"}>
                        <Skeleton className={"w-2/6 h-4"} />
                    </th>
                    <th scope="col" className="hidden md:table-cell">
                        <Skeleton className={"w-3/6 h-4"} />
                    </th>
                    <th scope="col" className={"hidden sm:table-cell"}>
                        <Skeleton className={"w-1/6 h-4"} />
                    </th>
                    <th scope="col" className="hidden lg:table-cell">
                        <Skeleton className={"w-3/6 h-4"} />
                    </th>
                    <th scope="col" className="hidden lg:table-cell">
                        <Skeleton className={"w-1/6 h-4"} />
                    </th>
                    <th scope="col" className={"w-12"}></th>
                </tr>
            </thead>
        );
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>DepositManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="overflow-x-auto w-full">
                <table className="table w-full">
                    {header()}
                    <tbody>
                        {[...Array(tableRowCount)].map((value: number, key: number) => {
                            return renderRow(key);
                        })}
                    </tbody>
                </table>
            </div>
        </Card>
    );
};
