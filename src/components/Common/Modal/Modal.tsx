import { FC, ReactNode, useEffect, useRef } from "react";
import { Modal as DaisyUIModal, Button, Divider } from "react-daisyui";

type ModalExtensions = {
    Header: typeof Header;
    Body: typeof Body;
    Actions: typeof Actions;
};

type ModalProps = {
    children: ReactNode;
    backdrop?: boolean;
    closeButton?: boolean;
    onClose?: () => void;
    show: boolean;
    className?: string;
    responsive?: boolean;
};
export const Modal: FC<ModalProps> & ModalExtensions = ({
    children,
    closeButton = true,
    backdrop = true,
    onClose = () => {},
    show = false,
    className = "",
    responsive = false
}: ModalProps) => {
    const refModal = useRef<HTMLDialogElement>(null);

    useEffect(() => {
        refModal.current?.removeEventListener("close", () => onClose());
        refModal.current?.addEventListener("close", () => onClose());
    }, [onClose, refModal]);

    useEffect(() => {
        if (show) {
            refModal.current?.showModal();
        } else {
            refModal.current?.close();
        }
    }, [show]);

    const closeButtonContent = (
        <form method={"dialog"}>
            <Button size="sm" color="ghost" shape="circle" className="absolute right-2 top-2">
                x
            </Button>
        </form>
    );

    return (
        <DaisyUIModal backdrop={backdrop} ref={refModal} className={className} responsive={responsive}>
            {closeButton ? closeButtonContent : <></>}
            {children}
        </DaisyUIModal>
    );
};

const Header = ({ children }: { children: ReactNode }) => {
    return (
        <>
            <DaisyUIModal.Header className={"mb-0"}>
                {children}
                <Divider />
            </DaisyUIModal.Header>
        </>
    );
};
Modal.Header = Header;

const Body = ({ children }: { children: ReactNode }) => {
    return <DaisyUIModal.Body>{children}</DaisyUIModal.Body>;
};
Modal.Body = Body;

const Actions = ({ children }: { children: ReactNode }) => {
    return <DaisyUIModal.Actions>{children}</DaisyUIModal.Actions>;
};
Modal.Actions = Actions;
export default Modal;
