import { HTMLAttributes } from "react";

type ThemeProps = {
    theme:
        | "light"
        | "dark"
        | "cupcake"
        | "bumblebee"
        | "emerald"
        | "corporate"
        | "retro"
        | "synthwave"
        | "cyberpank"
        | "valentine";
} & HTMLAttributes<HTMLDivElement>;

export const Theme = ({ children, theme, ...args }: ThemeProps) => {
    return (
        <div data-theme={theme} {...args}>
            {children}
        </div>
    );
};
