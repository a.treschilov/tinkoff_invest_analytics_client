import { ChangeEvent, useContext, useEffect, useRef, useState } from "react";
import { Trans } from "react-i18next";
import { ActionButtonText, ActionButtonTextProps } from "../../components/Common/ActionButtonText";
import { PageState } from "../../components/Common/Types";
import { ApiService } from "../../services/ApiService";
import { Modal } from "../../components/Common/Modal/Modal";
import { Alert } from "../../components/Common/Alert/Alert";
import Select from "../../components/Common/Form/Select";
import { FileInput } from "../../components/Common/Form/FileInput";
import { PageLoader } from "../../components/Loader/Loaders";
import Link from "next/link";
import { BrokerType } from "../Credential/CredentialFormBody";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Button } from "../../components/Common/Button/Button";

interface ICrowdfundingImportProps {
    isShow: boolean;
    hideModal: () => void;
    changeDataHandler: () => void;
}

export const CrowdfundingImport = ({ isShow, hideModal }: ICrowdfundingImportProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/crowdlanding");

    const [buttonHint, setButtonHint] = useState<ActionButtonTextProps>({
        status: "",
        children: <Trans>Import.EmptyText</Trans>
    });
    const [pageState, setPageState] = useState(PageState.EDIT_FORM);
    const [isUploadedDataAccepted, setIsUploadedDataAccepted] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [currentPlatform, setCurrentPlatform] = useState("jetlend");
    const [brokerList, setBrokerList] = useState<BrokerType[]>([]);
    const form = useRef<HTMLFormElement>(null);

    useEffect(() => {
        ApiService.fetch("/v1/broker/list").then((reposne: BrokerType[]) => {
            const broker = reposne.filter(el => {
                return ["jetlend", "potok", "money_friends"].indexOf(el.id) !== -1;
            });
            setBrokerList(broker);
        });
    }, []);

    const handleFormSubmit = (event: ChangeEvent<HTMLFormElement>) => {
        setPageState(PageState.SAVING_DATA);
        setButtonHint({
            children: <PageLoader />,
            status: "processing"
        });
        setIsUploadedDataAccepted(false);
        setShowAlert(false);

        const data = form.current !== null ? new FormData(form.current) : new FormData();
        const headers = {
            "Content-Type": "multipart/form-data"
        };
        ApiService.post("/v1/operations/import/files", data, headers)
            .then(() => {
                setPageState(PageState.EDIT_FORM);
                setButtonHint({
                    status: "success",
                    children: <Trans ns={"features/crowdlanding"}>Import.EmptyText</Trans>
                });
                setIsUploadedDataAccepted(true);
            })
            .catch(error => {
                if (error.response.data.error_code === 1050) {
                    setPageState(PageState.EDIT_FORM);
                    setShowAlert(true);
                    setButtonHint({
                        status: "success",
                        children: <Trans ns={"features/crowdlanding"}>Import.EmptyText</Trans>
                    });
                } else {
                    setPageState(PageState.ERROR);
                    setButtonHint({
                        status: "error",
                        children: <Trans ns={"features/crowdlanding"}>Import.ErrorText</Trans>
                    });
                    setTimeout(() => {
                        setPageState(PageState.EDIT_FORM);
                        setButtonHint({
                            status: "",
                            children: <Trans ns={"features/crowdlanding"}>Import.EmptyText</Trans>
                        });
                    }, 200000);
                }
            });

        event.preventDefault();
    };

    const handleHideModal = () => {
        setPageState(PageState.EDIT_FORM);
        setIsUploadedDataAccepted(false);
        hideModal();
    };

    return (
        <Modal show={isShow} onClose={handleHideModal} aria-labelledby="contained-modal-title-vcenter">
            <form onSubmit={handleFormSubmit} ref={form}>
                <Modal.Header>{t("Import.ModalHeader")}</Modal.Header>
                <Modal.Body>
                    <Alert show={showAlert} status="warning" className={"mb-6"}>
                        {t("Import.IncorrectFileFormatError")}
                    </Alert>
                    <Alert show={isUploadedDataAccepted} status={"info"} className={"mb-6"} dismissible={false}>
                        {t("Import.UploadedDataMessage")}
                    </Alert>
                    <div className={"grid gap-6 mb-6"}>
                        <Select>
                            <Select.Label>{t("Import.FormLabelPlatform")}</Select.Label>
                            <Select.Select
                                name="source"
                                defaultValue={currentPlatform}
                                onChange={event => {
                                    setCurrentPlatform(event.target.value);
                                }}
                            >
                                {brokerList.map(el => {
                                    return (
                                        <option key={"crowdfundingImportBroker-" + el.id} value={el.id}>
                                            {el.name}
                                        </option>
                                    );
                                })}
                            </Select.Select>
                        </Select>
                        <FileInput>
                            <FileInput.Label>{t("Import.FormLabelFile")}</FileInput.Label>
                            <FileInput.File accept={".xlsx, .xls"} name="operations[]" multiple={true} />
                            <FileInput.Description className={"h-4"}>
                                {brokerList.map(val => {
                                    if (val.id === currentPlatform && val.instructionLink !== null) {
                                        return (
                                            <Link
                                                key={"crowdfundingImportBrokerInstruction-" + val.id}
                                                href={process.env.NEXT_PUBLIC_LANDING_PAGE_URL + val.instructionLink}
                                                target={"_blank"}
                                                className={"text-primary"}
                                            >
                                                {t("Import.FormDescriptionFile")}
                                            </Link>
                                        );
                                    }
                                })}
                            </FileInput.Description>
                        </FileInput>
                    </div>
                </Modal.Body>
                <Modal.Actions>
                    <ActionButtonText status={buttonHint.status}>{buttonHint.children}</ActionButtonText>
                    <Button
                        type="submit"
                        color={(() => {
                            switch (pageState) {
                                case PageState.ERROR:
                                    return "warning";
                                case PageState.EDIT_FORM:
                                    return "primary";
                                case PageState.LOADING:
                                    return "secondary";
                            }
                        })()}
                    >
                        {t("Import.ButtonText")}
                    </Button>
                </Modal.Actions>
            </form>
        </Modal>
    );
};
