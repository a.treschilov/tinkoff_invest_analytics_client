import { PageState } from "../../../components/Common/Types";
import { useEffect, useState } from "react";
import { ApiService } from "../../../services/ApiService";
import { useErrorBoundary } from "react-error-boundary";
import { Item } from "../../../components/Item/Item";
import { PageLoader } from "../../../components/Loader/Loaders";
import { Modal } from "../../../components/Common/Modal/Modal";
import { Trans } from "react-i18next";
import { Button } from "../../../components/Common/Button/Button";

type ItemDeleteProps = {
    itemId: number;
    item?: Item;
    show?: boolean;
    onSuccess: () => void;
    onDiscard: () => void;
};

export const ItemDelete = (props: ItemDeleteProps) => {
    const { showBoundary } = useErrorBoundary();
    const [item, setItem] = useState(props.item);
    const [pageState, setPageState] = useState(PageState.LOADING);
    const { itemId, show, onSuccess, onDiscard } = props;

    useEffect(() => {
        if (show) {
            if (props.item === undefined) {
                setPageState(PageState.LOADING);
                ApiService.fetch("/v1/item/info/" + itemId)
                    .then((response: Item) => {
                        setItem(response);
                        setPageState(PageState.SUCCESS);
                    })
                    .catch(error => {
                        showBoundary(error);
                    });
            } else {
                setItem(props.item);
                setPageState(PageState.SUCCESS);
            }
        }
    }, [props.itemId, props.item, show, itemId, showBoundary]);

    const handleDelete = () => {
        setPageState(PageState.LOADING);
        ApiService.delete("/v1/item/info/" + itemId).then(() => {
            onSuccess();
        });
    };

    const getLabels = () => {
        switch (item?.type) {
            case "deposit":
                return {
                    header: "Item.Delete.DepositWarningHeader",
                    content: "Item.Delete.DepositWarningText"
                };
            case "loan":
                return {
                    header: "Item.Delete.LoanWarningHeader",
                    content: "Item.Delete.LoanWarningText"
                };
            case "real_estate":
                return {
                    header: "Item.Delete.RealEstateWarningHeader",
                    content: "Item.Delete.RealEstateWarningText"
                };
            default:
                return {
                    header: "DepositManage.DeleteWarningHeader",
                    content: "DepositManage.DeleteWarningText"
                };
        }
    };

    if (!show) {
        return <></>;
    }

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = <PageLoader height={"196px"} />;
            break;
        case PageState.SUCCESS:
            content = (
                <>
                    <Modal.Header>
                        <Trans>{getLabels().header}</Trans> {item?.name}
                    </Modal.Header>
                    <Modal.Body>
                        <Trans>{getLabels().content}</Trans>
                    </Modal.Body>
                    <Modal.Actions>
                        <Button
                            color="neutral"
                            onClick={() => {
                                onDiscard();
                            }}
                        >
                            <Trans>Item.Delete.WarningCancelButton</Trans>
                        </Button>
                        <Button
                            color="warning"
                            onClick={() => {
                                handleDelete();
                            }}
                        >
                            <Trans>Item.Delete.WarningConfirmButton</Trans>
                        </Button>
                    </Modal.Actions>
                </>
            );
            break;
    }

    return (
        <Modal
            show={show}
            onClose={() => {
                onDiscard();
            }}
        >
            {content}
        </Modal>
    );
};
