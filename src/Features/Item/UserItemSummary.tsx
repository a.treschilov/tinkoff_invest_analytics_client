"use client";

import { useContext, useEffect, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { IPrice } from "../../interfaces/IPrice";
import { PageState } from "../../components/Common/Types";
import {
    UserItemSummaryRowColoredPrice,
    UserItemSummaryRowColoredPriceChange,
    UserItemSummaryRowPrice,
    UserItemSummaryRowPriceChange
} from "./UserItemSummaryRow";
import { DefaultSummary } from "./UserItemStaticData";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Divider } from "../../components/Common/Divider/Divider";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Card } from "../../components/Common/Card/Card";

interface IUserItemSummaryProps {
    itemId: number;
    dataVersion?: number;
}

export interface IUserItemSummary {
    quantity: number;
    amount: IPrice;
    investments: IPrice;
    income: {
        amount: IPrice;
    };
    tax: {
        amount: IPrice;
        percent: number;
    };
    fee: {
        amount: IPrice;
        percent: number;
    };
    profit: {
        amount: IPrice;
        percent: number;
    };
    valueGrowth: {
        amount: IPrice;
        percent: number;
    };
    incomeForecast: IPrice;
}

export const UserItemSummary = ({ itemId, dataVersion = 0 }: IUserItemSummaryProps) => {
    const [pageSate, setPageState] = useState(PageState.LOADING);
    const [summary, setSummary] = useState<IUserItemSummary>(DefaultSummary);
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userItem");

    useEffect(() => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/item/details/" + itemId + "/summary")
            .then(response => {
                setSummary({
                    quantity: response.quantity,
                    amount: response.amount,
                    investments: response.investments,
                    income: response.income,
                    tax: response.tax,
                    fee: response.fee,
                    profit: response.profit,
                    valueGrowth: response.valueGrowth,
                    incomeForecast: response.incomeForecast
                });
                setPageState(PageState.SUCCESS);
            })
            .catch(() => {
                setPageState(PageState.ERROR);
            });
    }, [dataVersion, itemId]);

    const renderSummary = () => {
        const generalContent =
            pageSate === PageState.LOADING ? (
                <div className={"grid gap-2"}>
                    <Skeleton className={"h-6 w-full"} />
                    <Skeleton className={"h-6 w-full"} />
                    <Skeleton className={"h-6 w-full"} />
                </div>
            ) : (
                <div className={"grid gap-2"}>
                    <div className={"grid gap-4 grid-cols-12"}>
                        <div className="col-span-5">{t("UserItemSummary.QuantityRowTitle")}</div>
                        <div className="col-span-7 text-end">
                            {summary.quantity} {t("UserItemSummary.QuantityMeasure")}
                        </div>
                    </div>
                    <UserItemSummaryRowPrice
                        label={"UserItemSummary.AmountRowTitle"}
                        amount={summary.amount.amount}
                        currency={summary.amount.currency}
                    />
                    <UserItemSummaryRowPrice
                        label={"UserItemSummary.InvestedRowTitle"}
                        amount={summary.investments.amount}
                        currency={summary.investments.currency}
                    />
                </div>
            );

        const profitContent =
            pageSate === PageState.LOADING ? (
                <div className={"grid gap-2"}>
                    <Skeleton className={"h-6 w-full"} />
                    <Skeleton className={"h-6 w-full"} />
                    <Skeleton className={"h-6 w-full"} />
                </div>
            ) : (
                <div className={"grid gap-2"}>
                    <UserItemSummaryRowColoredPriceChange
                        label={"UserItemSummary.IncomeRowTitle"}
                        amount={summary.profit.amount.amount}
                        currency={summary.profit.amount.currency}
                        difference={summary.profit.percent}
                        helpMessageLabel={"UserItemSummary.ProfitHelpMessage"}
                    />
                    <UserItemSummaryRowColoredPriceChange
                        label={"UserItemSummary.ValueGrowthRowTitle"}
                        amount={summary.valueGrowth.amount.amount}
                        currency={summary.valueGrowth.amount.currency}
                        difference={summary.valueGrowth.percent}
                    />
                    <UserItemSummaryRowPriceChange
                        label={"UserItemSummary.FeeRowTitle"}
                        amount={summary.fee.amount.amount}
                        currency={summary.fee.amount.currency}
                        difference={summary.fee.percent}
                    />
                </div>
            );

        const dividendContent =
            pageSate === PageState.LOADING ? (
                <div className={"grid gap-2"}>
                    <Skeleton className={"h-6 w-full"} />
                    <Skeleton className={"h-6 w-full"} />
                    <Skeleton className={"h-6 w-full"} />
                </div>
            ) : (
                <div className={"grid gap-2"}>
                    <UserItemSummaryRowColoredPrice
                        label={"UserItemSummary.DividendRowTitle"}
                        amount={summary.income.amount.amount}
                        currency={summary.income.amount.currency}
                    />
                    <UserItemSummaryRowColoredPrice
                        label={"UserItemSummary.ForecastRowTitle"}
                        amount={summary.incomeForecast.amount}
                        currency={summary.incomeForecast.currency}
                    />
                    <UserItemSummaryRowPriceChange
                        label={"UserItemSummary.TaxRowTitle"}
                        amount={summary.tax.amount.amount}
                        currency={summary.tax.amount.currency}
                        difference={summary.tax.percent}
                    />
                </div>
            );

        return (
            <div className={"grid grid-cols-12 gap-4 " + (pageSate === PageState.ERROR ? "hidden" : "")}>
                <div className="col-span-12 md:col-span-6 xl:col-span-4">
                    <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                        <Card.Title>{t("UserItemSummary.GeneralTitle")}</Card.Title>
                        <Divider />
                        {generalContent}
                    </Card>
                </div>
                <div className="col-span-12 md:col-span-6 xl:col-span-4">
                    <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                        <Card.Title>{t("UserItemSummary.ProfitTitle")}</Card.Title>
                        <Divider />
                        {profitContent}
                    </Card>
                </div>
                <div className="col-span-12 md:col-span-6 xl:col-span-4">
                    <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                        <Card.Title>{t("UserItemSummary.DividendTitle")}</Card.Title>
                        <Divider />
                        {dividendContent}
                    </Card>
                </div>
            </div>
        );
    };

    return renderSummary();
};
