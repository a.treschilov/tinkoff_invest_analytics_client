import { IAggregateOperation } from "./AnalyticsAccumulationBar";

export const AnalyticsEarningDemoData: IAggregateOperation[] = [
    {
        date: "2022-05-01",
        amount: 13192
    },
    {
        date: "2022-06-01",
        amount: 14522
    },
    {
        date: "2022-07-01",
        amount: 16543
    },
    {
        date: "2022-08-01",
        amount: 17200
    },
    {
        date: "2022-09-01",
        amount: 19600
    },
    {
        date: "2022-10-01",
        amount: 20340
    },
    {
        date: "2022-11-01",
        amount: 22640
    },
    {
        date: "2022-12-01",
        amount: 23790
    },
    {
        date: "2023-01-01",
        amount: 25810
    },
    {
        date: "2023-02-01",
        amount: 26930
    },
    {
        date: "2023-03-01",
        amount: 27430
    },
    {
        date: "2023-04-01",
        amount: 29790
    }
];
