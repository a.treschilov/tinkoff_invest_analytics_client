import { useEffect, useState } from "react";
import { Progress } from "react-daisyui";

type UserStrategySettingsProgressBarProps = {
    progress: number;
};
export const UserStrategySettingsProgressBar = ({ progress }: UserStrategySettingsProgressBarProps) => {
    const [variant, setVariant] = useState<
        "neutral" | "primary" | "secondary" | "accent" | "ghost" | "info" | "success" | "warning" | "error" | undefined
    >("success");

    useEffect(() => {
        if (progress < 100) {
            setVariant("info");
        } else if (progress > 100) {
            setVariant("warning");
        } else if (progress === 100) {
            setVariant("success");
        }
    }, [progress]);

    return <Progress value={progress} max={100} color={variant} />;
};
