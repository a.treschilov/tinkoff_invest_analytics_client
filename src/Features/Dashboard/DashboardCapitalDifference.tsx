import { Price } from "../../components/Common/Types";
import { MoneyDynamic } from "../../components/Common/MoneyDynamic";
import { ReactNode, useCallback, useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Select } from "../../components/Common/Select/Select";

export type DashboardCapitalDifferenceProps = {
    capital: CapitalChangeType;
    isLoaded: boolean;
};

export type CapitalChangeType = {
    day: Price | null;
    month: Price | null;
    year: Price | null;
};

type PeriodItem = "day" | "month" | "year";

type PeriodType = {
    label: PeriodItem | ReactNode;
    value: PeriodItem;
};

const defaultPeriod = "day";

export const DashboardCapitalDifference = ({ capital, isLoaded }: DashboardCapitalDifferenceProps) => {
    const [selectedPeriod, setSelectedPeriod] = useState(defaultPeriod);
    const [periodList, setPeriodList] = useState<PeriodType[]>([]);

    const getDefaultPeriod = useCallback((): string | null => {
        if (capital.day !== null) {
            return "day";
        }

        if (capital.month !== null) {
            return "month";
        }

        if (capital.year !== null) {
            return "year";
        }

        return null;
    }, [capital.day, capital.month, capital.year]);

    useEffect(() => {
        const period: PeriodType[] = [];
        if (capital.day !== null) {
            period.push({
                label: <Trans>Dashboard.DashboardRevenuePeriodToday</Trans>,
                value: "day"
            });
        }
        if (capital.month !== null) {
            period.push({
                label: <Trans>Dashboard.DashboardRevenuePeriodMonth</Trans>,
                value: "month"
            });
        }
        if (capital.year !== null) {
            period.push({
                label: <Trans>Dashboard.DashboardRevenuePeriodYear</Trans>,
                value: "year"
            });
        }

        setSelectedPeriod(getDefaultPeriod() || defaultPeriod);
        setPeriodList(period);
    }, [capital, getDefaultPeriod]);

    const renderPeriods = () => {
        return (
            <>
                <Select
                    className={"small inline-block"}
                    defaultValue={selectedPeriod}
                    onChange={event => {
                        setSelectedPeriod(event.target.value || defaultPeriod);
                    }}
                >
                    {periodList.map(el => {
                        return (
                            <option key={"dashboard-period-option-" + el.value} value={el.value}>
                                {el.label}
                            </option>
                        );
                    })}
                </Select>
            </>
        );
    };

    if (!isLoaded) {
        return <Skeleton className={"w-[300px] h-[24px]"} />;
    }

    if (capital.day === null && capital.month === null && capital.year === null) {
        return <></>;
    }

    return (
        <>
            <span>
                <Trans>Dashboard.DashboardRevenueTitle</Trans>&nbsp;
            </span>
            <br className={"sm:hidden"} />
            <span>
                <MoneyDynamic show={selectedPeriod === "day"} amount={capital.day} />
                <MoneyDynamic show={selectedPeriod === "month"} amount={capital.month} />
                <MoneyDynamic show={selectedPeriod === "year"} amount={capital.year} />
            </span>
            &nbsp;&nbsp;{renderPeriods()}
        </>
    );
};
