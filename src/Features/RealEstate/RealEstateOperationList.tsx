import { useEffect, useState } from "react";
import { ItemType, PageState } from "../../components/Common/Types";
import { Trans } from "react-i18next";
import { PageLoader } from "../../components/Loader/Loaders";
import { RealEstateOperationListTable } from "./RealEstateOperationListTable";
import { ApiService } from "../../services/ApiService";
import { IOperationView } from "../../../app/[lng]/user/operations/list/page";
import { Modal } from "../../components/Common/Modal/Modal";
import { Alert } from "../../components/Common/Alert/Alert";
import { Button } from "../../components/Common/Button/Button";

interface IRealEstateOperationListProps {
    itemId: number;
    handleOperationPage: (operationId: number) => void;
    infoTextProp: string | null;
    itemType: ItemType;
}

export const RealEstateOperationList = ({
    itemId,
    handleOperationPage,
    infoTextProp,
    itemType
}: IRealEstateOperationListProps) => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [operationsData, setOperationsData] = useState<IOperationView[]>([]);
    const [infoText, setInfoText] = useState<string | null>(infoTextProp);

    useEffect(() => {
        setPageState(PageState.LOADING);
        setOperationsData([]);
        ApiService.fetch("/v1/operations/item/" + itemId).then((response: IOperationView[]) => {
            setPageState(PageState.SUCCESS);
            setOperationsData(
                response.map(el => {
                    return {
                        id: el.id,
                        currency: el.currency,
                        date: {
                            date: el.date.date,
                            timezone: el.date.timezone
                        },
                        itemId: el.itemId,
                        itemName: el.itemName,
                        itemLogo: el.itemLogo,
                        itemType: null,
                        itemExternalId: el.itemExternalId,
                        broker: el.broker,
                        amount: el.amount,
                        price: el.price,
                        quantity: el.quantity,
                        status: el.status,
                        type: el.type
                    };
                })
            );
        });
    }, [itemId]);

    const clearInfoText = () => {
        setInfoText(null);
    };

    const handleOperationEdit = (operationId = 0) => {
        handleOperationPage(operationId);
    };

    const renderInfoMessage = () => {
        if (infoText === null) {
            return;
        }

        let text;
        switch (infoText) {
            case "operationAdded":
                text = <Trans>RealEstateOperationsList.InfoAdded</Trans>;
                break;
            case "operationEdited":
                text = <Trans>RealEstateOperationsList.InfoEdited</Trans>;
                break;
        }

        return (
            <Alert onClose={clearInfoText} dismissible={true} status={"success"} className={"mb-6"}>
                {text}
            </Alert>
        );
    };

    const renderModal = () => {
        let content;

        if (operationsData.length > 0) {
            content = renderOperationListTable();
        } else {
            content = (
                <div>
                    {renderOperationsManageDate()}
                    <Trans>RealEstateOperationsList.EmptyOperationList</Trans>
                </div>
            );
        }

        return (
            <div>
                <Modal.Header>
                    <Trans>RealEstateOperationsList.OperationListTitle</Trans>
                </Modal.Header>
                {content}
            </div>
        );
    };

    const renderOperationListTable = () => {
        return (
            <div>
                {renderOperationsManageDate()}
                {renderInfoMessage()}
                <RealEstateOperationListTable
                    itemType={itemType}
                    operations={operationsData}
                    handleEditOperation={handleOperationEdit}
                />
            </div>
        );
    };

    const renderOperationsManageDate = () => {
        return (
            <div className="text-end mb-6">
                <Button size="sm" color="primary" onClick={() => handleOperationEdit(0)}>
                    <Trans>RealEstateOperationsList.AddButton</Trans>
                </Button>
            </div>
        );
    };

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = (
                <div className="mt-4 mb-4">
                    <PageLoader />
                </div>
            );
            break;
        default:
            content = renderModal();
            break;
    }
    return <div>{content}</div>;
};
