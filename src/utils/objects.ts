function isObject(item: unknown) {
    return item && typeof item === "object" && !Array.isArray(item);
}

export function mergeDeep(target: unknown, source: unknown) {
    const output = Object.assign({}, target);
    if (isObject(target) && isObject(source)) {
        Object.keys(source as object).forEach(key => {
            // @ts-expect-error: Because custom implementation
            if (isObject(source[key])) {
                // @ts-expect-error: Because custom implementation
                if (!(key in (target as object))) Object.assign(output, { [key]: source[key] });
                else {
                    // @ts-expect-error: Because custom implementation
                    output[key] = mergeDeep(target[key], source[key]);
                }
            } else {
                // @ts-expect-error: Because custom implementation
                Object.assign(output, { [key]: source[key] });
            }
        });
    }
    return output;
}
