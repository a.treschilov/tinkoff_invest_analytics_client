export const UserStrategyPredefinedCurrencyBalance = [
    {
        id: "russian",
        name: "UserStrategySettings.Strategies.CurrencyBalanceRussianInvestorName",
        description: "UserStrategySettings.Strategies.CurrencyBalanceRussianInvestorDescription",
        strategy: [
            {
                id: "RUB",
                value: 0.5
            },
            {
                id: "USD",
                value: 0
            },
            {
                id: "CNY",
                value: 0.25
            },
            {
                id: "HKD",
                value: 0.25
            }
        ]
    }
];
