import { RealEstate, RealEstatePrice } from "../../../components/Item/Item";
import React, { useCallback, useEffect, useState } from "react";
import { IPrice } from "../../../interfaces/IPrice";
import { Trans } from "react-i18next";
import { InputText } from "../../../components/Common/Form/InputText";

type ItemCloseRealEstateProps = {
    realEstate?: RealEstate;
    show: boolean;
    onChange: (price: IPrice) => void;
};
export const ItemCloseRealEstate = ({ realEstate, show, onChange }: ItemCloseRealEstateProps) => {
    const [price, setPrice] = useState<IPrice | null>(null);

    const getLastPrice = useCallback((): RealEstatePrice | null => {
        const realEstatePriceList = realEstate?.prices;
        if (realEstatePriceList === undefined || realEstatePriceList.length === 0) {
            return null;
        }

        realEstatePriceList.sort((el1: RealEstatePrice, el2: RealEstatePrice) => {
            return el1.date.date < el2.date.date ? 1 : -1;
        });

        return realEstatePriceList[0];
    }, [realEstate?.prices]);

    useEffect(() => {
        const lastPrice = getLastPrice();
        setPrice({
            amount: lastPrice?.amount || 0,
            currency: lastPrice?.currency || "RUB"
        });
        onChange({
            amount: lastPrice?.amount || 0,
            currency: lastPrice?.currency || "RUB"
        });
    }, [getLastPrice, onChange, show]);

    const handleChangePriceData = (event: React.ChangeEvent<HTMLInputElement>) => {
        const priceData: IPrice = {
            amount: parseInt(event.target.value),
            currency: "RUB"
        };

        setPrice(priceData);
        onChange(priceData);
    };

    if (!show) {
        return <></>;
    }

    return (
        <InputText
            label={<Trans>Item.Close.RealEstateWarningFormLabelPrice</Trans>}
            type="number"
            name="amount"
            step="100"
            onChange={handleChangePriceData}
            value={price?.amount || 0}
        />
    );
};
