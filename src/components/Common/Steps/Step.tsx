import { FC, HTMLAttributes, ReactNode } from "react";
import { Color } from "../Types";
import { daisyUIColorClass } from "../utils";
import { StepIcon } from "./StepIcon";

type StepProps = {
    children?: ReactNode;
    color?: Color;
} & HTMLAttributes<HTMLLIElement>;

type StepExtension = {
    Icon: typeof StepIcon;
};

export const Step: FC<StepProps> & StepExtension = ({ children, color, className, ...args }: StepProps) => {
    const classes = ["step"];
    if (className) {
        classes.push(...className.split(" "));
    }
    classes.push(daisyUIColorClass("step", color));

    return (
        <li className={classes.join(" ")} {...args}>
            {children}
        </li>
    );
};

Step.Icon = StepIcon;
