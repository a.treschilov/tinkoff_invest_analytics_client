import { Skeleton } from "../../components/Common/Skeleton/Skeleton";

type UserItemEventsContentLoadingProps = {
    show: boolean;
};

const LOADING_ROW_COUNT = 6;

export const UserItemEventsContentLoading = ({ show }: UserItemEventsContentLoadingProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <tbody>
            {[...Array(LOADING_ROW_COUNT)].map((value: number, key: number) => {
                return (
                    <tr key={"userItemEventsContentLoading_" + key}>
                        <td>
                            <Skeleton className={"h-8 w-full"} />
                        </td>
                        <td>
                            <Skeleton className={"h-8 w-full"} />
                        </td>
                        <td>
                            <Skeleton className={"h-8 w-full"} />
                        </td>
                    </tr>
                );
            })}
        </tbody>
    );
};
