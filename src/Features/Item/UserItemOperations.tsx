"use client";

import { useContext, useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { ApiService } from "../../services/ApiService";
import { toUserTimezone } from "../../utils/date";
import { BsFillTrashFill, BsTable } from "react-icons/bs";
import { OperationTypeTranslation } from "../OperationList/OperationTypeTranslation";
import { IOperationView } from "../../../app/[lng]/user/operations/list/page";
import { makeLink } from "../../utils/url";
import { MdEdit } from "react-icons/md";
import { useRouter } from "next/navigation";
import { ThemeContext } from "../../providers/ThemeProvider";
import { OperationDelete } from "../Operation/OperationDelete";
import { useTranslation } from "../../../app/i18n/client";
import { Divider } from "../../components/Common/Divider/Divider";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";
import { Table } from "../../components/Common/Table/Table";

interface IUserItemOperationsProps {
    itemId: number;
    onChange?: () => void;
}

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
    year: "numeric",
    month: "numeric",
    day: "numeric"
});

export const UserItemOperations = ({ itemId, onChange = () => {} }: IUserItemOperationsProps) => {
    const [pageSate, setPageState] = useState(PageState.LOADING);
    const [dataTime, setDataTime] = useState(new Date());
    const [operations, setOperations] = useState<IOperationView[]>([]);
    const [currentOperationId, setCurrentOperationId] = useState(0);
    const [isDelete, setIsDelete] = useState(false);
    const router = useRouter();
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userItem");

    useEffect(() => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/item/details/" + itemId + "/operations").then(response => {
            setOperations(response.operations);
            setPageState(PageState.SUCCESS);
        });
    }, [dataTime, itemId]);

    const renderLoading = () => {
        return (
            <div className={"grid gap-3"}>
                <Skeleton className={"h-8 w-full"} />
                <Skeleton className={"h-8 w-full"} />
                <Skeleton className={"h-8 w-full"} />
                <Skeleton className={"h-8 w-full"} />
                <Skeleton className={"h-8 w-full"} />
                <Skeleton className={"h-8 w-full"} />
            </div>
        );
    };

    const renderOperationTable = (operations: IOperationView[]) => {
        return (
            <div className="overflow-x-auto max-h-96">
                <Table size={"xs"} pinRows={true} className="sm:table-md w-full">
                    <thead>
                        <tr>
                            <th>{t("Operations.TableHeaderOperationType")}</th>
                            <th className="hidden lg:table-cell">{t("Operations.TableHeaderCostShare")}</th>
                            <th className="hidden lg:table-cell">{t("Operations.TableHeaderCountLots")}</th>
                            <th className={"hidden sm:table-cell"}>{t("Operations.TableHeaderAmount")}</th>
                            <th className={"hidden sm:table-cell"}>{t("Operations.TableHeaderDate")}</th>
                            <th className={"sm:hidden"}>{t("Operations.TableHeaderData")}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>{operations.map(operation => renderOperationRow(operation))}</tbody>
                </Table>
            </div>
        );
    };

    const renderOperationRow = (operation: IOperationView) => {
        const currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
            style: "currency",
            currency: operation.currency
        });

        return (
            <tr key={"userItemOperation_" + operation.id} className={"hover"}>
                <td>
                    <OperationTypeTranslation operationType={operation.type} itemType={operation.itemType} />
                </td>
                <td className="hidden lg:table-cell">
                    {operation.quantity === null
                        ? null
                        : currencyFormatter.format(operation.amount / operation.quantity)}
                </td>
                <td className="hidden lg:table-cell">{operation.quantity}</td>
                <td className={"hidden sm:table-cell"}>{currencyFormatter.format(operation.amount)}</td>
                <td className={"hidden sm:table-cell"}>{dateFormatter.format(toUserTimezone(operation.date))}</td>
                <td className={"sm:hidden"}>
                    {currencyFormatter.format(operation.amount)}
                    <br />
                    <small className={"text-primary"}>{dateFormatter.format(toUserTimezone(operation.date))}</small>
                </td>
                <td className={"text-right"}>
                    <Button
                        className={"btn btn-xs sm:btn-sm"}
                        color={"neutral"}
                        shape={"square"}
                        title={"Добавить операцию"}
                        onClick={() => {
                            router.push(makeLink("/user/operations/edit/" + operation.id, lng));
                        }}
                    >
                        <MdEdit />
                    </Button>
                    &nbsp;&nbsp;
                    <Button
                        className={"btn btn-xs sm:btn-sm"}
                        color={"error"}
                        shape={"square"}
                        title={"Удалить операцию"}
                        onClick={() => {
                            setCurrentOperationId(operation.id);
                            setIsDelete(true);
                        }}
                    >
                        <BsFillTrashFill />
                    </Button>
                </td>
            </tr>
        );
    };

    const renderEmptyOperationList = () => {
        return <div className="text-center">{t("Operations.EmptyList")}</div>;
    };

    let content;
    switch (pageSate) {
        case PageState.LOADING:
            content = renderLoading();
            break;
        case PageState.SUCCESS:
            content = operations.length ? renderOperationTable(operations) : renderEmptyOperationList();
            break;
        default:
            content = renderLoading();
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <BsTable size={16} /> {t("Operations.Header")}
            </Card.Title>
            <Divider />
            {content}
            <OperationDelete
                show={isDelete}
                operationId={currentOperationId}
                onDiscard={() => {
                    setIsDelete(false);
                }}
                onConfirm={() => {
                    setIsDelete(false);
                    setDataTime(new Date());
                    if (onChange) {
                        onChange();
                    }
                }}
            />
        </Card>
    );
};
