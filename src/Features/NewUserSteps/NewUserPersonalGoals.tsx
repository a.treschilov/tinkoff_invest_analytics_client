import { Trans } from "react-i18next";
import { formatInputDayDate } from "../../utils/date";
import { InputText } from "../../components/Common/Form/InputText";
import { useEffect, useState } from "react";
import { DefaultUserSettingsData } from "../UserSettings/UserSettingsStaticData";
import { ApiService } from "../../services/ApiService";
import { StepComponentProps } from "./NewUserSteps";
import { Button } from "../../components/Common/Button/Button";

export const NewUserPersonalGoals = ({ onComplete, active }: StepComponentProps) => {
    const [birthday, setBirthday] = useState(DefaultUserSettingsData.birthday);
    const [retirementAge, setRetirementAge] = useState(DefaultUserSettingsData.retirementAge);

    useEffect(() => {
        if (active) {
            ApiService.fetch("/v1/user/settings").then(response => {
                setBirthday(response.birthday);
                setRetirementAge(response.retirementAge);
            });
        }
    }, [active]);

    const onBirthDayChange = (value: string) => {
        {
            setBirthday({
                date: value,
                timezone: birthday?.timezone || "UTC",
                timezone_type: birthday?.timezone_type || 3
            });
        }
    };

    const onFormSubmit = () => {
        const data = {
            birthday: birthday,
            retirementAge: retirementAge
        };
        ApiService.put("/v1/user/settings", data);
        onComplete();
    };

    const onRetirementAgeChange = (value: string) => {
        setRetirementAge(parseFloat(value));
    };

    if (!active) {
        return <></>;
    }

    return (
        <form
            className={"grid gap-6"}
            onSubmit={e => {
                onFormSubmit();
                e.preventDefault();
            }}
        >
            <InputText
                label={<Trans>PortfolioSettings.BirthdayTitle</Trans>}
                type="date"
                name="birthday"
                defaultValue={
                    birthday !== null ? (formatInputDayDate(new Date(birthday.date)) ?? undefined) : undefined
                }
                onChange={e => {
                    onBirthDayChange(e.target.value);
                }}
            />

            <InputText
                label={<Trans>PortfolioSettings.RetirementAge</Trans>}
                type={"number"}
                step="1"
                min="0"
                max={100}
                name="retirementAge"
                value={retirementAge}
                onChange={e => {
                    onRetirementAgeChange(e.target.value);
                }}
            />
            <Button className="w-auto" color={"primary"} type={"submit"}>
                <Trans>NewUserSteps.PersonalGoals.NextButton</Trans>
            </Button>
        </form>
    );
};
