import { Color, Size } from "./Types";

export const daisyUISizeClass = (prefix: string, size?: Size): string => {
    switch (size) {
        case "xs":
            return prefix + "-xs";
        case "sm":
            return prefix + "-sm";
        case "md":
            return prefix + "-md";
        case "lg":
            return prefix + "-lg";
        case "xl":
            return prefix + "-xl";
    }
    return "";
};

export const daisyUIColorClass = (prefix: string, color?: Color): string => {
    switch (color) {
        case "primary":
            return prefix + "-primary";
        case "success":
            return prefix + "-success";
        case "warning":
            return prefix + "-warning";
        case "info":
            return prefix + "-info";
        case "neutral":
            return prefix + "-neutral";
        case "secondary":
            return prefix + "-secondary";
        case "accent":
            return prefix + "-accent";
        case "error":
            return prefix + "-error";
    }

    return "";
};
