import { formatInputDateTimeSeconds, formatInputDayDate } from "../../utils/date";
import { RealEstate } from "../../components/Item/Item";

const formattedDate = formatInputDateTimeSeconds(new Date()) || "";
export const DefaultRealEstateAsset: RealEstate = {
    realEstateId: null,
    itemId: null,
    name: "",
    payoutFrequency: 1,
    payoutFrequencyPeriod: "month",
    isActive: true,
    interestAmount: 0,
    interestCurrency: "RUB",
    currency: "RUB",
    purchaseDate: {
        date: formatInputDayDate(new Date()) || "",
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
    },
    purchaseAmount: 0,
    prices: [
        {
            realEstatePriceId: null,
            amount: 0,
            currency: "RUB",
            date: {
                date: formattedDate,
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
            }
        }
    ],
    saleOperation: null
};
