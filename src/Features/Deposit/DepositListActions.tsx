import { FiMoreHorizontal } from "react-icons/fi";
import { BsFillPencilFill, BsFillTrashFill, BsListUl } from "react-icons/bs";
import { Trans } from "react-i18next";
import { AiOutlineIssuesClose } from "react-icons/ai";
import { IDepositItem } from "./DepositManage";
import { Dropdown } from "../../components/Common/Dropdown/Dropdown";

interface DepositListActionsProps {
    deposit: IDepositItem;
    handleOperationManage: (itemId: number) => void;
    handleEditDeposit: (depositId: number) => void;
    handleCloseDeposit: (depositId: number) => void;
    handleDeleteDeposit: (depositId: number) => void;
}

export const DepositListActions = ({
    deposit,
    handleOperationManage,
    handleEditDeposit,
    handleCloseDeposit,
    handleDeleteDeposit
}: DepositListActionsProps) => {
    return (
        <Dropdown placement={"left"}>
            <Dropdown.Toggle>
                <FiMoreHorizontal></FiMoreHorizontal>
            </Dropdown.Toggle>
            <Dropdown.Menu className={"z-50 w-44"}>
                <Dropdown.Item onClick={() => handleOperationManage(deposit.itemId === null ? 0 : deposit.itemId)}>
                    <BsListUl className="mr-2" />
                    <Trans>DepositManage.ItemOperation</Trans>
                </Dropdown.Item>
                <Dropdown.Item onClick={() => handleEditDeposit(deposit.depositId === null ? 0 : deposit.depositId)}>
                    <BsFillPencilFill className="mr-2" />
                    <Trans>DepositManage.ItemEdit</Trans>
                </Dropdown.Item>
                <Dropdown.Item onClick={() => handleCloseDeposit(deposit.depositId === null ? 0 : deposit.depositId)}>
                    <AiOutlineIssuesClose className="mr-2" />
                    <Trans>DepositManage.ItemClose</Trans>
                </Dropdown.Item>
                <Dropdown.Item
                    onClick={() => {
                        handleDeleteDeposit(deposit.depositId || 0);
                    }}
                >
                    <BsFillTrashFill
                        style={{ cursor: "pointer" }}
                        title="Delete deposit"
                        className="mr-2 text-warning"
                    />
                    <span className="text-warning">
                        <Trans>DepositManage.ItemDelete</Trans>
                    </span>
                </Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
    );
};
