"use client";

import { Link } from "react-daisyui";
import { useContext, useEffect, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";

export const HeaderButton = () => {
    const [user, setUser] = useState<null | number>(null);
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "public/translation");
    useEffect(() => {
        ApiService.fetch("/v1/user/data")
            .then(resp => {
                setUser(resp.user_id);
            })
            .catch(() => {
                setUser(null);
            });
    }, []);

    return (
        <Link className={"btn btn-primary hover:no-underline"} href={"/"}>
            {user === null ? t("Header.NewUserButton") : t("Header.AuthUserButton")}
        </Link>
    );
};
