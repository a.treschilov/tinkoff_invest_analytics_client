import { Price } from "./Types";
import { useEffect, useState } from "react";

type MoneyDynamicProps = {
    show?: boolean;
    amount: Price | null;
};
export const MoneyDynamic = ({ show = true, amount }: MoneyDynamicProps) => {
    const [price] = useState(
        new Intl.NumberFormat("ru-RU", {
            style: "currency",
            currency: amount?.currency || "RUB",
            signDisplay: "exceptZero"
        })
    );
    const [textColorClass, setTextColorClass] = useState("text-success");

    useEffect(() => {
        setTextColorClass(amount?.amount === undefined || amount?.amount >= 0 ? "text-success" : "text-danger");
    }, [amount]);

    if (amount === null) {
        return <></>;
    }

    return <span className={textColorClass + (!show ? " hidden" : "")}>{price.format(amount?.amount)}</span>;
};
