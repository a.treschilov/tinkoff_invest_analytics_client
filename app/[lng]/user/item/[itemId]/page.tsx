"use client";

import { useErrorBoundary } from "react-error-boundary";
import { useContext, useEffect, useState, use } from "react";
import { PageState } from "../../../../../src/components/Common/Types";
import { ApiService } from "../../../../../src/services/ApiService";
import { UserItemBreadCrumbs } from "../../../../../src/Features/Item/UserItemBreadCrumbs";
import { UserItemSummary } from "../../../../../src/Features/Item/UserItemSummary";
import { UserItemOperations } from "../../../../../src/Features/Item/UserItemOperations";
import { PageLoader } from "../../../../../src/components/Loader/Loaders";
import { SessionContext } from "../../../../../src/providers/SessionProvider";
import { UserItemDescriptionManage } from "../../../../../src/Features/Item/UserItemDescription/UserItemDescriptionManage";
import { UserItemEvents } from "../../../../../src/Features/Item/UserItemEvents";
import { Item } from "../../../../../src/components/Item/Item";
import { PriceHistory } from "../../../../../src/Features/Item/PriceHistory/PriceHistory";
import { Card } from "../../../../../src/components/Common/Card/Card";

const ItemPage = (props: { params: Promise<{ itemId: string }> }) => {
    const params = use(props.params);
    const { showBoundary } = useErrorBoundary();

    const [itemId] = useState<number>(parseInt(params.itemId || "0"));
    const [item, setItem] = useState<Item>();
    const [dataVersion, setDataVersion] = useState(0);
    const [pageState, setPageState] = useState<string>(PageState.LOADING);
    const { setPageTitle } = useContext(SessionContext);

    useEffect(() => {
        ApiService.fetch("/v1/item/info/" + itemId)
            .then((response: Item) => {
                setPageTitle(response.name);
                setPageState(PageState.SUCCESS);
                setItem(response);
            })
            .catch(error => {
                showBoundary(error);
            });
    }, [dataVersion, itemId, setPageTitle, showBoundary]);

    const renderContent = () => {
        return (
            <>
                <UserItemBreadCrumbs />
                <UserItemDescriptionManage itemId={itemId} />
                <UserItemSummary itemId={itemId} dataVersion={dataVersion} />
                <UserItemOperations itemId={itemId} onChange={() => setDataVersion(dataVersion + 1)} />
                {item !== undefined && !item.isOutdated ? <UserItemEvents itemId={itemId} /> : <></>}

                {item !== undefined && item.type === "market" ? (
                    <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                        <PriceHistory isin={item.externalId} />
                    </Card>
                ) : (
                    <></>
                )}
            </>
        );
    };

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = <PageLoader />;
            break;
        case PageState.SUCCESS:
            content = renderContent();
            break;
        default:
            content = <PageLoader />;
            break;
    }

    return <>{content}</>;
};

export default ItemPage;
