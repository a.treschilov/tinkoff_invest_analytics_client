import { HTMLAttributes } from "react";
import { Size } from "../Types";
import { daisyUISizeClass } from "../utils";

type LoadingProps = {
    variant: "spinner" | "dots" | "ring-3" | "ball" | "bars" | "infinity";
    size?: Size;
} & HTMLAttributes<HTMLSpanElement>;

export const Loading = ({ variant, size = "md", className, ...args }: LoadingProps) => {
    const classes = ["loading"];
    if (className) {
        classes.push(...className.split(" "));
    }

    switch (variant) {
        case "spinner":
            classes.push("loading-spinner");
            break;
        case "dots":
            classes.push("loading-dots");
            break;
        case "ring-3":
            classes.push("loading-ring");
            break;
        case "ball":
            classes.push("loading-ball");
            break;
        case "bars":
            classes.push("loading-bars");
            break;
        case "infinity":
            classes.push("loading-infinity");
            break;
    }

    classes.push(daisyUISizeClass("loading", size));

    return <span className={classes.join(" ")} {...args} />;
};
