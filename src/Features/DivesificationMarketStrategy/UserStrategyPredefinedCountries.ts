export const UserStrategyPredefinedCountries = [
    {
        id: "gdp",
        name: "UserStrategySettings.Strategies.GrossDomesticProductName",
        description: "UserStrategySettings.Strategies.GrossDomesticProductDescription",
        strategy: [
            {
                id: "AU",
                value: 0.017
            },
            {
                value: 0.0187,
                id: "BR"
            },
            {
                value: 0.0217,
                id: "CA"
            },
            {
                value: 0.008,
                id: "CH"
            },
            {
                value: 0.1804,
                id: "CN"
            },
            {
                value: 0.0397,
                id: "DE"
            },
            {
                value: 0.0137,
                id: "ES"
            },
            {
                value: 0.0274,
                id: "FR"
            },
            {
                value: 0.0315,
                id: "GB"
            },
            {
                value: 0.0342,
                id: "IN"
            },
            {
                value: 0.0194,
                id: "IR"
            },
            {
                value: 0.0197,
                id: "IT"
            },
            {
                value: 0.0423,
                id: "JP"
            },
            {
                value: 0.0171,
                id: "KR"
            },
            {
                value: 0.0098,
                id: "NL"
            },
            {
                value: 0.021,
                id: "RU"
            },
            {
                value: 0.01,
                id: "SA"
            },
            {
                value: 0.0042,
                id: "SG"
            },
            {
                value: 0.2465,
                id: "US"
            },
            {
                value: 0.0041,
                id: "ZA"
            }
        ]
    }
];
