import { Divider, Form } from "react-daisyui";
import { BrokerType } from "./CredentialFormBody";
import { ItemLogoName } from "../../components/Item/ItemLogoName";

type BrokerApiIntegrationProps = {
    show: boolean;
    broker?: BrokerType | null;
};

export const BrokerApiIntegration = ({ show, broker = null }: BrokerApiIntegrationProps) => {
    if (!show || broker === null) {
        return <></>;
    }

    return (
        <Form>
            <Divider />
            <div className={"mb-4"}>
                <ItemLogoName
                    name={broker?.name || ""}
                    type={"market"}
                    logo={process.env.NEXT_PUBLIC_BASE_URL + "/images/broker/" + broker?.logo}
                />
            </div>
            <div>Coming soon....</div>
        </Form>
    );
};
