import { Trans } from "react-i18next";
import Link from "next/link";
import { makeLink } from "../../utils/url";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { Button } from "../../components/Common/Button/Button";

interface ICredentialListManagePanelProps {
    show: boolean;
    handleAddButton: (userCredentialId: number) => void;
}

export const CredentialListManagePanel = ({ show, handleAddButton }: ICredentialListManagePanelProps) => {
    const { lng } = useContext(ThemeContext);

    if (!show) {
        return <></>;
    }

    return (
        <div className="mb-6 text-end grid grid-cols-2 sm:block gap-2">
            <Link
                href={makeLink("/user/market/credential/add", lng)}
                className={"btn btn-primary decoration-transparent sm:mr-2"}
            >
                <Trans>CredentialList.UploadReportButton</Trans>
            </Link>
            <Button type="button" color={"primary"} onClick={() => handleAddButton(0)}>
                <Trans>CredentialList.AddButton</Trans>
            </Button>
        </div>
    );
};
