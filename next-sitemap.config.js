/** @type {import('next-sitemap').IConfig} */

module.exports = {
    siteUrl: process.env.NEXT_PUBLIC_BASE_URL,
    generateRobotsTxt: false,
    exclude: ["/en/user/*", "/ru/user/*"],
    additionalPaths: async () => {
        const result = [];

        const params = {
            method: "get"
        };

        let resp = await fetch(
            process.env.NEXT_PUBLIC_API_HOST + "/v1/market/stock/list?country=&currency=&sector=",
            params
        );
        let items = await resp.json();
        items.stocks.forEach(item => {
            result.push({
                loc: "/ru/public/item/" + item.isin,
                lastmod: new Date().toISOString(),
                changefreq: "weekly",
                priority: 0.7
            });
        });

        resp = await fetch(process.env.NEXT_PUBLIC_API_HOST + "/v1/market/bond/list", params);
        items = await resp.json();
        items.forEach(item => {
            result.push({
                loc: "/ru/public/item/" + item.isin,
                lastmod: new Date().toISOString(),
                changefreq: "weekly",
                priority: 0.7
            });
        });

        resp = await fetch(process.env.NEXT_PUBLIC_API_HOST + "/v1/market/future/list", params);
        items = await resp.json();
        items.forEach(item => {
            result.push({
                loc: "/ru/public/item/" + item.isin,
                lastmod: new Date().toISOString(),
                changefreq: "weekly",
                priority: 0.7
            });
        });

        return result;
    }
};
