"use client";

import { Item } from "../../../components/Item/Item";
import { ItemLogo } from "../../../components/Item/ItemLogo";
import { ItemTypeTranslation } from "../../../components/Item/ItemType";

type UserItemDescriptionContentNameProps = {
    item: Item | null;
};

export const UserDescriptionContentName = ({ item }: UserItemDescriptionContentNameProps) => {
    if (item === null) {
        return <></>;
    }

    return (
        <>
            <div>
                <ItemLogo src={item.logo || null} name={item.name || ""} width={"60px"} height={"60px"} />
            </div>
            <div className={"overflow-x-hidden"}>
                <div className={"text-ellipsis whitespace-nowrap overflow-hidden"}>{item.name}</div>
                <div className={"text-base-content/50"}>
                    <ItemTypeTranslation type={item.type} />
                </div>
            </div>
        </>
    );
};
