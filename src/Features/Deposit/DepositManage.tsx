"use client";

import { useCallback, useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { DepositEdit } from "./DepositEdit";
import { RealEstateOperationManage } from "../RealEstate/RealEstateOperationManage";
import { DepositOperationSuggestion } from "./DepositOperationSuggestion";
import { DepositList } from "./DepositList";
import { DepositAlert } from "./DepositAlert";
import { ApiService } from "../../services/ApiService";
import { PageState } from "../../components/Common/Types";
import { DepositClosedList } from "./DepositClosedList";
import { IDate } from "../../utils/date";
import { DepositListLoading } from "./DepositListLoading";
import { useErrorBoundary, withErrorBoundary } from "react-error-boundary";
import { ErrorComponent } from "../../components/ErrorContent/ErrorComponent";
import { IOperationView } from "../../../app/[lng]/user/operations/list/page";
import { DepositEmptyList } from "./DepositEmptyList";
import { Button } from "../../components/Common/Button/Button";
import { Toast } from "../../components/Common/Toast/Toast";

type DepositManageProps = {
    onChange?: () => void;
};

export type DepositAlertAction =
    | null
    | "depositAdded"
    | "depositEdited"
    | "operationAdded"
    | "depositDeleted"
    | "depositClosed";

export interface IResponseOperations {
    id: number | null;
    broker: string | null;
    itemId: number;
    itemName: string;
    type: string;
    externalId: string | null;
    quantity: number | null;
    date: {
        date: string;
        timezone: string;
    };
    amount: number;
    currency: string;
    status: string;
}

export interface IDepositItem {
    depositId: number | null;
    itemId: number | null;
    name: string;
    isActive: number;
    dealDate: IDate;
    closedDate: IDate | null;
    interestPercent: number;
    duration: number;
    durationPeriod: string;
    payoutFrequency: number;
    payoutFrequencyPeriod: string;
    amount: number;
    currency: string;
}

export interface IDepositPortfolioItem {
    amount: number;
    currency: string;
    income: number;
    inputAmount: number;
    deposit: IDepositItem;
    operations: IOperationView[];
}

export const DepositManage = ({ onChange }: DepositManageProps) => {
    const [isShowEditForm, setIsShowEditForm] = useState(false);
    const [isShowOperations, setIsShowOperations] = useState(false);
    const [isShowMessage, setIsShowMessage] = useState(false);
    const [editedDepositId, setEditedDepositId] = useState(0);
    const [editedItemId, setEditedItemId] = useState(0);
    const [messageText, setMessageText] = useState<DepositAlertAction>(null);
    const [currentTime, setCurrentTime] = useState(Date.now());
    const [depositList, setDepositList] = useState<IDepositPortfolioItem[]>([]);
    const [closedDepositList, setClosedDepositList] = useState<IDepositPortfolioItem[]>([]);
    const [pageState, setPageState] = useState(PageState.LOADING);
    const { showBoundary } = useErrorBoundary();

    const updateDepositList = useCallback(() => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/deposit/portfolio")
            .then(response => {
                const portfolio = response.map((el: IDepositPortfolioItem) => {
                    const depositItem: IDepositPortfolioItem = {
                        amount: el.amount,
                        income: el.income,
                        inputAmount: el.inputAmount,
                        currency: el.currency,
                        deposit: {
                            depositId: el.deposit.depositId,
                            itemId: el.deposit.itemId,
                            name: el.deposit.name,
                            isActive: el.deposit.isActive,
                            dealDate: {
                                date: el.deposit.dealDate.date,
                                timezone: el.deposit.dealDate.timezone
                            },
                            closedDate:
                                el.deposit.closedDate === null
                                    ? null
                                    : {
                                          date: el.deposit.closedDate.date,
                                          timezone: el.deposit.closedDate.timezone
                                      },
                            interestPercent: el.deposit.interestPercent,
                            duration: el.deposit.duration,
                            durationPeriod: el.deposit.durationPeriod,
                            payoutFrequency: el.deposit.payoutFrequency,
                            payoutFrequencyPeriod: el.deposit.payoutFrequencyPeriod,
                            amount: el.deposit.amount,
                            currency: el.deposit.currency
                        },
                        operations: el.operations
                    };
                    return depositItem;
                });

                setDepositList(
                    portfolio.filter((el: IDepositPortfolioItem) => {
                        return el.deposit.isActive === 1;
                    })
                );
                setClosedDepositList(
                    portfolio.filter((el: IDepositPortfolioItem) => {
                        return el.deposit.isActive === 0;
                    })
                );
                setPageState(PageState.SUCCESS);
            })
            .catch(error => {
                showBoundary(error);
            });
    }, [showBoundary]);

    useEffect(() => {
        updateDepositList();
    }, [updateDepositList]);

    const handleChange = () => {
        updateDepositList();
        setCurrentTime(Date.now());
        if (onChange !== undefined) {
            onChange();
        }
    };

    const handleEditDeposit = (depositId: number) => {
        setEditedDepositId(depositId);
        setIsShowEditForm(true);
    };

    const handleOperationManage = (itemId: number) => {
        setEditedItemId(itemId);
        setIsShowOperations(true);
    };

    const handleHideModal = (action: string | null) => {
        setIsShowEditForm(false);
        setIsShowOperations(false);

        if (action === "editComplete") {
            setIsShowMessage(true);
            setMessageText("depositEdited");
            handleChange();
        } else if (action === "addComplete") {
            setIsShowMessage(true);
            setMessageText("depositAdded");
            handleChange();
        }
    };

    const handleAddBatchOperations = () => {
        setIsShowMessage(true);
        setMessageText("operationAdded");
        handleChange();
    };

    const handleDeleteDeposit = () => {
        setIsShowMessage(true);
        setMessageText("depositDeleted");
        handleChange();
    };

    const handleCloseDeposit = () => {
        setIsShowMessage(true);
        setMessageText("depositClosed");
        handleChange();
    };

    const renderManageData = () => {
        return (
            <div className="text-end mb-4">
                <Button color="primary" onClick={() => handleEditDeposit(0)}>
                    <Trans>DepositManage.AddButtonText</Trans>
                </Button>
            </div>
        );
    };

    return (
        <>
            <div>
                {pageState !== PageState.LOADING && depositList.length + closedDepositList.length > 0 ? (
                    renderManageData()
                ) : (
                    <></>
                )}
                <Toast vertical={"top"} horizontal={"end"} className={"z-10" + (!isShowMessage ? " hidden" : "")}>
                    <DepositAlert
                        isShow={isShowMessage}
                        action={messageText}
                        handleOnClose={() => {
                            setIsShowMessage(false);
                        }}
                        closeTimeout={5000}
                    />
                </Toast>
                <DepositOperationSuggestion handleOperationsAdded={handleAddBatchOperations} time={currentTime} />
                <DepositEmptyList
                    show={depositList.length + closedDepositList.length === 0 && pageState === PageState.SUCCESS}
                    onAddDeposit={() => handleEditDeposit(0)}
                />
                <DepositListLoading show={pageState === PageState.LOADING} />
                <DepositList
                    show={pageState !== PageState.LOADING && depositList.length > 0}
                    depositList={depositList}
                    depositLoadStatus={pageState}
                    handleEditDeposit={handleEditDeposit}
                    handleOperationManage={handleOperationManage}
                    handleDeleteDeposit={handleDeleteDeposit}
                    handleCloseDeposit={handleCloseDeposit}
                />

                <DepositEdit
                    isShow={isShowEditForm}
                    depositId={editedDepositId}
                    handleHideModal={handleHideModal}
                ></DepositEdit>
                <RealEstateOperationManage
                    isShow={isShowOperations}
                    itemType="deposit"
                    hideModal={handleHideModal}
                    itemId={editedItemId}
                    onChange={handleChange}
                />
            </div>
            <DepositClosedList depositList={closedDepositList} depositLoadStatus={pageState} />
        </>
    );
};

export const DepositManageWithBoundaryError = withErrorBoundary(DepositManage, {
    FallbackComponent: ErrorComponent
});
