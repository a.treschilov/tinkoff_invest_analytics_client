import { EmptyContent } from "../../components/Common/EmptyContent/EmptyContent";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";

type UserItemEventsContentLoadingProps = {
    show: boolean;
};

export const UserItemEventsContentEmpty = ({ show }: UserItemEventsContentLoadingProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userItem");

    if (!show) {
        return <></>;
    }

    return (
        <tbody>
            <tr>
                <td colSpan={3}>
                    <EmptyContent size={"xs"}>{t("Events.EmptyEventList")}</EmptyContent>
                </td>
            </tr>
        </tbody>
    );
};
