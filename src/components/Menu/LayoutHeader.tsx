"use client";

import { useContext } from "react";
import { SessionContext } from "../../providers/SessionProvider";

export const LayoutHeader = () => {
    const { pageTitle } = useContext(SessionContext);

    return <h1 className={"text-2xl font-semibold ml-2"}>{pageTitle}</h1>;
};
