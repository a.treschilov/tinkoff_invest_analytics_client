import { CSSProperties, useEffect, useState } from "react";
import { Loading } from "../Common/Loading/Loading";

type PageLoaderProps = {
    show?: boolean;
    height?: string | null;
};

export const PageLoader = ({ show = true, height = null }: PageLoaderProps) => {
    const [style, setStyle] = useState<CSSProperties>({});
    useEffect(() => {
        if (height !== null) {
            setStyle({ height: height, lineHeight: height });
        }
    }, [height]);

    if (!show) {
        return <></>;
    }

    return (
        <div className={"text-center"} style={style}>
            <Loading size={"lg"} variant="bars" />
        </div>
    );
};
