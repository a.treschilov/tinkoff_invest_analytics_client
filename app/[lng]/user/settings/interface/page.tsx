"use client";

import { UserSettingsInterface } from "../../../../../src/Features/UserSettingsInterface/UserSettingsInterface";

const UserSettingsInterfacePage = () => {
    return <UserSettingsInterface />;
};

export default UserSettingsInterfacePage;
