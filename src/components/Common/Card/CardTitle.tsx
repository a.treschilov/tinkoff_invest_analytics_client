import { ReactNode } from "react";

type CardTitleProps = {
    children?: ReactNode;
    className?: string;
};

export const CardTitle = ({ children, className }: CardTitleProps) => {
    return <h2 className={"card-title" + (className ? " " + className : "")}>{children}</h2>;
};
