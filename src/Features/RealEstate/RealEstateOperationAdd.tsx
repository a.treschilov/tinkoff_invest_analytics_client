import { Trans } from "react-i18next";
import { Modal } from "../../components/Common/Modal/Modal";
import { OperationEdit } from "../Operation/OperationEdit";

interface IRealEstateOperationAddProps {
    handleOperationList: (text: string | null) => void;
    handleChangeOperation: () => void;
    itemId: number;
    operationId: number;
}

export const RealEstateOperationAdd = ({
    handleOperationList,
    handleChangeOperation,
    itemId,
    operationId
}: IRealEstateOperationAddProps) => {
    return (
        <>
            <Modal.Header>
                <Trans>
                    {operationId === 0
                        ? "RealEstateOperationsList.ModalHeaderTitleAdd"
                        : "RealEstateOperationsList.ModalHeaderTitleEdit"}
                </Trans>
            </Modal.Header>
            <OperationEdit
                onCancel={handleOperationList}
                onChangeData={text => {
                    handleChangeOperation();
                    handleOperationList(text || null);
                }}
                itemId={itemId}
                operationId={operationId}
            />
        </>
    );
};
