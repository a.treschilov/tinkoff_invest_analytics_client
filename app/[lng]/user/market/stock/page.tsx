"use client";

import { useState } from "react";
import { StockFilters } from "../../../../../src/Features/StockList/StockFilters";
import { StockList } from "../../../../../src/Features/StockList/StockList";

export interface IStockFilters {
    country: string;
    currency: string;
    marketSector: number;
}

const StockListPage = () => {
    const [country, setCountry] = useState("");
    const [currency, setCurrency] = useState("");
    const [marketSector, setMarketSector] = useState(0);

    const handleApplyFilters = (filters: IStockFilters) => {
        setCountry(filters.country);
        setCurrency(filters.currency);
        setMarketSector(filters.marketSector);
    };

    return (
        <>
            <StockFilters handleApplyFilters={handleApplyFilters} />
            <StockList country={country} currency={currency} marketSector={marketSector} />
        </>
    );
};

export default StockListPage;
