"use client";

import { useEffect, useState } from "react";
import i18next, { InitOptions } from "i18next";
import { initReactI18next, useTranslation as useTranslationOrg } from "react-i18next";
import resourcesToBackend from "i18next-resources-to-backend";
import LanguageDetector from "i18next-browser-languagedetector";
import { defaultNS, fallbackLng, getOptions, languages, lngCookieName } from "./settings";
import { getCookie, setCookie } from "cookies-next";

const runsOnServerSide = typeof window === "undefined";
const i18nextOptions: InitOptions<unknown> = {
    ...getOptions(fallbackLng, ["translation"]),
    lng: undefined,
    detection: {
        order: ["path", "htmlTag", "cookie", "navigator"]
    },
    preload: runsOnServerSide ? languages : []
};

i18next
    .use(initReactI18next)
    .use(LanguageDetector)
    .use(resourcesToBackend((language: string, namespace: string) => import(`./locales/${language}/${namespace}.json`)))
    .init(i18nextOptions);

export function useTranslation(lng: string, ns = defaultNS) {
    const ret = useTranslationOrg(ns);
    const { i18n } = ret;

    if (runsOnServerSide && lng && i18n.resolvedLanguage !== lng) {
        i18n.changeLanguage(lng);
    } else {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const [activeLng, setActiveLng] = useState(i18n.resolvedLanguage);
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEffect(() => {
            if (activeLng === i18n.resolvedLanguage) return;
            setActiveLng(i18n.resolvedLanguage);
        }, [activeLng, i18n.resolvedLanguage]);
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEffect(() => {
            if (!lng || i18n.resolvedLanguage === lng) return;
            i18n.changeLanguage(lng);
        }, [lng, i18n]);
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useEffect(() => {
            if (getCookie(lng) === lng) return;
            setCookie(lngCookieName, lng, { path: "/" });
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [lng, getCookie(lng)]);
    }
    return ret;
}
