import { ChangeEvent, useEffect, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { Trans } from "react-i18next";
import { IStockFilters } from "../../../app/[lng]/user/market/stock/page";
import Select from "../../components/Common/Form/Select";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

interface ICurrency {
    id: number;
    name: string;
    iso: string;
    symbol: string;
}

interface IMarketSector {
    id: number;
    name: string;
}

interface StockFiltersProps {
    handleApplyFilters: (filters: IStockFilters) => void;
}

type ICountry = {
    iso: string;
    name: string;
};

export const StockFilters = ({ handleApplyFilters }: StockFiltersProps) => {
    const [selectedCountry, setSelectedCountry] = useState<string>("");
    const [selectedCurrency, setSelectedCurrency] = useState<string>("");
    const [selectedMarketSector, setSelectedMarketSector] = useState<number>(0);
    const [countryList, setCountryList] = useState<ICountry[]>([]);
    const [currencyList, setCurrencyList] = useState<ICurrency[]>([]);
    const [marketSectorList, setMarketSectorList] = useState<IMarketSector[]>([]);

    useEffect(() => {
        ApiService.fetch("/v1/market/stock/filters").then(response => {
            setCountryList(response.country);
            setCurrencyList(response.currency);
            setMarketSectorList(response.marketSector);
        });
    }, []);

    const handleCountryChange = (event: ChangeEvent<HTMLSelectElement>) => {
        setSelectedCountry(event.target.value);
    };

    const handleCurrencyChange = (event: ChangeEvent<HTMLSelectElement>) => {
        setSelectedCurrency(event.target.value);
    };

    const handleSectorChange = (event: ChangeEvent<HTMLSelectElement>) => {
        setSelectedMarketSector(parseInt(event.target.value));
    };

    const handleFormSubmit = (event: ChangeEvent<HTMLFormElement>) => {
        handleApplyFilters({
            country: selectedCountry,
            currency: selectedCurrency,
            marketSector: selectedMarketSector
        });
        event.preventDefault();
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <form onSubmit={handleFormSubmit}>
                <div className={"grid grid-cols-12 gap-6"}>
                    <Select containerClassName={"col-span-6 xl:col-span-2 lg:col-span-3 sm:col-span-4"}>
                        <Select.Select onChange={handleCountryChange}>
                            <option value="">
                                <Trans>StockList.FilterCountry</Trans>
                            </option>
                            {countryList.map((country: ICountry) => {
                                return (
                                    <option key={"country-" + country.iso} value={country.iso}>
                                        {country.name}
                                    </option>
                                );
                            })}
                        </Select.Select>
                    </Select>
                    <Select containerClassName={"col-span-6 xl:col-span-2 lg:col-span-3 sm:col-span-4"}>
                        <Select.Select onChange={handleCurrencyChange}>
                            <option value="">
                                <Trans>StockList.FilterCurrency</Trans>
                            </option>
                            {currencyList.map((currency: ICurrency) => {
                                return (
                                    <option key={"currency-" + currency.iso} value={currency.iso}>
                                        {currency.symbol}
                                    </option>
                                );
                            })}
                        </Select.Select>
                    </Select>
                    <Select containerClassName={"col-span-6 xl:col-span-2 lg:col-span-3  sm:col-span-4"}>
                        <Select.Select onChange={handleSectorChange}>
                            <option value={0}>
                                <Trans>StockList.FilterSector</Trans>
                            </option>
                            {marketSectorList.map((sector: IMarketSector) => {
                                return (
                                    <option key={"sector-" + sector.id} value={sector.id}>
                                        {sector.name}
                                    </option>
                                );
                            })}
                        </Select.Select>
                    </Select>
                    <div className="mb-2 mb-lg-0 text-end lg:text-start">
                        <Button color={"primary"} type="submit">
                            <Trans>StockList.FilterApplyButton</Trans>
                        </Button>
                    </div>
                </div>
            </form>
        </Card>
    );
};
