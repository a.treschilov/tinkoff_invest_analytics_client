import { NextRequest, NextResponse, URLPattern } from "next/server";
import acceptLanguage from "accept-language";
import { fallbackLng, languages, lngCookieName } from "./app/i18n/settings";
import { UserService } from "./src/services/UserSerice";

acceptLanguage.languages(languages);

export const config = {
    matcher: ["/((?!api|_next|_vercel|.*\\..*).*)"]
};

const PATTERNS = [[new URLPattern({ pathname: "/:locale/:slug" }), ({ pathname }) => pathname.groups]];

const params = (url: string): { locale: string; slug: string } => {
    const input = url.split("?")[0];
    let result = {};

    for (const [pattern, handler] of PATTERNS) {
        const patternResult = pattern.exec(input);
        if (patternResult !== null && "pathname" in patternResult) {
            result = handler(patternResult);
            break;
        }
    }
    return result;
};

export async function middleware(req: NextRequest) {
    const requestHeaders = new Headers(req.headers);

    const { locale } = params(req.url);

    let lng: string | null = acceptLanguage.get(locale);

    const path = req.nextUrl.pathname;
    if (path === "/") {
        const user = await UserService.getUser();
        if (user.userId !== null) {
            const response = NextResponse.next();
            response.cookies.set(lngCookieName, user.language);
        }
    }

    if (lng === null && req.cookies.has(lngCookieName)) {
        lng = acceptLanguage.get(req.cookies.get(lngCookieName)?.value);
    }
    if (lng === null) {
        lng = acceptLanguage.get(req.headers.get("Accept-Language"));
    }
    if (lng === null) {
        lng = fallbackLng;
    }

    // Redirect if lng in path is not supported
    if (
        !languages.some(loc => req.nextUrl.pathname.startsWith(`/${loc}`)) &&
        !req.nextUrl.pathname.startsWith("/_next")
    ) {
        const searchParams = req.nextUrl.searchParams;
        return NextResponse.redirect(new URL(`/${lng}${req.nextUrl.pathname}/?${searchParams.toString()}`, req.url));
    }

    if (req.headers.has("referer")) {
        const refererUrl = new URL(req.headers.get("referer"));
        const lngInReferer = languages.find(l => refererUrl.pathname.startsWith(`/${l}`));
        const response = NextResponse.next();
        if (lngInReferer) response.cookies.set(lngCookieName, lngInReferer);
        return response;
    }

    requestHeaders.set("x-invoke-locale", lng);
    return NextResponse.next({
        request: {
            // Apply new request headers
            headers: requestHeaders
        }
    });
}
