import { HTMLAttributes, ReactNode } from "react";

type DockItemLabelProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLSpanElement>;

export const DockItemLabel = ({ children, className, ...args }: DockItemLabelProps) => {
    const classes = ["dock-label"];
    if (className) {
        classes.push(...className?.split(" "));
    }
    return (
        <span className={classes.join(" ")} {...args}>
            {children}
        </span>
    );
};
