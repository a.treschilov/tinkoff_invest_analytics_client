import { ReactNode } from "react";

type RadioLabelProps = {
    children?: ReactNode;
};

export const RadioLabel = ({ children }: RadioLabelProps) => {
    return <span className={"label-text cursor-pointer"}>{children}</span>;
};
