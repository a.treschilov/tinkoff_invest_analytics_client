import { FC, HTMLAttributes, ReactNode } from "react";
import { StatTitle } from "./StatTitle";
import { StatDesc } from "./StatDesc";
import { StatValue } from "./StatValue";

type StatProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

type StatExtension = {
    Title: typeof StatTitle;
    Value: typeof StatValue;
    Desc: typeof StatDesc;
};

export const Stat: FC<StatProps> & StatExtension = ({ children, className, ...args }: StatProps) => {
    const classN = ["stat"];
    if (className) {
        classN.push(...className.split(" "));
    }

    return (
        <div className={classN.join(" ")} {...args}>
            {children}
        </div>
    );
};

Stat.Title = StatTitle;
Stat.Value = StatValue;
Stat.Desc = StatDesc;
