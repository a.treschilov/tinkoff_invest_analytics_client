import { Fragment, ReactElement, useContext, useEffect, useState } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { ApiService } from "../../services/ApiService";
import { PageState } from "../../components/Common/Types";
import { FaGoogle, FaYandex } from "react-icons/fa";
import { FaTelegram } from "react-icons/fa6";
import "./UserSocialAccount.css";
import { getSocialAccountUrl } from "../Auth/socialAccounts";
import { UserSocialAccountSkeleton } from "./UserSocialAccountSkeleton";
import { Modal } from "../../components/Common/Modal/Modal";
import { useSearchParams } from "next/navigation";
import { Alert } from "../../components/Common/Alert/Alert";
import { Card } from "../../components/Common/Card/Card";
import { Divider } from "../../components/Common/Divider/Divider";
import { Button } from "../../components/Common/Button/Button";
import Link from "next/link";

type SocialAccount = {
    code: string;
};

type UserSocialAccount = {
    code: string;
};

type LinkedAccounts = {
    code: string;
    isLinked: boolean;
};
const redirectUrl = "/auth/link";

export const UserSocialAccount = () => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userSettings");

    const [accountList, setAccountList] = useState<SocialAccount[]>([]);
    const [userAccountList, setUserAccountList] = useState<UserSocialAccount[]>([]);
    const [linkedAccounts, setLinkedAccounts] = useState<LinkedAccounts[]>([]);
    const [accountListLoaded, setAccountListLoaded] = useState(false);
    const [userAccountListLoaded, setUserAccountListLoaded] = useState(false);
    const [unlinkMessageCode, setUnlinkMessageCode] = useState(0);
    const [linkMessageCode, setLinkMessageCode] = useState<string | null>(null);
    const [pageState, setPageState] = useState(PageState.LOADING);

    const [confirmationShow, setConfirmationShow] = useState(false);
    const [currentLinkedAccount, setCurrentLinkedAccount] = useState<LinkedAccounts | null>(null);

    const searchParams = useSearchParams();
    const linkStatus = searchParams.get("link");

    useEffect(() => {
        ApiService.fetch("/v2/auth").then(response => {
            const accounts: SocialAccount[] = [];
            response.forEach((el: { code: string }) => {
                accounts.push({
                    code: el.code
                });
            });
            setAccountList(accounts);
            setAccountListLoaded(true);
        });

        init();
    }, []);

    useEffect(() => {
        if (userAccountListLoaded && accountListLoaded) {
            const accounts: LinkedAccounts[] = [];
            accountList.forEach(el => {
                const linkedAccount = userAccountList.find(userAccount => userAccount.code === el.code);
                accounts.push({
                    code: el.code,
                    isLinked: linkedAccount !== undefined
                });
            });
            setLinkedAccounts(accounts);
            setPageState(PageState.EDIT_FORM);
        } else {
            setPageState(PageState.LOADING);
        }
    }, [userAccountListLoaded, accountListLoaded, accountList, userAccountList]);

    useEffect(() => {
        setLinkMessageCode(linkStatus);
    }, [linkStatus]);

    const init = () => {
        setUserAccountList([]);
        setUserAccountListLoaded(false);

        ApiService.fetch("/v1/user/account").then(response => {
            const userAccount: UserSocialAccount[] = [];
            response.forEach((el: { code: string }) => {
                userAccount.push({
                    code: el.code
                });
            });
            setUserAccountList(userAccount);
            setUserAccountListLoaded(true);
        });
    };

    const unlinkAccount = (code: string) => {
        setUserAccountListLoaded(false);
        setLinkMessageCode(null);
        const data = {
            code: code
        };
        ApiService.delete("/v1/auth/link", data)
            .then(() => {
                setUnlinkMessageCode(200);
            })
            .catch(error => {
                console.log(error);
                setUnlinkMessageCode(error.response.data.error_code);
            })
            .finally(() => {
                init();
            });
    };

    const getSocialIcon = (code: string): ReactElement => {
        switch (code) {
            case "YANDEX":
                return <FaYandex />;
            case "GOOGLE":
                return <FaGoogle />;
            case "TELEGRAM":
                return <FaTelegram />;
            default:
                return <Fragment />;
        }
    };

    const unLinkAccountMessage = (code: number): ReactElement => {
        switch (code) {
            case 2041:
                return <>{t("SocialAccount.UnlinkAccountMessageNotExists")}</>;
            case 2042:
                return <>{t("SocialAccount.UnlinkAccountMessageIsOnlyOne")}</>;
            default:
                return <>{t("SocialAccount.UnlinkAccountMessageSuccess")}</>;
        }
    };

    const linkAccountMessage = (code: string | null): ReactElement => {
        switch (code) {
            case "success":
                return <>{t("SocialAccount.LinkMessageSuccess")}</>;
            case "exist":
                return <>{t("SocialAccount.LinkMessageAccountExist")}</>;
            case "error":
                return <>{t("SocialAccount.LinkMessageError")}</>;
            default:
                return <></>;
        }
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title className="mb-6">{t("SocialAccount.Header")}</Card.Title>
            <Alert
                status={linkMessageCode === "success" ? "success" : linkMessageCode === "error" ? "error" : "info"}
                className={"mb-4"}
                show={linkMessageCode !== null}
                dismissible={true}
                onClose={() => setLinkMessageCode(null)}
            >
                {linkAccountMessage(linkMessageCode)}
            </Alert>
            <Alert
                className={"mb-4"}
                show={unlinkMessageCode !== 0}
                onClose={() => setUnlinkMessageCode(0)}
                status={unlinkMessageCode >= 2000 ? "info" : "success"}
            >
                {unLinkAccountMessage(unlinkMessageCode)}
            </Alert>
            <div className={"mb-8"}>{t("SocialAccount.Description")}</div>
            <div className={"max-w-lg"}>
                <UserSocialAccountSkeleton show={pageState === PageState.LOADING} />
                {linkedAccounts.map(el => {
                    return (
                        <Fragment key={"social-account-" + el.code}>
                            <div className="grid grid-cols-2">
                                <div className={"my-auto social"}>
                                    {getSocialIcon(el.code)}
                                    {t("SocialAccount.SocialAccount-" + el.code)}
                                </div>
                                <div className={"text-right"}>
                                    {el.isLinked ? (
                                        <Button link={true} size={"sm"} onClick={() => unlinkAccount(el.code)}>
                                            {t("SocialAccount.DeleteButton")}
                                        </Button>
                                    ) : (
                                        <Button
                                            color={"primary"}
                                            size={"sm"}
                                            onClick={() => {
                                                setConfirmationShow(true);
                                                setCurrentLinkedAccount(el);
                                            }}
                                        >
                                            {t("SocialAccount.AddButton")}
                                        </Button>
                                    )}
                                </div>
                            </div>
                            <Divider />
                        </Fragment>
                    );
                })}
            </div>
            <Modal show={confirmationShow} onClose={() => setConfirmationShow(false)}>
                <Modal.Body>
                    {t("SocialAccount.ModalWarning", {
                        replace: { SOCIAL_TYPE: t("SocialAccount.SocialAccount-" + currentLinkedAccount?.code) }
                    })}
                </Modal.Body>
                <Modal.Actions>
                    <Button link={true} onClick={() => setConfirmationShow(false)}>
                        {t("SocialAccount.ModalCancel")}
                    </Button>
                    <Link
                        className={"btn btn-primary hover:no-underline"}
                        href={getSocialAccountUrl(currentLinkedAccount?.code || "", redirectUrl)}
                    >
                        {t("SocialAccount.ModalConfirm")}
                    </Link>
                </Modal.Actions>
            </Modal>
        </Card>
    );
};
