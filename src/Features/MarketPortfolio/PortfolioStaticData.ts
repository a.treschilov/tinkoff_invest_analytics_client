import { IPrice } from "../../interfaces/IPrice";
import { IPortfolio } from "../../../app/[lng]/user/market/portfolio/page";

export const DemoPortfolioData: IPortfolio = {
    shares: [
        {
            share: 0.38,
            targetShare: 0.35,
            diffPercent: 3,
            diffAmount: 75000,
            name: "Stock"
        },
        {
            share: 0.08,
            targetShare: 0.1,
            diffPercent: -0.02,
            diffAmount: -50000,
            name: "Etf"
        },
        {
            share: 0.2,
            targetShare: 0.15,
            diffPercent: 0.5,
            diffAmount: 125000,
            name: "Bond"
        },
        {
            share: 0.1,
            targetShare: 0.15,
            diffPercent: -0.05,
            diffAmount: -125000,
            name: "Currency"
        },
        {
            share: 0.21,
            targetShare: 0.15,
            diffPercent: 0.09,
            diffAmount: 150000,
            name: "Gold"
        },
        {
            share: 0.03,
            targetShare: 0.1,
            diffPercent: -0.07,
            diffAmount: -175000,
            name: "Federal Bond"
        }
    ],
    sharesCurrency: [
        {
            share: 0.4,
            targetShare: 0.4,
            diffPercent: 0,
            diffAmount: 0,
            name: "RUB"
        },
        {
            share: 0.25,
            targetShare: 0.3,
            diffPercent: -0.05,
            diffAmount: -125000,
            name: "USD"
        },
        {
            share: 0.14,
            targetShare: 0.1,
            diffPercent: 0.4,
            diffAmount: 100000,
            name: "EUR"
        },
        {
            share: 0,
            targetShare: 0.05,
            diffPercent: -0.05,
            diffAmount: -141770.070061025,
            name: "CNY"
        },
        {
            share: 0.21,
            targetShare: 0.15,
            diffPercent: 0.06,
            diffAmount: 150000,
            name: "HKD"
        },
        {
            share: 0,
            targetShare: 0,
            diffPercent: 0,
            diffAmount: 0,
            name: "JPY"
        }
    ],
    shareSector: [
        {
            share: 0.032605479925917724,
            targetShare: 0.02,
            diffPercent: 0.012605479925917724,
            diffAmount: 9813.837115999999,
            name: "Basic Materials"
        },
        {
            share: 0.05588550967583733,
            targetShare: 0.12,
            diffPercent: -0.06411449032416267,
            diffAmount: -49915.52630400001,
            name: "Communication Services"
        },
        {
            share: 0.11995852350126678,
            targetShare: 0.13,
            diffPercent: -0.010041476498733229,
            diffAmount: -7817.664646000015,
            name: "Consumer Cyclical"
        },
        {
            share: 0.02955038519395958,
            targetShare: 0.06,
            diffPercent: -0.03044961480604042,
            diffAmount: -23706.16285200001,
            name: "Consumer Defensive"
        },
        {
            share: 0.05363056664968732,
            targetShare: 0.02,
            diffPercent: 0.03363056664968732,
            diffAmount: 26182.652715999997,
            name: "Energy"
        },
        {
            share: 0.0022709250465027322,
            targetShare: 0.13,
            diffPercent: -0.12772907495349728,
            diffAmount: -99441.85734600003,
            name: "Financial Services"
        },
        {
            share: 0.037316434426821816,
            targetShare: 0.15,
            diffPercent: -0.11268356557317818,
            diffAmount: -87728.36613000002,
            name: "Healthcare"
        },
        {
            share: 0.0192283642229332,
            targetShare: 0.07,
            diffPercent: -0.05077163577706681,
            diffAmount: -39527.61549400002,
            name: "Industrials"
        },
        {
            share: 0.003942778010602255,
            targetShare: 0.03,
            diffPercent: -0.026057221989397743,
            diffAmount: -20286.520926000005,
            name: "Real Estate"
        },
        {
            share: 0.1285956970130477,
            targetShare: 0.24,
            diffPercent: -0.11140430298695228,
            diffAmount: -86732.41240800003,
            name: "Technology"
        },
        {
            share: 0.016140948177757346,
            targetShare: 0.03,
            diffPercent: -0.013859051822242653,
            diffAmount: -10789.789676000004,
            name: "Utilities"
        },
        {
            share: 0.07744407670395531,
            targetShare: 0,
            diffPercent: 0.07744407670395531,
            diffAmount: 60293.107350000006,
            name: "Health Care"
        },
        {
            share: 0.4234303114517107,
            targetShare: 0,
            diffPercent: 0.4234303114517107,
            diffAmount: 329656.3186,
            name: "Other"
        }
    ],
    shareCountry: [
        {
            share: 0,
            targetShare: 0.0217,
            diffPercent: -0.0217,
            diffAmount: -16894.260803140005,
            name: "Canada"
        },
        {
            share: 0.013036505332572192,
            targetShare: 0.008,
            diffPercent: 0.005036505332572192,
            diffAmount: 3921.1075864,
            name: "Switzerland"
        },
        {
            share: 0.2039880407836179,
            targetShare: 0.1804,
            diffPercent: 0.023588040783617897,
            diffAmount: 18364.171098319985,
            name: "China"
        },
        {
            share: 0.0850868366068332,
            targetShare: 0.0397,
            diffPercent: 0.045386836606833195,
            diffAmount: 35335.348141259994,
            name: "Germany"
        },
        {
            share: 0,
            targetShare: 0.0137,
            diffPercent: -0.0137,
            diffAmount: -10665.961889540004,
            name: "Spain"
        },
        {
            share: 0,
            targetShare: 0.0274,
            diffPercent: -0.0274,
            diffAmount: -21331.923779080007,
            name: "France"
        },
        {
            share: 0.016722173139856602,
            targetShare: 0.0315,
            diffPercent: -0.014777826860143398,
            diffAmount: -11505.090372300006,
            name: "United Kingdom"
        },
        {
            share: 0,
            targetShare: 0.0342,
            diffPercent: -0.0342,
            diffAmount: -26625.97785564001,
            name: "India"
        },
        {
            share: 0.0472084995403744,
            targetShare: 0.0423,
            diffPercent: 0.004908499540374402,
            diffAmount: 3821.450294339999,
            name: "Japan"
        },
        {
            share: 0,
            targetShare: 0.0171,
            diffPercent: -0.0171,
            diffAmount: -13312.988927820004,
            name: "Korea, Republic of"
        },
        {
            share: 0,
            targetShare: 0.0098,
            diffPercent: -0.0098,
            diffAmount: -7629.666169160001,
            name: "Netherlands"
        },
        {
            share: 0.37764303631350354,
            targetShare: 0.021,
            diffPercent: 0.3566430363135035,
            diffAmount: 277659.9294518,
            name: "Russian Federation"
        },
        {
            share: 0,
            targetShare: 0.01,
            diffPercent: -0.01,
            diffAmount: -7785.373642000002,
            name: "Saudi Arabia"
        },
        {
            share: 0.23418342790438412,
            targetShare: 0.2465,
            diffPercent: -0.012316572095615874,
            diffAmount: -9588.911575300055,
            name: "United States"
        }
    ],
    shareCurrencyBalance: [
        {
            share: 0.04133907356203418,
            targetShare: 0.25,
            diffPercent: -0.20866092643796583,
            diffAmount: -52708.65866875001,
            name: "RUB"
        },
        {
            share: 0.10199481411612367,
            targetShare: 0.15,
            diffPercent: -0.048005185883876325,
            diffAmount: -12126.31900125,
            name: "USD"
        },
        {
            share: 0.08135631628932447,
            targetShare: 0.05,
            diffPercent: 0.03135631628932446,
            diffAmount: 7920.742041249997,
            name: "EUR"
        },
        {
            share: 0.505512168087092,
            targetShare: 0.35,
            diffPercent: 0.155512168087092,
            diffAmount: 39283.05086375001,
            name: "CNY"
        },
        {
            share: 0.26979762794542567,
            targetShare: 0.2,
            diffPercent: 0.06979762794542566,
            diffAmount: 17631.184764999995,
            name: "HKD"
        },
        {
            share: 0,
            targetShare: 0,
            diffPercent: 0,
            diffAmount: 0,
            name: "JPY"
        }
    ],
    totalAmount: 2500000
};

export const DemoRevenueTodayData: IPrice[] = [
    { amount: 5400.21, currency: "RUB" },
    { amount: 66.43, currency: "USD" },
    { amount: 482.86, currency: "CNY" },
    { amount: 441.39, currency: "HKD" },
    { amount: 63.82, currency: "EUR" }
];
