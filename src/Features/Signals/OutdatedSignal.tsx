"use client";

import { useContext, useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { ApiService } from "../../services/ApiService";
import { VolumeSignal, VolumeSignalList } from "./VolumeSignalList";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { convertVolumeResponseToSignal, VolumeSignalApiResponseType } from "./utils";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

export const OutdatedVolumeSignal = () => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/signals");

    const [pageState, setPageState] = useState(PageState.LOADING);
    const [signals, setSignals] = useState<VolumeSignal[]>([]);

    useEffect(() => {
        ApiService.fetch("/v2/trading/signals/volume/outdated").then((response: VolumeSignalApiResponseType[]) => {
            setSignals(convertVolumeResponseToSignal(response));
            setPageState(PageState.SUCCESS);
        });
    }, []);

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>{t("VolumeSignal.OutdatedSignalTitle")}</Card.Title>
            <Divider />
            <VolumeSignalList signals={signals} loading={pageState === PageState.LOADING} />
        </Card>
    );
};
