"use client";

import { useEffect, useState } from "react";
import type { SelectOption } from "../../components/Common/Types";
import { Trans } from "react-i18next";
import { FilterCheckbox } from "../../components/Common/Filter/FilterCheckbox";
import { Modal } from "../../components/Common/Modal/Modal";
import { Button } from "../../components/Common/Button/Button";

type ModalOptions = {
    isShown: boolean;
    close: () => void;
};

type FilterOption = {
    option: SelectOption[];
    selected: string[];
};

type Props = {
    title: string;
    brokers: FilterOption;
    operationTypes: FilterOption;
    operationStatuses: FilterOption;
    modal: ModalOptions;
    confirm: (selectedFilters: Record<string, boolean>) => void;
};

export const OperationFilterModal = (props: Props) => {
    const [selectedFilters, setState] = useState({});
    const { brokers, operationTypes, operationStatuses, title, modal, confirm } = props;

    useEffect(() => {
        const selected: Record<string, boolean> = {};
        brokers.selected.forEach((val: string) => {
            selected[val] = true;
        });
        operationTypes.selected.forEach((val: string) => {
            selected[val] = true;
        });
        operationStatuses.selected.forEach((val: string) => {
            selected[val] = true;
        });
        setState(selected);
    }, [brokers, operationTypes, operationStatuses]);

    const toggleFilter = (label: string) => {
        const selected: Record<string, boolean> = selectedFilters;

        if (Object.keys(selected).indexOf(label) !== -1) {
            delete selected[label];
        } else {
            selected[label] = true;
        }
        setState(selected);
        confirm(selected);
    };

    return (
        <Modal
            show={modal.isShown}
            onClose={() => {
                modal.close();
            }}
        >
            <Modal.Header>
                <Trans>{title}</Trans>
            </Modal.Header>
            <Modal.Body>
                <div>
                    <FilterCheckbox
                        name="Operations.FilterBroker"
                        options={brokers.option}
                        selected={brokers.selected}
                        onChange={toggleFilter}
                    />
                    <FilterCheckbox
                        name="Operations.FilterType"
                        options={operationTypes.option}
                        selected={operationTypes.selected}
                        onChange={toggleFilter}
                    />
                    <FilterCheckbox
                        name="Operations.FilterStatus"
                        options={operationStatuses.option}
                        selected={operationStatuses.selected}
                        onChange={toggleFilter}
                    />
                </div>
            </Modal.Body>
            <Modal.Actions>
                <Button
                    color="primary"
                    type="button"
                    onClick={() => {
                        modal.close();
                    }}
                >
                    <Trans>Operations.FilterCloseButton</Trans>
                </Button>
            </Modal.Actions>
        </Modal>
    );
};
