import axios, { AxiosError } from "axios";
import { getCookie } from "../utils/_actions";

export const ApiService = {
    baseURL: process.env.NEXT_PUBLIC_API_HOST,
    baseURLV2: process.env.NEXT_PUBLIC_API_V2_HOST,

    fetch: async function (command: string, params?: object) {
        const jwt = await getCookie("jwt");
        return await axios({
            method: "get",
            baseURL: command.startsWith("/v1/") ? this.baseURL : this.baseURLV2,
            url: command,
            params: params,
            headers: {
                Authorization: "Bearer " + jwt
            }
        })
            .then(response => {
                return response.data;
            })
            .catch((error: AxiosError) => {
                throw new AxiosError(error.message, error.code, error.config, error.request, error.response);
            });
    },

    post: async function (command: string, data?: object, headers?: object) {
        const jwt = await getCookie("jwt");
        return await axios({
            method: "post",
            baseURL: command.startsWith("/v1/") ? this.baseURL : this.baseURLV2,
            url: command,
            data: data,
            headers: {
                Authorization: "Bearer " + jwt,
                ...headers
            }
        })
            .then(response => {
                return response.data;
            })
            .catch((error: AxiosError) => {
                throw new AxiosError(error.message, error.code, error.config, error.request, error.response);
            });
    },

    put: async function (command: string, data?: object) {
        const jwt = await getCookie("jwt");
        return await axios({
            method: "put",
            baseURL: command.startsWith("/v1/") ? this.baseURL : this.baseURLV2,
            url: command,
            data: data,
            headers: {
                Authorization: "Bearer " + jwt
            }
        })
            .then(response => {
                return response.data;
            })
            .catch((error: AxiosError) => {
                throw new AxiosError(error.message, error.code, error.config, error.request, error.response);
            });
    },

    delete: async function (command: string, params?: object) {
        const jwt = await getCookie("jwt");
        return await axios({
            method: "delete",
            baseURL: command.startsWith("/v1/") ? this.baseURL : this.baseURLV2,
            url: command,
            data: params,
            headers: {
                Authorization: "Bearer " + jwt
            }
        })
            .then(response => {
                return response.data;
            })
            .catch((error: AxiosError) => {
                throw new AxiosError(error.message, error.code, error.config, error.request, error.response);
            });
    }
};
