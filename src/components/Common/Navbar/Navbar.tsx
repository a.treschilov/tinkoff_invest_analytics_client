import { HTMLAttributes, ReactNode } from "react";

type NavbarProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const Navbar = ({ className, children, ...args }: NavbarProps) => {
    const classes = ["navbar"];
    if (className) {
        classes.push(...className.split(" "));
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
