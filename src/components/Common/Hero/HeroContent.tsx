import { HTMLAttributes, ReactNode } from "react";

type HeroContentProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const HeroContent = ({ children, className, ...args }: HeroContentProps) => {
    const classes = ["hero-content"];
    if (className) {
        classes.push(...className.split(" "));
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
