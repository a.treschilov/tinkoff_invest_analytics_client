import { InputTextLoader } from "../../components/Common/Form/InputTextLoader";

export const UserSettingsLoading = () => {
    return (
        <div className="grid grid-cols-1 md:grid-cols-2 gap-6 mb-6">
            {[...Array(4)].map((value, index) => {
                return <InputTextLoader key={"portfolioSettingLoaderKey-" + index} />;
            })}
        </div>
    );
};
