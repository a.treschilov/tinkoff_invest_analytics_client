import { StrategyTypes } from "../../components/Common/Types";
import { useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { UserStrategyPredefinedStock } from "./UserStrategyPredefinedStock";
import { UserStrategyPredefinedCountries } from "./UserStrategyPredefinedCountries";
import { UserStrategyPredefinedCurrencyBalance } from "./UserStrategyPredefinedCurrencyBalance";
import { UserStrategyPredefinedSector } from "./UserStrategyPredefinedSector";
import { UserStrategyPredefinedCurrency } from "./UserStrategyPredefinedCurrency";
import { Popover } from "../../components/Common/Popover/Popover";
import { Button } from "../../components/Common/Button/Button";

type UserStrategyPredefinedProps = {
    type: StrategyTypes;
    handleSetStrategy: (strategy: UserStrategyItemTypeValueType[]) => void;
};

type UserStrategyItemType = {
    id: string;
    name: string;
    description: string;
    strategy: UserStrategyItemTypeValueType[];
};

export type UserStrategyItemTypeValueType = {
    id: string;
    value: number;
};

export const UserStrategyPredefined = ({ type, handleSetStrategy }: UserStrategyPredefinedProps) => {
    const [list, setList] = useState<UserStrategyItemType[]>([]);

    useEffect(() => {
        switch (type) {
            case StrategyTypes.STOCK:
                setList(UserStrategyPredefinedStock);
                break;
            case StrategyTypes.COUNTRY:
                setList(UserStrategyPredefinedCountries);
                break;
            case StrategyTypes.CURRENCY_BALANCE:
                setList(UserStrategyPredefinedCurrencyBalance);
                break;
            case StrategyTypes.SECTOR:
                setList(UserStrategyPredefinedSector);
                break;
            case StrategyTypes.CURRENCY:
                setList(UserStrategyPredefinedCurrency);
                break;
        }
    }, [type]);

    if (list.length === 0) {
        return <></>;
    }

    return (
        <div className={"flex align-items-center flex-wrap gap-2"}>
            {list.map(el => {
                return (
                    <div key={"predefined-strategy-" + el.id}>
                        <Popover content={<Trans>{el.description}</Trans>} trigger={"hover"} vertical={"bottom"}>
                            <Button
                                outline={true}
                                color="primary"
                                size="sm"
                                onClick={() => handleSetStrategy(el.strategy)}
                            >
                                <Trans>{el.name}</Trans>
                            </Button>
                        </Popover>
                    </div>
                );
            })}
        </div>
    );
};
