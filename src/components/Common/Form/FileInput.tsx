import React, { FC, ReactNode } from "react";
import { FileInput as DaisyUIFileInput } from "react-daisyui";
import { IComponentBaseProps } from "react-daisyui/dist/types";

type FileInputExtensions = {
    Label: typeof Label;
    File: typeof File;
    Description: typeof Description;
};

type FileInputProps = { children: ReactNode };

export const FileInput: FC<FileInputProps> & FileInputExtensions = ({ children }: FileInputProps) => {
    return <div className={"form-control w-full"}>{children}</div>;
};

const Label = ({ children }: { children: ReactNode }) => {
    return (
        <label className="label">
            <span className={"label-text text-base-content"}>{children}</span>
        </label>
    );
};
FileInput.Label = Label;

const File = ({
    ...args
}: Omit<React.InputHTMLAttributes<HTMLInputElement>, "size"> &
    IComponentBaseProps & {
        size?: "lg" | "md" | "sm" | "xs" | undefined;
        color?:
            | "neutral"
            | "primary"
            | "secondary"
            | "accent"
            | "ghost"
            | "info"
            | "success"
            | "warning"
            | "error"
            | undefined;
        bordered?: boolean | undefined;
    } & React.RefAttributes<HTMLInputElement>) => {
    return <DaisyUIFileInput bordered={true} {...args} />;
};
FileInput.File = File;

const Description = ({ children, className = "" }: { children: ReactNode; className?: string }) => {
    return <div className={"text-xs mt-1 " + className}>{children}</div>;
};
FileInput.Description = Description;
