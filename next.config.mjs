import withSerwistInit from "@serwist/next";
import withBundleAnalyzerInit from "@next/bundle-analyzer";

/** @type {import('next').NextConfig} */
const nextConfig = {
    output: "standalone",
    reactStrictMode: true,
    eslint: {
        ignoreDuringBuilds: false
    }
};

const withSerwist = withSerwistInit({
    swSrc: "app/sw.ts",
    swDest: "public/sw.js",
    disable: false
});

const withBundleAnalyzer = withBundleAnalyzerInit({
    enabled: process.env.ANALYZE === "true"
});

export default withBundleAnalyzer(withSerwist(nextConfig));
