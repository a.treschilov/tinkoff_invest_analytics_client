import { Trans } from "react-i18next";
import { TbChartInfographic } from "react-icons/tb";
import { useEffect, useState } from "react";
import { ApiService } from "../../../services/ApiService";
import { AnalyticsCompareWithIndexChart } from "./AnalyticsCompareWithIndexChart";
import { PageState } from "../../../components/Common/Types";
import { PageLoader } from "../../../components/Loader/Loaders";
import { AnalyticsCompareWithIndexFilters } from "./AnalyticsCompareWithIndexFilters";
import { AxiosError } from "axios";
import { Divider } from "../../../components/Common/Divider/Divider";
import { Card } from "../../../components/Common/Card/Card";

export type CompareWithIndexResponseType = {
    capital: number;
    time: number;
    index: CompareWithIndexIndexType[];
};

export type CompareWithIndexIndexType = {
    id: string;
    name: string;
    value: number;
    normalizedIndex: number;
};

export type LineChartSeriesDataType = {
    name: string;
    data: number[][];
};

const benchmark = ["IMOEX.MOEX"];
const currentDate = new Date();

export const AnalyticsCompareWithIndex = () => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [data, setData] = useState<LineChartSeriesDataType[]>([]);
    const [startDate, setStartDate] = useState<Date>(new Date(new Date().setFullYear(currentDate.getFullYear() - 1)));
    const [endDate, setEndDate] = useState<Date>(new Date());

    useEffect(() => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/analytics/compareWithIndex", {
            from: startDate.toISOString().substring(0, 10),
            to: endDate.toISOString().substring(0, 10),
            benchmark: benchmark
        })
            .then((response: CompareWithIndexResponseType[]) => {
                const capital: number[][] = [];
                const indexes: { [key: string]: LineChartSeriesDataType } = {};

                response.map(item => {
                    capital.push([item.time * 1000, item.capital]);

                    item.index.map(el => {
                        if (indexes[el.id] === undefined) {
                            indexes[el.id] = {
                                name: el.name,
                                data: []
                            };
                        }
                        indexes[el.id].data.push([item.time * 1000, el.normalizedIndex]);
                    });
                });
                const seriesData = [];
                seriesData.push({
                    name: "Capital",
                    data: capital
                });

                for (const indexesKey in indexes) {
                    seriesData.push({
                        name: indexes[indexesKey].name,
                        data: indexes[indexesKey].data
                    });
                }
                setData(seriesData);
                setPageState(PageState.SUCCESS);
            })
            .catch((error: AxiosError) => {
                if (error?.response?.status === 422) {
                    setData([]);
                } else {
                    setPageState(PageState.ERROR);
                }
            });
    }, [startDate, endDate]);

    const onDatesChange = (from: Date, to: Date): void => {
        setStartDate(from);
        setEndDate(to);
    };

    if (pageState === PageState.ERROR) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <TbChartInfographic className="me-1" />
                <Trans>Analytics.CompareWithIndexHeader</Trans>
            </Card.Title>
            <Divider />
            <AnalyticsCompareWithIndexFilters dateFrom={startDate} dateTo={endDate} onDatesChange={onDatesChange} />
            <PageLoader show={pageState === PageState.LOADING} height={"500px"} />
            <AnalyticsCompareWithIndexChart show={pageState === PageState.SUCCESS} data={data} />
        </Card>
    );
};
