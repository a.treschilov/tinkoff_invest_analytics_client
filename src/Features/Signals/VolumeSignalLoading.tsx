import { Skeleton } from "../../components/Common/Skeleton/Skeleton";

const rowNumbers = 4;

type VolumeSignalLoadingProps = {
    show: boolean;
};
export const VolumeSignalLoading = ({ show }: VolumeSignalLoadingProps) => {
    const renderRow = (key: number) => {
        return (
            <tr key={"volume-signal-loading_" + key}>
                <td>
                    <Skeleton className={"h-4 w-2/3"} />
                </td>
                <td>
                    <Skeleton className={"h-4 w-2/3"} />
                </td>
                <td>
                    <Skeleton className={"h-4 w-2/3"} />
                </td>
            </tr>
        );
    };

    if (!show) {
        return <></>;
    }

    return (
        <>
            {[...Array(rowNumbers)].map((_value: number, key: number) => {
                return renderRow(key);
            })}
        </>
    );
};
