import { ApiServiceNext } from "./ApiServiceNext";
import { getCookie } from "../utils/_actions";

export type UserType = {
    userId: number | null;
    userLogin: string | null;
    language: string;
    isNewUser: boolean;
};

export const DefaultUser: UserType = {
    userId: null,
    userLogin: null,
    language: "en",
    isNewUser: true
};

export const UserService = {
    getUser: async function () {
        let user: UserType = DefaultUser;

        const jwt = await getCookie("jwt");

        if (jwt === undefined) {
            return user;
        }

        const userResponse = await ApiServiceNext.fetch("/v1/user/data");

        if (userResponse.status === 200) {
            const json = await userResponse.json();
            user = {
                userId: json.user_id,
                userLogin: json.login,
                language: json.language,
                isNewUser: !!json.isNewUser
            };
        }

        return user;
    }
};
