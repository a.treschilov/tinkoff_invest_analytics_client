import { OperationTypeProps } from "./OperationTypeBadge";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";

export type OperationType =
    | "Unspecified"
    | "Input"
    | "Output"
    | "SecurityIn"
    | "SecurityOut"
    | "Buy"
    | "BuyCard"
    | "Sell"
    | "SellCard"
    | "Repayment"
    | "Dividend"
    | "DividendCard"
    | "Tax"
    | "Fee"
    | "Hold"
    | "UnHold"
    | "Default"
    | "DefaultPayouts";

export const OperationTypeTranslation = ({ operationType, itemType }: OperationTypeProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "operation");

    let label = operationType.toString();

    if (itemType === "loan") {
        switch (operationType) {
            case "DividendCard":
            case "Dividend":
                label = "LoanDividend";
                break;
            case "BuyCard":
            case "Buy":
                label = "LoanInput";
                break;
            case "SellCard":
            case "Sell":
                label = "LoanOutput";
                break;
        }
    } else if (itemType === "deposit") {
        switch (operationType) {
            case "BuyCard":
            case "Buy":
                label = "OperationDepositInput";
                break;
            case "SellCard":
            case "Sell":
                label = "OperationDepositOutput";
                break;
            case "DividendCard":
                label = "OperationDepositDividendCard";
                break;
            case "Dividend":
                label = "OperationDepositDividend";
                break;
        }
    } else if (itemType === "real_estate") {
        switch (operationType) {
            case "SellCard":
            case "Sell":
                label = "OperationRealEstateSell";
                break;
            case "BuyCard":
            case "Buy":
                label = "OperationRealEstateBuy";
                break;
            case "DividendCard":
            case "Dividend":
                label = "OperationRealDividendCard";
                break;
        }
    } else if (itemType === "crowdfunding") {
        switch (operationType) {
            case "Buy":
                label = "OperationCrowdfundingBuy";
                break;
        }
    }

    label = "OperationType." + label;

    return <>{t(label)}</>;
};
