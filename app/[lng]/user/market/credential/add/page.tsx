"use client";

import { BrokerListManage } from "../../../../../../src/Features/Credential/BrokerListManage";
import Link from "next/link";
import { useContext } from "react";
import { ThemeContext } from "../../../../../../src/providers/ThemeProvider";
import { makeLink } from "../../../../../../src/utils/url";
import { useTranslation } from "../../../../../i18n/client";
import { Card } from "../../../../../../src/components/Common/Card/Card";
import { Breadcrumbs } from "../../../../../../src/components/Common/Breadcrumbs/Breadcrumbs";

const CredentialAddPage = () => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "broker");

    return (
        <>
            <Card className={"bg-base-100 p-2 pl-6 mb-2"} border={true}>
                <Breadcrumbs>
                    <li>
                        <Link href={makeLink("/", lng)} className={"link"}>
                            {t("Breadcrumbs.Dashboard")}
                        </Link>
                    </li>
                    <li>
                        <Link href={makeLink("/user/market/credential", lng)} className={"link"}>
                            {t("Breadcrumbs.MyAccounts")}
                        </Link>
                    </li>
                    <li>{t("Breadcrumbs.UploadReport")}</li>
                </Breadcrumbs>
            </Card>
            <BrokerListManage />
        </>
    );
};

export default CredentialAddPage;
