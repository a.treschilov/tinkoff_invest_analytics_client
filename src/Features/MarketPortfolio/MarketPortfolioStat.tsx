import { Trans } from "react-i18next";
import { ComponentState } from "../../components/Common/Types";
import { useEffect, useState } from "react";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Stats } from "../../components/Common/Stats/Stats";

type MarketPortfolioStatProps = {
    amount: number;
    state: ComponentState;
};

const currencyFormatter = new Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB"
});

export const MarketPortfolioStat = ({ state, amount }: MarketPortfolioStatProps) => {
    const [value, setValue] = useState(<></>);

    useEffect(() => {
        switch (state) {
            case "loading":
                setValue(<Skeleton className={"w-60 h-8 my-1"} />);
                break;
            case "success":
                setValue(<>{currencyFormatter.format(amount)}</>);
                break;
        }
    }, [amount, state]);

    return (
        <Stats.Stat className={"place-items-center"}>
            <Stats.Stat.Title>
                <Trans>Portfolio.PageHeader</Trans>
            </Stats.Stat.Title>
            <Stats.Stat.Value>{value}</Stats.Stat.Value>
        </Stats.Stat>
    );
};
