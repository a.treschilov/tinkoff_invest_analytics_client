export interface IDate {
    date: string;
    timezone: string;
}
export function toUserTimezone(dateObject: IDate): Date {
    const date = new Date(dateObject.date.replace(/ /g, "T"));

    const invdate = new Date(
        date.toLocaleString("en-US", {
            timeZone: dateObject.timezone
        })
    );

    const diff = date.getTime() - invdate.getTime();

    return new Date(date.getTime() + diff);
}

export function formatInputDayDate(date: Date, type: "date" | "month" = "date"): string | undefined {
    if (date.toString() === "Invalid Date") {
        return undefined;
    }

    const year = new Intl.DateTimeFormat("en", { year: "numeric" }).format(date);
    const month = new Intl.DateTimeFormat("en", { month: "2-digit" }).format(date);
    const day = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(date);

    switch (type) {
        case "date":
            return year + "-" + month + "-" + day;
        case "month":
            return year + "-" + month;
    }
}

export function formatInputDateTime(date: Date): string {
    let hours = new Intl.DateTimeFormat("ru", { hour: "numeric" }).format(date);
    hours = hours.length === 1 ? "0" + hours : hours;
    let minutes = new Intl.DateTimeFormat("ru", { minute: "numeric" }).format(date);
    minutes = minutes.length === 1 ? "0" + minutes : minutes;

    return (
        new Intl.DateTimeFormat("en", { year: "numeric" }).format(date) +
        "-" +
        new Intl.DateTimeFormat("en", { month: "2-digit" }).format(date) +
        "-" +
        new Intl.DateTimeFormat("en", { day: "2-digit" }).format(date) +
        "T" +
        hours +
        ":" +
        minutes
    );
}

export function formatInputDateTimeSeconds(date: Date): string {
    let hours = new Intl.DateTimeFormat("ru", { hour: "numeric" }).format(date);
    hours = hours.length === 1 ? "0" + hours : hours;
    let minutes = new Intl.DateTimeFormat("ru", { minute: "numeric" }).format(date);
    minutes = minutes.length === 1 ? "0" + minutes : minutes;
    const seconds = new Intl.DateTimeFormat("ru", { second: "2-digit" }).format(date);

    return (
        new Intl.DateTimeFormat("en", { year: "numeric" }).format(date) +
        "-" +
        new Intl.DateTimeFormat("en", { month: "2-digit" }).format(date) +
        "-" +
        new Intl.DateTimeFormat("en", { day: "2-digit" }).format(date) +
        "T" +
        hours +
        ":" +
        minutes +
        ":" +
        seconds
    );
}

export function convertDateToDateString(date: Date): string {
    return (
        date.getFullYear() +
        "-" +
        ("0" + (date.getMonth() + 1).toString()).slice(-2) +
        "-" +
        ("0" + date.getDate().toString()).slice(-2)
    );
}
