import { ItemType } from "../Common/Types";
import { Trans } from "react-i18next";

type ItemNameProps = {
    name: string;
    type?: ItemType | null;
};

export const ItemName = ({ name, type = null }: ItemNameProps) => {
    if (name.trim() !== "") {
        return name;
    }

    switch (type) {
        case "deposit":
            return <Trans>ItemName.DefaultDepositName</Trans>;
        case "real_estate":
            return <Trans>ItemName.DefaultRealEstateName</Trans>;
        case "loan":
            return <Trans>ItemName.DefaultLoanName</Trans>;
        default:
            return <Trans>ItemName.DefaultItemName</Trans>;
    }
};
