import { Skeleton } from "../Common/Skeleton/Skeleton";

export const ItemLogoNameLoading = () => {
    return (
        <div className="flex">
            <Skeleton className={"w-6 h-6 rounded-sm mr-2"} />
            <Skeleton className={"w-1/2 h-6 rounded-sm"} />
        </div>
    );
};
