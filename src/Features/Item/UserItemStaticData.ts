import { IUserItemSummary } from "./UserItemSummary";

export const DefaultSummary: IUserItemSummary = {
    quantity: 0,
    amount: {
        amount: 0,
        currency: "USD"
    },
    investments: {
        amount: 0,
        currency: "USD"
    },
    income: {
        amount: {
            amount: 0,
            currency: "USD"
        }
    },
    tax: {
        amount: {
            amount: 0,
            currency: "USD"
        },
        percent: 0
    },
    fee: {
        amount: {
            amount: 0,
            currency: "USD"
        },
        percent: 0
    },
    profit: {
        amount: {
            amount: 0,
            currency: "USD"
        },
        percent: 0
    },
    valueGrowth: {
        amount: {
            amount: 0,
            currency: "USD"
        },
        percent: 0
    },
    incomeForecast: {
        amount: 0,
        currency: "USD"
    }
};
