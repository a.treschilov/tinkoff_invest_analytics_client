"use client";

import { UserSettings } from "../../../../../src/Features/UserSettings/UserSettings";
import { Trans } from "react-i18next";
import { Divider } from "../../../../../src/components/Common/Divider/Divider";
import { Card } from "../../../../../src/components/Common/Card/Card";

const Page = () => {
    return (
        <Card className={"bg-base-100 p-6"} border={true}>
            <Card.Title>
                <Trans>PortfolioSettings.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <UserSettings />
        </Card>
    );
};

export default Page;
