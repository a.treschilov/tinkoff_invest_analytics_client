import { IDashboardData } from "./Dashboard";

export const DashboardDemoData: IDashboardData = {
    targetCapital: 28000000,
    actives: 9000000,
    passives: 2500000,
    shouldIncrease: 21500000,
    saveSpeed: 35000,
    pension: 48000,
    futurePension: 53500,
    capital: 6500000,
    predictInterest: 380000,
    currentInterest: 290000,
    wealthRate: 3.6,
    safeRatio: 0.2,
    capitalBefore: {
        day: {
            amount: 8500,
            currency: "RUB"
        },
        month: {
            amount: 13400,
            currency: "RUB"
        },
        year: {
            amount: 1650000,
            currency: "RUB"
        }
    }
};

export const DashboardDefaultData: IDashboardData = {
    targetCapital: 0,
    actives: 0,
    passives: 0,
    shouldIncrease: 0,
    saveSpeed: 0,
    pension: 0,
    futurePension: 0,
    capital: 0,
    predictInterest: 0,
    currentInterest: 0,
    wealthRate: null,
    safeRatio: null,
    capitalBefore: {
        day: null,
        month: null,
        year: null
    }
};
