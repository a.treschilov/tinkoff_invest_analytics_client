import { FiMoreHorizontal } from "react-icons/fi";
import { BsFillPencilFill, BsFillTrashFill, BsListUl } from "react-icons/bs";
import { Trans } from "react-i18next";
import { AiOutlineIssuesClose } from "react-icons/ai";
import { useState } from "react";
import { Loan } from "../../components/Item/Item";
import { ItemDelete } from "../Item/Delete/ItemDelete";
import { ItemClose } from "../Item/Close/ItemClose";
import { Dropdown } from "../../components/Common/Dropdown/Dropdown";

type LoanListActionsProps = {
    loan: Loan;
    handleOperationManage: (itemId: number) => void;
    handleEditLoan: (loanId: number) => void;
    handleCloseLoan: () => void;
    handleDeleteLoan: () => void;
};

export const LoanListActions = ({
    loan,
    handleOperationManage,
    handleEditLoan,
    handleCloseLoan,
    handleDeleteLoan
}: LoanListActionsProps) => {
    const [isShowCloseConfirmation, setIsShowCloseConfirmation] = useState(false);
    const [isShowDeleteConfirmation, setIsShowDeleteConfirmation] = useState(false);

    const handleConfirmLoanClose = () => {
        setIsShowCloseConfirmation(false);
        handleCloseLoan();
    };

    return (
        <>
            <Dropdown key={"loanActions-" + loan.loanId} placement={"left"}>
                <Dropdown.Toggle>
                    <FiMoreHorizontal></FiMoreHorizontal>
                </Dropdown.Toggle>
                <Dropdown.Menu className={"z-50 w-44"}>
                    <Dropdown.Item onClick={() => handleOperationManage(loan.itemId === null ? 0 : loan.itemId)}>
                        <BsListUl className="me-2" />
                        <Trans>LoanManage.ItemOperation</Trans>
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => handleEditLoan(loan.loanId === null ? 0 : loan.loanId)}>
                        <BsFillPencilFill className="me-2" />
                        <Trans>LoanManage.ItemEdit</Trans>
                    </Dropdown.Item>
                    <Dropdown.Item
                        onClick={() => {
                            setIsShowCloseConfirmation(true);
                        }}
                    >
                        <AiOutlineIssuesClose className="me-2" />
                        <Trans>LoanManage.ItemClose</Trans>
                    </Dropdown.Item>
                    <Dropdown.Item
                        onClick={() => {
                            setIsShowDeleteConfirmation(true);
                        }}
                    >
                        <BsFillTrashFill
                            style={{ cursor: "pointer" }}
                            title="Delete deposit"
                            className="me-2 text-warning"
                        />
                        <span className="text-warning">
                            <Trans>LoanManage.ItemDelete</Trans>
                        </span>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <ItemClose
                itemId={loan.itemId ?? 0}
                show={isShowCloseConfirmation}
                onSuccess={handleConfirmLoanClose}
                onDiscard={() => {
                    setIsShowCloseConfirmation(false);
                }}
            />
            <ItemDelete
                itemId={loan.itemId ?? 0}
                show={isShowDeleteConfirmation}
                onSuccess={() => handleDeleteLoan()}
                onDiscard={() => {
                    setIsShowDeleteConfirmation(false);
                }}
            />
        </>
    );
};
