"use client";

import { FooterComponent } from "../../components/Footer/FooterComponent";
import { TFunction } from "i18next";
import { Trans } from "react-i18next";
import Image from "next/image";
import { FaTelegram } from "react-icons/fa6";
import { FaGoogle, FaYandex } from "react-icons/fa";
import { getSocialAccountUrl } from "./socialAccounts";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

export const AuthForm = ({ t, lng }: { t: TFunction; lng: string }) => {
    const redirectUrl = "/auth/redirect";

    return (
        <>
            <div className={"container mx-auto pt-4 pl-4 pr-4 grow"}>
                <div>
                    <Image
                        src="/images/logo_white.webp"
                        alt="Hakkes"
                        width={89}
                        height={14}
                        style={{ width: "113px" }}
                    />
                    <div className="max-w-2xl mx-auto prose text-center mt-8">
                        <h1 className={"text-4xl font-bold"}>
                            <Trans i18nKey={"PageHeader"} t={t} />
                        </h1>
                    </div>
                    <div className="max-w-2xl mx-auto mt-8">
                        <Card className={"shadow-xl"} border={true}>
                            <Card.Body>
                                <Card.Title className={"mx-auto mb-4"}>
                                    <Trans t={t}>SignInBlockHeader</Trans>
                                </Card.Title>
                                <Button
                                    color={"primary"}
                                    size={"lg"}
                                    onClick={() => (window.location.href = getSocialAccountUrl("GOOGLE", redirectUrl))}
                                >
                                    <FaGoogle className="w-6 h-6" />
                                    <Trans i18nKey="SignInGoogle" t={t}></Trans>
                                </Button>
                                <Button
                                    color={"primary"}
                                    size={"lg"}
                                    onClick={() => (window.location.href = getSocialAccountUrl("YANDEX", redirectUrl))}
                                >
                                    <FaYandex className="w-6 h-6" />
                                    <Trans t={t}>SignInYandex</Trans>
                                </Button>
                                <Button
                                    color={"primary"}
                                    size={"lg"}
                                    onClick={() => {
                                        window.location.href = getSocialAccountUrl("TELEGRAM", redirectUrl);
                                    }}
                                >
                                    <FaTelegram className={"w-6 h-6"} />
                                    <Trans t={t}>SignInTelegram</Trans>
                                </Button>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </div>
            <FooterComponent lng={lng} />
        </>
    );
};
