import { ItemType } from "../../components/Common/Types";
import { Trans } from "react-i18next";
import { ChangeEvent, useEffect, useState } from "react";
import { OperationType, OperationTypeTranslation } from "../OperationList/OperationTypeTranslation";
import Select from "../../components/Common/Form/Select";

type OperationAddTypeSelectProps = {
    itemType: ItemType;
    defaultValue: string;
    onChange: (event: ChangeEvent<HTMLSelectElement>) => void;
};
export const OperationAddTypeSelect = ({ itemType, defaultValue, onChange }: OperationAddTypeSelectProps) => {
    const [list, setList] = useState<OperationType[]>([]);

    useEffect(() => {
        switch (itemType) {
            case "deposit":
                setList(["Dividend", "BuyCard", "SellCard"]);
                break;
            case "loan":
                setList(["SellCard", "Dividend", "BuyCard"]);
                break;
            case "crowdfunding":
                setList(["Repayment", "Dividend", "Buy", "Default", "DefaultPayouts"]);
                break;
            case "market":
                setList(["Buy", "Sell", "Dividend", "Repayment", "Tax", "Fee"]);
                break;
            default:
                setList([]);
        }
    }, [itemType, defaultValue, onChange]);

    if (list.length === 0) {
        return <></>;
    }

    return (
        <>
            <Select>
                <Select.Label>
                    <Trans>OperationAdd.FormLabelOperationType</Trans>
                </Select.Label>
                <Select.Select name="operationType" onChange={event => onChange(event)} defaultValue={defaultValue}>
                    {list.map((type: OperationType) => {
                        return (
                            <option value={type} key={"type-" + type}>
                                <OperationTypeTranslation operationType={type} itemType={itemType} />
                            </option>
                        );
                    })}
                </Select.Select>
            </Select>
        </>
    );
};
