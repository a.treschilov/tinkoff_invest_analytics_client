import { Button, Card, Divider } from "react-daisyui";
import { Trans } from "react-i18next";

type RealEstateEmptyListProps = {
    show: boolean;
    onAddDeposit: () => void;
};
export const RealEstateEmptyList = ({ show, onAddDeposit }: RealEstateEmptyListProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
            <Card.Title tag="h2">
                <Trans>RealEstateManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="text-center my-4">
                <Button size="lg" color={"primary"} onClick={onAddDeposit}>
                    <Trans>RealEstateManage.AddFirstRealEstateButton</Trans>
                </Button>
            </div>
        </Card>
    );
};
