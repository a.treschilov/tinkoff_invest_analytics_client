import { Trans } from "react-i18next";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Card } from "../../components/Common/Card/Card";
import { Divider } from "../../components/Common/Divider/Divider";

type OperationListLoadingProps = {
    show: boolean;
};
export const OperationListLoading = ({ show }: OperationListLoadingProps) => {
    if (!show) {
        return <></>;
    }

    const renderRow = (rowNumber: number) => {
        return (
            <tr key={"operation-list-loading-row-" + rowNumber}>
                <td>
                    <Skeleton className={"w-4 h-4 inline-block rounded-md mr-1.5"} />
                    <Skeleton className={"w-[160px] h-4 inline-block"} />
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-[80px] h-4"} />
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-[120px] h-4"} />
                </td>
                <td className="hidden lg:table-cell">
                    <Skeleton className={"w-[80px] h-4"} />
                </td>
                <td className="hidden sm:table-cell">
                    <Skeleton className={"w-[30px] h-4"} />
                </td>
                <td className="hidden sm:table-cell">
                    <Skeleton className={"w-[80px] h-4"} />
                </td>
                <td className="hidden sm:table-cell">
                    <Skeleton className={"w-[90px] h-4"} />
                </td>
                <td className="hidden sm:table-cell">
                    <Skeleton className={"w-[40px] h-4"} />
                </td>
                <td className="sm:hidden">
                    <Skeleton className={"w-[60px] h-4 mb-1"} />
                    <Skeleton className={"w-[85px] h-3 mb-1"} />
                    <Skeleton className={"w-[50px] h-3"} />
                </td>
            </tr>
        );
    };
    const renderHeader = () => {
        return (
            <thead>
                <tr>
                    <td>
                        <Skeleton className={"w-[30px] h-4"} />
                    </td>
                    <td className="hidden lg:table-cell">
                        <Skeleton className={"w-[50px] h-4"} />
                    </td>
                    <td className="hidden lg:table-cell">
                        <Skeleton className={"w-[120px] h-4"} />
                    </td>
                    <td className="hidden lg:table-cell">
                        <Skeleton className={"w-[100px] h-4"} />
                    </td>
                    <td className="hidden sm:table-cell">
                        <Skeleton className={"w-[100px] h-4"} />
                    </td>
                    <td className="hidden sm:table-cell">
                        <Skeleton className={"w-[70px] h-4"} />
                    </td>
                    <td className="hidden sm:table-cell">
                        <Skeleton className={"w-[50px] h-4"} />
                    </td>
                    <td className="hidden sm:table-cell">
                        <Skeleton className={"w-[70px] h-4"} />
                    </td>
                    <td className="sm:hidden">
                        <Skeleton className={"w-[40px] h-4"} />
                    </td>
                </tr>
            </thead>
        );
    };

    const renderFooter = () => {
        return (
            <tfoot>
                <tr>
                    <th>
                        <Skeleton className={"w-[80px] h-4"} />
                    </th>
                    <th className="hidden lg:table-cell" colSpan={4}></th>
                    <th colSpan={3}>
                        <Skeleton className={"w-[100px] h-4"} />
                    </th>
                </tr>
            </tfoot>
        );
    };

    const renderBody = () => {
        const rowNumbers = 7;
        return (
            <tbody>
                {[...Array(rowNumbers)].map((value: number, key: number) => {
                    return renderRow(key);
                })}
            </tbody>
        );
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>Operations.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <table className="table w-full">
                {renderHeader()}
                {renderBody()}
                {renderFooter()}
            </table>
        </Card>
    );
};
