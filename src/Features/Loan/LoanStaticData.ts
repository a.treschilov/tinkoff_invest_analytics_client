import { formatInputDayDate } from "../../utils/date";
import { Loan } from "../../components/Item/Item";

const formattedDate = formatInputDayDate(new Date()) || "";

export const LoanDataDefault: Loan = {
    loanId: null,
    itemId: null,
    name: "",
    amount: 0,
    currency: "RUB",
    isActive: 1,
    date: {
        date: formattedDate,
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
    },
    closedDate: null,
    paymentAmount: 0,
    paymentCurrency: "RUB",
    payoutFrequency: 1,
    payoutFrequencyPeriod: "month",
    interestPercent: 0
};
