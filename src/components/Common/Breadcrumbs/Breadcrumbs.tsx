import { HTMLAttributes, ReactNode } from "react";

type BreadcrumbsProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const Breadcrumbs = ({ children, className, ...args }: BreadcrumbsProps) => {
    const classes = ["breadcrumbs", "text-sm"];
    if (className) {
        classes.push(...className.split(" "));
    }
    return (
        <div className={classes.join(" ")} {...args}>
            <ul>{children}</ul>
        </div>
    );
};
