"use client";

import { ApiService } from "../../../../../src/services/ApiService";
import { getDomain } from "../../../../../src/utils/url";
import { useEffect, use } from "react";
import { clientRedirect } from "../../../../../src/utils/_actions";
import { setCookie } from "cookies-next";
import { LogoLoader } from "../../../../../src/components/Loader/LogoLoader";
import { usePathname } from "next/navigation";
import { parseToken } from "../../../../../src/Features/Auth/socialAccounts";

type AuthResponseType = {
    jwt: string;
};

const Page = (props: { params: Promise<{ provider: string }> }) => {
    const params = use(props.params);
    const pathname = usePathname();

    useEffect(() => {
        const hash = window.location.href.split("#")[1];
        const socialCode = params.provider.toUpperCase();

        const token = parseToken(socialCode, hash);

        const path = pathname.split("/");
        const lng = path[1];

        const data = {
            auth_type: socialCode,
            access_token: token,
            lng: lng
        };

        ApiService.post("/v1/auth/authorization", data)
            .then((responseData: AuthResponseType) => {
                const expireCookieDate = new Date();
                expireCookieDate.setTime(expireCookieDate.getTime() + 1000 * 60 * 60 * 24 * 30); //30 days

                setCookie("jwt", responseData.jwt, {
                    path: "/",
                    expires: expireCookieDate,
                    domain: "." + getDomain()
                });
            })
            .catch(error => {
                console.log(error);
            })
            .then(() => {
                clientRedirect("/?auth_redirect=1").then();
            });
    }, [params.provider, pathname]);

    return <LogoLoader />;
};

export default Page;
