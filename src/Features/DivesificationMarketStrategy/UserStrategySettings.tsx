import { Fragment, ReactNode, useCallback, useEffect, useState } from "react";
import { PageState, StrategyTypes } from "../../components/Common/Types";
import { Trans } from "react-i18next";
import { ApiService } from "../../services/ApiService";
import { PageLoader } from "../../components/Loader/Loaders";
import { UserStrategySettingsProgressBar } from "./UserStrategySettingsProgressBar";
import { UserStrategySettingsSaveButton } from "./UserStrategySettingsSaveButton";
import { UserStrategyItemTypeValueType, UserStrategyPredefined } from "./UserStrategyPredefined";
import { InputText } from "../../components/Common/Form/InputText";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";
import { Alert } from "../../components/Common/Alert/Alert";

type UserStrategySettingsProps = {
    type: StrategyTypes;
};

interface ITargetShare {
    externalId: string;
    name: string;
    value: number;
}

interface IStrategies {
    [StrategyTypes.STOCK]: IStrategyData;
    [StrategyTypes.CURRENCY]: IStrategyData;
    [StrategyTypes.CURRENCY_BALANCE]: IStrategyData;
    [StrategyTypes.COUNTRY]: IStrategyData;
    [StrategyTypes.SECTOR]: IStrategyData;
}

const strategyTypes: IStrategies = {
    [StrategyTypes.STOCK]: {
        apiPath: "stock",
        pageTitle: <Trans>UserStrategySettings.PageHeaderInstrumentTypeStrategy</Trans>,
        pageInfoText: <Trans>UserStrategySettings.DescriptionInstrumentTypeStrategy</Trans>
    },
    [StrategyTypes.CURRENCY]: {
        apiPath: "currency",
        pageTitle: <Trans>UserStrategySettings.PageHeaderInstrumentCurrencyStrategy</Trans>,
        pageInfoText: <Trans>UserStrategySettings.DescriptionInstrumentCurrencyStrategy</Trans>
    },
    [StrategyTypes.CURRENCY_BALANCE]: {
        apiPath: "currencyBalance",
        pageTitle: <Trans>UserStrategySettings.PageHeaderCurrencyBalanceStrategy</Trans>,
        pageInfoText: <Trans>UserStrategySettings.DescriptionCurrencyBalanceStrategy</Trans>
    },
    [StrategyTypes.COUNTRY]: {
        apiPath: "country",
        pageTitle: <Trans>UserStrategySettings.PageHeaderInstrumentCountryStrategy</Trans>,
        pageInfoText: <Trans>UserStrategySettings.DescriptionInstrumentCountryStrategy</Trans>
    },
    [StrategyTypes.SECTOR]: {
        apiPath: "sector",
        pageTitle: <Trans>UserStrategySettings.PageHeaderInstrumentSectorStrategy</Trans>,
        pageInfoText: <Trans>UserStrategySettings.DescriptionInstrumentSectorStrategy</Trans>
    }
};

interface IStrategyData {
    apiPath: string;
    pageTitle: ReactNode;
    pageInfoText: ReactNode;
}

export const UserStrategySettings = ({ type }: UserStrategySettingsProps) => {
    const [shares, setShares] = useState<ITargetShare[]>([]);
    const [sumShares, setSumShares] = useState(0);
    const [pageState, setPageState] = useState(PageState.LOADING);

    const initPageData = useCallback(() => {
        setPageState(PageState.LOADING);

        ApiService.fetch("/v1/user/strategy/" + strategyTypes[type].apiPath).then((response: ITargetShare[]) => {
            updateSharesFromPercent(response);
            setPageState(PageState.EDIT_FORM);
        });
    }, [type]);

    useEffect(() => {
        initPageData();
    }, [initPageData]);

    const handleSetStrategy = (strategy: UserStrategyItemTypeValueType[]): void => {
        let currentShares = shares;
        currentShares = currentShares.map(el => {
            const strategyItem = strategy.find(strategyItem => {
                return strategyItem.id === el.externalId;
            });

            el.value = strategyItem === undefined ? 0 : strategyItem.value;
            return el;
        });
        updateSharesFromPercent(currentShares);
    };

    const handleShareValueChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const currentShares: ITargetShare[] = shares.map((share: ITargetShare) => {
            return event.target.name === share.name
                ? {
                      name: share.name,
                      externalId: share.externalId,
                      value: parseFloat(event.target.value === "" ? "0" : event.target.value)
                  }
                : share;
        });
        const sumShares = currentShares.reduce((accum: number, share: ITargetShare) => {
            return accum + share.value;
        }, 0);

        setShares(currentShares);
        setSumShares(parseFloat(sumShares.toFixed(2)));
    };

    const handleSubmitForm = (event: React.ChangeEvent<HTMLFormElement>) => {
        const requestedShares = shares.map((share: ITargetShare) => {
            return {
                externalId: share.externalId,
                value: parseFloat((share.value / 100).toFixed(4))
            };
        });

        setPageState(PageState.SAVING_DATA);
        ApiService.post("/v1/user/strategy/" + strategyTypes[type].apiPath, requestedShares)
            .then(() => {
                setPageState(PageState.SUCCESS);
            })
            .catch(() => {
                setPageState(PageState.ERROR);
            });

        event.preventDefault();
    };

    const updateSharesFromPercent = (response: ITargetShare[]) => {
        const shares = response.map((share: ITargetShare) => {
            return {
                name: share.name,
                externalId: share.externalId,
                value: parseFloat((share.value * 100).toFixed(2))
            };
        });
        const sumShares: number = shares.reduce((accum: number, share: ITargetShare) => {
            return accum + share.value;
        }, 0);

        setShares(shares);
        setSumShares(parseFloat(sumShares.toFixed(2)));
    };

    const renderEditForm = () => {
        return (
            <div className={"grid grid-cols-1 gap-6"}>
                <div>
                    <Alert status="info" className={"text-left"}>
                        <Trans>{strategyTypes[type].pageInfoText}</Trans>
                    </Alert>
                </div>
                <div>
                    <UserStrategyPredefined type={type} handleSetStrategy={handleSetStrategy} />
                </div>
                <div className="ps-0 pe-0">
                    <UserStrategySettingsProgressBar progress={sumShares} />
                </div>
                <div>
                    <form onSubmit={handleSubmitForm}>
                        <div className={"grid grid-cols-12 gap-2 mb-6"}>
                            {shares.length > 0 ? (
                                shares.map((share: ITargetShare) => {
                                    return (
                                        <Fragment key={"strategy_" + share.externalId}>
                                            <InputText
                                                label={<>{share.name}</>}
                                                type="number"
                                                step="0.01"
                                                name={share.name}
                                                value={share.value}
                                                onChange={handleShareValueChange}
                                                containerClassName={
                                                    "lg:col-span-2 md:col-span-3 sm:col-span-4 col-span-6"
                                                }
                                            />
                                        </Fragment>
                                    );
                                })
                            ) : (
                                <div className={"col-span-12"}>
                                    <Trans>UserStrategySettings.DataNotFound</Trans>
                                </div>
                            )}
                        </div>
                        <div>
                            <UserStrategySettingsSaveButton status={pageState} />
                        </div>
                    </form>
                </div>
            </div>
        );
    };

    let content: ReactNode;
    switch (pageState) {
        case PageState.LOADING:
            content = <PageLoader />;
            break;
        default:
            content = renderEditForm();
            break;
    }

    return (
        <div className="mb-6">
            <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                <Card.Title>{strategyTypes[type].pageTitle}</Card.Title>
                <Divider />
                {content}
            </Card>
        </div>
    );
};
