"use client";

import { JSX, useContext } from "react";
import { ActiveVolumeSignal } from "../../../../../src/Features/Signals/ActiveVolumeSignal";
import { useTranslation } from "../../../../i18n/client";
import { ThemeContext } from "../../../../../src/providers/ThemeProvider";
import { OutdatedVolumeSignal } from "../../../../../src/Features/Signals/OutdatedSignal";
import { Trans } from "react-i18next";
import Link from "next/link";
import { Divider } from "../../../../../src/components/Common/Divider/Divider";
import { Card } from "../../../../../src/components/Common/Card/Card";

const Page = (): JSX.Element => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/signals");
    return (
        <>
            <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                <Card.Title>{t("VolumeSignal.Title")}</Card.Title>
                <Divider />
                <p className={"mb-2"}>{t("VolumeSignal.Description")}</p>
                <p>
                    <Trans
                        ns={"features/signals"}
                        components={{ Link: <Link href={"/" + lng + "/user/settings/interface"} className={"link"} /> }}
                    >
                        VolumeSignal.Subscribe
                    </Trans>
                </p>
            </Card>
            <ActiveVolumeSignal />
            <OutdatedVolumeSignal />
        </>
    );
};

export default Page;
