"use client";

import { ILoanPortfolioItem } from "./LoanManage";
import { toUserTimezone } from "../../utils/date";
import { Trans } from "react-i18next";
import { ItemName } from "../../components/Item/ItemName";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { SessionContext } from "../../providers/SessionProvider";
import { makeLink } from "../../utils/url";
import Link from "next/link";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

type LoanListClosedProps = {
    loans: ILoanPortfolioItem[];
    isShow: boolean;
};

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
    year: "numeric",
    month: "numeric",
    day: "numeric"
});

const percentFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
    style: "percent",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

export const LoanListClosed = ({ loans, isShow }: LoanListClosedProps) => {
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    if (!isShow) {
        return <></>;
    }

    const renderLoanList = () => {
        return loans.map((loan: ILoanPortfolioItem) => {
            const currencyAmountFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                style: "currency",
                currency: loan.loan.currency
            });
            const currencyOverPaymentFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                style: "currency",
                currency: loan.overPayment.price.currency
            });

            return (
                <tr key={"loan-closer-" + loan.loan.itemId}>
                    <td>
                        <Link
                            href={makeLink("/user/item/" + loan.loan.itemId, lng)}
                            onClick={() => updatePreviousPage("loan")}
                            className={"link"}
                        >
                            <ItemName name={loan.loan.name} type={"loan"} />
                        </Link>
                    </td>
                    <td>{currencyAmountFormatter.format(loan.loan.amount)}</td>
                    <td className="hidden sm:table-cell">
                        {currencyOverPaymentFormatter.format(loan.overPayment.price.amount)} (
                        {percentFormatter.format(loan.overPayment.percent)})
                    </td>
                    <td className="hidden md:table-cell">
                        {dateFormatter.format(toUserTimezone(loan.loan.date))} &mdash;{" "}
                        {loan.loan.closedDate === null
                            ? "n/a"
                            : dateFormatter.format(toUserTimezone(loan.loan.closedDate))}
                    </td>
                </tr>
            );
        });
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>LoanManage.ClosedLoansTitle</Trans>
            </Card.Title>
            <Divider />
            <table className="table w-full">
                <thead>
                    <tr>
                        <th>
                            <Trans>LoanManage.ClosedLoansTableHeaderName</Trans>
                        </th>
                        <th>
                            <Trans>LoanManage.ClosedLoansTableHeaderAmount</Trans>
                        </th>
                        <th className="hidden sm:table-cell">
                            <Trans>LoanManage.ClosedLoansTableHeaderOverPayment</Trans>
                        </th>
                        <th className="hidden md:table-cell">
                            <Trans>LoanManage.ClosedLoansTableHeaderPeriod</Trans>
                        </th>
                    </tr>
                </thead>
                <tbody>{renderLoanList()}</tbody>
            </table>
        </Card>
    );
};
