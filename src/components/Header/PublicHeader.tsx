import Link from "next/link";
import Image from "next/image";
import { HeaderButton } from "./HeaderButton";

export const PublicHeader = () => {
    return (
        <div className={"mb-6 grid grid-cols-2"}>
            <div>
                <Link href="/">
                    <Image
                        src="/images/logo_white.webp"
                        width={200}
                        height={32}
                        alt="Hakkes - онлайн помошник долгосрочного инвестора"
                    />
                </Link>
            </div>
            <div className={"text-right"}>
                <HeaderButton />
            </div>
        </div>
    );
};
