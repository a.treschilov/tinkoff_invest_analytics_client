import { Trans } from "react-i18next";
import { RealEstateActionType, RealEstateListActions } from "./RealEstateListActions";
import { ItemName } from "../../components/Item/ItemName";
import { useContext } from "react";
import { SessionContext } from "../../providers/SessionProvider";
import Link from "next/link";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import { RealEstate, RealEstatePrice } from "../../components/Item/Item";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

type RealEstateListProps = {
    isShow: boolean;
    realEstateList: RealEstate[];
    handleOpenEditForm: (realEstateId: number) => void;
    handleOpenOperationForm: (itemId: number) => void;
    handleActionComplete: (action: RealEstateActionType) => void;
};

export const RealEstateList = ({
    isShow,
    realEstateList,
    handleOpenEditForm,
    handleOpenOperationForm,
    handleActionComplete
}: RealEstateListProps) => {
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    const getLastPrice = (realEstatePriceList: RealEstatePrice[]): RealEstatePrice | null => {
        if (realEstatePriceList.length === 0) {
            return null;
        }

        realEstatePriceList.sort((el1: RealEstatePrice, el2: RealEstatePrice) => {
            return el1.date.date < el2.date.date ? 1 : -1;
        });

        return realEstatePriceList[0];
    };

    const renderList = () => {
        return realEstateList.map((realEstate: RealEstate) => {
            const price = getLastPrice(realEstate.prices);
            const priceCurrencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                style: "currency",
                currency: price?.currency || "RUB",
                maximumSignificantDigits: 3,
                notation: "compact"
            });
            const interestCurrencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                style: "currency",
                currency: realEstate.interestCurrency
            });
            return (
                <tr key={"realEstate-" + realEstate.itemId}>
                    <td>
                        <Link
                            href={makeLink("/user/item/" + realEstate.itemId, lng)}
                            onClick={() => updatePreviousPage("realEstate")}
                            className={"link"}
                        >
                            <ItemName name={realEstate.name} type={"real_estate"} />
                        </Link>
                        <br className="sm:hidden" />
                        <span className="sm:hidden">
                            {price === null ? <span>&mdash;</span> : priceCurrencyFormatter.format(price.amount)}
                        </span>
                    </td>
                    <td className="hidden sm:table-cell">
                        {price === null ? <span>&mdash;</span> : priceCurrencyFormatter.format(price.amount)}
                    </td>
                    <td className="hidden md:table-cell">
                        {interestCurrencyFormatter.format(realEstate.interestAmount)}
                    </td>
                    <td className="hidden lg:table-cell">
                        {realEstate.payoutFrequency + " " + realEstate.payoutFrequencyPeriod}
                    </td>
                    <td>
                        <RealEstateListActions
                            realEstate={realEstate}
                            handleEditRealEstate={handleOpenEditForm}
                            handleOperationManage={handleOpenOperationForm}
                            handleActionComplete={handleActionComplete}
                        />
                    </td>
                </tr>
            );
        });
    };

    if (!isShow) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>RealEstateManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="w-full">
                <table className="table w-full">
                    <thead>
                        <tr>
                            <th>
                                <Trans>RealEstateManage.TableHeaderName</Trans>
                            </th>
                            <th className="hidden sm:table-cell">
                                <Trans>RealEstateManage.TableHeaderAmount</Trans>
                            </th>
                            <th className="hidden md:table-cell">
                                <Trans>RealEstateManage.TableHeaderInterest</Trans>
                            </th>
                            <th className="hidden lg:table-cell">
                                <Trans>RealEstateManage.TableHeaderPayoutFrequency</Trans>
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>{renderList()}</tbody>
                </table>
            </div>
        </Card>
    );
};
