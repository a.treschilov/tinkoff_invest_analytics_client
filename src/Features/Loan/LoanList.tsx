import { ILoanPortfolioItem, loanActionList } from "./LoanManage";
import { Trans } from "react-i18next";
import { LoanListActions } from "./LoanListActions";
import { ItemName } from "../../components/Item/ItemName";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { SessionContext } from "../../providers/SessionProvider";
import { makeLink } from "../../utils/url";
import Link from "next/link";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

const percentFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
    style: "percent",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

type LoanListPropsType = {
    isShow: boolean;
    loans: ILoanPortfolioItem[];
    handleEditLoan: (loanId: number) => void;
    handleOperationManage: (itemId: number) => void;
    handleEventComplete: (eventName: loanActionList) => void;
};
export const LoanList = ({
    isShow,
    loans,
    handleEditLoan,
    handleOperationManage,
    handleEventComplete
}: LoanListPropsType) => {
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    const renderLoanList = () => {
        {
            return loans.map((loan: ILoanPortfolioItem) => {
                const currencyAmountFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                    style: "currency",
                    currency: loan.loan.currency
                });
                const currencyBalanceFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                    style: "currency",
                    currency: loan.currency
                });
                const paymentFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                    style: "currency",
                    currency: loan.loan.paymentCurrency
                });
                const currencyOverpaymentFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                    style: "currency",
                    currency: loan.overPayment.price.currency
                });
                return (
                    <tr key={"loan-" + loan.loan.loanId}>
                        <td>
                            <Link
                                href={makeLink("/user/item/" + loan.loan.itemId, lng)}
                                onClick={() => updatePreviousPage("loan")}
                                className={"link"}
                            >
                                <ItemName name={loan.loan.name} type={"loan"} />
                            </Link>
                            <br className="sm:hidden" />
                            <span className="sm:hidden">{currencyBalanceFormatter.format(-1 * loan.amount + 0)}</span>
                        </td>
                        <td className="hidden sm:table-cell">
                            {currencyBalanceFormatter.format(-1 * loan.amount + 0)}
                        </td>
                        <td className="hidden sm:table-cell">
                            {currencyOverpaymentFormatter.format(loan.overPayment.price.amount)} (
                            {percentFormatter.format(loan.overPayment.percent)})
                        </td>
                        <td className="hidden lg:table-cell">{currencyAmountFormatter.format(loan.loan.amount)}</td>
                        <td className="hidden md:table-cell">{paymentFormatter.format(loan.loan.paymentAmount)}</td>
                        <td>
                            <LoanListActions
                                loan={loan.loan}
                                handleOperationManage={handleOperationManage}
                                handleEditLoan={handleEditLoan}
                                handleCloseLoan={() => handleEventComplete("close")}
                                handleDeleteLoan={() => handleEventComplete("delete")}
                            />
                        </td>
                    </tr>
                );
            });
        }
    };

    if (!isShow) {
        return <></>;
    }
    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>LoanManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="w-full">
                <table className="table w-full">
                    <thead>
                        <tr>
                            <th>
                                <Trans>LoanManage.TableHeaderName</Trans>
                            </th>
                            <th className="hidden sm:table-cell">
                                <Trans>LoanManage.TableHeaderBalance</Trans>
                            </th>
                            <th className="hidden sm:table-cell">
                                <Trans>LoanManage.TableHeaderOverPayment</Trans>
                            </th>
                            <th className="hidden lg:table-cell">
                                <Trans>LoanManage.TableHeaderAmount</Trans>
                            </th>
                            <th className="hidden md:table-cell">
                                <Trans>LoanManage.TableHeaderPayment</Trans>
                            </th>
                            <th className={"w-12"}></th>
                        </tr>
                    </thead>
                    <tbody>{renderLoanList()}</tbody>
                </table>
            </div>
        </Card>
    );
};
