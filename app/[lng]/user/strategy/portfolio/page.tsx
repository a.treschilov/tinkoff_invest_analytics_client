"use client";

import { UserSettings } from "../../../../../src/Features/UserSettings/UserSettings";
import { Trans } from "react-i18next";
import { Card, Divider } from "react-daisyui";

const Page = () => {
    return (
        <Card className={"bg-base-100 p-6"} bordered={true}>
            <Card.Title tag="h2">
                <Trans>PortfolioSettings.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <UserSettings />
        </Card>
    );
};

export default Page;
