import { NextResponse } from "next/server";

type healthNodeResponseType = {
    error: boolean;
    code: number;
    message: string;
};

export async function GET() {
    return NextResponse.json({
        app: createMessage(false),
        api: await checkAppApi()
    });
}

const checkAppApi = async () => {
    try {
        const res = await fetch(process.env.NEXT_PUBLIC_API_HOST + "/v1/health_check/up", { cache: "no-store" });
        if (!res.ok) {
            return createMessage(true, res.status, res.statusText);
        }

        const data: healthNodeResponseType = await res.json();
        return createMessage(data.error, data.code, data.message);
    } catch (error: unknown) {
        return createMessage(true, 500, (error as Error).message);
    }
};

const createMessage = (error: boolean, code: number = 0, message: string = "OK"): healthNodeResponseType => {
    return {
        error: error,
        code: code,
        message: message
    };
};
