import { ItemType } from "../../components/Common/Types";
import { OperationType, OperationTypeTranslation } from "./OperationTypeTranslation";
import { Badge } from "../../components/Common/Badge/Badge";

export type OperationTypeProps = {
    operationType: OperationType;
    itemType: ItemType | null;
};

export const OperationTypeBadge = ({ operationType, itemType }: OperationTypeProps) => {
    let color:
        | "neutral"
        | "primary"
        | "secondary"
        | "accent"
        | "ghost"
        | "info"
        | "success"
        | "warning"
        | "error"
        | undefined;

    switch (operationType) {
        case "Dividend":
        case "DividendCard":
            color = "success";
            break;
        default:
            color = "primary";
    }

    return (
        <Badge color={color}>
            <OperationTypeTranslation operationType={operationType} itemType={itemType} />
        </Badge>
    );
};
