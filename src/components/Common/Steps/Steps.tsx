import { FC, HTMLAttributes, ReactNode } from "react";
import { Step } from "./Step";

type StepsType = {
    children?: ReactNode;
} & HTMLAttributes<HTMLUListElement>;

type StepsExtension = {
    Step: typeof Step;
};

export const Steps: FC<StepsType> & StepsExtension = ({ children, className, ...args }: StepsType) => {
    const classes = ["steps"];
    if (className) {
        classes.push(...className.split(" "));
    }

    return (
        <ul className={classes.join(" ")} {...args}>
            {children}
        </ul>
    );
};

Steps.Step = Step;
