import { ReactNode, TextareaHTMLAttributes } from "react";
import { Color, Size } from "../Types";
import { daisyUIColorClass, daisyUISizeClass } from "../utils";

export type TextareaProps = {
    children?: ReactNode;
    color?: Color;
    size?: Size;
    ghost?: boolean;
} & TextareaHTMLAttributes<HTMLTextAreaElement>;

export const Textarea = ({ children, color, size = "md", ghost, className, ...args }: TextareaProps) => {
    const classes = ["textarea"];
    if (className) {
        classes.push(...className?.split(" "));
    }
    classes.push(daisyUISizeClass("textarea", size));
    classes.push(daisyUIColorClass("textarea", color));
    if (ghost) {
        classes.push("textarea-ghost");
    }

    return (
        <textarea className={classes.join(" ")} {...args}>
            {children}
        </textarea>
    );
};
