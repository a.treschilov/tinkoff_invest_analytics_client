import { HTMLAttributes, ReactNode } from "react";

type DropdownItemProps = {
    children: ReactNode;
} & HTMLAttributes<HTMLAnchorElement>;

export const DropdownItem = ({ children, ...args }: DropdownItemProps) => {
    return (
        <li>
            <a {...args}>{children}</a>
        </li>
    );
};
