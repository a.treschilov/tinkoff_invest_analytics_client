export const getDomain = () => {
    return document.location.hostname.split(".").reverse().splice(0, 2).reverse().join(".");
};

export const makeLink = (url: string, lng: string) => {
    return "/" + lng + url;
};
