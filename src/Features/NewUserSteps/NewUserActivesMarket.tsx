import { useState } from "react";
import { CredentialList } from "../Credential/CredentialList";
import { Trans } from "react-i18next";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

export type NewUserActivesComponentProps = {
    onComplete?: () => void;
    show: boolean;
};

export type NewUserActivesMarketStepProps = {
    show: boolean;
    onNegative?: () => void;
    onPositive?: () => void;
};
export const NewUserActivesMarket = ({ show, onComplete }: NewUserActivesComponentProps) => {
    const [marketStep, setMarketStep] = useState(1);

    if (!show) {
        return <></>;
    }
    return (
        <>
            <NewUserActivesMarketStep1
                show={marketStep === 1}
                onNegative={onComplete}
                onPositive={() => {
                    setMarketStep(2);
                }}
            />
            <NewUserActivesMarketStep2
                show={marketStep === 2}
                onNegative={onComplete}
                onPositive={() => {
                    setMarketStep(3);
                }}
            />
            <NewUserActivesMarketStep3 show={marketStep === 3} onPositive={onComplete} />
        </>
    );
};

export const NewUserActivesMarketStep1 = ({
    show,
    onNegative = () => {},
    onPositive = () => {}
}: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <div className={"text-lg mb-2"}>
                <Trans>NewUserWizard.Actives.MarketExistQuestion</Trans>
            </div>
            <div>
                <Button
                    size="sm"
                    color={"primary"}
                    onClick={() => {
                        onNegative();
                    }}
                >
                    <Trans>NewUserWizard.Actives.MarketExistNegativeAnswer</Trans>
                </Button>
                &nbsp;&nbsp;
                <Button
                    size="sm"
                    color={"primary"}
                    onClick={() => {
                        onPositive();
                    }}
                >
                    <Trans>NewUserWizard.Actives.MarketExistPositiveAnswer</Trans>
                </Button>
            </div>
        </Card>
    );
};

const NewUserActivesMarketStep2 = ({
    show,
    onNegative = () => {},
    onPositive = () => {}
}: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <div className={"text-lg mb-2"}>
                <Trans>NewUserWizard.Actives.MarketSetupIntegrationQuestion</Trans>
            </div>
            <div>
                <span className={"link"} onClick={onNegative}>
                    <Trans>NewUserWizard.Actives.MarketSetupIntegrationNegativeAnswer</Trans>
                </span>
                &nbsp;&nbsp;
                <Button size="sm" color={"primary"} onClick={onPositive}>
                    <Trans>NewUserWizard.Actives.MarketSetupIntegrationPositiveAnswer</Trans>
                </Button>
            </div>
        </Card>
    );
};

const NewUserActivesMarketStep3 = ({ show, onPositive = () => {} }: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <>
            <CredentialList />
            <div className="text-end">
                <Button color={"success"} onClick={onPositive}>
                    <Trans>NewUserWizard.Actives.MarketCompleteIntegration</Trans>
                </Button>
            </div>
        </>
    );
};
