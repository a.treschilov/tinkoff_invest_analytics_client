import { MenuComponent } from "./MenuComponent";
import { MobileMenu } from "./MobileMenu";
import { useContext } from "react";
import { AuthContext } from "../../providers/AuthProvider";
import { ThemeContext } from "../../providers/ThemeProvider";

export const MenuLayout = () => {
    const { isNewUser } = useContext(AuthContext);
    const { lng } = useContext(ThemeContext);

    if (isNewUser) {
        return <></>;
    }

    return (
        <>
            <div className={"left-column w-64 hidden lg:block"}>
                <MenuComponent lng={lng} />
            </div>
            <MobileMenu />
        </>
    );
};
