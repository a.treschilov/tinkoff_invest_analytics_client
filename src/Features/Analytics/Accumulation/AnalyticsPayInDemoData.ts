import { IAggregateOperation } from "./AnalyticsAccumulationBar";

export const AnalyticsPayInDemoData: IAggregateOperation[] = [
    {
        date: "2022-05-01",
        amount: 70000
    },
    {
        date: "2022-06-01",
        amount: 70000
    },
    {
        date: "2022-07-01",
        amount: 70000
    },
    {
        date: "2022-08-01",
        amount: 70000
    },
    {
        date: "2022-09-01",
        amount: 70000
    },
    {
        date: "2022-10-01",
        amount: 70000
    },
    {
        date: "2022-11-01",
        amount: 70000
    },
    {
        date: "2022-12-01",
        amount: 70000
    },
    {
        date: "2023-01-01",
        amount: 80000
    },
    {
        date: "2023-02-01",
        amount: 80000
    },
    {
        date: "2023-03-01",
        amount: 80000
    },
    {
        date: "2023-04-01",
        amount: 80000
    }
];
