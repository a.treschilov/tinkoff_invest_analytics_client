export const fallbackLng = "en";
export const languages = [fallbackLng, "ru"];
export const lngCookieName = "lng";
export const defaultNS = "translation";

export function getOptions(lng = fallbackLng, ns: string | string[] = defaultNS) {
    return {
        // debug: true,
        supportedLngs: languages,
        fallbackLng,
        lng,
        fallbackNS: defaultNS,
        defaultNS,
        ns,
        react: {
            transSupportBasicHtmlNodes: true,
            transKeepBasicHtmlNodesFor: ["br", "strong", "i", "p", "u"]
        },
        keyPrefix: {}
    };
}
