"use client";

import { FaTelegramPlane } from "react-icons/fa";
import Image from "next/image";
import { MdContactSupport } from "react-icons/md";
import { Trans } from "react-i18next";
import { useTranslation } from "../../../app/i18n/client";
import { Footer } from "../Common/Footer/Footer";

const date = new Date();
export const FooterComponent = ({ lng }: { lng: string }) => {
    const { t } = useTranslation(lng);

    return (
        <Footer
            direction={"horizontal"}
            className="hidden lg:grid p-10 bg-base-100 text-neutral-content bottom-0 static shadow-xl"
        >
            <nav>
                <Image src="/images/logo_white.webp" alt="Hakkes" width={89} height={14} style={{ width: "113px" }} />
                <div>
                    Hakkes &copy; 2022-{date.getFullYear()}
                    <br />
                    {t("Footer.Copyright")}
                    <br />
                    <a href={"https://stats.uptimerobot.com/VjolWS2RJW"} target={"_blank"}>
                        <Trans>Footer.ServicesOperational</Trans>
                    </a>
                </div>
            </nav>
            <nav className={"hidden md:grid"}>&nbsp;</nav>
            <nav>
                <Footer.Title>
                    <Trans>Footer.FooterTitleResources</Trans>
                </Footer.Title>
                <div className={"flex gap-x-2"}>
                    <a href={"https://t.me/hakkes_invest"} target={"_blank"} className={"whitespace-nowrap"}>
                        <FaTelegramPlane className={"inline"} /> <Trans>Footer.TgChannel</Trans>
                    </a>
                    <a href={"https://t.me/hakkesChat"} target={"_blank"} className={"whitespace-nowrap"}>
                        <FaTelegramPlane className={"inline"} /> <Trans>Footer.TgChat</Trans>
                    </a>
                    <a
                        href={process.env.NEXT_PUBLIC_LANDING_PAGE_URL + "/help"}
                        className={"whitespace-nowrap"}
                        target={"_blank"}
                    >
                        <MdContactSupport className={"inline"} /> <Trans>Footer.HelpCenter</Trans>
                    </a>
                </div>
            </nav>
            <nav className={"hidden md:grid"}>&nbsp;</nav>
            <nav>
                <Footer.Title>
                    <Trans>Footer.FooterTitleLegal</Trans>
                </Footer.Title>
                <a href="https://hakkes.com/legal/privacyPolicy" target={"_blank"}>
                    <Trans>Footer.PrivacyPolicy</Trans>
                </a>
                <a href="https://hakkes.com/legal/userAgreement" target={"_blank"}>
                    <Trans>Footer.UserAgreement</Trans>
                </a>
            </nav>
        </Footer>
    );
};
