import { useContext, useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { ApiService } from "../../services/ApiService";
import { convertDateToDateString } from "../../utils/date";
import { UserItemEventsContentData } from "./UserItemEventsContentData";
import { IOperationView } from "../../../app/[lng]/user/operations/list/page";
import { UserItemEventsContentLoading } from "./UserItemEventsContentLoading";
import { IoCalendarNumberOutline } from "react-icons/io5";
import { UserItemEventsContentEmpty } from "./UserItemEventsContentEmpty";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";
import { Table } from "../../components/Common/Table/Table";

type UserItemEventsProps = {
    itemId: number;
};

export const UserItemEvents = ({ itemId }: UserItemEventsProps) => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [paymentSchedule, setPaymentSchedule] = useState<IOperationView[]>([]);
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userItem");

    useEffect(() => {
        const dateFrom = new Date();
        const dateTo = new Date();
        dateTo.setFullYear(dateTo.getFullYear() + 50);

        ApiService.fetch("/v1/item/details/" + itemId + "/events", {
            dateFrom: convertDateToDateString(dateFrom),
            dateTo: convertDateToDateString(dateTo)
        }).then((response: IOperationView[]) => {
            setPaymentSchedule(response);
            setPageState(response.length === 0 ? PageState.EMPTY_CONTENT : PageState.EDIT_FORM);
        });
    }, [itemId]);

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <IoCalendarNumberOutline /> {t("Events.Header")}
            </Card.Title>
            <Divider />
            <div className="overflow-x-auto max-h-96">
                <Table pinRows={true} size={"xs"} className="sm:table-md w-full">
                    <thead>
                        <tr>
                            <th>{t("Events.TableHeaderDate")}</th>
                            <th>{t("Events.TableHeaderAmount")}</th>
                            <th>{t("Events.TableHeaderOperationType")}</th>
                        </tr>
                    </thead>
                    <UserItemEventsContentLoading show={pageState === PageState.LOADING} />
                    <UserItemEventsContentData
                        show={pageState === PageState.EDIT_FORM}
                        paymentSchedule={paymentSchedule}
                    />
                    <UserItemEventsContentEmpty show={pageState === PageState.EMPTY_CONTENT} />
                </Table>
            </div>
        </Card>
    );
};
