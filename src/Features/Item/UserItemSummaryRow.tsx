import { UserItemSummaryRowPopover } from "./UserItemSummaryRowPopover";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";

interface UserItemSummaryRowPriceProps {
    label: string;
    amount: number;
    currency: string;
}

interface UserItemSummaryRowPriceChangeProps {
    label: string;
    amount: number;
    currency: string;
    difference: number;
    helpMessageLabel?: string | null;
}

export const UserItemSummaryRowPrice = ({ label, amount, currency }: UserItemSummaryRowPriceProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userItem");

    const price = new Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: currency
    });
    return (
        <div className={"grid grid-cols-12 gap-6"}>
            <div className="col-span-5">{t(label)}</div>
            <div className="col-span-7 text-end">{price.format(amount)}</div>
        </div>
    );
};

export const UserItemSummaryRowColoredPrice = ({ label, amount, currency }: UserItemSummaryRowPriceProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userItem");

    const price = new Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: currency,
        signDisplay: "exceptZero"
    });

    const textColorClass = amount >= 0 ? "text-success" : "text-error";

    return (
        <div className={"grid grid-cols-12"}>
            <div className="col-span-5">{t(label)}</div>
            <div className={"col-span-7 text-end " + textColorClass}>{price.format(amount)}</div>
        </div>
    );
};

export const UserItemSummaryRowColoredPriceChange = ({
    label,
    helpMessageLabel = null,
    amount,
    currency,
    difference
}: UserItemSummaryRowPriceChangeProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userItem");

    const price = new Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: currency,
        signDisplay: "exceptZero"
    });
    const differenceFormat = new Intl.NumberFormat("ru-RU", {
        style: "percent",
        signDisplay: "never",
        minimumFractionDigits: 1
    });

    const arrowClass = amount >= 0 ? "fa-caret-up" : "fa-caret-down";
    const textColorClass = amount >= 0 ? "text-success" : "text-error";
    const popover = helpMessageLabel === null ? null : <UserItemSummaryRowPopover label={helpMessageLabel} />;
    return (
        <div className={"grid grid-cols-12"}>
            <div className="col-span-5 flex">
                {t(label)}&nbsp;
                {popover}
            </div>
            <div className={"col-span-7 text-end " + textColorClass}>
                {price.format(amount)} (<i className={"fas " + arrowClass} /> {differenceFormat.format(difference)})
            </div>
        </div>
    );
};

export const UserItemSummaryRowPriceChange = ({
    label,
    helpMessageLabel = null,
    amount,
    currency,
    difference
}: UserItemSummaryRowPriceChangeProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userItem");

    const price = new Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: currency
    });
    const differenceFormat = new Intl.NumberFormat("ru-RU", {
        style: "percent",
        signDisplay: "never",
        minimumFractionDigits: 1
    });
    const popover = helpMessageLabel === null ? null : <UserItemSummaryRowPopover label={helpMessageLabel} />;

    return (
        <div className={"grid grid-cols-12"}>
            <div className="col-span-5 flex">
                {t(label)}&nbsp;&nbsp;
                {popover}
            </div>
            <div className={"col-span-7 text-end"}>
                {price.format(amount)} ({differenceFormat.format(difference)})
            </div>
        </div>
    );
};
