import { ReactNode, useEffect, useState } from "react";
import { Alert as DaisyAlert, AlertProps as DaisyAlertProps } from "react-daisyui";
import { IoMdClose } from "react-icons/io";

interface IAlert {
    children: ReactNode;
    onClose?: () => void;
    dismissible?: boolean;
    show?: boolean;
    closeTimeout?: number | null;
}

type AlertProps = IAlert & DaisyAlertProps;

export const Alert = ({
    children,
    onClose = () => {},
    closeTimeout = null,
    dismissible = true,
    show = true,
    ...args
}: AlertProps) => {
    const [timerId, setTimerId] = useState<NodeJS.Timeout | null>(null);
    useEffect(() => {
        if (show && closeTimeout !== null && closeTimeout !== undefined) {
            setTimerId(
                setTimeout(() => {
                    if (onClose) {
                        onClose();
                    }
                }, closeTimeout)
            );
        }
        if (!show && timerId !== null) {
            setTimerId(null);
        }
    }, [closeTimeout, onClose, show, timerId]);

    const closeButton = dismissible ? (
        <div className={"absolute top-3 right-3 cursor-pointer"} onClick={onClose}>
            <IoMdClose />
        </div>
    ) : (
        <></>
    );

    return (
        <div className={show ? " relative" : "hidden"}>
            <DaisyAlert {...args}>
                <div>{children}</div>
                {closeButton}
            </DaisyAlert>
        </div>
    );
};
