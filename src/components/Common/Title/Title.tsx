import { Divider } from "react-daisyui";
import { ReactNode } from "react";

type TitleProps = {
    children: ReactNode;
};

export const Title = ({ children }: TitleProps) => {
    return (
        <>
            <div className={"text-xl font-semibold"}>{children}</div>
            <Divider className={"mt-2"} />
        </>
    );
};
