import { useEffect, useState } from "react";
import { LoanDataDefault } from "./LoanStaticData";
import { PageState } from "../../components/Common/Types";
import { ApiService } from "../../services/ApiService";
import { formatInputDayDate, toUserTimezone } from "../../utils/date";
import { Alert } from "../../components/Common/Alert/Alert";
import { InputText } from "../../components/Common/Form/InputText";
import { Trans } from "react-i18next";
import { InputCurrencyAmount } from "../../components/Common/Form/InputCurrencyAmount";
import { PageLoader } from "../../components/Loader/Loaders";
import { Loan } from "../../components/Item/Item";
import { Button } from "../../components/Common/Button/Button";
import { Join } from "../../components/Common/Join/Join";
import { Select } from "../../components/Common/Select/Select";

type LoanEditFormProps = {
    loanId: number;
    show?: boolean;
    onSubmit?: (message: string | null) => void;
    onDiscard?: () => void;
};

export const LoanEditForm = ({ loanId, show = true, onSubmit = () => {}, onDiscard = () => {} }: LoanEditFormProps) => {
    const [loan, setLoan] = useState<Loan>(LoanDataDefault);
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [message, setMessage] = useState<string | null>(null);

    useEffect(() => {
        setPageState(PageState.LOADING);

        if (!show) {
            return;
        }

        if (loanId !== 0) {
            ApiService.fetch("/v1/loan/item/" + loanId).then(response => {
                const date = formatInputDayDate(toUserTimezone(response.date)) ?? "";

                let closedDate = "";
                if (response.closedDate !== null) {
                    closedDate = formatInputDayDate(toUserTimezone(response.closedDate)) ?? "";
                }

                const loanResponse: Loan = {
                    loanId: response.loanId,
                    itemId: response.itemId,
                    name: response.name,
                    amount: response.amount,
                    currency: response.currency,
                    isActive: response.isActive,
                    date: {
                        date: date,
                        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                    },
                    closedDate:
                        response.closedDate === null || closedDate === ""
                            ? null
                            : {
                                  date: closedDate,
                                  timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                              },
                    paymentAmount: response.paymentAmount,
                    paymentCurrency: response.paymentCurrency,
                    payoutFrequency: response.payoutFrequency,
                    payoutFrequencyPeriod: response.payoutFrequencyPeriod,
                    interestPercent: response.interestPercent
                };
                setPageState(PageState.EDIT_FORM);
                setLoan(loanResponse);
            });
        } else {
            setLoan(LoanDataDefault);
            setPageState(PageState.EDIT_FORM);
        }
    }, [loanId, show]);

    const changeElement = (name: string, value: string, inputType: string) => {
        let elementLoan = loan;

        let typedValue: number | string | object;
        switch (inputType) {
            case "number":
                typedValue =
                    name === "interestPercent" ? parseFloat((parseFloat(value) / 100).toFixed(4)) : parseFloat(value);
                break;
            case "date":
                typedValue = {
                    date: value,
                    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                };
                break;
            default:
                typedValue = value;
        }

        const data = {
            [name]: typedValue
        };
        elementLoan = { ...elementLoan, ...data };

        setLoan(elementLoan);
    };

    const handleFormSubmit = () => {
        setPageState(PageState.LOADING);

        if (loanId === 0) {
            ApiService.post("/v1/loan/item", loan)
                .then(() => {
                    onSubmit("addComplete");
                })
                .catch(error => {
                    setMessage(error.response.data.message);
                    setPageState(PageState.ERROR);
                });
        } else {
            ApiService.put("/v1/loan/item/" + loanId, loan)
                .then(() => {
                    onSubmit("editComplete");
                })
                .catch(error => {
                    setMessage(error.response.data.message);
                    setPageState(PageState.ERROR);
                });
        }
    };

    const handleChangeFormData = (event: React.ChangeEvent<HTMLInputElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleChangeSelectData = (event: React.ChangeEvent<HTMLSelectElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const renderLoading = () => {
        return <PageLoader show={pageState === PageState.LOADING} height={"336px"} />;
    };

    const renderBody = () => {
        const buttonText = loanId !== 0 ? "ButtonSave" : "ButtonAdd";
        return (
            <form onSubmit={handleFormSubmit}>
                <Alert show={message !== null} status={pageState === PageState.ERROR ? "error" : "info"}>
                    {message}
                </Alert>
                <div className={"grid grid-cols-2 gap-6 mb-6"}>
                    <InputText
                        containerClassName={"col-span-2 sm:col-span-1"}
                        label={<Trans>LoanEdit.FormLabelName</Trans>}
                        type="text"
                        name="name"
                        onChange={handleChangeFormData}
                        value={loan.name}
                    />
                    <InputText
                        containerClassName={"col-span-2 sm:col-span-1"}
                        label={<Trans>LoanEdit.FormLabelInterestPercent</Trans>}
                        type="number"
                        step="0.01"
                        name="interestPercent"
                        onChange={handleChangeFormData}
                        defaultValue={(loan.interestPercent * 100).toFixed(2)}
                    />
                    <InputCurrencyAmount
                        containerClassName={"col-span-2 sm:col-span-1"}
                        amount={loan.amount}
                        onChange={changeElement}
                        currency={loan.currency}
                        id={"loan-amount-" + loanId}
                        step={0.01}
                        label={<Trans>LoanEdit.FormLabelAmount</Trans>}
                    />
                    <InputText
                        containerClassName={"col-span-2 sm:col-span-1"}
                        label={<Trans>LoanEdit.FormLabelDate</Trans>}
                        type="date"
                        name="date"
                        onChange={handleChangeFormData}
                        defaultValue={loan.date.date}
                    />
                    <InputCurrencyAmount
                        containerClassName={"col-span-2 sm:col-span-1"}
                        label={<Trans>LoanEdit.FormLabelPaymentAmount</Trans>}
                        amount={loan.paymentAmount}
                        onChange={changeElement}
                        currency={loan.paymentCurrency}
                        id={"loan-payment-amount" + loanId}
                        step={0.01}
                        amountInputName={"paymentAmount"}
                        currencyInputName={"paymentCurrency"}
                    />
                    <div className={`form-control w-full col-span-2 sm:col-span-1 grid-cols-2`}>
                        <label className="label">
                            <span className={"label-text text-base-content"}>
                                <Trans>LoanEdit.FormLabelPayoutFrequency</Trans>
                            </span>
                        </label>
                        <Join>
                            <input
                                type="number"
                                onChange={handleChangeFormData}
                                name="payoutFrequency"
                                defaultValue={loan.payoutFrequency}
                                className={"input input-bordered w-full rounded-r-none"}
                            ></input>
                            <Select
                                name="payoutFrequencyPeriod"
                                onChange={handleChangeSelectData}
                                defaultValue={loan.payoutFrequencyPeriod}
                                className={"rounded-l-none"}
                            >
                                <option value="day">
                                    <Trans>LoanEdit.PeriodDay</Trans>
                                </option>
                                <option value="month">
                                    <Trans>LoanEdit.PeriodMonth</Trans>
                                </option>
                                <option value="year">
                                    <Trans>LoanEdit.PeriodYear</Trans>
                                </option>
                            </Select>
                        </Join>
                    </div>
                </div>
                <div className={"text-right"}>
                    <Button type={"button"} color={"neutral"} onClick={onDiscard} className={"mr-2"}>
                        <Trans>LoanEdit.ButtonCancel</Trans>
                    </Button>
                    <Button type="submit" color="primary">
                        <Trans>LoanEdit.{buttonText}</Trans>
                    </Button>
                </div>
            </form>
        );
    };

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = renderLoading();
            break;
        case PageState.EDIT_FORM:
        case PageState.ERROR:
            content = renderBody();
            break;
    }

    return <>{content}</>;
};
