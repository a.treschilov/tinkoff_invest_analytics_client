"use client";

import { useContext, useEffect, useState, use } from "react";
import { ApiService } from "../../../../../../src/services/ApiService";
import { Item } from "../../../../../../src/components/Item/Item";
import { LoanEditForm } from "../../../../../../src/Features/Loan/LoanEditForm";
import { useRouter } from "next/navigation";
import { makeLink } from "../../../../../../src/utils/url";
import { ThemeContext } from "../../../../../../src/providers/ThemeProvider";
import { DepositEditForm } from "../../../../../../src/Features/Deposit/DepositEditForm";
import { RealEstateEditForm } from "../../../../../../src/Features/RealEstate/RealEstateEditForm";
import { useErrorBoundary } from "react-error-boundary";
import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";
import { Divider } from "../../../../../../src/components/Common/Divider/Divider";
import { Card } from "../../../../../../src/components/Common/Card/Card";

const moveToPage = (router: AppRouterInstance, itemId: number, lng: string): void => {
    router.push(makeLink("/user/item/" + itemId, lng));
    return;
};

const ItemEditPage = (props: { params: Promise<{ itemId: string }> }) => {
    const params = use(props.params);
    const [itemId] = useState(parseInt(params.itemId));
    const [item, setItem] = useState<Item>();
    const [component, setComponent] = useState(<></>);
    const router = useRouter();
    const { lng } = useContext(ThemeContext);
    const { showBoundary } = useErrorBoundary();

    useEffect(() => {
        ApiService.fetch("/v1/item/info/" + itemId)
            .then((response: Item) => {
                setItem(response);

                switch (response?.type) {
                    case "loan":
                        setComponent(
                            <LoanEditForm
                                loanId={response?.loan?.loanId || 0}
                                onSubmit={() => {
                                    moveToPage(router, itemId, lng);
                                }}
                                onDiscard={() => moveToPage(router, itemId, lng)}
                            />
                        );
                        break;
                    case "deposit":
                        setComponent(
                            <DepositEditForm
                                show={true}
                                depositId={response?.deposit?.depositId || 0}
                                onSubmit={() => {
                                    moveToPage(router, itemId, lng);
                                }}
                                onDiscard={() => moveToPage(router, itemId, lng)}
                            />
                        );
                        break;
                    case "real_estate":
                        setComponent(
                            <RealEstateEditForm
                                realEstateId={response?.realEstate?.realEstateId || 0}
                                show={true}
                                onDiscard={() => moveToPage(router, itemId, lng)}
                                onSubmit={() => moveToPage(router, itemId, lng)}
                            />
                        );
                        break;
                }
            })
            .catch(error => {
                showBoundary(error);
            });
    }, [showBoundary, itemId, lng, router]);
    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>{item?.name}</Card.Title>
            <Divider />
            {component}
        </Card>
    );
};

export default ItemEditPage;
