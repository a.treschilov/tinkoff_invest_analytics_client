"use client";

import { CredentialList } from "../../../../../src/Features/Credential/CredentialList";

const CredentialPage = () => {
    return <CredentialList />;
};

export default CredentialPage;
