import { useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { Checkbox as DaisyUiCheckbox } from "../../Common/Checkbox/Checkbox";

type Props = {
    id: string;
    name: string;
    checked: boolean;
    handleCheckboxChange: (id: string) => void;
};

export const Checkbox = (props: Props) => {
    const { id, name, checked, handleCheckboxChange } = props;
    const [isChecked, setState] = useState(checked);

    useEffect(() => {
        setState(props.checked);
    }, [props.checked]);
    const handleChange = (): void => {
        setState(!isChecked);
        handleCheckboxChange(id);
    };

    return (
        <div className={"mb-2"}>
            <label className={"flex place-content-between"}>
                <div>
                    <Trans>{name}</Trans>
                </div>
                <DaisyUiCheckbox checked={isChecked} onChange={() => handleChange()} />
            </label>
        </div>
    );
};
