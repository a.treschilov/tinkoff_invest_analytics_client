import { Candle } from "./PriceHistory";
import { defaultChartOptions } from "../../Analytics/ChatOptions";
import dynamic from "next/dynamic";
import { ApexOptions } from "apexcharts";
import { mergeDeep } from "../../../utils/objects";
const Chart = dynamic(() => import("react-apexcharts"), {
    ssr: false
});

type PriceHistoryChartProps = {
    candles: Candle[];
};

export const PriceHistoryChart = ({ candles }: PriceHistoryChartProps) => {
    const currency = candles[0] ? candles[0].currency : "USD";
    const currencyFormatter = Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: currency
    });

    const compactCurrencyFormatter = Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: currency,
        notation: "compact"
    });

    const data = candles.map((candle: Candle) => {
        const date = new Date(candle.date.date);
        return [date.valueOf(), candle.close];
    });

    let max = candles[0] ? candles[0].close : 0;
    let min = candles[0] ? candles[0].close : 0;
    candles.forEach((candle: Candle) => {
        if (candle.close < min) {
            min = candle.close;
        }
        if (candle.close > max) {
            max = candle.close;
        }
    });

    const annotations: ApexAnnotations = {
        yaxis: [
            {
                y: min,
                strokeDashArray: 3,
                borderColor: "rgba(173, 181, 189, 0.7)",
                label: {
                    text: "min: " + currencyFormatter.format(min),
                    borderColor: "transparent",
                    style: {
                        background: "transparent",
                        color: "rgb(173, 181, 189)"
                    }
                }
            },
            {
                y: max,
                strokeDashArray: 3,
                borderColor: "rgba(173, 181, 189, 0.7)",
                label: {
                    text: "max: " + currencyFormatter.format(max),
                    borderColor: "transparent",
                    style: {
                        background: "transparent",
                        color: "rgb(173, 181, 189)"
                    }
                }
            }
        ]
    };

    const yaxis = [
        {
            labels: {
                formatter: (value: number) => {
                    return compactCurrencyFormatter.format(value);
                }
            }
        }
    ];

    const options: ApexOptions = mergeDeep(defaultChartOptions, { annotations: annotations, yaxis: yaxis });

    const series: ApexAxisChartSeries = [
        {
            name: "Цена",
            data: data,
            type: "line"
        }
    ];

    return <Chart options={options} type="line" series={series} width="100%" height={500} group={"dynamic"} />;
};
