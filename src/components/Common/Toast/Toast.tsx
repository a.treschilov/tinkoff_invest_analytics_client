import { HTMLAttributes, ReactNode } from "react";

type ToastProps = {
    children?: ReactNode;
    horizontal?: "start" | "center" | "end";
    vertical?: "top" | "bottom" | "middle";
} & HTMLAttributes<HTMLDivElement>;

export const Toast = ({ children, className, vertical = "bottom", horizontal = "end", ...args }: ToastProps) => {
    const classes = ["toast"];
    if (className) {
        classes.push(...className.split(" "));
    }
    switch (vertical) {
        case "top":
            classes.push("toast-top");
            break;
        case "bottom":
            classes.push("toast-bottom");
            break;
        case "middle":
            classes.push("toast-middle");
            break;
    }

    switch (horizontal) {
        case "start":
            classes.push("toast-start");
            break;
        case "center":
            classes.push("toast-center");
            break;
        case "end":
            classes.push("toast-end");
            break;
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
