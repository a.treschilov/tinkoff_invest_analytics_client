"use client";

import { Card } from "react-daisyui";
import { Item } from "../../../components/Item/Item";
import { UserDescriptionContentName } from "../UserItemDescription/UserDescriptionContentName";
import { PriceHistory } from "./PriceHistory";
import { useTranslation } from "../../../../app/i18n/client";

type PriceHistoryManageProps = {
    item: Item;
    lng: string;
};
export const PriceHistoryManage = ({ item, lng }: PriceHistoryManageProps) => {
    const { t } = useTranslation(lng, "public/translation");
    return (
        <>
            <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
                <div className={"grid grid-cols-1 gap-2"}>
                    <div className={"flex gap-2 text-lg"}>
                        <UserDescriptionContentName item={item} />
                    </div>
                </div>
            </Card>
            <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
                <Card.Title tag={"h2"}>{t("Item.PriceDynamicHeader")}</Card.Title>
                <PriceHistory isin={item.externalId} />
            </Card>
        </>
    );
};
