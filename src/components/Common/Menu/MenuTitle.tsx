import { HTMLAttributes, ReactNode } from "react";

type MenuTitleProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLLIElement>;

export const MenuTitle = ({ children, className, ...args }: MenuTitleProps) => {
    const classes = ["menu-title"];
    if (className) {
        classes.push(...className.split(" "));
    }

    return (
        <li className={classes.join(" ")} {...args}>
            {children}
        </li>
    );
};
