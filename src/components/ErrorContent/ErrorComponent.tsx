import { useEffect, useState } from "react";
import { ErrorContentProps } from "./ErrorContent";

export const ErrorComponent = ({ error }: ErrorContentProps) => {
    const [status] = useState(error?.response?.status);
    const [statusText, setStatusText] = useState(error?.response?.statusText);

    useEffect(() => {
        if (error?.response !== undefined) {
            if (error?.response?.status >= 500) {
                setStatusText(error?.response?.statusText);
            } else if (error?.response?.status >= 400) {
                if (
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    error?.response?.data?.error_code !== undefined &&
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    error?.response?.data?.error_message !== undefined
                ) {
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    setStatusText(error?.response?.data?.error_code + ": " + error?.response?.data?.error_message);
                } else {
                    setStatusText(error?.response?.statusText);
                }
            }
        }
    }, [error]);

    return (
        <div className="text-center mt-4">
            <h1>{status}</h1>
            <p className="lead">{statusText}</p>
        </div>
    );
};
