import { NextResponse } from "next/server";

export async function GET() {
    return NextResponse.json({
        error: false,
        code: 0,
        message: "OK"
    });
}
