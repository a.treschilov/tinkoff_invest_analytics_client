"use client";

import { ComponentState, useContext, useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { AuthContext } from "../../../../../src/providers/AuthProvider";
import { DemoPortfolioData } from "../../../../../src/Features/MarketPortfolio/PortfolioStaticData";
import { ApiService } from "../../../../../src/services/ApiService";
import { PageState } from "../../../../../src/components/Common/Types";
import { ErrorContent } from "../../../../../src/components/ErrorContent/ErrorContent";
import { PortfolioRevenueToday } from "../../../../../src/Features/MarketPortfolio/PortfolioRevenueToday";
import { PortfolioContentGrid } from "../../../../../src/Features/MarketPortfolio/PortfolioContentGrid";
import Link from "next/link";
import { ThemeContext } from "../../../../../src/providers/ThemeProvider";
import { makeLink } from "../../../../../src/utils/url";
import { MarketPortfolioStat } from "../../../../../src/Features/MarketPortfolio/MarketPortfolioStat";
import { Alert } from "../../../../../src/components/Common/Alert/Alert";
import { Stats } from "../../../../../src/components/Common/Stats/Stats";

export interface IShare {
    name: string;
    share: number;
    targetShare: number;
    diffPercent: number;
    diffAmount: number;
}

export interface IPortfolio {
    shares: IShare[] | null;
    shareCountry: IShare[] | null;
    shareSector: IShare[] | null;
    sharesCurrency: IShare[] | null;
    shareCurrencyBalance: IShare[] | null;
    totalAmount: number;
}

const StockPortfolioPage = () => {
    const user = useContext(AuthContext);
    const { lng } = useContext(ThemeContext);
    const [portfolio, setPortfolio] = useState<IPortfolio>({
        shares: null,
        shareCountry: null,
        shareSector: null,
        sharesCurrency: null,
        shareCurrencyBalance: null,
        totalAmount: 0
    });
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [isEmptyPortfolio, setIsEmptyPortfolio] = useState<boolean>(user.isNewUser);

    useEffect(() => {
        if (isEmptyPortfolio) {
            setPortfolio(DemoPortfolioData);
            setPageState(PageState.SUCCESS);
        } else {
            ApiService.fetch("/v1/portfolio")
                .then(response => {
                    setPortfolio(response);
                    setPageState(PageState.SUCCESS);
                })
                .catch(error => {
                    switch (error.response.data.error_code) {
                        case 1031:
                        case 1033:
                            setPortfolio(DemoPortfolioData);
                            setIsEmptyPortfolio(true);
                            setPageState(PageState.SUCCESS);
                            break;
                        default:
                            setPageState(PageState.ERROR);
                    }
                });
        }
    }, [isEmptyPortfolio]);

    let dataTableState: ComponentState;
    switch (pageState) {
        case PageState.LOADING:
            dataTableState = "loading";
            break;
        default:
            dataTableState = "success";
    }

    if (pageState === PageState.ERROR) {
        return <ErrorContent />;
    }

    return (
        <>
            <Stats className="shadow-sm font-sans mb-6">
                <MarketPortfolioStat
                    amount={portfolio.totalAmount}
                    state={pageState === PageState.LOADING ? "loading" : "success"}
                />
                <PortfolioRevenueToday demoData={isEmptyPortfolio} />
            </Stats>

            <Alert status={"info"} show={isEmptyPortfolio} dismissible={false}>
                <Trans>Portfolio.EmptyAccountMessage</Trans>{" "}
                <Link className={"link"} href={makeLink("/user/market/credential", lng)}>
                    <Trans>Portfolio.EmptyAccountMessageAction</Trans>
                </Link>
                .
            </Alert>

            <PortfolioContentGrid portfolio={portfolio} state={dataTableState} />
        </>
    );
};

export default StockPortfolioPage;
