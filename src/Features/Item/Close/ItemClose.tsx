import { PageState } from "../../../components/Common/Types";
import { PageLoader } from "../../../components/Loader/Loaders";
import { Trans } from "react-i18next";
import { Modal } from "../../../components/Common/Modal/Modal";
import { useEffect, useState } from "react";
import { ApiService } from "../../../services/ApiService";
import { Item } from "../../../components/Item/Item";
import { IPrice } from "../../../interfaces/IPrice";
import { ItemCloseRealEstate } from "./ItemCloseRealEstate";
import { Button } from "../../../components/Common/Button/Button";

type ItemCloseProps = {
    itemId: number;
    item?: Item;
    show?: boolean;
    onSuccess: () => void;
    onDiscard: () => void;
};

export const ItemClose = (props: ItemCloseProps) => {
    const [state, setState] = useState(PageState.LOADING);
    const [item, setItem] = useState(props.item);
    const [price, setPrice] = useState<IPrice>();

    const { itemId, show = true, onSuccess, onDiscard } = props;

    useEffect(() => {
        if (show) {
            setState(PageState.LOADING);
            if (props.item === undefined) {
                ApiService.fetch("/v1/item/info/" + itemId).then((response: Item) => {
                    setItem(response);
                    setState(PageState.SUCCESS);
                });
            } else {
                setState(PageState.LOADING);
            }
        }
    }, [itemId, props.item, show]);

    const getLabels = () => {
        switch (item?.type) {
            case "deposit":
                return {
                    header: "Item.Close.DepositWarningHeader",
                    content: "Item.Close.DepositWarningText",
                    closeUrl: "/v1/deposit/close/" + item?.deposit?.depositId,
                    data: {}
                };
            case "loan":
                return {
                    header: "Item.Close.LoanWarningHeader",
                    content: "Item.Close.LoanWarningText",
                    closeUrl: "/v1/loan/close/" + item?.loan?.loanId,
                    data: {}
                };
            case "real_estate":
                return {
                    header: "Item.Close.RealEstateWarningHeader",
                    content: "Item.Close.RealEstateWarningText",
                    closeUrl: "/v1/real_estate/sell/" + item?.realEstate?.realEstateId,
                    data: { price: price }
                };
            default:
                return {
                    header: "Item.Close.DepositWarningHeader",
                    content: "Item.Close.DepositWarningText",
                    closeUrl: "/v1/deposit/close/" + item?.deposit?.depositId,
                    data: {}
                };
        }
    };

    const handlePriceChange = (priceData: IPrice): void => {
        setPrice(priceData);
    };

    const handleClose = () => {
        setState(PageState.LOADING);
        ApiService.put(getLabels().closeUrl, getLabels().data).then(() => {
            onSuccess();
        });
    };

    const renderBody = () => {
        switch (state) {
            case PageState.LOADING:
                return <PageLoader height={"244px"} />;
            default:
                return (
                    <>
                        <Modal.Header>
                            <Trans
                                i18nKey={getLabels().header}
                                values={{
                                    name: item?.name
                                }}
                            ></Trans>
                        </Modal.Header>
                        <Modal.Body>
                            <Trans
                                i18nKey={getLabels().content}
                                values={{
                                    name: item?.name
                                }}
                            ></Trans>
                            <ItemCloseRealEstate
                                realEstate={item?.realEstate ?? undefined}
                                show={item?.type === "real_estate"}
                                onChange={handlePriceChange}
                            />
                        </Modal.Body>
                        <Modal.Actions>
                            <Button
                                color="neutral"
                                onClick={() => {
                                    onDiscard();
                                }}
                            >
                                <Trans>Item.Close.WarningCancelButton</Trans>
                            </Button>
                            <Button color="info" onClick={handleClose}>
                                <Trans>Item.Close.WarningConfirmButton</Trans>
                            </Button>
                        </Modal.Actions>
                    </>
                );
        }
    };

    return (
        <Modal
            show={show}
            onClose={() => {
                onDiscard();
            }}
        >
            {renderBody()}
        </Modal>
    );
};
