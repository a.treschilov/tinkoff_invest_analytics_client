import { initData, init as initSDK, isTMA } from "@telegram-apps/sdk-react";

/**
 * Initializes the application and configures its dependencies.
 */
export async function tgInit() {
    if (await isTMA("complete")) {
        initSDK();
        initData.restore();
    }
}
