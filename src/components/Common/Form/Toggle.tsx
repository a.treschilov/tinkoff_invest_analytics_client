import { FC, ReactNode } from "react";
import { Toggle as DaisyUIToggle, ToggleProps as DaisyUIToggleProps } from "../Toggle/Toggle";

type ToggleExtensions = {
    Label: typeof Label;
    Checkbox: typeof Checkbox;
};

type ToggleProps = {
    children: ReactNode;
};

export const Toggle: FC<ToggleProps> & ToggleExtensions = ({ children }: ToggleProps) => {
    return <label className="cursor-pointer label justify-start">{children}</label>;
};

type CheckboxProps = DaisyUIToggleProps;

const Checkbox = ({ ...args }: CheckboxProps) => {
    return <DaisyUIToggle {...args} />;
};
Toggle.Checkbox = Checkbox;

const Label = ({ children }: { children: ReactNode }) => {
    return <span className="label-text me-2">{children}</span>;
};
Toggle.Label = Label;

export default Toggle;
