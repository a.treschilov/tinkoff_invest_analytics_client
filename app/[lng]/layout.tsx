import { Metadata } from "next";
import Script from "next/script";
import { dir } from "i18next";

import "./../../src/css/global.css";

import { languages } from "../i18n/settings";
import { ReactNode } from "react";

const APP_NAME = "Hakkes";
const APP_DEFAULT_TITLE = "Hakkes — инструмент учета инвестиций";
const APP_TITLE_TEMPLATE = "%s - Hakkes";
const APP_DESCRIPTION =
    "Управление и анализ инвестиционного портфеля, балансировка портфеля, аналитика компаний и дивидендов. Планирование инвестиций в акции, облигации, недвижимость, краудлэндинг.";

export const metadata: Metadata = {
    metadataBase: new URL(process.env.NEXT_PUBLIC_BASE_URL || "https://app.hakkes.com"),
    applicationName: APP_NAME,
    title: "Hakkes — инструмент учета инвестиций",
    description: APP_DESCRIPTION,
    keywords: "Диверсификация портфеля, акции, облигации, инвестиции, накопления",
    icons: {
        icon: {
            url: "/favicon.svg",
            type: "image/svg+xml"
        }
    },
    manifest: "/manifest.json",
    appleWebApp: {
        capable: true,
        statusBarStyle: "default",
        title: APP_DEFAULT_TITLE
    },
    formatDetection: {
        telephone: false
    },
    openGraph: {
        type: "website",
        siteName: APP_NAME,
        title: {
            default: APP_DEFAULT_TITLE,
            template: APP_TITLE_TEMPLATE
        },
        description: APP_DESCRIPTION
    },
    twitter: {
        card: "summary",
        title: {
            default: APP_DEFAULT_TITLE,
            template: APP_TITLE_TEMPLATE
        },
        description: APP_DESCRIPTION
    }
};

export async function generateStaticParams() {
    return languages.map(lng => ({ lng }));
}

export default async function LngLayout(props: { children: ReactNode; params: Promise<{ lng: string }> }) {
    const params = await props.params;
    const { children } = props;

    return (
        <html lang={params.lng} dir={dir(params.lng)}>
            <Script id="google-tag-manager" strategy="afterInteractive">
                {`
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','${process.env.NEXT_PUBLIC_GMT_ID || ""}');
        `}
            </Script>
            <body id="root">{children}</body>
        </html>
    );
}
