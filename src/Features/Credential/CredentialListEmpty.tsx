import { Trans } from "react-i18next";
import { makeLink } from "../../utils/url";
import Link from "next/link";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useContext } from "react";
import { Button } from "../../components/Common/Button/Button";

type CredentialListEmptyProps = {
    onAddCredential: () => void;
};
export const CredentialListEmpty = ({ onAddCredential }: CredentialListEmptyProps) => {
    const { lng } = useContext(ThemeContext);
    return (
        <div className="text-center my-4">
            <Link
                href={makeLink("/user/market/credential/add", lng)}
                className={"btn btn-primary btn-lg decoration-transparent sm:mr-2"}
            >
                <Trans>CredentialList.UploadReportButton</Trans>
            </Link>
            <Button size="lg" color={"primary"} onClick={onAddCredential}>
                <Trans>CredentialList.AddButton</Trans>
            </Button>
        </div>
    );
};
