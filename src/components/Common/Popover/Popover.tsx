"use client";
import { ReactNode, MouseEvent, useEffect, useRef, useState } from "react";

export function Popover({
    children,
    content,
    trigger = "click",
    vertical = "middle"
}: {
    children: ReactNode;
    content: ReactNode;
    trigger?: "hover" | "click";
    vertical?: "top" | "bottom" | "middle";
}) {
    const [show, setShow] = useState(false);
    const wrapperRef = useRef(null);
    const tooltipRef = useRef(null);
    const [top, setTop] = useState(0);

    const handleMouseOver = () => {
        if (trigger === "hover") {
            setShow(true);
        }
    };

    const handleMouseLeft = () => {
        if (trigger === "hover") {
            setShow(false);
        }
    };

    useEffect(() => {
        function handleClickOutside(event: MouseEvent<HTMLElement>) {
            //@ts-expect-error unknown
            if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
                setShow(false);
            }
        }

        switch (vertical) {
            case "top":
                //@ts-expect-error unknown
                setTop(-1 * tooltipRef.current?.offsetHeight - 1 || 0);
                break;
            case "middle":
                setTop(0);
                break;
            case "bottom":
                //@ts-expect-error unknown
                setTop(wrapperRef.current?.offsetHeight + 2 || 0);
                break;
        }

        if (show) {
            // Bind the event listener
            //@ts-expect-error unknown
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                // Unbind the event listener on clean up
                //@ts-expect-error unknown
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }
    }, [show, vertical, wrapperRef]);

    return (
        <div
            ref={wrapperRef}
            onMouseEnter={handleMouseOver}
            onMouseLeave={handleMouseLeft}
            className="w-fit h-fit relative flex justify-center"
        >
            <div className="inline" onClick={() => setShow(!show)}>
                {children}
            </div>
            <div
                ref={tooltipRef}
                hidden={!show}
                className={"min-w-fit w-72 h-fit absolute left-0 z-50 transition-all bg-base-100 shadow-xl"}
                style={{ top: top }}
            >
                <div className="rounded-sm text-sm p-3 mb-[10px]">{content}</div>
            </div>
        </div>
    );
}
