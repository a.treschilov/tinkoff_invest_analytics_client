import { getCookie } from "../utils/_actions";

export const ApiServiceNext = {
    baseURL: process.env.NEXT_PUBLIC_API_HOST || "",

    fetch: async function (command: string) {
        const url = ApiServiceNext.baseURL + command;

        const params: RequestInit = {
            method: "get",
            headers: {
                Authorization: "Bearer " + (await getCookie("jwt"))
            }
        };

        return fetch(url, params);
    }
};
