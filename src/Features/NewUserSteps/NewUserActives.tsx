import { StepComponentProps } from "./NewUserSteps";
import { useEffect, useState } from "react";
import { NewUserActivesMarket } from "./NewUserActivesMarket";
import { ItemType } from "../../components/Common/Types";
import { NewUserActivesDeposit } from "./NewUserActivesDeposit";
import { Progress } from "../../components/Common/Progress/Progress";

export const NewUserActives = ({ onComplete, active }: StepComponentProps) => {
    const [currentStep, setCurrentStep] = useState<ItemType>("market");
    const [progress, setProgress] = useState(0);

    useEffect(() => {
        switch (currentStep) {
            case "market":
                setProgress(2);
                break;
            case "deposit":
                setProgress(7);
                break;
        }
    }, [currentStep]);

    if (!active) {
        return <></>;
    }

    return (
        <>
            <Progress color={"success"} value={progress} max={10} className={"mb-6"} />
            <NewUserActivesMarket show={currentStep === "market"} onComplete={() => setCurrentStep("deposit")} />
            <NewUserActivesDeposit show={currentStep === "deposit"} onComplete={() => onComplete()} />
        </>
    );
};
