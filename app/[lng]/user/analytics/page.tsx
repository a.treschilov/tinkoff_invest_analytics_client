"use client";

import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../../../../src/providers/AuthProvider";
import { PageState } from "../../../../src/components/Common/Types";
import { WealthHistoryTypesDemoData } from "../../../../src/Features/Analytics/WealthHistoryTypesDemoData";
import { WealthHistoryDynamicDemoData } from "../../../../src/Features/Analytics/WealthDynamic/WealthHistoryDynamicDemoData";
import { ApiService } from "../../../../src/services/ApiService";
import { BiLineChart } from "react-icons/bi";
import { Trans } from "react-i18next";
import { AnalyticsWealthDynamicChart } from "../../../../src/Features/Analytics/WealthDynamic/AnalyticsWealthDynamicChart";
import { BsPieChart } from "react-icons/bs";
import { AnalyticsAssetsTypes } from "../../../../src/Features/Analytics/AnalyticsAssetsTypes";
import { AnalyticsAccumulation } from "../../../../src/Features/Analytics/Accumulation/AnalyticsAccumulation";
import Link from "next/link";
import { ThemeContext } from "../../../../src/providers/ThemeProvider";
import { makeLink } from "../../../../src/utils/url";
import { PageLoader } from "../../../../src/components/Loader/Loaders";
import { Alert } from "../../../../src/components/Common/Alert/Alert";
import { AnalyticsCompareWithIndex } from "../../../../src/Features/Analytics/Benchmark/AnalyticsCompareWithIndex";
import { Divider } from "../../../../src/components/Common/Divider/Divider";
import { Card } from "../../../../src/components/Common/Card/Card";

export interface IWealth {
    date: {
        date: string;
        timezone: string;
    };
    currency: string;
    stock: number;
    realEstate: number;
    loan: number;
    assets: number;
    deposit: number;
    expenses: number | null;
    wealthRate: number | null;
}

const AnalyticsPage = () => {
    const user = useContext(AuthContext);
    const { lng } = useContext(ThemeContext);
    const [pageState, setPageState] = useState({
        general: PageState.LOADING,
        wealthTypes: PageState.LOADING,
        wealthDynamic: PageState.LOADING
    });
    const [wealthHistory, setWealthHistory] = useState<IWealth[]>([]);
    const [wealthTypes, setWealthTypes] = useState<IWealth | null>(null);

    useEffect(() => {
        const actualPageState = pageState;
        if (user.isNewUser) {
            actualPageState.wealthDynamic = PageState.EDIT_FORM;
            actualPageState.wealthTypes = PageState.EDIT_FORM;

            setWealthTypes(WealthHistoryTypesDemoData);
            setWealthHistory(WealthHistoryDynamicDemoData);

            setPageState(actualPageState);
        } else {
            ApiService.fetch("/v1/analytics/wealthDynamic").then(response => {
                const wealthDynamic = response.map((wealthItem: IWealth) => {
                    return {
                        date: {
                            date: wealthItem.date.date,
                            timezone: wealthItem.date.timezone
                        },
                        currency: wealthItem.currency,
                        stock: wealthItem.stock,
                        realEstate: wealthItem.realEstate,
                        loan: wealthItem.loan,
                        assets: wealthItem.assets,
                        deposit: wealthItem.deposit,
                        expenses: wealthItem.expenses,
                        wealthRate: wealthItem.wealthRate
                    };
                });
                actualPageState.wealthDynamic = PageState.EDIT_FORM;
                setWealthHistory(wealthDynamic);
                setPageState(actualPageState);
            });

            ApiService.fetch("/v1/analytics/wealthTypes").then(response => {
                if (response === null) {
                    actualPageState.general = PageState.ERROR;
                    setWealthTypes(null);
                } else {
                    actualPageState.general = PageState.SUCCESS;
                    const wealthItem = response;
                    const wealthTypes = {
                        date: {
                            date: wealthItem.date.date,
                            timezone: wealthItem.date.timezone
                        },
                        currency: wealthItem.currency,
                        stock: wealthItem.stock,
                        realEstate: wealthItem.realEstate,
                        loan: wealthItem.loan,
                        assets: wealthItem.assets,
                        deposit: wealthItem.deposit,
                        expenses: wealthItem.expenses,
                        wealthRate: wealthItem.wealthRate
                    };

                    actualPageState.wealthTypes = PageState.EDIT_FORM;

                    setWealthTypes(wealthTypes);
                    setPageState(pageState);
                }
            });
        }
    }, [pageState, user.isNewUser]);

    let content = (
        <div className={"grid grid-cols-3 gap-6"}>
            <div className="col-span-3 lg:col-span-2">
                <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                    <Card.Title>
                        <BiLineChart className={"me-1"} />
                        <Trans>Analytics.WealthDynamicHeader</Trans>
                    </Card.Title>
                    <Divider />
                    <AnalyticsWealthDynamicChart
                        wealthHistory={wealthHistory}
                        dataState={pageState.wealthDynamic}
                    ></AnalyticsWealthDynamicChart>
                </Card>
            </div>
            <div className="col-span-3 lg:col-span-1">
                <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                    <Card.Title>
                        <BsPieChart className="me-1" />
                        <Trans>Analytics.AssetTypesHeader</Trans>
                    </Card.Title>
                    <Divider />
                    <AnalyticsAssetsTypes
                        assetsTypes={wealthTypes}
                        dataState={pageState.wealthTypes}
                    ></AnalyticsAssetsTypes>
                </Card>
            </div>
            <div className={"col-span-3"}>
                <AnalyticsAccumulation />
            </div>
            <div className={"col-span-3"}>
                <AnalyticsCompareWithIndex />
            </div>
            <small>
                <Trans>Analytics.FrequencyUpdatedMessage</Trans>
            </small>
        </div>
    );

    if (user.isNewUser) {
        content = (
            <>
                <Alert dismissible={false} status={"info"} className={"mb-6"}>
                    <Trans>Analytics.NewUserDescription</Trans>&nbsp;
                    <Link href={makeLink("/user", lng)} className={"link"}>
                        <Trans>Analytics.NewUserDescriptionFillData</Trans>
                    </Link>
                    <Trans>Analytics.NewUserDescriptionPostFillData</Trans>
                </Alert>
                {content}
            </>
        );
    } else {
        if (pageState.general === PageState.LOADING) {
            content = <PageLoader />;
        }

        if (pageState.general === PageState.ERROR) {
            content = (
                <Alert dismissible={false} status={"info"}>
                    <Trans>Analytics.NotCalculatedDataYetMessage</Trans>
                </Alert>
            );
        }
    }

    return <>{content}</>;
};

export default AnalyticsPage;
