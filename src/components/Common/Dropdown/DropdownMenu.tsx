import { HTMLAttributes, ReactNode } from "react";

type DropdownToggleMenuProps = {
    children?: ReactNode;
    className?: string;
} & HTMLAttributes<HTMLUListElement>;

export const DropdownMenu = ({ children, className, ...args }: DropdownToggleMenuProps) => {
    return (
        <ul
            tabIndex={0}
            className={"dropdown-content menu p-2 shadow-sm bg-base-100 rounded-box" + (className ? " " + className : "")}
            {...args}
        >
            {children}
        </ul>
    );
};
