import { FC, HTMLAttributes, ReactNode } from "react";
import { HeroContent } from "./HeroContent";

type HeroProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

type HeroExtension = {
    Content: typeof HeroContent;
};

export const Hero: FC<HeroProps> & HeroExtension = ({ children, className, ...args }: HeroProps) => {
    const classes = ["hero"];
    if (className) {
        classes.push(...className.split(" "));
    }
    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};

Hero.Content = HeroContent;
