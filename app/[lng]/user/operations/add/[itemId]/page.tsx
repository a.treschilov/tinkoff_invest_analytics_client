"use client";

import { useContext, useState, use } from "react";
import { OperationEdit } from "../../../../../../src/Features/Operation/OperationEdit";
import { makeLink } from "../../../../../../src/utils/url";
import { ThemeContext } from "../../../../../../src/providers/ThemeProvider";
import { useRouter } from "next/navigation";
import { useTranslation } from "react-i18next";
import { Divider } from "../../../../../../src/components/Common/Divider/Divider";
import { Card } from "../../../../../../src/components/Common/Card/Card";

const OperationAddPage = (props: { params: Promise<{ itemId: string }> }) => {
    const params = use(props.params);
    const [itemId] = useState<number>(parseInt(params.itemId || "0"));
    const { lng } = useContext(ThemeContext);
    const router = useRouter();
    const { t } = useTranslation("pages");

    const moveToPage = () => {
        router.push(makeLink("/user/item/" + itemId, lng));
    };
    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>{t("UserOperationAdd.PageTitle")}</Card.Title>
            <Divider />
            <OperationEdit
                itemId={itemId}
                onCancel={() => {
                    moveToPage();
                }}
                onChangeData={() => {
                    moveToPage();
                }}
            />
        </Card>
    );
};

export default OperationAddPage;
