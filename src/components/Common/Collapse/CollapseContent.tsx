import { HTMLAttributes, ReactNode } from "react";

type CollapseContentProps = {
    children: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const CollapseContent = ({ children, className, ...args }: CollapseContentProps) => {
    const classes = ["collapse-content"];
    if (className) {
        classes.push(...className?.split(" "));
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
