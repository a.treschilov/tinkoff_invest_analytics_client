import { RealEstateManage } from "./RealEstateManage";
import { Trans } from "react-i18next";

//TODO: remove it
export const RealEstatePage = () => {
    return (
        <div className="container-fluid px-4">
            <h1 className="mt-4 mb-4">
                <Trans>RealEstateManage.PageHeader</Trans>
            </h1>
            <RealEstateManage />
        </div>
    );
};
