import { FC, HTMLAttributes, ReactNode } from "react";
import { FooterTitle } from "./FooterTitle";

type FooterProps = {
    children: ReactNode;
    direction?: "horizontal" | "vertical";
} & HTMLAttributes<HTMLElement>;

type FooterExtension = {
    Title: typeof FooterTitle;
};

export const Footer: FC<FooterProps> & FooterExtension = ({
    children,
    direction = "vertical",
    className,
    ...args
}: FooterProps) => {
    const classes = ["footer"];
    if (className) {
        classes.push(...className.split(" "));
    }
    switch (direction) {
        case "horizontal":
            classes.push("footer-horizontal");
            break;
        case "vertical":
            classes.push("footer-vertical");
            break;
    }
    return (
        <footer className={classes.join(" ")} {...args}>
            {children}
        </footer>
    );
};

Footer.Title = FooterTitle;
