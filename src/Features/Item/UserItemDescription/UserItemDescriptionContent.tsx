import { makeLink } from "../../../utils/url";
import { Item } from "../../../components/Item/Item";
import { useContext, useState } from "react";
import { ThemeContext } from "../../../providers/ThemeProvider";
import { useRouter } from "next/navigation";
import { MdAdd, MdDelete, MdEdit } from "react-icons/md";
import { ItemDelete } from "../Delete/ItemDelete";
import { ItemClose } from "../Close/ItemClose";
import { AiOutlineIssuesClose } from "react-icons/ai";
import { UserDescriptionContentName } from "./UserDescriptionContentName";
import { Button } from "../../../components/Common/Button/Button";

type UserItemDescriptionContentProps = {
    item: Item | null;
    isEditEnable: boolean;
    show?: boolean;
};

export const UserItemDescriptionContent = ({ item, isEditEnable, show = true }: UserItemDescriptionContentProps) => {
    const { lng } = useContext(ThemeContext);
    const [isDeleteConfirmation, setIsDeleteConfirmation] = useState(false);
    const [isCloseConfirmation, setIsCloseConfirmation] = useState(false);
    const router = useRouter();

    const isClosed =
        item?.deposit?.isActive === 0 || item?.loan?.isActive === 0 || item?.realEstate?.isActive === false;

    if (!show || item === null) {
        return <></>;
    }

    return (
        <>
            <div className={"grid grid-cols-4 gap-2"}>
                <div className={"flex gap-2 text-lg col-span-3"}>
                    <UserDescriptionContentName item={item} />
                </div>
                <div className={"text-right flex gap-2 flex-row-reverse flex-wrap"}>
                    {isEditEnable ? (
                        <>
                            <Button
                                color={"neutral"}
                                size={"sm"}
                                shape={"square"}
                                onClick={() => {
                                    setIsDeleteConfirmation(true);
                                }}
                                type={"button"}
                                title={"Удалить"}
                            >
                                <MdDelete />
                            </Button>
                            <Button
                                color={"neutral"}
                                size={"sm"}
                                shape={"square"}
                                onClick={() => {
                                    setIsCloseConfirmation(true);
                                }}
                                type={"button"}
                                title={"Закрыть"}
                                className={isClosed ? "hidden" : ""}
                            >
                                <AiOutlineIssuesClose />
                            </Button>
                        </>
                    ) : (
                        <></>
                    )}
                    <Button
                        size={"sm"}
                        color={"neutral"}
                        shape={"square"}
                        title={"Добавить операцию"}
                        onClick={() => {
                            router.push(makeLink("/user/operations/add/" + item.itemId, lng));
                        }}
                    >
                        <MdAdd />
                    </Button>
                    {isEditEnable ? (
                        <Button
                            color={"neutral"}
                            size={"sm"}
                            shape={"square"}
                            onClick={() => {
                                router.push(makeLink("/user/item/edit/" + item.itemId, lng));
                            }}
                            type={"button"}
                            title={"Изменить"}
                        >
                            <MdEdit />
                        </Button>
                    ) : (
                        <></>
                    )}
                </div>
            </div>
            <ItemDelete
                itemId={item.itemId}
                show={isDeleteConfirmation}
                onSuccess={() => {
                    router.push(makeLink("/user/portfolio", lng));
                }}
                onDiscard={() => {
                    setIsDeleteConfirmation(false);
                }}
            />
            <ItemClose
                itemId={item.itemId}
                show={isCloseConfirmation}
                onSuccess={() => {
                    router.push(makeLink("/user/portfolio", lng));
                }}
                onDiscard={() => {
                    setIsCloseConfirmation(false);
                }}
            />
        </>
    );
};
