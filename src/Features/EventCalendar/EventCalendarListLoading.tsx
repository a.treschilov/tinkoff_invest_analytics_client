import { Trans } from "react-i18next";
import { Card } from "../../components/Common/Card/Card";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Divider } from "../../components/Common/Divider/Divider";

type EventCalendarListLoadingProps = {
    show: boolean;
};

const rowsCount = 5;

export const EventCalendarListLoading = ({ show }: EventCalendarListLoadingProps) => {
    const header = () => {
        return (
            <tr>
                <th>
                    <Trans>PaymentCalendar.PayoutList.ItemNameTitle</Trans>
                </th>
                <th className="hidden md:table-cell">
                    <Trans>PaymentCalendar.PayoutList.PayoutDateTitle</Trans>
                </th>
                <th className="hidden md:table-cell">
                    <Trans>PaymentCalendar.PayoutList.IncomeTitle</Trans>
                </th>
                <th className="hidden md:table-cell">
                    <Trans>PaymentCalendar.PayoutList.OperationTypeTitle</Trans>
                </th>
                <th className="md:hidden">Info</th>
            </tr>
        );
    };

    const row = (value: number) => {
        return (
            <tr key={"eventCalendarListLoading-" + value}>
                <td>
                    <Skeleton className="h-4 w-[175px] sm:w-[215px]" />
                </td>
                <td className="hidden md:table-cell">
                    <Skeleton className="h-4 w-[100px]" />
                </td>
                <td className="hidden md:table-cell">
                    <Skeleton className="h-4 w-[40px]" />
                </td>
                <td className="hidden md:table-cell">
                    <Skeleton className="h-4 w-[55px]" />
                </td>
                <td className="md:hidden">
                    <Skeleton className="h-3 w-[45px]" />
                    <div className={"h-1"}></div>
                    <small className={"my-2"}>
                        <Skeleton className="h-3 w-[85px]" />
                    </small>
                    <div className={"h-1"}></div>
                    <small>
                        <Skeleton className="h-3 w-[55px]" />
                    </small>
                </td>
            </tr>
        );
    };

    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>PaymentCalendar.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <table className="table w-full">
                <thead>{header()}</thead>
                <tbody>
                    {[...Array(rowsCount)].map((value, key) => {
                        return row(key);
                    })}
                </tbody>
            </table>
        </Card>
    );
};
