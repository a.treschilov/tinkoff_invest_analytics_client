import { ButtonHTMLAttributes, ReactNode } from "react";
import { Color, Size } from "../Types";
import { daisyUIColorClass, daisyUISizeClass } from "../utils";

type ButtonProps = {
    color?: Color;
    size?: Size;
    shape?: "circle" | "square";
    link?: boolean;
    outline?: boolean;
    loading?: boolean;
    ghost?: boolean;
    children?: ReactNode;
} & ButtonHTMLAttributes<HTMLButtonElement>;

export const Button = ({
    children,
    className,
    color,
    link = false,
    outline = false,
    loading = false,
    ghost = false,
    size,
    shape,
    ...args
}: ButtonProps) => {
    const classN = ["btn"];
    if (className) {
        classN.push(...className?.split(" "));
    }
    classN.push(daisyUISizeClass("btn", size));
    classN.push(daisyUIColorClass("btn", color));
    if (link) {
        classN.push("btn-link");
    }
    if (outline) {
        classN.push("btn-outline");
    }
    if (ghost) {
        classN.push("btn-ghost");
    }

    switch (shape) {
        case "circle":
            classN.push("btn-circle");
            break;
        case "square":
            classN.push("btn-square");
            break;
    }

    return (
        <button className={classN.join(" ")} {...args}>
            {loading ? <span className="loading loading-spinner"></span> : <></>}
            {children}
        </button>
    );
};
