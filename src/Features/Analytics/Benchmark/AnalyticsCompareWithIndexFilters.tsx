import { DateRange } from "../../../components/Common/DateRange/DateRange";
import Link from "next/link";
import { useContext } from "react";
import { ThemeContext } from "../../../providers/ThemeProvider";
import { makeLink } from "../../../utils/url";

type AnalyticsCompareWithIndexFiltersProps = {
    dateFrom: Date;
    dateTo: Date;
    onDatesChange: (from: Date, to: Date) => void;
};

const AnalyticsCompareWithIndexQuickFilterTranslations = (lng: string) => {
    switch (lng) {
        case "ru":
            return {
                OneDayPeriod: "1Д",
                OneMonthPeriod: "1М",
                ThreeMonthsPeriod: "3М",
                SixMonthsPeriod: "6M",
                YTDPeriod: "YTD",
                OneYearPeriod: "1Г",
                ThreeYearsPeriod: "3Г",
                FiveYearsPeriod: "5Л",
                AllTimePeriod: "Все"
            };
        default:
            return {
                OneDayPeriod: "1D",
                OneMonthPeriod: "1M",
                ThreeMonthsPeriod: "3M",
                SixMonthsPeriod: "6M",
                YTDPeriod: "YTD",
                OneYearPeriod: "1Y",
                ThreeYearsPeriod: "3Y",
                FiveYearsPeriod: "5Y",
                AllTimePeriod: "All"
            };
    }
};

const currentDate = new Date();

export const AnalyticsCompareWithIndexFilters = ({
    dateFrom,
    dateTo,
    onDatesChange
}: AnalyticsCompareWithIndexFiltersProps) => {
    const { lng } = useContext(ThemeContext);
    const translations = AnalyticsCompareWithIndexQuickFilterTranslations(lng);

    const quickFilterOptions = [
        {
            name: translations.OneDayPeriod,
            startDate: new Date(new Date().setDate(currentDate.getDate() - 7))
        },
        {
            name: translations.OneMonthPeriod,
            startDate: new Date(new Date().setMonth(currentDate.getMonth() - 1))
        },
        {
            name: translations.ThreeMonthsPeriod,
            startDate: new Date(new Date().setMonth(currentDate.getMonth() - 3))
        },
        {
            name: translations.SixMonthsPeriod,
            startDate: new Date(new Date().setMonth(currentDate.getMonth() - 6))
        },
        {
            name: translations.YTDPeriod,
            startDate: new Date(currentDate.getFullYear() + "-01-01")
        },
        {
            name: translations.OneYearPeriod,
            startDate: new Date(new Date().setFullYear(currentDate.getFullYear() - 1))
        },
        {
            name: translations.ThreeYearsPeriod,
            startDate: new Date(new Date().setFullYear(currentDate.getFullYear() - 3))
        },
        {
            name: translations.FiveYearsPeriod,
            startDate: new Date(new Date().setFullYear(currentDate.getFullYear() - 5))
        },
        {
            name: translations.AllTimePeriod,
            startDate: new Date("2019-01-01")
        }
    ];

    const onQuickFilterChange = (startDate: Date) => {
        onDatesChange(startDate, currentDate);
    };

    return (
        <div>
            <DateRange
                dateFrom={dateFrom}
                dateTo={dateTo}
                onChange={(dateFrom, dateEnd) => {
                    onDatesChange(dateFrom, dateEnd);
                }}
            />
            <div>
                <small>
                    {quickFilterOptions.map(el => {
                        return (
                            <span key={"analytics-quick-filter-" + el.name}>
                                <Link
                                    key={"filter-option-" + el.name}
                                    href={makeLink("/user/analytics", lng)}
                                    onClick={event => {
                                        onQuickFilterChange(el.startDate);
                                        event.preventDefault();
                                    }}
                                >
                                    {el.name}
                                </Link>
                                &nbsp;&nbsp;&nbsp;
                            </span>
                        );
                    })}
                </small>
            </div>
        </div>
    );
};
