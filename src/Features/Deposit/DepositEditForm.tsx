import { ChangeEvent, useContext, useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { IDepositItem } from "./DepositManage";
import { DefaultDepositData } from "./DepositStaticData";
import { ThemeContext } from "../../providers/ThemeProvider";
import { ApiService } from "../../services/ApiService";
import { formatInputDayDate, toUserTimezone } from "../../utils/date";
import { PageLoader } from "../../components/Loader/Loaders";
import { Alert } from "../../components/Common/Alert/Alert";
import { InputText } from "../../components/Common/Form/InputText";
import { Trans } from "react-i18next";
import { InputCurrencyAmount } from "../../components/Common/Form/InputCurrencyAmount";
import { Button } from "../../components/Common/Button/Button";
import { Join } from "../../components/Common/Join/Join";
import { Select } from "../../components/Common/Select/Select";

type DepositEditFormProps = {
    depositId: number;
    show?: boolean;
    onSubmit?: (message: string | null) => void;
    onDiscard?: () => void;
};

const depositEditTranslation = (lng: string) => {
    switch (lng) {
        case "ru":
            return {
                ItemCashType: "Наличные",
                ItemDepositType: "Вклад"
            };
        default:
            return {
                ItemCashType: "Cash",
                ItemDepositType: "Deposit"
            };
    }
};

export const DepositEditForm = ({
    depositId,
    show = true,
    onSubmit = () => {
        return;
    },
    onDiscard = () => {
        return;
    }
}: DepositEditFormProps) => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [deposit, setDeposit] = useState<IDepositItem>(DefaultDepositData);
    const [message, setMessage] = useState<string | null>(null);
    const [assetType, setAssetType] = useState<"deposit" | "cash">(
        DefaultDepositData.interestPercent === 0 ? "cash" : "deposit"
    );
    const { lng } = useContext(ThemeContext);
    const translations = depositEditTranslation(lng);

    useEffect(() => {
        if (!show) {
            return;
        }
        setPageState(PageState.LOADING);

        if (depositId !== 0) {
            ApiService.fetch("/v1/deposit/item/" + depositId).then(response => {
                const dealDate = formatInputDayDate(toUserTimezone(response.dealDate)) ?? "";
                if (dealDate === "") {
                    setPageState(PageState.ERROR);
                }

                let closedDate = "";
                if (response.closedDate !== null) {
                    closedDate = formatInputDayDate(toUserTimezone(response.dealDate)) ?? "";
                }

                const depositResponse: IDepositItem = {
                    depositId: response.depositId,
                    itemId: response.itemId,
                    isActive: response.isActive,
                    dealDate: {
                        date: dealDate,
                        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                    },
                    closedDate:
                        response.closedDate === null || closedDate === ""
                            ? null
                            : {
                                  date: closedDate,
                                  timezone: response.closedDate.timezone
                              },
                    name: response.name,
                    interestPercent: response.interestPercent,
                    duration: response.duration,
                    durationPeriod: response.durationPeriod,
                    payoutFrequency: response.payoutFrequency,
                    payoutFrequencyPeriod: response.payoutFrequencyPeriod,
                    amount: response.amount,
                    currency: response.currency
                };

                setAssetType(depositResponse.interestPercent === 0 ? "cash" : "deposit");
                setDeposit(depositResponse);
                setPageState(PageState.EDIT_FORM);
            });
        } else {
            setDeposit(DefaultDepositData);
            setPageState(PageState.EDIT_FORM);
        }
    }, [depositId, show]);

    const handleComponentHideModal = (action: string | null) => {
        onSubmit(action);
    };

    const handleFormSubmit = (event: ChangeEvent<HTMLFormElement>) => {
        setPageState(PageState.LOADING);

        const depositRequest = deposit;
        if (assetType === "cash") {
            depositRequest.interestPercent = 0;
        }
        if (depositId === 0) {
            ApiService.post("/v1/deposit/item", depositRequest)
                .then(() => {
                    handleComponentHideModal("addComplete");
                })
                .catch(error => {
                    setPageState(PageState.ERROR);
                    setMessage(error.response.data.message);
                });
        } else {
            ApiService.put("/v1/deposit/item", deposit)
                .then(() => {
                    handleComponentHideModal("editComplete");
                })
                .catch(error => {
                    setPageState(PageState.ERROR);
                    setMessage(error.response.data.message);
                });
        }
        event.preventDefault();
    };

    const handleChangeFormData = (event: ChangeEvent<HTMLInputElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleChangeSelectData = (event: ChangeEvent<HTMLSelectElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const changeElement = (name: string, value: string, inputType: string) => {
        let depositChanged = deposit;

        let typedValue: number | string | object;
        switch (inputType) {
            case "number":
                typedValue =
                    name === "interestPercent" ? parseFloat((parseFloat(value) / 100).toFixed(4)) : parseFloat(value);
                break;
            case "date":
                typedValue = {
                    date: value,
                    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                };
                break;
            default:
                typedValue = value;
        }

        const data = {
            [name]: typedValue
        };
        depositChanged = { ...depositChanged, ...data };

        setDeposit(depositChanged);
    };

    const handleChangeAssetType = (e: ChangeEvent<HTMLInputElement>) => {
        switch (e.target.value) {
            case "cash":
                setAssetType("cash");
                break;
            default:
                setAssetType("deposit");
        }
    };

    const renderLoading = () => {
        return <PageLoader show={true} height={"336px"} />;
    };

    const renderBody = () => {
        const buttonText = depositId !== 0 ? "ButtonSave" : "ButtonAdd";
        const date = formatInputDayDate(toUserTimezone(deposit.dealDate)) || "";
        return (
            <form onSubmit={handleFormSubmit}>
                <Alert
                    show={message !== null}
                    onClose={() => setMessage(null)}
                    dismissible
                    color={pageState === PageState.ERROR ? "danger" : "info"}
                >
                    {message}
                </Alert>
                <div className={"grid grid-cols-2 gap-6 mb-6"}>
                    <InputText
                        label={<Trans>DepositEdit.FormLabelName</Trans>}
                        type="text"
                        name="name"
                        onChange={handleChangeFormData}
                        defaultValue={deposit.name}
                        containerClassName={"col-span-2"}
                    />
                    <InputCurrencyAmount
                        containerClassName={"col-span-2 sm:col-span-1"}
                        id={"deposit-" + deposit.depositId}
                        amount={deposit.amount}
                        currency={deposit.currency}
                        onChange={changeElement}
                        label={<Trans>DepositEdit.FormLabelAmount</Trans>}
                    />
                    <InputText
                        containerClassName={"col-span-2 sm:col-span-1"}
                        label={<Trans>DepositEdit.FormLabelDealDate</Trans>}
                        type="date"
                        name="dealDate"
                        onChange={handleChangeFormData}
                        value={date}
                    />
                    <Join className={"grid-cols-2"} onChange={handleChangeAssetType}>
                        <input
                            className="join-item btn"
                            value={"cash"}
                            type="radio"
                            name="assetTypeRadio"
                            aria-label={translations.ItemCashType}
                            id="asset-type-bth-cash"
                            checked={assetType === "cash"}
                        />
                        <input
                            className="join-item btn"
                            value={"deposit"}
                            type="radio"
                            name="assetTypeRadio"
                            aria-label={translations.ItemDepositType}
                            id="asset-type-bth-deposit"
                            checked={assetType === "deposit"}
                        />
                    </Join>
                </div>
                <div className={"grid grid-cols-2 gap-6 mb-6 " + (assetType === "cash" ? "hidden" : "")}>
                    <InputText
                        label={<Trans>DepositEdit.FormLabelInterestPercent</Trans>}
                        type="number"
                        min="0"
                        step="0.01"
                        onChange={handleChangeFormData}
                        name="interestPercent"
                        defaultValue={(deposit.interestPercent * 100).toFixed(2)}
                        containerClassName={"col-span-2"}
                    />
                    <div className={`form-control w-full col-span-2 sm:col-span-1`}>
                        <label className="label">
                            <span className={"label-text text-base-content"}>
                                <Trans>DepositEdit.FormLabelPayoutFrequency</Trans>
                            </span>
                        </label>
                        <Join>
                            <input
                                type="number"
                                min="0"
                                onChange={handleChangeFormData}
                                name="payoutFrequency"
                                defaultValue={deposit.payoutFrequency}
                                className={"input input-bordered w-full rounded-r-none"}
                            />
                            <Select
                                name="payoutFrequencyPeriod"
                                onChange={handleChangeSelectData}
                                defaultValue={deposit.payoutFrequencyPeriod}
                                className={"rounded-l-none"}
                            >
                                <option value="day">
                                    <Trans>DepositEdit.PeriodDay</Trans>
                                </option>
                                <option value="month">
                                    <Trans>DepositEdit.PeriodMonth</Trans>
                                </option>
                                <option value="year">
                                    <Trans>DepositEdit.PeriodYear</Trans>
                                </option>
                            </Select>
                        </Join>
                    </div>
                    <div className={`form-control w-full col-span-2 sm:col-span-1`}>
                        <label className="label">
                            <span className={"label-text text-base-content"}>
                                <Trans>DepositEdit.FormLabelDurationPeriod</Trans>
                            </span>
                        </label>
                        <Join>
                            <input
                                type="number"
                                min="0"
                                name="duration"
                                onChange={handleChangeFormData}
                                defaultValue={deposit.duration}
                                className={"input input-bordered w-full rounded-r-none"}
                            />
                            <Select
                                name="durationPeriod"
                                onChange={handleChangeSelectData}
                                defaultValue={deposit.durationPeriod}
                                className={"rounded-l-none"}
                            >
                                <option value="day">
                                    <Trans>DepositEdit.PeriodDay</Trans>
                                </option>
                                <option value="month">
                                    <Trans>DepositEdit.PeriodMonth</Trans>
                                </option>
                                <option value="year">
                                    <Trans>DepositEdit.PeriodYear</Trans>
                                </option>
                            </Select>
                        </Join>
                    </div>
                </div>
                <div className={"text-right"}>
                    <Button type={"button"} color={"neutral"} onClick={onDiscard} className={"mr-2"}>
                        <Trans>DepositEdit.ButtonCancel</Trans>
                    </Button>
                    <Button type="submit" color="primary">
                        <Trans>DepositEdit.{buttonText}</Trans>
                    </Button>
                </div>
            </form>
        );
    };

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = renderLoading();
            break;
        case PageState.EDIT_FORM:
        case PageState.ERROR:
            content = renderBody();
            break;
    }

    return <>{content}</>;
};
