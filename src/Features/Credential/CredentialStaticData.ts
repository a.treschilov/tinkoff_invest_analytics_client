import { UserCredentialType } from "./CredentialForm";
import { BrokerType } from "./CredentialFormBody";

export const DefaultUserCredential: UserCredentialType = {
    userCredentialId: 0,
    brokerId: "tinkoff2",
    brokerName: "Тинькофф",
    apiKey: "",
    isActive: 1
};

export const DefaultBrokerList: BrokerType[] = [
    {
        id: "tinkoff2",
        name: "Тинькофф",
        isApi: true,
        isManual: false,
        logo: "tinkoff.png",
        instructionLink: null
    }
];
