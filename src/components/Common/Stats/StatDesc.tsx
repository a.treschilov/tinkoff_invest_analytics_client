import { HTMLAttributes, ReactNode } from "react";

type StatDescProps = {
    children: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const StatDesc = ({ children, className, ...args }: StatDescProps) => {
    const classN = ["stat-desc"];
    if (className) {
        classN.push(...className.split(" "));
    }
    return (
        <div className={classN.join(" ")} {...args}>
            {children}
        </div>
    );
};
