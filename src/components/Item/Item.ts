import { ItemType, PeriodType } from "../Common/Types";
import { IDate } from "../../utils/date";
import { IPrice } from "../../interfaces/IPrice";

export type Item = {
    itemId: number;
    externalId: string;
    type: ItemType;
    name: string;
    isOutdated: boolean;
    logo: string | null;
    loan: Loan | null;
    deposit: Deposit | null;
    realEstate: RealEstate | null;
};

export type Loan = {
    loanId: number | null;
    itemId: number | null;
    name: string;
    amount: number;
    currency: string;
    date: IDate;
    isActive: number;
    closedDate: IDate | null;
    paymentAmount: number;
    paymentCurrency: string;
    payoutFrequency: number;
    payoutFrequencyPeriod: PeriodType;
    interestPercent: number;
};

export type Deposit = {
    depositId: number;
    itemId: number;
    name: string;
    isActive: number;
    dealDate: IDate;
    closedDate: IDate | null;
    interestPercent: number;
    duration: number;
    durationPeriod: PeriodType;
    payoutFrequency: number;
    payoutFrequencyPeriod: PeriodType;
    amount: number;
    currency: string;
};

export type RealEstate = {
    realEstateId: number | null;
    itemId: number | null;
    payoutFrequency: number;
    payoutFrequencyPeriod: PeriodType;
    interestAmount: number;
    interestCurrency: string;
    currency: string;
    name: string;
    prices: RealEstatePrice[];
    purchaseDate: IDate;
    purchaseAmount: number;
    isActive: boolean;
    saleOperation: null | {
        price: IPrice;
        date: IDate;
    };
};

export type RealEstatePrice = {
    realEstatePriceId: number | null;
    amount: number;
    currency: string;
    date: IDate;
};
