"use client";

import { PageState } from "../../components/Common/Types";
import { IWealth } from "../../../app/[lng]/user/analytics/page";

import dynamic from "next/dynamic";
import { ApexOptions } from "apexcharts";
import { PageLoader } from "../../components/Loader/Loaders";
import { defaultChartOptions } from "./ChatOptions";
import { mergeDeep } from "../../utils/objects";
const Chart = dynamic(() => import("react-apexcharts"), {
    ssr: false
});

type ApexChartTotalValueType = {
    globals: {
        seriesTotals: number[];
    };
};
interface IAnalyticsAssetsTypesProps {
    assetsTypes: IWealth | null;
    dataState: string;
}

const compactCurrencyFormatter = Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB",
    notation: "compact"
});

export const AnalyticsAssetsTypes = ({ assetsTypes, dataState }: IAnalyticsAssetsTypesProps) => {
    const renderChart = () => {
        return <Chart options={chartOptions} series={series} type="donut" width="100%" height="500" />;
    };

    const series = [
        assetsTypes?.stock || 0,
        assetsTypes?.realEstate || 0,
        assetsTypes?.deposit || 0,
        assetsTypes?.assets || 0
    ];

    const options: ApexOptions = {
        labels: ["Stock", "Real Estate", "Deposit", "Crowdfunding"],
        dataLabels: {
            enabled: true
        },
        stroke: {
            width: 3,
            colors: ["transparent"]
        },
        legend: {
            position: "bottom"
        },
        plotOptions: {
            pie: {
                donut: {
                    labels: {
                        show: true,
                        value: {
                            show: true,
                            formatter: function (val) {
                                return compactCurrencyFormatter.format(parseFloat(val));
                            }
                        },
                        total: {
                            show: true,
                            label: "Всего",
                            formatter(value: ApexChartTotalValueType): string {
                                const total = value.globals.seriesTotals.reduce((a: number, b: number) => {
                                    return a + b;
                                }, 0);

                                return compactCurrencyFormatter.format(total);
                            }
                        }
                    }
                }
            }
        }
    };

    const chartOptions = mergeDeep(defaultChartOptions, options);

    let content;
    switch (dataState) {
        case PageState.LOADING:
            content = <PageLoader />;
            break;
        case PageState.EDIT_FORM:
            content = renderChart();
            break;
        default:
            content = <></>;
    }

    return content;
};
