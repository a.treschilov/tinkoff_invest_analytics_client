"use client";

import { useContext, useEffect, useState } from "react";
import { BrokerApiIntegration } from "./BrokerApiIntegration";
import { BrokerManualIntegration } from "./BrokerManualIntegration";
import { BrokerList } from "./BrokerList";
import { BrokerType } from "./CredentialFormBody";
import { ApiService } from "../../services/ApiService";
import { PageState } from "../../components/Common/Types";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { BrokerUploadReport } from "./BrokerUploadReport";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

export const BrokerListManage = () => {
    const [brokerId, setBrokerId] = useState<string | null>(null);
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [brokerList, setBrokerList] = useState<BrokerType[]>([]);
    const [currentBroker, setCurrentBroker] = useState<BrokerType | null>(null);
    const [reportListTime, setReportListTime] = useState(new Date().valueOf());
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "broker");

    useEffect(() => {
        ApiService.fetch("/v1/broker/list").then((response: BrokerType[]) => {
            setBrokerList(response);
            const firstBroker = response.at(0);
            if (firstBroker !== undefined) {
                setBrokerId(firstBroker.id);
            }

            setPageState(PageState.SUCCESS);
        });
    }, []);

    useEffect(() => {
        if (brokerId === null) {
            setCurrentBroker(null);
        } else {
            const broker = brokerList.find(broker => {
                return broker.id === brokerId;
            });
            setCurrentBroker(broker === undefined ? null : broker);
        }
    }, [brokerList, brokerId]);

    return (
        <>
            <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                <Card.Title>{t("List.Title")}</Card.Title>
                <Divider />
                <BrokerList
                    onItemClick={itemId => {
                        setBrokerId(itemId);
                    }}
                    brokerList={brokerList}
                    pageState={pageState}
                    currentBrokerId={brokerId}
                />
                <BrokerApiIntegration
                    show={currentBroker !== null ? currentBroker.isApi : false}
                    broker={currentBroker}
                />
                <BrokerManualIntegration
                    show={currentBroker !== null ? currentBroker.isManual : false}
                    broker={currentBroker}
                    onUpload={() => {
                        setReportListTime(new Date().valueOf());
                    }}
                />
            </Card>
            <BrokerUploadReport date={reportListTime} />
        </>
    );
};
