import { StepComponentProps } from "./NewUserSteps";
import { InputCurrencyAmount } from "../../components/Common/Form/InputCurrencyAmount";
import { Trans } from "react-i18next";
import { useEffect, useState } from "react";
import { DefaultUserSettingsData } from "../UserSettings/UserSettingsStaticData";
import { ApiService } from "../../services/ApiService";
import { Button } from "../../components/Common/Button/Button";

export const NewUserCapital = ({ onComplete, active }: StepComponentProps) => {
    const [expenses, setExpenses] = useState(DefaultUserSettingsData.expenses);
    const [desiredPension, setDesiredPension] = useState(DefaultUserSettingsData.desiredPension);

    useEffect(() => {
        ApiService.fetch("/v1/user/settings").then(response => {
            setExpenses({
                amount: response.expenses.amount || 0,
                currency: response.expenses.currency || "RUB"
            });
            setDesiredPension({
                amount: response.desiredPension.amount || 0,
                currency: response.desiredPension.currency || "RUB"
            });
        });
    }, []);

    const changeElement = (name: string, value: string): void => {
        switch (name) {
            case "expensesAmount":
                setExpenses({
                    amount: parseFloat(value),
                    currency: expenses.currency
                });
                break;
            case "expensesCurrency":
                setExpenses({
                    amount: expenses.amount,
                    currency: value
                });
                break;
            case "desiredPensionAmount":
                setDesiredPension({
                    amount: parseFloat(value),
                    currency: desiredPension.currency
                });
                break;
            case "desiredPensionCurrency":
                setDesiredPension({
                    amount: desiredPension.amount,
                    currency: value
                });
                break;
        }
    };

    const onFormSubmit = () => {
        const data = {
            desiredPension: desiredPension,
            expenses: expenses
        };
        ApiService.put("/v1/user/settings", data);
        onComplete();
    };

    if (!active) {
        return <></>;
    }

    return (
        <form
            className={"grid gap-6"}
            onSubmit={e => {
                onFormSubmit();
                e.preventDefault();
            }}
        >
            <InputCurrencyAmount
                amount={desiredPension.amount}
                onChange={changeElement}
                currency={desiredPension.currency}
                id={"desiredPension"}
                currencyInputName={"desiredPensionCurrency"}
                amountInputName={"desiredPensionAmount"}
                label={<Trans>PortfolioSettings.DesiredIncomeTitle</Trans>}
                topDescription={<Trans>PortfolioSettings.DesiredPensionDescription</Trans>}
            />
            <InputCurrencyAmount
                amountInputName="expensesAmount"
                currencyInputName="expensesCurrency"
                amount={desiredPension.amount}
                onChange={changeElement}
                currency={desiredPension.currency}
                id={"expenses"}
                label={<Trans>PortfolioSettings.Expenses</Trans>}
                topDescription={<Trans>PortfolioSettings.ExpensesDescription</Trans>}
            />
            <Button className="w-auto" color={"primary"} type={"submit"}>
                <Trans>NewUserSteps.Capital.NextButton</Trans>
            </Button>
        </form>
    );
};
