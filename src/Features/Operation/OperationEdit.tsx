import { ItemType, PageState } from "../../components/Common/Types";
import { ChangeEvent, useCallback, useEffect, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { formatInputDateTime, toUserTimezone } from "../../utils/date";
import { Alert } from "../../components/Common/Alert/Alert";
import { Trans } from "react-i18next";
import { PageLoader } from "../../components/Loader/Loaders";
import { InputCurrencyAmount } from "../../components/Common/Form/InputCurrencyAmount";
import { OperationAddTypeSelect } from "./OperationAddTypeSelect";
import { InputText } from "../../components/Common/Form/InputText";
import { OperationType } from "../OperationList/OperationTypeTranslation";
import { DefaultOperationData } from "./OperationStaticData";
import { Item } from "../../components/Item/Item";
import { useErrorBoundary } from "react-error-boundary";
import { Button } from "../../components/Common/Button/Button";

type OperationEditProps = {
    onCancel?: (text: string | null) => void;
    onChangeData?: (text?: string) => void;
    itemId: number;
    operationId?: number;
};

export interface IOperation {
    itemOperationId: number | null;
    brokerId: string | null;
    itemId: number;
    itemName: string;
    itemType: ItemType;
    operationType: OperationType;
    externalId: string | null;
    quantity: number | null;
    date: {
        date: string;
        timezone: string;
    };
    amount: number;
    currencyIso: string;
    status: string;
}

export const OperationEdit = ({
    operationId = 0,
    itemId,
    onCancel = () => {
        return;
    },
    onChangeData = () => {
        return;
    }
}: OperationEditProps) => {
    const [operation, setOperation] = useState<IOperation>(DefaultOperationData);
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [hasError, setHasError] = useState(false);
    const [stepAmount, setStepAmount] = useState(1);
    const [isQuantityApplicable, setIsQuantityApplicable] = useState(false);
    const { showBoundary } = useErrorBoundary();

    const getDefaultOperationData = useCallback(
        (itemType: ItemType): IOperation => {
            const data = DefaultOperationData;
            data.itemId = itemId;

            switch (itemType) {
                case "deposit":
                    data.operationType = "BuyCard";
                    break;
                case "loan":
                    data.operationType = "SellCard";
                    break;
                case "market":
                    data.operationType = "Buy";
                    break;
                default:
                    data.operationType = "DividendCard";
            }

            return data;
        },
        [itemId]
    );

    const prepareOperationForServer = useCallback(
        (operationData: IOperation): IOperation => {
            if (operationData.itemType === "loan") {
                if (["SellCard", "Dividend"].indexOf(operationData.operationType) !== -1) {
                    operationData.amount = -1 * operationData.amount;
                }
            }
            if (operationData.itemType === "deposit") {
                if (["BuyCard"].indexOf(operationData.operationType) !== -1) {
                    operationData.amount = -1 * operationData.amount;
                }
            }
            if (operationData.itemType === "crowdfunding") {
                if (["Buy"].indexOf(operationData.operationType) !== -1) {
                    operationData.amount = -1 * operationData.amount;
                }
            }

            if (operationData.itemType === "market") {
                if (!isQuantityApplicable) {
                    operationData.quantity = null;
                }

                if (["Buy", "BuyCard", "Taxes", "Fee"].indexOf(operationData.operationType) !== -1) {
                    operationData.amount = -1 * operationData.amount;
                }
            }

            return operationData;
        },
        [isQuantityApplicable]
    );

    useEffect(() => {
        if (operationId !== 0) {
            ApiService.fetch("/v1/operations/operation/" + operationId)
                .then(data => {
                    const operationData = prepareOperationForServer({
                        itemOperationId: data.id,
                        brokerId: data.broker,
                        itemId: data.itemId,
                        itemName: data.itemName,
                        itemType: data.itemType,
                        operationType: data.type,
                        externalId: data.externalId,
                        quantity: data.quantity,
                        date: {
                            date: formatInputDateTime(toUserTimezone(data.date)),
                            timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                        },
                        amount: data.amount,
                        currencyIso: data.currency,
                        status: data.status
                    });

                    defineStepAmount(operationData.itemType);
                    defineQuantityApplicable(operationData.itemType, operationData.operationType);
                    setOperation(operationData);
                    setPageState(PageState.SUCCESS);
                })
                .catch(error => {
                    showBoundary(error);
                });
        } else {
            ApiService.fetch("/v1/item/info/" + itemId)
                .then((data: Item) => {
                    defineStepAmount(data.type);
                    defineQuantityApplicable(data.type, getDefaultOperationData(data.type).operationType);
                    setOperation({ ...getDefaultOperationData(data.type), ...{ itemType: data.type } });
                    setPageState(PageState.SUCCESS);
                })
                .catch(error => {
                    showBoundary(error);
                });
        }
    }, [getDefaultOperationData, itemId, operationId, prepareOperationForServer, showBoundary]);

    const defineStepAmount = (itemType: ItemType) => {
        switch (itemType) {
            case "real_estate":
                setStepAmount(1000);
                break;
            case "loan":
            case "deposit":
                setStepAmount(0.01);
                break;
            default:
                setStepAmount(1);
        }
    };

    const defineQuantityApplicable = (itemType: ItemType, operationType: OperationType): void => {
        if (itemType !== "market") {
            setIsQuantityApplicable(false);
        } else {
            setIsQuantityApplicable(["Sell", "SellCard", "Buy", "BuyCard"].indexOf(operationType) !== -1);
        }
    };

    const handleChangeFormData = (event: ChangeEvent<HTMLInputElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleChangeSelectData = (event: ChangeEvent<HTMLSelectElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleSubmitForm = (event: ChangeEvent<HTMLFormElement>) => {
        setPageState(PageState.LOADING);

        let $func;
        let text: string;
        if (operationId === 0) {
            $func = addOperation;
            text = "operationAdded";
        } else {
            $func = editOperation;
            text = "operationEdited";
        }
        $func(operation)
            .then(() => {
                if (onChangeData) {
                    onChangeData(text);
                }
            })
            .catch(() => {
                setPageState(PageState.SUCCESS);
                setHasError(true);
            });

        event.preventDefault();
    };

    const changeElement = (name: string, value: string, inputType: string) => {
        let operationData = operation;

        let typedValue: number | string | object;
        switch (inputType) {
            case "number":
                typedValue = parseFloat(value);
                break;
            case "datetime-local":
                typedValue = {
                    date: value,
                    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                };
                break;
            default:
                typedValue = value;
        }

        const data = {
            [name]: typedValue
        };
        operationData = { ...operationData, ...data };

        defineQuantityApplicable(operationData.itemType, operationData.operationType);
        setOperation(operationData);
    };

    const addOperation = (operationData: IOperation) => {
        operationData = prepareOperationForServer(operationData);

        return ApiService.post("/v1/operations", operationData);
    };

    const editOperation = (operationData: IOperation) => {
        operationData = prepareOperationForServer(operationData);

        return ApiService.put("/v1/operations/operation/" + operationData.itemOperationId, operationData);
    };

    const closeErrorMessage = () => {
        setHasError(false);
    };

    const renderError = () => {
        if (hasError) {
            return (
                <Alert status="error" onClose={closeErrorMessage} dismissible>
                    <Trans>OperationAdd.ErrorMessage</Trans>
                </Alert>
            );
        }

        return;
    };

    const buttonVariant = "primary";
    const buttonText = (
        <Trans>
            OperationAdd.
            {operationId === 0 ? "ButtonTextAdd" : "ButtonTextEdit"}
        </Trans>
    );

    if (pageState === PageState.LOADING) {
        return <PageLoader height={"250px"} />;
    }

    return (
        <form onSubmit={handleSubmitForm}>
            {renderError()}
            <div className={"grid grid-cols-1 gap-2 mb-2"}>
                <OperationAddTypeSelect
                    itemType={operation.itemType}
                    defaultValue={operation.operationType}
                    onChange={handleChangeSelectData}
                />
                <InputCurrencyAmount
                    label={<Trans>OperationAdd.FormLabelAmount</Trans>}
                    id={"operation" + operation.itemOperationId}
                    amount={operation.amount}
                    currency={operation.currencyIso}
                    onChange={changeElement}
                    step={stepAmount}
                    currencyInputName={"currencyIso"}
                />
                <InputText
                    label={<>Quantity</>}
                    type={"number"}
                    name={"quantity"}
                    defaultValue={operation.quantity || undefined}
                    onChange={handleChangeFormData}
                    hidden={!isQuantityApplicable}
                />
                <InputText
                    label={<Trans>OperationAdd.FormLabelDate</Trans>}
                    type="datetime-local"
                    name="date"
                    value={operation.date.date}
                    onChange={handleChangeFormData}
                />
            </div>
            <div className={"flex flex-row-reverse mt-4"}>
                <div>
                    <Button type="submit" color={buttonVariant}>
                        {buttonText}
                    </Button>
                </div>
                <div>
                    <Button link={true} type="button" onClick={() => (onCancel ? onCancel(null) : {})}>
                        <Trans>OperationAdd.ButtonTextCancel</Trans>
                    </Button>
                </div>
            </div>
        </form>
    );
};
