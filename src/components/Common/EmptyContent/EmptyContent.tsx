import { ReactNode, useEffect, useState } from "react";
import { BsHourglass } from "react-icons/bs";

type EmptyContentProps = {
    children: ReactNode;
    show?: boolean;
    size?: "xs" | "sm" | "lg";
};
export const EmptyContent = ({ children, show = true, size = "lg" }: EmptyContentProps) => {
    const [classNameHours, setClassNameHours] = useState("h-32 w-32 lg:h-48 lg:w-48");
    const [classNameText, setClassNameText] = useState("text-3xl lg:text-4xl");

    useEffect(() => {
        switch (size) {
            case "xs":
                setClassNameHours("h-14 w-14 lg:h-20 lg:w-20");
                setClassNameText("text-lg lg:text-xl");
                break;
            case "sm":
                setClassNameHours("h-20 w-20 lg:h-28 lg:w-28");
                setClassNameText("text-xl lg:text-2xl");
                break;
            case "lg":
                setClassNameHours("h-32 w-32");
                setClassNameText("text-3xl");
                break;
        }
    }, [size]);

    if (!show) {
        return <></>;
    }

    return (
        <div className={"text-center"}>
            <BsHourglass className={classNameHours + " inline-block"} />
            <h1 className={classNameText + " font-bold"}>{children}</h1>
        </div>
    );
};
