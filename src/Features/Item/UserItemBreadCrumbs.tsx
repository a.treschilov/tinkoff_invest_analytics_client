"use client";

import { Trans } from "react-i18next";
import Link from "next/link";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import { SessionContext } from "../../providers/SessionProvider";
import { Card } from "../../components/Common/Card/Card";
import { Breadcrumbs } from "../../components/Common/Breadcrumbs/Breadcrumbs";

export const UserItemBreadCrumbs = () => {
    const { lng } = useContext(ThemeContext);
    const { previousPage } = useContext(SessionContext);

    const renderPreviousPage = () => {
        const defaultCrumb = (
            <Link href={makeLink("/user/portfolio", lng)} className={"link"}>
                <Trans>UserItem.OperationBreadCrumb</Trans>
            </Link>
        );

        if (previousPage === null) {
            return defaultCrumb;
        }

        switch (previousPage) {
            case "operations": {
                return (
                    <Link href={makeLink("/user/operations/list", lng)} className={"link"}>
                        <Trans>UserItem.OperationsBreadCrumb</Trans>
                    </Link>
                );
            }
            case "deposit": {
                return (
                    <Link href={makeLink("/user/deposit", lng)} className={"link"}>
                        <Trans>UserItem.DepositBreadCrumb</Trans>
                    </Link>
                );
            }
            case "loan": {
                return (
                    <Link href={makeLink("/user/loan", lng)} className={"link"}>
                        <Trans>UserItem.LoanBreadCrumb</Trans>
                    </Link>
                );
            }
            case "realEstate": {
                return (
                    <Link href={makeLink("/user/realEstate", lng)} className={"link"}>
                        <Trans>UserItem.RealEstateBreadCrumb</Trans>
                    </Link>
                );
            }
            case "crowdlanding": {
                return (
                    <Link href={makeLink("/user/crowdlanding", lng)} className={"link"}>
                        <Trans>UserItem.CrowdLandingBreadCrumb</Trans>
                    </Link>
                );
            }
            case "stockList": {
                return (
                    <Link href={makeLink("/user/market/stock", lng)} className={"link"}>
                        <Trans>UserItem.StockListBreadCrumb</Trans>
                    </Link>
                );
            }
            case "bondList": {
                return (
                    <Link href={makeLink("/user/market/bond", lng)} className={"link"}>
                        <Trans>UserItem.BondListBreadCrumb</Trans>
                    </Link>
                );
            }
            case "futureList": {
                return (
                    <Link href={makeLink("/user/market/future", lng)} className={"link"}>
                        <Trans>UserItem.FutureListBreadCrumb</Trans>
                    </Link>
                );
            }
            default:
                return defaultCrumb;
        }
    };

    return (
        <Card className={"bg-base-100 p-2 pl-6 mb-2"} border={true}>
            <Breadcrumbs>
                <li>
                    <Link href={makeLink("/user", lng)} className={"link"}>
                        <Trans>UserItem.DashboardBreadCrumb</Trans>
                    </Link>
                </li>
                <li>{renderPreviousPage()}</li>
                <li>
                    <Trans>UserItem.UserItemBreadCrumb</Trans>
                </li>
            </Breadcrumbs>
        </Card>
    );
};
