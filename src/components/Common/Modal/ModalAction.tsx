import { HTMLAttributes, ReactNode } from "react";

type ModalActionProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const ModalAction = ({ children, className, ...args }: ModalActionProps) => {
    const classes = ["modal-action"];
    if (className) {
        classes.push(...className?.split(" "));
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
