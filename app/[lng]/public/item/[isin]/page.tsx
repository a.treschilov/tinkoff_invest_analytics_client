import { Metadata } from "next";
import { ApiServiceNext } from "../../../../../src/services/ApiServiceNext";
import { PriceHistoryManage } from "../../../../../src/Features/Item/PriceHistory/PriceHistoryManage";

export async function generateMetadata(props: { params: Promise<{ isin: string }> }): Promise<Metadata> {
    const params = await props.params;
    const resp = await ApiServiceNext.fetch("/v1/market/item/" + params.isin);

    if (resp.status === 422) {
        return {
            title: "Страница не найдена. Hakkes — инструмент учета инвестиций",
            description:
                "Управление и анализ инвестиционного портфеля, балансировка портфеля, аналитика компаний и дивидендов. Планирование инвестиций в акции, облигации, недвижимость, краудлэндинг.",
            openGraph: {
                title: "Страница не найдена. Hakkes — инструмент учета инвестиций",
                description:
                    "Управление и анализ инвестиционного портфеля, балансировка портфеля, аналитика компаний и дивидендов. Планирование инвестиций в акции, облигации, недвижимость, краудлэндинг.",
                images: ["/images/open_graph_image.png"]
            }
        };
    }

    const item = await resp.json();
    return {
        title: item.name + " - управление и анализ инвестиционного портфеля, Hakkes",
        description:
            "Управление и анализ инвестиционного портфеля, балансировка портфеля, аналитика компаний и дивидендов. Планирование инвестиций в акции, облигации, недвижимость, краудлэндинг.",
        openGraph: {
            title: item.name + "— управление и анализ инвестиционного портфеля, Hakkes",
            description:
                "Управление и анализ инвестиционного портфеля, балансировка портфеля, аналитика компаний и дивидендов. Планирование инвестиций в акции, облигации, недвижимость, краудлэндинг.",
            images: ["/images/open_graph_image.png"]
        }
    };
}

const ItemPage = async (props: { params: Promise<{ lng: string; isin: string }> }) => {
    const params = await props.params;
    const resp = await ApiServiceNext.fetch("/v1/market/item/" + params.isin);
    const item = await resp.json();

    return <PriceHistoryManage item={item} lng={params.lng} />;
};

export default ItemPage;
