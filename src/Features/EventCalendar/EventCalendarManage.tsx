import { useCallback, useEffect, useState } from "react";
import { DateType, PageState } from "../../components/Common/Types";
import { EventCalendarFilters, EventCalendarFilterType } from "./EventCalendarFilters";
import { formatInputDayDate } from "../../utils/date";
import { ApiService } from "../../services/ApiService";
import { EventCalendarListLoading } from "./EventCalendarListLoading";
import { EventCalendarList } from "./EventCalendarList";
import { Item } from "../../components/Item/Item";
import { IPrice } from "../../interfaces/IPrice";

export type PaymentEventType = {
    itemId: number;
    item: Item;
    interest: IPrice;
    debt: IPrice;
    date: DateType;
};

const initialDateFrom = new Date();
initialDateFrom.setUTCHours(0, 0, 0, 0);
const initialDateTo = new Date();
initialDateTo.setDate(initialDateTo.getDate() - 1);
initialDateTo.setMonth(initialDateTo.getMonth() + 1);
initialDateTo.setHours(23, 59, 59, 999);

const EventCalendarManage = () => {
    const [events, setEvents] = useState<PaymentEventType[]>([]);
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [filters, setFilters] = useState<EventCalendarFilterType>({
        dateFrom: initialDateFrom,
        dateTo: initialDateTo,
        assetTypes: []
    });

    const fetchEvents = useCallback(() => {
        setPageState(PageState.LOADING);
        const params = {
            dateFrom: formatInputDayDate(filters.dateFrom),
            dateTo: formatInputDayDate(filters.dateTo),
            assetTypes: filters.assetTypes.length > 0 ? filters.assetTypes.join(",") : null
        };
        ApiService.fetch("/v1/item/portfolio/eventsCalendar", params)
            .then(response => {
                const paymentEvents = response.events.map((responseItem: PaymentEventType) => {
                    return {
                        itemId: responseItem.itemId,
                        item: responseItem.item,
                        interest: responseItem.interest,
                        debt: responseItem.debt,
                        date: responseItem.date
                    };
                });
                setEvents(paymentEvents);
                setPageState(PageState.SUCCESS);
            })
            .catch(() => {
                setPageState(PageState.ERROR);
            });
    }, [filters.assetTypes, filters.dateFrom, filters.dateTo]);

    useEffect(() => {
        fetchEvents();
    }, [fetchEvents, filters]);

    const handleFilterApply = (formFilters: EventCalendarFilterType) => {
        setFilters(formFilters);
    };

    return (
        <>
            <EventCalendarFilters defaultValues={filters} onSubmit={handleFilterApply} />
            <EventCalendarListLoading show={pageState === PageState.LOADING} />
            <EventCalendarList show={pageState === PageState.SUCCESS} events={events} />
        </>
    );
};

export default EventCalendarManage;
