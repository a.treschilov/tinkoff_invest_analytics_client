import { useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { Modal } from "../../components/Common/Modal/Modal";
import { LoanEditForm } from "./LoanEditForm";

type LoanEditPropsType = {
    isShow: boolean;
    loanId: number;
    hideModalHandler: (action: string | null) => void;
};

type ModeType = "add" | "edit";

export const LoanEdit = ({ isShow, loanId, hideModalHandler }: LoanEditPropsType) => {
    const [mode, setMode] = useState<ModeType>("edit");

    useEffect(() => {
        if (!isShow) {
            return;
        }

        if (loanId !== 0) {
            setMode("edit");
        } else {
            setMode("add");
        }
    }, [loanId, isShow]);

    const handleHideModal = (action: string | null) => {
        hideModalHandler(action);
    };

    return (
        <Modal show={isShow} onClose={() => handleHideModal(null)}>
            <Modal.Header>
                <Trans>
                    LoanEdit.
                    {mode === "edit" ? "ModalEditHeader" : "ModalAddHeader"}
                </Trans>
            </Modal.Header>
            <LoanEditForm
                show={isShow}
                loanId={loanId}
                onSubmit={handleHideModal}
                onDiscard={() => {
                    handleHideModal(null);
                }}
            />
        </Modal>
    );
};
