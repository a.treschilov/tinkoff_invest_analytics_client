import { FC, HTMLAttributes, ReactNode } from "react";
import { Tab } from "./Tab";

type TabsProps = {
    children: ReactNode;
    decorate?: "box" | "border" | "lift";
} & HTMLAttributes<HTMLDivElement>;

type TabsExtension = {
    Tab: typeof Tab;
};

export const Tabs: FC<TabsProps> & TabsExtension = ({ children, className, decorate, ...args }) => {
    const classes = ["tabs"];
    switch (decorate) {
        case "box":
            classes.push("tabs-box");
            break;
        case "border":
            classes.push("tabs-bordered");
            break;
        case "lift":
            classes.push("tabs-lifted");
    }
    if (className) {
        classes.push(...className.split(" "));
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};

Tabs.Tab = Tab;
