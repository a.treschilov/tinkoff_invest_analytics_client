import { ChangeEvent, useState } from "react";
import { Trans } from "react-i18next";
import { MultiValue } from "react-select";
import { DateRange } from "../../components/Common/DateRange/DateRange";
import { AsyncSelectWrapper } from "../../components/Common/Form/AsyncSelectWrapper";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

const initialDateFrom = new Date();
initialDateFrom.setUTCHours(0, 0, 0, 0);
const initialDateTo = new Date();
initialDateTo.setDate(initialDateTo.getDate() - 1);
initialDateTo.setMonth(initialDateTo.getMonth() + 1);
initialDateTo.setHours(23, 59, 59, 999);

type EventCalendarFiltersProps = {
    defaultValues: EventCalendarFilterType;
    onSubmit: (filter: EventCalendarFilterType) => void;
};

export type EventCalendarFilterType = {
    dateFrom: Date;
    dateTo: Date;
    assetTypes: string[];
};

const options = [
    { value: "market", label: "Market" },
    { value: "deposit", label: "Deposit" },
    { value: "real_estate", label: "Real Estate" },
    { value: "crowdfunding", label: "Crowdfunding" },
    { value: "loan", label: "Loan" }
];

export const EventCalendarFilters = ({ defaultValues, onSubmit }: EventCalendarFiltersProps) => {
    const [dateFrom, setDateFrom] = useState(defaultValues.dateFrom);
    const [dateTo, setDateTo] = useState(defaultValues.dateTo);
    const [assetTypes, setAssetTypes] = useState<string[]>(defaultValues.assetTypes);

    const handleSubmit = (event: ChangeEvent<HTMLFormElement>) => {
        onSubmit({ dateFrom: dateFrom, dateTo: dateTo, assetTypes: assetTypes });
        event.preventDefault();
    };

    const changeDateRange = (dateFrom: Date, dateTo: Date): void => {
        setDateTo(dateTo);
        setDateFrom(dateFrom);
    };

    const changeSelect = (value: MultiValue<{ value: string; label: string }>) => {
        const assets: string[] = [];
        value.forEach(item => {
            assets.push(item.value);
        });
        setAssetTypes(assets);
    };

    return (
        <Card className={"bg-base-100 p-3 sm:p-6 mb-6"} border={true}>
            <form onSubmit={handleSubmit}>
                <div className={"flex flex-wrap gap-2"}>
                    <DateRange dateFrom={dateFrom} dateTo={dateTo} onChange={changeDateRange} />
                    <AsyncSelectWrapper
                        id="assetTypes"
                        defaultOptions={options}
                        isMulti={true}
                        placeholder={<Trans>PaymentCalendar.Filters.AssetTypeLabel</Trans>}
                        onChange={changeSelect}
                        className={"min-w-64"}
                    />
                    <Button type="submit" color={"primary"}>
                        <Trans>PaymentCalendar.Filters.SearchButton</Trans>
                    </Button>
                </div>
            </form>
        </Card>
    );
};
