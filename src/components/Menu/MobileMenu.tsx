import { BsFillPieChartFill } from "react-icons/bs";
import { MdContactSupport, MdRealEstateAgent } from "react-icons/md";
import { GiDivergence } from "react-icons/gi";
import { Modal } from "../Common/Modal/Modal";
import { useContext, useState } from "react";
import { MenuPortfolio, MenuAssets, MenuMarket, MenuLinks } from "./MenuComponent";
import { ThemeContext } from "../../providers/ThemeProvider";
import { Dock } from "../Common/Dock/Dock";

type SubMenuTabType = "portfolio" | "assets" | "market" | "links";

export const MobileMenu = () => {
    const [subMenuShow, setSubMenuShow] = useState(false);
    const [subMenu, setSubMenu] = useState<SubMenuTabType>("portfolio");

    const closeSubMenu = () => {
        setSubMenuShow(false);
    };

    const openSubMenu = (name: SubMenuTabType) => {
        setSubMenuShow(true);
        setSubMenu(name);
    };

    return (
        <>
            <Dock className={"flex lg:hidden"}>
                <Dock.Item onClick={() => openSubMenu("portfolio")}>
                    <BsFillPieChartFill title={"Portfolio"} />
                </Dock.Item>
                <Dock.Item onClick={() => openSubMenu("assets")}>
                    <MdRealEstateAgent title={"Real Estate"} />
                </Dock.Item>
                <Dock.Item onClick={() => openSubMenu("market")}>
                    <GiDivergence title={"Market"} />
                </Dock.Item>
                <Dock.Item onClick={() => openSubMenu("links")}>
                    <MdContactSupport title={"Help Center"} />
                </Dock.Item>
            </Dock>
            <PortfolioMenu
                show={subMenuShow}
                tab={subMenu}
                onClose={closeSubMenu}
                onMenuClick={() => setSubMenuShow(false)}
            />
        </>
    );
};

export const PortfolioMenu = ({
    show,
    onClose,
    tab,
    onMenuClick
}: {
    show: boolean;
    onClose: () => void;
    tab: SubMenuTabType;
    onMenuClick: () => void;
}) => {
    const { lng } = useContext(ThemeContext);
    return (
        <Modal show={show} onClose={onClose} responsive={true} closeButton={false}>
            <Modal.Body>
                <MenuPortfolio lng={lng} title={false} show={tab === "portfolio"} onMenuClick={onMenuClick} />
                <MenuAssets lng={lng} title={false} show={tab === "assets"} onMenuClick={onMenuClick} />
                <MenuMarket lng={lng} title={false} show={tab === "market"} onMenuClick={onMenuClick} />
                <MenuLinks lng={lng} title={false} show={tab === "links"} onMenuClick={onMenuClick} />
            </Modal.Body>
        </Modal>
    );
};
