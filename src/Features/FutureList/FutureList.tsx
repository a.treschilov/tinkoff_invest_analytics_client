import { ApiServiceNext } from "../../services/ApiServiceNext";
import { FutureTable } from "./FutureTable";
import { DateType } from "../../components/Common/Types";

export type FutureItem = {
    itemId: number;
    isin: string;
    name: string;
    type: string | null;
    asset: string | null;
    expirationDate: DateType | null;
};

export const FutureList = async () => {
    const response = await ApiServiceNext.fetch("/v1/market/future/list");
    const futures: FutureItem[] = await response.json();

    return <FutureTable futures={futures} />;
};
