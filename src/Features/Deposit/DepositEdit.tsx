"use client";

import { useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { Modal } from "../../components/Common/Modal/Modal";
import { DepositEditForm } from "./DepositEditForm";

interface IDepositEditProps {
    isShow: boolean;
    depositId: number;
    handleHideModal: (action: string | null) => void;
}

export const DepositEdit = ({ isShow, depositId, handleHideModal }: IDepositEditProps) => {
    const [mode, setMode] = useState<"add" | "edit">("edit");

    useEffect(() => {
        if (!isShow) {
            return;
        }

        if (depositId !== 0) {
            setMode("edit");
        } else {
            setMode("add");
        }
    }, [depositId, isShow]);

    const handleComponentHideModal = (action: string | null) => {
        handleHideModal(action);
    };

    return (
        <Modal show={isShow} onClose={() => handleComponentHideModal(null)}>
            <Modal.Header>
                <Trans>
                    DepositEdit.
                    {mode === "edit" ? "ModalEditHeader" : "ModalAddHeader"}
                </Trans>
            </Modal.Header>
            <DepositEditForm
                depositId={depositId}
                show={isShow}
                onSubmit={handleHideModal}
                onDiscard={() => handleHideModal(null)}
            />
        </Modal>
    );
};
