import { Button, Card, Divider } from "react-daisyui";
import { Trans } from "react-i18next";

type DepositEmptyListProps = {
    show: boolean;
    onAddDeposit: () => void;
};
export const DepositEmptyList = ({ show, onAddDeposit }: DepositEmptyListProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
            <Card.Title tag="h2">
                <Trans>DepositManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="text-center my-4">
                <Button size="lg" color={"primary"} onClick={onAddDeposit}>
                    <Trans>DepositManage.AddFirstDepositButton</Trans>
                </Button>
            </div>
        </Card>
    );
};
