import { Trans } from "react-i18next";
import { BsQuestionCircle } from "react-icons/bs";
import { Popover } from "../../components/Common/Popover/Popover";

interface IUserItemSummaryRowPopoverProps {
    label: string;
}

export const UserItemSummaryRowPopover = ({ label }: IUserItemSummaryRowPopoverProps) => {
    return (
        <Popover content={<Trans ns={"features/userItem"}>{label}</Trans>}>
            <BsQuestionCircle className={"inline-block"} style={{ cursor: "pointer" }} />
        </Popover>
    );
};
