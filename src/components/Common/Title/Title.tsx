import { ReactNode } from "react";
import { Divider } from "../Divider/Divider";

type TitleProps = {
    children: ReactNode;
};

export const Title = ({ children }: TitleProps) => {
    return (
        <>
            <div className={"text-xl font-semibold"}>{children}</div>
            <Divider className={"mt-2"} />
        </>
    );
};
