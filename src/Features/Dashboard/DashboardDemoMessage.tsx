import { Trans } from "react-i18next";
import { Alert } from "../../components/Common/Alert/Alert";

interface IDashboardDemoMessageProps {
    isShow: boolean;
}

export const DashboardDemoMessage = ({ isShow }: IDashboardDemoMessageProps) => {
    if (!isShow) {
        return <></>;
    }

    return (
        <Alert className={"mb-4"} status={"info"} dismissible={false}>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                className="stroke-current shrink-0 w-6 h-6"
            >
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                ></path>
            </svg>
            <Trans>Dashboard.DemoAlert</Trans>
        </Alert>
    );
};
