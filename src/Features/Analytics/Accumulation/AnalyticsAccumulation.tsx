import {
    AggregatePeriodOptions,
    AnalyticsAccumulationFilter,
    convertDateToString,
    getDateFromPast
} from "./AnalyticsAccumulationFilter";
import { Trans } from "react-i18next";
import { AnalyticsAccumulationBar } from "./AnalyticsAccumulationBar";
import { useContext, useEffect, useState } from "react";
import { BsBarChartLineFill } from "react-icons/bs";
import { AuthContext } from "../../../providers/AuthProvider";
import { Divider } from "../../../components/Common/Divider/Divider";
import { Card } from "../../../components/Common/Card/Card";

export type AggregatedPeriod = "year" | "month";

export const AnalyticsAccumulation = () => {
    const [dateFromState, setDateFromState] = useState<string>(getDateFromPast(12, AggregatePeriodOptions.MONTH));
    const [dateToState, setDateToState] = useState<string>(convertDateToString(new Date()));
    const [aggregationPeriodState, setAggregationPeriodState] = useState<AggregatedPeriod>(
        AggregatePeriodOptions.MONTH
    );
    const user = useContext(AuthContext);

    useEffect(() => {
        if (user.isNewUser) {
            setDateFromState(convertDateToString(new Date("2022-05-01")));
            setDateToState(convertDateToString(new Date("2023-04-30")));
        }
    }, [user.isNewUser]);

    const handleSubmitFilters = (dateFrom: string, dateTo: string, aggregationPeriod: AggregatedPeriod) => {
        setDateFromState(dateFrom);
        setDateToState(dateTo);
        setAggregationPeriodState(aggregationPeriod);
    };

    const filters = !user.isNewUser ? (
        <AnalyticsAccumulationFilter
            handleSubmit={handleSubmitFilters}
            dateFrom={dateFromState}
            dateTo={dateToState}
            aggregationPeriod={aggregationPeriodState}
        />
    ) : (
        <></>
    );

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <BsBarChartLineFill className={"me-1"} />
                <Trans>Analytics.AccumulationHeader</Trans>
            </Card.Title>
            <Divider />
            {filters}
            <AnalyticsAccumulationBar
                dateFrom={dateFromState}
                dateTo={dateToState}
                aggregationPeriod={aggregationPeriodState}
            />
        </Card>
    );
};
