import { HTMLAttributes } from "react";
import { Color } from "../Types";
import { daisyUIColorClass } from "../utils";

type ProgressProps = {
    color?: Color;
    max?: number;
    value?: number;
} & HTMLAttributes<HTMLProgressElement>;

export const Progress = ({ className, color, max = 100, value, ...args }: ProgressProps) => {
    const classes = ["progress"];
    if (className) {
        classes.push(...className.split(" "));
    }

    classes.push(daisyUIColorClass("progress", color));

    return <progress className={classes.join(" ")} {...args} max={max} value={value} />;
};
