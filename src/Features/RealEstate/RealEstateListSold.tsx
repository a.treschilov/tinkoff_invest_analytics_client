"use client";

import { toUserTimezone } from "../../utils/date";
import { Trans } from "react-i18next";
import { ItemName } from "../../components/Item/ItemName";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { SessionContext } from "../../providers/SessionProvider";
import { makeLink } from "../../utils/url";
import Link from "next/link";
import { RealEstate } from "../../components/Item/Item";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

type RealEstateListSoldProps = {
    show: boolean;
    list: RealEstate[];
};

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
    year: "numeric",
    month: "numeric",
    day: "numeric"
});

export const RealEstateListSold = ({ show, list }: RealEstateListSoldProps) => {
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    if (!show) {
        return <></>;
    }

    const renderList = () => {
        return list.map((realEstate: RealEstate) => {
            const currencyAmountFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                style: "currency",
                currency: realEstate.saleOperation?.price.currency || "RUB",
                maximumSignificantDigits: 3,
                notation: "compact"
            });
            return (
                <tr key={"real-estate-sold-" + realEstate.realEstateId}>
                    <td>
                        <Link
                            href={makeLink("/user/item/" + realEstate.itemId, lng)}
                            onClick={() => updatePreviousPage("realEstate")}
                            className={"link"}
                        >
                            <ItemName name={realEstate.name} type={"real_estate"} />
                        </Link>
                    </td>
                    <td className="hidden sm:table-cell">
                        {currencyAmountFormatter.format(realEstate.purchaseAmount)}
                    </td>
                    <td>
                        {realEstate.saleOperation?.price.amount === undefined
                            ? "—"
                            : currencyAmountFormatter.format(realEstate.saleOperation?.price.amount)}
                    </td>
                    <td className="hidden md:table-cell">
                        {dateFormatter.format(toUserTimezone(realEstate.purchaseDate))} &mdash;{" "}
                        {realEstate.saleOperation?.date === undefined
                            ? "n/a"
                            : dateFormatter.format(toUserTimezone(realEstate.saleOperation?.date))}
                    </td>
                </tr>
            );
        });
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>RealEstateManage.SoldRealEstateTitle</Trans>
            </Card.Title>
            <Divider />
            <table className="table w-full">
                <thead>
                    <tr>
                        <th>
                            <Trans>RealEstateManage.SoldTableHeaderName</Trans>
                        </th>
                        <th className="hidden sm:table-cell">
                            <Trans>RealEstateManage.SoldTableHeaderPurchaseAmount</Trans>
                        </th>
                        <th>
                            <Trans>RealEstateManage.SoldTableHeaderSaleAmount</Trans>
                        </th>
                        <th className="hidden md:table-cell">
                            <Trans>RealEstateManage.SoldTableHeaderPeriod</Trans>
                        </th>
                    </tr>
                </thead>
                <tbody>{renderList()}</tbody>
            </table>
        </Card>
    );
};
