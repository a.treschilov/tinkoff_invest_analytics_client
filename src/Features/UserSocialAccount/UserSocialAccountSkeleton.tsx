import { Divider, Skeleton } from "react-daisyui";
import { Fragment } from "react";

type UserSocialAccountSkeletonProps = {
    show: boolean;
};

const rowCount = 3;

export const UserSocialAccountSkeleton = ({ show }: UserSocialAccountSkeletonProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <>
            {[...Array(rowCount)].map((x, i) => {
                return (
                    <Fragment key={"social-account-skeleton-" + i}>
                        <Skeleton className={"h-8"} />
                        <Divider />
                    </Fragment>
                );
            })}
        </>
    );
};
