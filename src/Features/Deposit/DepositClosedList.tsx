"use client";

import { IDepositPortfolioItem } from "./DepositManage";
import { toUserTimezone } from "../../utils/date";
import { Trans } from "react-i18next";
import { PageState } from "../../components/Common/Types";
import { ItemName } from "../../components/Item/ItemName";
import Link from "next/link";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import { SessionContext } from "../../providers/SessionProvider";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

type DepositClosedListProps = {
    depositList: IDepositPortfolioItem[];
    depositLoadStatus: string;
};

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
    year: "numeric",
    month: "numeric",
    day: "numeric"
});

export const DepositClosedList = ({ depositList, depositLoadStatus }: DepositClosedListProps) => {
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    if (depositLoadStatus === PageState.LOADING) {
        return <></>;
    }

    if (depositList.length === 0) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6"} border={true}>
            <Card.Title>
                <Trans>DepositManage.ClosedTitle</Trans>
            </Card.Title>
            <Divider />
            <table className="table w-full">
                <thead>
                    <tr>
                        <th>
                            <Trans>DepositManage.ClosedHeaderName</Trans>
                        </th>
                        <th className={"hidden sm:table-cell"}>
                            <Trans>DepositManage.ClosedHeaderPeriod</Trans>
                        </th>
                        <th>
                            <Trans>DepositManage.ClosedHeaderIncome</Trans>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {depositList.map(deposit => {
                        const currencyBalanceFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                            style: "currency",
                            currency: deposit.currency
                        });

                        return (
                            <tr key={"closedDeposit-" + deposit.deposit.depositId}>
                                <td>
                                    <Link
                                        href={makeLink("/user/item/" + deposit.deposit.itemId, lng)}
                                        onClick={() => updatePreviousPage("deposit")}
                                        className={"link"}
                                    >
                                        <ItemName name={deposit.deposit.name} type={"real_estate"} />
                                    </Link>
                                    <br className={"sm:hidden"} />
                                    <span className={"sm:hidden"}>
                                        {dateFormatter.format(toUserTimezone(deposit.deposit.dealDate))}
                                        &nbsp;&mdash;&nbsp;
                                        {deposit.deposit.closedDate
                                            ? dateFormatter.format(toUserTimezone(deposit.deposit.closedDate))
                                            : "n/a"}
                                    </span>
                                </td>
                                <td className={"hidden sm:table-cell"}>
                                    {dateFormatter.format(toUserTimezone(deposit.deposit.dealDate))}
                                    &nbsp;&mdash;&nbsp;
                                    {deposit.deposit.closedDate
                                        ? dateFormatter.format(toUserTimezone(deposit.deposit.closedDate))
                                        : "n/a"}
                                </td>
                                <td>{currencyBalanceFormatter.format(deposit.income)}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </Card>
    );
};
