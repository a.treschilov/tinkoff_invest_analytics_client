import { formatInputDayDate } from "../../utils/date";
import { IDepositItem } from "./DepositManage";

const date = new Date();
const formattedDate = formatInputDayDate(date) || "";

export const DefaultDepositData: IDepositItem = {
    depositId: null,
    itemId: null,
    isActive: 1,
    dealDate: {
        date: formattedDate,
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
    },
    closedDate: null,
    name: "",
    interestPercent: 0,
    duration: 12,
    durationPeriod: "month",
    payoutFrequency: 1,
    payoutFrequencyPeriod: "month",
    amount: 0,
    currency: "RUB"
};
