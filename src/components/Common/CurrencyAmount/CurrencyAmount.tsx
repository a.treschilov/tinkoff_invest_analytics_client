import AsyncSelect from "react-select/async";
import { Currencies } from "../Currencies";
import { useEffect, useState } from "react";
import CurrencyInput from "react-currency-input-field";

export type CurrencyAmountProps = {
    id: string;
    amount: number;
    currency: string;
    onChange: (name: string, value: string, type: string) => void;
    step?: number;
    amountInputName?: string;
    currencyInputName?: string;
};

const getCurrencySymbol = (currency: string): string => {
    return (0)
        .toLocaleString("ru-RU", { style: "currency", currency, minimumFractionDigits: 0, maximumFractionDigits: 0 })
        .replace(/\d/g, "")
        .trim();
};

export const CurrencyAmount = ({
    id,
    amount,
    currency,
    onChange,
    step = 0.01,
    amountInputName = "amount",
    currencyInputName = "currency"
}: CurrencyAmountProps) => {
    const [inputCurrency, setInputCurrency] = useState<string>(currency);
    const [inputAmount, setInputAmount] = useState<string>("" + amount);

    useEffect(() => {
        setInputCurrency(currency);
        setInputAmount("" + amount);
    }, [amount, currency, id]);
    const changeElement = (name: string, value: string, type: string) => {
        onChange(name, value, type);
    };

    const getCurrencyObject = (currency: string): { label: string; value: string } | null => {
        const currencyObject = Currencies.find(el => {
            return el.value === currency;
        });
        return currencyObject === undefined ? null : currencyObject;
    };

    return (
        <div className={"flex flex-wrap items-stretch w-full"}>
            <CurrencyInput
                defaultValue={amount}
                value={inputAmount}
                name={amountInputName}
                className="flex-1 w-[1%] input input-bordered rounded-r-none"
                step={step}
                allowNegativeValue={false}
                onValueChange={(value?: string, name?: string) => {
                    const newValue = value ? value : "";
                    changeElement(name ? name : "", newValue.replaceAll(",", "."), "number");
                    setInputAmount("" + newValue);
                }}
                prefix={getCurrencySymbol(inputCurrency)}
                decimalSeparator=","
                groupSeparator=" "
            />
            <AsyncSelect
                name={currencyInputName}
                onChange={value => {
                    changeElement(currencyInputName, value ? value.value : "RUB", "select");
                    setInputCurrency(value ? value.value : "RUB");
                }}
                defaultValue={getCurrencyObject(currency)}
                defaultOptions={Currencies}
                classNames={{
                    control: () => "py-[1px] rounded-r-lg"
                }}
                theme={theme => ({
                    ...theme,
                    borderRadius: 0,
                    colors: {
                        ...theme.colors,
                        neutral0: "var(--color-base-100)",
                        neutral20: "color-mix(in oklab, var(--color-base-content) 20%, #0000)",
                        neutral80: "var(--color-base-content)",
                        primary: "var(--color-primary)",
                        primary25: "color-mix(in oklab, var(--btn-color, var(--color-base-200)), #000 7%)",
                        primary50: "color-mix(in oklab, var(--btn-color, var(--color-base-200)), #000 7%)"
                    }
                })}
            />
        </div>
    );
};
