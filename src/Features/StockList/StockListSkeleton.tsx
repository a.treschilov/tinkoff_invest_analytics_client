import { Skeleton } from "../../components/Common/Skeleton/Skeleton";

const rowNumbers = 8;

export const StockListSkeleton = () => {
    const renderRow = (key: number) => {
        return (
            <tr id={"stock-list-skeleton-row" + key}>
                <td>
                    <Skeleton className={"h-4 w-1/3"} />
                </td>
                <td>
                    <Skeleton className={"h-4 w-5/6"} />
                </td>
                <td className={"hidden lg:table-cell"}>
                    <Skeleton className={"h-4 w-2/3"} />
                </td>
                <td className={"hidden lg:table-cell"}>
                    <Skeleton className={"h-4 w-2/3"} />
                </td>
                <td className={"hidden lg:table-cell"}>
                    <Skeleton className={"h-4 w-3/4"} />
                </td>
            </tr>
        );
    };

    return (
        <>
            {[...Array(rowNumbers)].map((value: number, key: number) => {
                return renderRow(key);
            })}
        </>
    );
};
