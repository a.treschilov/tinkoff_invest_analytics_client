import { Color, PageState } from "../../components/Common/Types";
import { useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { Button } from "../../components/Common/Button/Button";

type UserStrategySettingsSaveButtonProps = {
    status: PageState;
};

export const UserStrategySettingsSaveButton = ({ status }: UserStrategySettingsSaveButtonProps) => {
    const [variant, setVariant] = useState<Color>("primary");
    const [text, setText] = useState(<Trans>UserStrategySettings.ButtonTextSave</Trans>);
    const [disabled, setDisabled] = useState(false);

    useEffect(() => {
        switch (status) {
            case PageState.EDIT_FORM:
                setVariant("primary");
                setText(<Trans>UserStrategySettings.ButtonTextSave</Trans>);
                setDisabled(false);
                break;
            case PageState.SAVING_DATA:
                setVariant("primary");
                setText(<Trans>UserStrategySettings.ButtonTextSaving</Trans>);
                setDisabled(true);
                break;
            case PageState.ERROR:
                setVariant("warning");
                setText(<Trans>UserStrategySettings.ButtonTextErrorSave</Trans>);
                setDisabled(false);
                break;
            case PageState.SUCCESS:
                setVariant("success");
                setText(<Trans>UserStrategySettings.ButtonTextSuccessSave</Trans>);
                setDisabled(false);
                break;
        }

        if ([PageState.SUCCESS, PageState.ERROR].indexOf(status) !== -1) {
            setTimeout(() => {
                setVariant("primary");
                setText(<Trans>UserStrategySettings.ButtonTextSave</Trans>);
                setDisabled(false);
            }, 2000);
        }
    }, [status]);

    return (
        <Button type="submit" color={variant} disabled={disabled}>
            {text}
        </Button>
    );
};
