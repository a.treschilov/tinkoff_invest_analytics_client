"use client";

import { UserStrategySettings } from "../../../../../src/Features/DivesificationMarketStrategy/UserStrategySettings";
import { StrategyTypes } from "../../../../../src/components/Common/Types";

const MarketStrategyPage = () => {
    return (
        <>
            <UserStrategySettings type={StrategyTypes.STOCK} />
            <UserStrategySettings type={StrategyTypes.CURRENCY} />
            <UserStrategySettings type={StrategyTypes.CURRENCY_BALANCE} />
            <UserStrategySettings type={StrategyTypes.COUNTRY} />
            <UserStrategySettings type={StrategyTypes.SECTOR} />
        </>
    );
};

export default MarketStrategyPage;
