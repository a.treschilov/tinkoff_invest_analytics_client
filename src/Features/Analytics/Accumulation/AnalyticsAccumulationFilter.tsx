import { Trans } from "react-i18next";
import { ChangeEvent, MouseEvent, useContext, useState } from "react";
import { AggregatedPeriod } from "./AnalyticsAccumulation";
import { DateRange } from "../../../components/Common/DateRange/DateRange";
import { ThemeContext } from "../../../providers/ThemeProvider";
import Link from "next/link";
import { makeLink } from "../../../utils/url";
import Select from "../../../components/Common/Form/Select";
import { Button } from "../../../components/Common/Button/Button";

export enum AggregatePeriodOptions {
    MONTH = "month",
    YEAR = "year"
}

interface IAnalyticsAccumulationFilterProps {
    handleSubmit: (dateFrom: string, dateTo: string, aggregatedPeriod: AggregatedPeriod) => void;
    dateFrom: string;
    dateTo: string;
    aggregationPeriod: AggregatedPeriod;
}

export const convertDateToString = (date: Date) => {
    return (
        date.getFullYear() +
        "-" +
        ("0" + (date.getMonth() + 1).toString()).slice(-2) +
        "-" +
        ("0" + date.getDate().toString()).slice(-2)
    );
};

export const getDateFromPast = (months: number, aggregatePeriod: AggregatedPeriod) => {
    const date: Date = new Date();

    if (aggregatePeriod === AggregatePeriodOptions.YEAR) {
        date.setMonth(date.getMonth() - months - ((date.getMonth() - months) % 12));
    } else {
        date.setMonth(date.getMonth() - months + 1);
    }
    date.setDate(1);

    return convertDateToString(date);
};

export const AnalyticsAccumulationFilter = ({
    dateFrom,
    dateTo,
    aggregationPeriod,
    handleSubmit
}: IAnalyticsAccumulationFilterProps) => {
    const { lng } = useContext(ThemeContext);
    const [aggregatePeriod, setAggregatePeriod] = useState<AggregatedPeriod>(aggregationPeriod);

    const [dateFromValue, setDateFromValue] = useState(new Date(dateFrom));
    const [dateToValue, setDateToValue] = useState(new Date(dateTo));

    const handleAggregatePeriodChange = (event: ChangeEvent<HTMLSelectElement>) => {
        let period;
        switch (event.target.value) {
            case AggregatePeriodOptions.YEAR:
                period = AggregatePeriodOptions.YEAR;
                break;
            default:
                period = AggregatePeriodOptions.MONTH;
        }

        setAggregatePeriod(period);
    };

    const handleDateChange = (dateFromDate: Date, dateToDate: Date): void => {
        setDateFromValue(dateFromDate);
        setDateToValue(dateToDate);
    };

    const setFilters = (months: number) => {
        const dateTo: Date = new Date();

        setDateFromValue(new Date(getDateFromPast(months, aggregatePeriod)));
        setDateToValue(dateTo);
    };

    const handleSetFilters = (event: MouseEvent<HTMLAnchorElement>, months: number) => {
        setFilters(months);

        event.preventDefault();
    };

    const handleSubmitFilters = (event: ChangeEvent<HTMLFormElement>) => {
        handleSubmit(convertDateToString(dateFromValue), convertDateToString(dateToValue), aggregatePeriod);

        event.preventDefault();
    };

    return (
        <form onSubmit={handleSubmitFilters}>
            <div className={"gap-2 flex flex-wrap"}>
                <DateRange
                    className={"flex-none"}
                    onChange={handleDateChange}
                    dateTo={dateToValue}
                    dateFrom={dateFromValue}
                    type={"month"}
                />
                <div className={"flex-initial w-40"}>
                    <Select>
                        <Select.Select onChange={e => handleAggregatePeriodChange(e)}>
                            <option value={"month"}>
                                <Trans>OperationTypeSummary.FilterPeriodMonthly</Trans>
                            </option>
                            <option value={"year"}>
                                <Trans>OperationTypeSummary.FilterPeriodYearly</Trans>
                            </option>
                        </Select.Select>
                    </Select>
                </div>
                <Button type="submit" className="flex-none" color="primary">
                    <Trans>OperationTypeSummary.FilterApplyButton</Trans>
                </Button>
            </div>
            <div className="mb-2">
                <div className={aggregatePeriod === AggregatePeriodOptions.YEAR ? "hidden" : ""}>
                    <small>
                        <Link
                            href={makeLink("/user/analytics", lng)}
                            className={"link"}
                            onClick={e => handleSetFilters(e, 3)}
                        >
                            <Trans>OperationTypeSummary.Period3Month</Trans>
                        </Link>
                    </small>
                    &nbsp;&nbsp;
                    <small>
                        <Link
                            href={makeLink("/user/analytics", lng)}
                            className={"link"}
                            onClick={e => handleSetFilters(e, 6)}
                        >
                            <Trans>OperationTypeSummary.Period6Month</Trans>
                        </Link>
                    </small>
                    &nbsp;&nbsp;
                    <small>
                        <Link
                            href={makeLink("/user/analytics", lng)}
                            className={"link"}
                            onClick={e => handleSetFilters(e, 12)}
                        >
                            <Trans>OperationTypeSummary.Period12Month</Trans>
                        </Link>
                    </small>
                    &nbsp;&nbsp;
                    <small>
                        <Link
                            href={makeLink("/user/analytics", lng)}
                            className={"link"}
                            onClick={e => handleSetFilters(e, 24)}
                        >
                            <Trans>OperationTypeSummary.Period24Month</Trans>
                        </Link>
                    </small>
                </div>
                <div className={aggregatePeriod === AggregatePeriodOptions.MONTH ? "hidden" : ""}>
                    <small>
                        <Link
                            href={makeLink("/user/analytics", lng)}
                            className={"link"}
                            onClick={e => handleSetFilters(e, 12)}
                        >
                            <Trans>OperationTypeSummary.Period1Year</Trans>
                        </Link>
                    </small>
                    &nbsp;&nbsp;
                    <small>
                        <Link
                            href={makeLink("/user/analytics", lng)}
                            className={"link"}
                            onClick={e => handleSetFilters(e, 36)}
                        >
                            <Trans>OperationTypeSummary.Period3Year</Trans>
                        </Link>
                    </small>
                    &nbsp;&nbsp;
                    <small>
                        <Link
                            href={makeLink("/user/analytics", lng)}
                            className={"link"}
                            onClick={e => handleSetFilters(e, 60)}
                        >
                            <Trans>OperationTypeSummary.Period5Year</Trans>
                        </Link>
                    </small>
                    &nbsp;&nbsp;
                    <small>
                        <Link
                            href={makeLink("/user/analytics", lng)}
                            className={"link"}
                            onClick={e => handleSetFilters(e, 120)}
                        >
                            <Trans>OperationTypeSummary.Period10Year</Trans>
                        </Link>
                    </small>
                </div>
            </div>
        </form>
    );
};
