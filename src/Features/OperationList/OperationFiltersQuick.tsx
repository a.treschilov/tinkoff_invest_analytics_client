import { Trans } from "react-i18next";
import { Divider } from "../../components/Common/Divider/Divider";
import { Dropdown } from "../../components/Common/Dropdown/Dropdown";

type OperationFiltersQuickProps = {
    onChange: (dateFrom: Date, dateTo: Date) => void;
    onToggle: (filterType: "type" | "status" | "broker", values: Record<string, boolean>) => void;
};

export const OperationFiltersQuick = ({ onChange, onToggle }: OperationFiltersQuickProps) => {
    const handleChangeFilter = (dateFrom: Date, dateTo: Date) => {
        onChange(dateFrom, dateTo);
    };

    const lastNDays = (countDays: number) => {
        const dateTo: Date = new Date();
        const dateFrom: Date = new Date();
        dateFrom.setDate(dateFrom.getDate() - countDays);

        handleChangeFilter(dateFrom, dateTo);
    };

    const weekDays = (countWeek: number) => {
        const dateTo = new Date();
        if (dateTo.getDay() !== 0) {
            dateTo.setDate(dateTo.getDate() - dateTo.getDay() + 7 + 7 * countWeek);
        }

        const dateFrom = new Date(dateTo.getTime());
        dateFrom.setDate(dateFrom.getDate() - 6);

        handleChangeFilter(dateFrom, dateTo);
    };

    const monthDays = (countMonth: number) => {
        const today = new Date();

        const dateFrom = new Date();
        dateFrom.setDate(1);
        dateFrom.setMonth(dateFrom.getMonth() + countMonth);

        const dateTo = new Date(today.getFullYear(), today.getMonth() + 1 + countMonth, 0);

        handleChangeFilter(dateFrom, dateTo);
    };

    return (
        <Dropdown placement={"bottom"}>
            <Dropdown.Toggle className={"text-primary"} button={false}>
                <small>
                    <Trans>Operations.Filters.QuickFilterHeader</Trans>
                </small>
            </Dropdown.Toggle>
            <Dropdown.Menu className={"z-50 w-36"}>
                <Dropdown.Item
                    onClick={event => {
                        lastNDays(7);
                        event.stopPropagation();
                    }}
                >
                    <Trans>Operations.Filters.Last7Days</Trans>
                </Dropdown.Item>
                <Dropdown.Item
                    onClick={event => {
                        lastNDays(30);
                        event.stopPropagation();
                    }}
                >
                    <Trans>Operations.Filters.Last30Days</Trans>
                </Dropdown.Item>
                <Dropdown.Item
                    onClick={event => {
                        weekDays(-1);
                        event.stopPropagation();
                    }}
                >
                    <Trans>Operations.Filters.LastWeek</Trans>
                </Dropdown.Item>
                <Dropdown.Item
                    onClick={event => {
                        weekDays(0);
                        event.stopPropagation();
                    }}
                >
                    <Trans>Operations.Filters.CurrentWeek</Trans>
                </Dropdown.Item>
                <Dropdown.Item
                    onClick={event => {
                        monthDays(-1);
                        event.stopPropagation();
                    }}
                >
                    <Trans>Operations.Filters.LastMonth</Trans>
                </Dropdown.Item>
                <Dropdown.Item
                    onClick={event => {
                        monthDays(0);
                        event.stopPropagation();
                    }}
                >
                    <Trans>Operations.Filters.CurrentMonth</Trans>
                </Dropdown.Item>
                <Divider className={"my-1"} />
                <Dropdown.Item
                    onClick={event => {
                        onToggle("type", { Input: true });
                        event.stopPropagation();
                    }}
                >
                    <Trans>Operations.Filters.Deposits</Trans>
                </Dropdown.Item>
                <Dropdown.Item
                    onClick={event => {
                        onToggle("type", { Dividend: true, DividendCard: true });
                        event.stopPropagation();
                    }}
                >
                    <Trans>Operations.Filters.Income</Trans>
                </Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
    );
};
