"use client";

import { ItemLogoName } from "../../components/Item/ItemLogoName";
import { toUserTimezone } from "../../utils/date";
import { Trans } from "react-i18next";
import { useContext, useEffect, useState } from "react";
import { OperationType, OperationTypeTranslation } from "../OperationList/OperationTypeTranslation";
import Link from "next/link";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import { IOperationView } from "../../../app/[lng]/user/operations/list/page";
import { PaymentEventType } from "./EventCalendarManage";
import { EmptyContent } from "../../components/Common/EmptyContent/EmptyContent";
import { Card } from "../../components/Common/Card/Card";
import { Divider } from "../../components/Common/Divider/Divider";
import { Badge } from "../../components/Common/Badge/Badge";

type EventCalendarListProps = {
    show: boolean;
    events: PaymentEventType[];
};

let browserLocale = "en-US";
if (typeof navigator !== "undefined") {
    browserLocale = navigator.languages != undefined ? navigator.languages[0] : "en-US";
}

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat(browserLocale, {
    year: "numeric",
    month: "short",
    day: "numeric"
});

export const EventCalendarList = ({ show, events }: EventCalendarListProps) => {
    const [operations, setOperations] = useState<IOperationView[]>([]);
    const { lng } = useContext(ThemeContext);

    useEffect(() => {
        const sortedEvents = events.sort((event1, event2) => {
            const date1 = new Date(event1.date.date);
            const date2 = new Date(event2.date.date);
            return date1.getTime() > date2.getTime() ? 1 : -1;
        });

        const temporaryOperations: IOperationView[] = [];
        sortedEvents.forEach(event => {
            if (event.interest.amount !== 0) {
                temporaryOperations.push({
                    id: 0,
                    currency: event.interest.currency,
                    date: event.date,
                    itemId: event.itemId,
                    itemName: event.item.name,
                    itemType: event.item.type,
                    itemLogo: event.item.logo,
                    itemExternalId: event.item.externalId,
                    broker: "",
                    amount: event.interest.amount,
                    price: null,
                    quantity: null,
                    status: "Done",
                    type: "Dividend"
                });
            }
            if (event.debt.amount !== 0) {
                let operationType: OperationType;
                switch (event.item.type) {
                    case "loan":
                        operationType = "SellCard";
                        break;
                    default:
                        operationType = "Repayment";
                }
                temporaryOperations.push({
                    id: 0,
                    currency: event.debt.currency,
                    date: event.date,
                    itemId: event.itemId,
                    itemName: event.item.name,
                    itemLogo: event.item.logo,
                    itemType: event.item.type,
                    itemExternalId: event.item.externalId,
                    broker: "",
                    amount: event.debt.amount,
                    price: null,
                    quantity: null,
                    status: "Done",
                    type: operationType
                });
            }
        });
        setOperations(temporaryOperations);
    }, [events]);

    if (!show) {
        return <></>;
    }

    if (operations.length === 0) {
        return (
            <EmptyContent>
                <Trans>PaymentCalendar.PayoutList.EmptyList</Trans>
            </EmptyContent>
        );
    }

    return (
        <Card className={"bg-base-100 p-3 sm:p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>PaymentCalendar.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <table className="table w-full">
                <thead>
                    <tr>
                        <th>
                            <Trans>PaymentCalendar.PayoutList.ItemNameTitle</Trans>
                        </th>
                        <th className="hidden md:table-cell">
                            <Trans>PaymentCalendar.PayoutList.PayoutDateTitle</Trans>
                        </th>
                        <th className="hidden md:table-cell">
                            <Trans>PaymentCalendar.PayoutList.IncomeTitle</Trans>
                        </th>
                        <th className="hidden md:table-cell">
                            <Trans>PaymentCalendar.PayoutList.OperationTypeTitle</Trans>
                        </th>
                        <th className="md:hidden">
                            <Trans>PaymentCalendar.PayoutList.InfoTitle</Trans>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {operations.map((paymentEvent: IOperationView) => {
                        const currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                            style: "currency",
                            currency: paymentEvent.currency
                        });
                        return (
                            <tr
                                key={
                                    "operation-payment-" +
                                    paymentEvent.itemExternalId +
                                    "-" +
                                    paymentEvent.date.date +
                                    "-" +
                                    paymentEvent.type
                                }
                            >
                                <td>
                                    <Link href={makeLink("/user/item/" + paymentEvent.itemId, lng)} className={"link"}>
                                        <ItemLogoName
                                            name={paymentEvent.itemName}
                                            type={paymentEvent.itemType}
                                            logo={paymentEvent.itemLogo}
                                        />
                                    </Link>
                                </td>
                                <td className="hidden md:table-cell">
                                    {dateFormatter.format(toUserTimezone(paymentEvent.date))}
                                </td>
                                <td className="hidden md:table-cell">
                                    {currencyFormatter.format(paymentEvent.amount)}
                                </td>
                                <td className="hidden md:table-cell">
                                    <OperationTypeTranslation
                                        operationType={paymentEvent.type}
                                        itemType={paymentEvent.itemType}
                                    />
                                </td>
                                <td className="md:hidden">
                                    {currencyFormatter.format(paymentEvent.amount)}
                                    <br />
                                    <small className="text-accent">
                                        {dateFormatter.format(toUserTimezone(paymentEvent.date))}
                                    </small>
                                    <br />
                                    <small>
                                        <Badge color="info">
                                            <OperationTypeTranslation
                                                operationType={paymentEvent.type}
                                                itemType={paymentEvent.itemType}
                                            />
                                        </Badge>
                                    </small>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </Card>
    );
};
