"use client";

import { useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { LoanEdit } from "./LoanEdit";
import { Trans } from "react-i18next";
import { RealEstateOperationManage } from "../RealEstate/RealEstateOperationManage";
import { LoanSuggestionOperation } from "./LoanSuggestionOperation";
import { ApiService } from "../../services/ApiService";
import { LoanList } from "./LoanList";
import { LoanListClosed } from "./LoanListClosed";
import { IPrice } from "../../interfaces/IPrice";
import { LoanLoading } from "./LoanLoading";
import { withErrorBoundary } from "react-error-boundary";
import { ErrorComponent } from "../../components/ErrorContent/ErrorComponent";
import { Alert } from "../../components/Common/Alert/Alert";
import { LoanEmptyList } from "./LoanEmptyList";
import { Loan } from "../../components/Item/Item";
import { IOperation } from "../Operation/OperationEdit";
import { Button } from "../../components/Common/Button/Button";
import { Toast } from "../../components/Common/Toast/Toast";

type LoanManageProps = {
    onChange?: () => void;
};

type OverPaymentType = {
    price: IPrice;
    percent: number;
};

export interface ILoanPortfolioItem {
    amount: number;
    currency: string;
    overPayment: OverPaymentType;
    loan: Loan;
}

export type loanActionList = "close" | "delete";

export const LoanManage = ({ onChange }: LoanManageProps) => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [loans, setLoans] = useState<ILoanPortfolioItem[]>([]);
    const [closedLoans, setClosedLoans] = useState<ILoanPortfolioItem[]>([]);
    const [editedLoanId, setEditedLoanId] = useState(0);
    const [editedItemId, setEditedItemId] = useState(0);
    const [isShowModal, setIsShowModal] = useState(false);
    const [isShowMessage, setIsShowMessage] = useState(false);
    const [messageText, setMessageText] = useState("");
    const [isShowOperations, setIsShowOperations] = useState(false);
    const [updateTimestamp, setUpdateTimestamp] = useState(Date.now);

    useEffect(() => {
        updateData();
    }, []);

    const updateData = () => {
        setPageState(PageState.LOADING);
        setUpdateTimestamp(Date.now);

        ApiService.fetch("/v1/loan/portfolio").then(response => {
            const portfolio = response.map((el: ILoanPortfolioItem) => {
                const loanItem: ILoanPortfolioItem = {
                    amount: el.amount,
                    currency: el.currency,
                    overPayment: {
                        price: {
                            amount: el.overPayment.price.amount,
                            currency: el.overPayment.price.currency
                        },
                        percent: el.overPayment.percent
                    },
                    loan: {
                        loanId: el.loan.loanId,
                        itemId: el.loan.itemId,
                        currency: el.loan.currency,
                        amount: el.loan.amount,
                        name: el.loan.name,
                        isActive: el.loan.isActive,
                        date: el.loan.date,
                        closedDate: el.loan.closedDate,
                        paymentAmount: el.loan.paymentAmount,
                        paymentCurrency: el.loan.paymentCurrency,
                        payoutFrequency: el.loan.payoutFrequency,
                        payoutFrequencyPeriod: el.loan.payoutFrequencyPeriod,
                        interestPercent: el.loan.interestPercent
                    }
                };
                return loanItem;
            });
            setLoans(
                portfolio.filter((loan: ILoanPortfolioItem) => {
                    return loan.loan.isActive === 1;
                })
            );
            setClosedLoans(
                portfolio.filter((loan: ILoanPortfolioItem) => {
                    return loan.loan.isActive === 0;
                })
            );
            setPageState(PageState.SUCCESS);
        });
    };

    const handleChange = () => {
        updateData();
        if (onChange !== undefined) {
            onChange();
        }
    };

    const handleEditLoan = (loanId: number) => {
        setEditedLoanId(loanId);
        setIsShowModal(true);
        setIsShowMessage(false);
    };

    const handleOperationManage = (itemId: number) => {
        setIsShowOperations(true);
        setEditedItemId(itemId);
    };

    const handleHideMessage = () => {
        setIsShowMessage(false);
    };

    const handleHideModal = (action: string | null) => {
        setIsShowModal(false);
        setIsShowOperations(false);

        if (action === "editComplete") {
            handleChange();
            setIsShowMessage(true);
            setMessageText("LoanManage.EditComplete");
        } else if (action === "addComplete") {
            handleChange();
            setIsShowMessage(true);
            setMessageText("LoanManage.AddComplete");
        }
    };

    const handleEventComplete = (eventName: loanActionList) => {
        switch (eventName) {
            case "close":
                setMessageText("LoanManage.CloseComplete");
                break;
            case "delete":
                setMessageText("LoanManage.DeleteComplete");
                break;
        }
        setIsShowMessage(true);
        handleChange();
    };

    const handleAddBatchOperations = (operations: IOperation[]) => {
        setPageState(PageState.LOADING);

        ApiService.post("/v1/operations/addBatch", operations).then(() => {
            handleChange();
            setIsShowMessage(true);
            setMessageText("LoanManage.OperationsAdded");
        });
    };

    const renderManageData = () => {
        return (
            <div className="text-end mb-4 mt-2">
                <Button color={"primary"} onClick={() => handleEditLoan(0)}>
                    <Trans>LoanManage.AddButtonText</Trans>
                </Button>
            </div>
        );
    };

    return (
        <div>
            {loans.length + closedLoans.length > 0 ? renderManageData() : <></>}
            <Toast vertical={"top"} horizontal={"end"} className={"z-10" + (!isShowMessage ? " hidden" : "")}>
                <Alert
                    show={isShowMessage}
                    className={"mb-6"}
                    status="success"
                    onClose={handleHideMessage}
                    closeTimeout={5000}
                    dismissible
                >
                    <Trans>{messageText}</Trans>
                </Alert>
            </Toast>
            <LoanSuggestionOperation
                dataTimestamp={updateTimestamp}
                handleAddBatchOperations={handleAddBatchOperations}
            />
            <LoanEmptyList
                show={pageState !== PageState.LOADING && loans.length + closedLoans.length === 0}
                onAddLoan={() => handleEditLoan(0)}
            />
            <LoanLoading show={pageState === PageState.LOADING} />
            <LoanList
                isShow={pageState !== PageState.LOADING && loans.length > 0}
                loans={loans}
                handleEditLoan={handleEditLoan}
                handleOperationManage={handleOperationManage}
                handleEventComplete={handleEventComplete}
            />
            <LoanListClosed
                isShow={pageState !== PageState.LOADING && closedLoans.length > 0}
                loans={closedLoans}
            ></LoanListClosed>
            <LoanEdit isShow={isShowModal} loanId={editedLoanId} hideModalHandler={handleHideModal} />
            <RealEstateOperationManage
                isShow={isShowOperations}
                itemType="loan"
                hideModal={handleHideModal}
                itemId={editedItemId}
            />
        </div>
    );
};

export const LoanManageWithBoundaryError = withErrorBoundary(LoanManage, {
    FallbackComponent: ErrorComponent
});
