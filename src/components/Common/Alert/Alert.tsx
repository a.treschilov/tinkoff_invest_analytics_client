import { HTMLAttributes, ReactNode, useEffect, useState } from "react";
import { IoMdClose } from "react-icons/io";

type AlertProps = {
    children: ReactNode;
    onClose?: () => void;
    dismissible?: boolean;
    show?: boolean;
    closeTimeout?: number | null;
    status?: "success" | "error" | "info" | "warning";
    className?: string;
} & HTMLAttributes<HTMLDivElement>;

export const Alert = ({
    children,
    onClose = () => {},
    closeTimeout = null,
    dismissible = true,
    show = true,
    status = "info",
    className,
    ...args
}: AlertProps) => {
    const [timerId, setTimerId] = useState<NodeJS.Timeout | null>(null);
    useEffect(() => {
        if (show && closeTimeout !== null && closeTimeout !== undefined) {
            setTimerId(
                setTimeout(() => {
                    if (onClose) {
                        onClose();
                    }
                }, closeTimeout)
            );
        }
        if (!show && timerId !== null) {
            setTimerId(null);
        }
    }, [closeTimeout, onClose, show, timerId]);

    const closeButton = dismissible ? (
        <div className={"absolute top-3 right-3 cursor-pointer"} onClick={onClose}>
            <IoMdClose />
        </div>
    ) : (
        <></>
    );

    let classN = "";
    switch (status) {
        case "success":
            classN += " alert-success";
            break;
        case "error":
            classN += " alert-error";
            break;
        case "warning":
            classN += " alert-warning";
            break;
        case "info":
            classN += " alert-info";
            break;
    }

    return (
        <div className={(show ? "relative" : "hidden") + (className ? " " + className : "")} {...args}>
            <div role="alert" className={"alert" + classN}>
                <div>{children}</div>
                {closeButton}
            </div>
        </div>
    );
};
