import { IOperationView } from "../../../app/[lng]/user/operations/list/page";
import { OperationTypeBadge } from "../OperationList/OperationTypeBadge";

type UserItemEventsContentDataProps = {
    show: boolean;
    paymentSchedule: IOperationView[];
};

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
    year: "numeric",
    month: "numeric",
    day: "numeric"
});

export const UserItemEventsContentData = ({ show, paymentSchedule }: UserItemEventsContentDataProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <tbody>
            {paymentSchedule.map(paymentScheduleItem => {
                let currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                    style: "currency",
                    currency: paymentScheduleItem.currency
                });
                if (paymentScheduleItem.amount < 0.02) {
                    currencyFormatter = new Intl.NumberFormat("ru-RU", {
                        style: "currency",
                        currency: paymentScheduleItem.currency,
                        minimumFractionDigits: 4,
                        maximumFractionDigits: 4
                    });
                }

                return (
                    <tr
                        key={"paymentScheduleItem_" + paymentScheduleItem.date.date + "_" + paymentScheduleItem.type}
                        className={"hover"}
                    >
                        <td>{dateFormatter.format(new Date(paymentScheduleItem.date.date))}</td>
                        <td>{currencyFormatter.format(paymentScheduleItem.amount)}</td>
                        <td>
                            <OperationTypeBadge
                                operationType={paymentScheduleItem.type}
                                itemType={paymentScheduleItem.itemType}
                            />
                        </td>
                    </tr>
                );
            })}
        </tbody>
    );
};
