"use client";

import { useContext, useEffect, useState } from "react";
import { IResponseOperations } from "../Deposit/DepositManage";
import { ApiService } from "../../services/ApiService";
import { PageState } from "../../components/Common/Types";
import { IOperation } from "../Operation/OperationEdit";
import { OperationSuggestion } from "../Operation/Suggestion/OperationSuggestion";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

type LoanSuggestionOperationProps = {
    dataTimestamp: number;
    handleAddBatchOperations: (operations: IOperation[]) => void;
};

export const LoanSuggestionOperation = ({ dataTimestamp, handleAddBatchOperations }: LoanSuggestionOperationProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/loan");

    const [operations, setOperations] = useState<IOperation[]>([]);
    const [pageState, setPageState] = useState(PageState.LOADING);

    useEffect(() => {
        setPageState(PageState.LOADING);
        setOperations([]);
        ApiService.fetch("/v1/loan/paymentsSuggestion").then(response => {
            const suggestionPayments = response.map((el: IResponseOperations) => {
                return {
                    itemOperationId: el.id,
                    brokerId: el.broker,
                    itemId: el.itemId,
                    itemName: el.itemName,
                    operationType: el.type,
                    externalId: el.externalId,
                    quantity: el.quantity,
                    date: {
                        date: el.date.date,
                        timezone: el.date.timezone
                    },
                    amount: el.amount,
                    currencyIso: el.currency,
                    status: el.status
                };
            });
            setOperations(suggestionPayments);
            setPageState(PageState.SUCCESS);
        });
    }, [dataTimestamp]);

    const onAddOperations = (operations: IOperation[]) => {
        setPageState(PageState.SAVING_DATA);
        handleAddBatchOperations(operations);
    };

    return operations.length === 0 ? (
        <></>
    ) : (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>{t("Loan.SuggestionOperation.Title")}</Card.Title>
            <Divider />
            <div className={"mb-4"}>{t("Loan.SuggestionOperation.Description")}</div>
            <div className={"grid grid-cols-12 gap-4"}>
                <div className={"col-span-12 sm:col-span-3 text-gray-500 font-bold text-xs"}>
                    {t("Loan.SuggestionOperation.TableHeaderName")}
                </div>
                <div className={"col-span-12 sm:col-span-9 text-gray-500 font-bold text-xs"}>
                    {t("Loan.SuggestionOperation.TableHeaderDate")} / {t("Loan.SuggestionOperation.TableHeaderAmount")}
                </div>
                <div className={"col-span-12 h-0.25 divider m-0"}></div>
                <OperationSuggestion operations={operations} onChange={ops => setOperations(ops)} state={pageState} />
            </div>
            <Card.Actions className={"mt-4"}>
                <Button
                    onClick={() => {
                        onAddOperations(operations);
                    }}
                    disabled={pageState === PageState.SAVING_DATA}
                    color={"primary"}
                >
                    {t("Loan.SuggestionOperation.AddButton")}
                </Button>
            </Card.Actions>
        </Card>
    );
};
