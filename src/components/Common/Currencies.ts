export const currencySymbol = (currency: string): string | undefined => {
    const locale = "ru-RU";
    const formatter = new Intl.NumberFormat(locale, { style: "currency", currency });
    const parts = formatter.formatToParts(1);
    const part = parts.find(x => {
        return x.type === "currency";
    });
    return part === undefined ? undefined : part.value;
};

export const Currencies = [
    {
        label: "RUB",
        value: "RUB"
    },
    {
        label: "USD",
        value: "USD"
    },
    {
        label: "EUR",
        value: "EUR"
    },
    {
        label: "CNY",
        value: "CNY"
    },
    {
        label: "MYR",
        value: "MYR"
    },
    {
        label: "KZT",
        value: "KZT"
    }
];
