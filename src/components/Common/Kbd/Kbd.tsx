import { HTMLAttributes, ReactNode } from "react";
import { Size } from "../Types";
import { daisyUISizeClass } from "../utils";

type KbdProps = {
    children?: ReactNode;
    size?: Size;
    className?: string;
} & HTMLAttributes<HTMLDivElement>;

export const Kbd = ({ children, size = "md", className, ...args }: KbdProps) => {
    const classN = ["kbd"];
    if (className) {
        classN.push(...className?.split(" "));
    }
    classN.push(daisyUISizeClass("kbd", size));

    return (
        <kbd className={classN.join(" ")} {...args}>
            {children}
        </kbd>
    );
};
