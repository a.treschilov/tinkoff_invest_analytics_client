import { createContext, ReactNode, useEffect, useState } from "react";
import { usePathname } from "next/navigation";
import { TFunction } from "i18next";

type SessionContextType = {
    previousPage: string | null;
    updatePreviousPage: (value: string | null) => void;
    pageTitle: string | null;
    setPageTitle: (pageTitle: string) => void;
};

export const SessionContext = createContext<SessionContextType>(null!);

export const SessionProvider = ({ children, t }: { children: ReactNode; t: TFunction }) => {
    const [previousPage, setPreviousPage] = useState<string | null>(null);
    const [pageTitle, setPageTitle] = useState<string | null>(null);

    const path = usePathname();
    useEffect(() => {
        const parts = path.split("/");
        parts.splice(0, 2);

        let title = t("/" + parts.join("/"));
        if (title === "/" + parts.join("/")) {
            title = "";
        }
        setPageTitle(title);
    }, [path, t]);

    const updatePreviousPage = (value: string | null) => {
        setPreviousPage(value);
    };

    const value = { previousPage, updatePreviousPage, pageTitle, setPageTitle };

    return <SessionContext.Provider value={value}>{children}</SessionContext.Provider>;
};
