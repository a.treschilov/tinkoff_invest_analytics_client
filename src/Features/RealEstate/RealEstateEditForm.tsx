import { ChangeEvent, ReactNode, useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { DefaultRealEstateAsset } from "./RealEstateStaticData";
import { ApiService } from "../../services/ApiService";
import { formatInputDateTimeSeconds, formatInputDayDate, toUserTimezone } from "../../utils/date";
import { Trans } from "react-i18next";
import { InputText } from "../../components/Common/Form/InputText";
import { InputCurrencyAmount } from "../../components/Common/Form/InputCurrencyAmount";
import Toggle from "../../components/Common/Form/Toggle";
import Select from "../../components/Common/Form/Select";
import { ActionButtonText } from "../../components/Common/ActionButtonText";
import { PageLoader } from "../../components/Loader/Loaders";
import { RealEstatePrice } from "../../components/Item/Item";
import { Button } from "../../components/Common/Button/Button";
import { Join } from "../../components/Common/Join/Join";

type RealEstateEditFormProps = {
    show?: boolean;
    realEstateId: number;
    onSubmit?: () => void;
    onDiscard?: () => void;
};
export const RealEstateEditForm = ({
    realEstateId,
    show = true,
    onSubmit = () => {},
    onDiscard = () => {}
}: RealEstateEditFormProps) => {
    const [pageState, setPageState] = useState(PageState.EDIT_FORM);
    const [realEstateData, setRealEstateData] = useState(DefaultRealEstateAsset);
    const [message, setMessage] = useState<string | null>(null);
    const [price, setPrice] = useState<RealEstatePrice | null>(null);
    const [rentBlockShow, setRentBlockShow] = useState(false);

    useEffect(() => {
        setPageState(PageState.LOADING);
        setRealEstateData(DefaultRealEstateAsset);

        if (realEstateId !== 0) {
            ApiService.fetch("/v1/real_estate/item/" + realEstateId).then(response => {
                const date = new Date();
                const formattedDate = formatInputDateTimeSeconds(date);
                const lastPrice = getLastPrice(response.prices);
                response.purchaseDate = {
                    date: formatInputDayDate(toUserTimezone(response.purchaseDate)),
                    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                };

                setPrice({
                    realEstatePriceId: null,
                    amount: lastPrice === null ? 0 : lastPrice.amount,
                    currency: lastPrice?.currency || "RUB",
                    date: {
                        date: formattedDate,
                        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
                    }
                });
                setRealEstateData(response);
                setRentBlockShow(response.interestAmount > 0);
                setPageState(PageState.EDIT_FORM);
            });
        } else {
            setPageState(PageState.EDIT_FORM);
            setRealEstateData(DefaultRealEstateAsset);
        }
    }, [realEstateId]);

    const changeElement = (name: string, value: string, inputType: string) => {
        let realEstate = realEstateData;
        if (realEstate === null) {
            realEstate = DefaultRealEstateAsset;
        }

        let typedValue: number | string;
        switch (inputType) {
            case "number":
                typedValue = parseFloat(value);
                break;
            default:
                typedValue = value;
        }

        if (name === "purchaseCurrency") {
            realEstate.currency = typedValue + "";
        } else if (["amount", "currency"].indexOf(name) !== -1) {
            const inputPrice = price;
            if (inputPrice !== null) {
                if (name === "amount") {
                    inputPrice.amount = parseFloat(value);
                }
                if (name === "currency") {
                    inputPrice.currency = value + "";
                }

                setPrice(inputPrice);
            }
        } else if (name === "purchaseDate") {
            realEstate.purchaseDate.date = typedValue.toString();
        } else {
            const data = {
                [name]: typedValue
            };
            realEstate = { ...realEstate, ...data };
        }

        setRealEstateData(realEstate);
    };

    const getLastPrice = (realEstatePriceList: RealEstatePrice[]): RealEstatePrice | null => {
        if (realEstatePriceList.length === 0) {
            return null;
        }

        realEstatePriceList.sort((el1: RealEstatePrice, el2: RealEstatePrice) => {
            return el1.date.date < el2.date.date ? 1 : -1;
        });

        return realEstatePriceList[0];
    };

    const handleChangeFormData = (event: ChangeEvent<HTMLInputElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleChangeSelectData = (event: ChangeEvent<HTMLSelectElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleSubmitForm = (event: ChangeEvent<HTMLFormElement>) => {
        setPageState(PageState.SAVING_DATA);

        const data = {
            realEstateId: realEstateData.realEstateId,
            itemId: realEstateData.itemId,
            name: realEstateData.name,
            payoutFrequency: realEstateData.payoutFrequency,
            payoutFrequencyPeriod: realEstateData.payoutFrequencyPeriod,
            interestAmount: rentBlockShow ? realEstateData.interestAmount : 0,
            interestCurrency: rentBlockShow ? realEstateData.interestCurrency : realEstateData.currency,
            currency: realEstateData.currency,
            purchaseDate: realEstateData.purchaseDate,
            purchaseAmount: realEstateData.purchaseAmount,
            prices: realEstateData.prices
        };
        if (price !== null) {
            data.prices.push(price);
        }

        ApiService.put("/v1/real_estate/item/" + realEstateId, data)
            .then(() => {
                setPageState(PageState.SUCCESS);
                onSubmit();
            })
            .catch(error => {
                setPageState(PageState.ERROR);
                setMessage(error.response.data.error_message);
            })
            .finally(() => {
                setTimeout(() => {
                    setPageState(PageState.EDIT_FORM);
                }, 2000);
            });

        event.preventDefault();
    };

    const renderData = () => {
        let buttonVariant: "secondary" | "success" | "warning" | "primary" | "neutral";
        let buttonText;
        let buttonHint: ReactNode;
        let buttonHintStatus;
        switch (pageState) {
            case PageState.SAVING_DATA:
                buttonVariant = "neutral";
                buttonText = <Trans>RealEstateEdit.ButtonTextSaving</Trans>;
                buttonHint = <></>;
                buttonHintStatus = "";
                break;
            case PageState.SUCCESS:
                buttonVariant = "success";
                buttonText = <Trans>RealEstateEdit.ButtonTextSuccess</Trans>;
                buttonHint = <span></span>;
                buttonHintStatus = "";
                break;
            case PageState.ERROR:
                buttonVariant = "warning";
                buttonText = <Trans>RealEstateEdit.ButtonTextSave</Trans>;
                buttonHint = <span>{message}</span>;
                buttonHintStatus = "error";
                break;
            default:
                buttonVariant = "primary";
                buttonText = <Trans>RealEstateEdit.ButtonTextSave</Trans>;
                buttonHint = <span></span>;
                buttonHintStatus = "";
        }

        const date = realEstateData?.purchaseDate ? toUserTimezone(realEstateData?.purchaseDate) : new Date();
        const formattedDate = formatInputDayDate(date) ?? "";
        const lastPrice = getLastPrice(realEstateData.prices);

        return (
            <form onSubmit={handleSubmitForm}>
                <div className={"grid grid-cols-2 gap-6 mb-6"}>
                    <InputText
                        label={<Trans>RealEstateEdit.FormLabelName</Trans>}
                        type="text"
                        name="name"
                        onChange={handleChangeFormData}
                        value={realEstateData.name}
                        containerClassName={"col-span-2"}
                    />
                    <InputCurrencyAmount
                        containerClassName={"col-span-2"}
                        label={<Trans>RealEstateEdit.FormLabelAmount</Trans>}
                        amount={lastPrice?.amount || 0}
                        onChange={changeElement}
                        currency={lastPrice?.currency || "RUB"}
                        id={"real-estate-price" + realEstateData.realEstateId}
                        step={100}
                    />
                    <InputCurrencyAmount
                        containerClassName={"col-span-2 sm:col-span-1"}
                        label={<Trans>RealEstateEdit.FormLabelPurchaseAmount</Trans>}
                        amount={realEstateData.purchaseAmount}
                        onChange={changeElement}
                        currency={realEstateData.currency}
                        id={"real-estate-purchase" + realEstateId}
                        currencyInputName={"purchaseCurrency"}
                        amountInputName={"purchaseAmount"}
                        step={100}
                    />
                    <InputText
                        containerClassName={"col-span-2 sm:col-span-1"}
                        label={<Trans>RealEstateEdit.FormLabelPurchaseDate</Trans>}
                        type="date"
                        name="purchaseDate"
                        onChange={handleChangeFormData}
                        defaultValue={formattedDate}
                    />
                    <div className="form-control col-span-2">
                        <Toggle>
                            <Toggle.Label>
                                <Trans>RealEstateAdd.ForRent</Trans>
                            </Toggle.Label>
                            <Toggle.Checkbox
                                checked={rentBlockShow}
                                onChange={(event: ChangeEvent<HTMLInputElement>) => {
                                    setRentBlockShow(event.target.checked);
                                }}
                                color={"primary"}
                            />
                        </Toggle>
                    </div>

                    {rentBlockShow ? (
                        <>
                            <InputCurrencyAmount
                                containerClassName={"col-span-2 sm:col-span-1"}
                                label={<Trans>RealEstateEdit.FormLabelInterest</Trans>}
                                amount={realEstateData.interestAmount}
                                onChange={changeElement}
                                currency={realEstateData.interestCurrency}
                                id={"real-estate-interest-" + realEstateId}
                                currencyInputName={"interestCurrency"}
                                amountInputName={"interestAmount"}
                                step={100}
                            />
                            <div className={"col-span-2 sm:col-span-1"}>
                                <label className="label">
                                    <span className={"label-text text-base-content"}>
                                        <Trans>RealEstateAdd.FormLabelPayoutFrequency</Trans>
                                    </span>
                                </label>
                                <Join>
                                    <input
                                        type="number"
                                        name="payoutFrequency"
                                        onChange={handleChangeFormData}
                                        value={realEstateData.payoutFrequency}
                                        className={"input input-bordered w-full rounded-r-none"}
                                    ></input>
                                    <Select>
                                        <Select.Select
                                            name="payoutFrequencyPeriod"
                                            onChange={handleChangeSelectData}
                                            defaultValue={realEstateData?.payoutFrequencyPeriod}
                                            className={"rounded-l-none"}
                                        >
                                            <option value="day">
                                                <Trans>RealEstateAdd.PeriodDay</Trans>
                                            </option>
                                            <option value="month">
                                                <Trans>RealEstateAdd.PeriodMonth</Trans>
                                            </option>
                                            <option value="year">
                                                <Trans>RealEstateAdd.PeriodYear</Trans>
                                            </option>
                                        </Select.Select>
                                    </Select>
                                </Join>
                            </div>
                        </>
                    ) : (
                        <></>
                    )}
                </div>
                <div className={"text-right"}>
                    <Button type="button" onClick={onDiscard} color={"neutral"} className={"mr-2"}>
                        <Trans>RealEstateEdit.ButtonTextDiscard</Trans>
                    </Button>
                    <ActionButtonText status={buttonHintStatus}>{buttonHint}</ActionButtonText>
                    <Button type="submit" color={buttonVariant}>
                        {buttonText}
                    </Button>
                </div>
            </form>
        );
    };

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = <PageLoader />;
            break;
        default:
            content = renderData();
            break;
    }

    if (!show) {
        return <></>;
    }

    return <>{content}</>;
};
