import { Modal } from "../../components/Common/Modal/Modal";
import { Trans } from "react-i18next";
import { ApiService } from "../../services/ApiService";
import { Alert } from "../../components/Common/Alert/Alert";
import { useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { Button } from "../../components/Common/Button/Button";
import { Toast } from "../../components/Common/Toast/Toast";
import { Progress } from "../../components/Common/Progress/Progress";

type OperationDeleteProps = {
    show: boolean;
    operationId: number;
    onDiscard: () => void;
    onConfirm: () => void;
};

export const OperationDelete = ({ show, operationId, onDiscard, onConfirm }: OperationDeleteProps) => {
    const [pageState, setPageState] = useState(PageState.EDIT_FORM);
    const [isShowDeletedMessage, setIsShowDeletedMessage] = useState(false);
    const [error, setError] = useState<null | string>(null);

    useEffect(() => {
        setPageState(PageState.EDIT_FORM);
    }, [operationId]);

    const deleteItem = () => {
        setPageState(PageState.SAVING_DATA);
        ApiService.delete("/v1/operations/operation/" + operationId)
            .then(() => {
                onConfirm();
                setIsShowDeletedMessage(true);
            })
            .catch(response => {
                setError(response.response?.data?.error_message);
            })
            .finally(() => {
                setPageState(PageState.EDIT_FORM);
            });
    };

    return (
        <>
            <Toast vertical={"top"} horizontal={"end"} className={"z-10" + (!isShowDeletedMessage ? " hidden" : "")}>
                <Alert
                    status={"success"}
                    dismissible={true}
                    onClose={() => {
                        setIsShowDeletedMessage(false);
                    }}
                    closeTimeout={5000}
                    show={isShowDeletedMessage}
                >
                    <Trans>Operations.DeleteConfirmation.DeleteSuccessMessage</Trans>
                </Alert>
            </Toast>
            <Modal show={show} onClose={() => onDiscard()}>
                <Progress
                    className="w-full absolute top-0 left-0"
                    color={"primary"}
                    hidden={pageState !== PageState.SAVING_DATA}
                />
                <Modal.Header>
                    <Trans>Operations.DeleteConfirmation.Title</Trans>
                </Modal.Header>
                <Modal.Body>
                    <Alert
                        dismissible={true}
                        show={error !== null}
                        status={"error"}
                        className={"mb-4"}
                        onClose={() => setError(null)}
                    >
                        {error}
                    </Alert>
                    <Trans>Operations.DeleteConfirmation.Description</Trans>
                </Modal.Body>
                <Modal.Actions>
                    <Button
                        color="neutral"
                        onClick={() => {
                            onDiscard();
                        }}
                        disabled={pageState === PageState.SAVING_DATA}
                    >
                        <Trans>Operations.DeleteConfirmation.CancelButton</Trans>
                    </Button>
                    <Button color="error" onClick={deleteItem} disabled={pageState === PageState.SAVING_DATA}>
                        <Trans>Operations.DeleteConfirmation.ConfirmButton</Trans>
                    </Button>
                </Modal.Actions>
            </Modal>
        </>
    );
};
