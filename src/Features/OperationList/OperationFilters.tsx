import { OperationFilterModal } from "./OperationFilterModal";
import { BsFillFunnelFill } from "react-icons/bs";
import { Trans } from "react-i18next";
import { SelectOption } from "../../components/Common/Types";
import { ChangeEvent, useEffect, useState } from "react";
import { OperationFiltersQuick } from "./OperationFiltersQuick";
import { DateRange } from "../../components/Common/DateRange/DateRange";
import { ApiService } from "../../services/ApiService";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

type OperationFiltersProps = {
    onFilterApply: (filterValues: OperationFilterValuesType, dateFrom: Date, dateTo: Date) => void;
};

export type OperationFilterValuesType = {
    operationType: string[];
    brokerId: string[];
    operationStatus: string[];
};

export const getOperationInitialDateFrom = (): Date => {
    const initialDateFrom: Date = new Date();
    initialDateFrom.setMonth(initialDateFrom.getMonth() - 1);

    return initialDateFrom;
};

export const getOperationInitialDateTo = (): Date => {
    return new Date();
};

const defaultValue = {
    operationType: [],
    brokerId: [],
    operationStatus: [],
    dateFrom: getOperationInitialDateFrom(),
    dateTo: getOperationInitialDateTo()
};

export const OperationFilters = ({ onFilterApply }: OperationFiltersProps) => {
    const [filterValues, setFilterValues] = useState<OperationFilterValuesType>(defaultValue);
    const [dateFrom, setDateFrom] = useState(defaultValue.dateFrom);
    const [dateTo, setDateTo] = useState(defaultValue.dateTo);
    const [showFilterModal, setShowFilterModal] = useState(false);
    const [filtersApplied, setFiltersApplied] = useState(0);
    const [statusList, setStatusList] = useState<SelectOption[]>([]);
    const [brokerList, setBrokerList] = useState<SelectOption[]>([]);
    const [typeList, setTypeList] = useState<SelectOption[]>([]);

    useEffect(() => {
        ApiService.fetch("/v1/operations/filters").then(response => {
            setStatusList(response.operationStatuses);
            setBrokerList(response.brokers);
            setTypeList(response.operationTypes);
        });
    }, []);

    useEffect(() => {
        setFiltersApplied(
            filterValues.brokerId.length + filterValues.operationStatus.length + filterValues.operationType.length
        );
    }, [filterValues]);

    const closeModal = () => {
        setShowFilterModal(false);
    };

    const openModal = () => {
        setShowFilterModal(true);
    };

    const findOperationTypeById = (id: string): SelectOption | undefined => {
        return typeList.find((type: SelectOption) => {
            return type.id === id;
        });
    };

    const findOperationStatusById = (id: string): SelectOption | undefined => {
        return statusList.find((status: SelectOption) => {
            return status.id === id;
        });
    };

    const findBrokerById = function (id: string): SelectOption | undefined {
        return brokerList.find((broker: SelectOption) => {
            return broker.id === id;
        });
    };

    const handleCheckboxFilterChange = function (selectedFilters: Record<string, boolean>) {
        let brokersChecked: string[] = [];
        let operationTypesChecked: string[] = [];
        let operationStatusesChecked: string[] = [];
        if (selectedFilters !== undefined) {
            Object.keys(selectedFilters).forEach((id: string) => {
                if (findBrokerById(id) !== undefined) {
                    brokersChecked.push(id);
                }
                if (findOperationTypeById(id) !== undefined) {
                    operationTypesChecked.push(id);
                }
                if (findOperationStatusById(id) !== undefined) {
                    operationStatusesChecked.push(id);
                }
            });
        } else {
            brokersChecked = filterValues.brokerId;
            operationTypesChecked = filterValues.operationType;
            operationStatusesChecked = filterValues.operationStatus;
        }

        setFilterValues({
            brokerId: brokersChecked,
            operationStatus: operationStatusesChecked,
            operationType: operationTypesChecked
        });
    };

    const handleDateFilterChange = (dateFrom: Date, dateTo: Date) => {
        setDateTo(dateTo);
        setDateFrom(dateFrom);
    };

    const handleFilterValuesChange = (filterType: "type" | "status" | "broker", values: Record<string, boolean>) => {
        handleCheckboxFilterChange(values);
    };

    const handleSubmitFilters = (event: ChangeEvent<HTMLFormElement>) => {
        onFilterApply(filterValues, dateFrom, dateTo);
        event.preventDefault();
    };

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <form onSubmit={handleSubmitFilters}>
                <div className="flex flex-wrap gap-2">
                    {statusList.length > 0 ? (
                        <OperationFilterModal
                            title="Operations.FilterModalHeader"
                            brokers={{ option: brokerList, selected: filterValues.brokerId }}
                            operationTypes={{ option: typeList, selected: filterValues.operationType }}
                            operationStatuses={{
                                option: statusList,
                                selected: filterValues.operationStatus
                            }}
                            modal={{ isShown: showFilterModal, close: closeModal }}
                            confirm={selectedFilters => {
                                handleCheckboxFilterChange(selectedFilters);
                            }}
                        />
                    ) : (
                        <></>
                    )}

                    <DateRange
                        dateFrom={dateFrom}
                        dateTo={dateTo}
                        onChange={(dateFrom, dateEnd) => {
                            handleDateFilterChange(dateFrom, dateEnd);
                        }}
                    />
                    <Button type="button" color="primary" onClick={openModal}>
                        <BsFillFunnelFill style={{ marginRight: 10 }}></BsFillFunnelFill>
                        <Trans>Operations.FilterButton</Trans>
                        {" (" + filtersApplied + ")"}
                    </Button>
                    <Button type="submit" color="primary">
                        <Trans>Operations.SearchButton</Trans>
                    </Button>
                </div>
                <div>
                    <OperationFiltersQuick onChange={handleDateFilterChange} onToggle={handleFilterValuesChange} />
                </div>
            </form>
        </Card>
    );
};
