"use client";
import { createContext, ReactNode } from "react";

export type ThemeContextType = {
    lng: string;
};

export const ThemeContext = createContext<ThemeContextType>(null!);

export const ThemeProvider = ({ lng, children }: { lng: string; children: ReactNode }) => {
    const value = { lng };
    return <ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>;
};
