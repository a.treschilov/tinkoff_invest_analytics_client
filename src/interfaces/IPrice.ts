export interface IPrice {
    currency: string;
    amount: number;
}
