import { NextResponse } from "next/server";
import { NextURL } from "next/dist/server/web/next-url";

export async function GET() {
    const response = NextResponse.redirect(new NextURL("/", process.env.NEXT_PUBLIC_BASE_URL));
    response.cookies.delete("jwt");
    return response;
}
