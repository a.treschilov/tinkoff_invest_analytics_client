import "./Item.css";
import { ItemType } from "../Common/Types";
import { ItemName } from "./ItemName";
import { ItemLogo } from "./ItemLogo";

type ItemLogoNameProps = {
    name: string;
    type: ItemType | null;
    logo?: string | null;
};

export const ItemLogoName = ({ name, type, logo = null }: ItemLogoNameProps) => {
    return (
        <span className="item-logo-name" style={{ verticalAlign: "middle" }}>
            <ItemLogo src={logo} name={name} />
            <span style={{ visibility: "hidden" }}>&nbsp;&nbsp;</span>
            <ItemName name={name} type={type} />
        </span>
    );
};
