import { useState } from "react";
import { NewUserActivesComponentProps, NewUserActivesMarketStepProps } from "./NewUserActivesMarket";
import { DepositManage } from "../Deposit/DepositManage";
import { Trans } from "react-i18next";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

export const NewUserActivesDeposit = ({ show, onComplete }: NewUserActivesComponentProps) => {
    const [depositStep, setDepositStep] = useState(1);

    if (!show) {
        return <></>;
    }
    return (
        <>
            <NewUserActivesDepositStep1
                show={depositStep === 1}
                onNegative={onComplete}
                onPositive={() => {
                    setDepositStep(2);
                }}
            />
            <NewUserActivesDepositStep2
                show={depositStep === 2}
                onNegative={onComplete}
                onPositive={() => {
                    setDepositStep(3);
                }}
            />
            <NewUserActivesDepositStep3 show={depositStep === 3} onPositive={onComplete} />
        </>
    );
};

export const NewUserActivesDepositStep1 = ({
    show,
    onNegative = () => {},
    onPositive = () => {}
}: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <div className={"text-lg mb-2"}>
                <Trans>NewUserWizard.Actives.DepositExistQuestion</Trans>
            </div>
            <div>
                <Button
                    size="sm"
                    color={"primary"}
                    onClick={() => {
                        onNegative();
                    }}
                >
                    <Trans>NewUserWizard.Actives.DepositExistNegativeAnswer</Trans>
                </Button>
                &nbsp;&nbsp;
                <Button
                    size="sm"
                    color={"primary"}
                    onClick={() => {
                        onPositive();
                    }}
                >
                    <Trans>NewUserWizard.Actives.DepositExistPositiveAnswer</Trans>
                </Button>
            </div>
        </Card>
    );
};

const NewUserActivesDepositStep2 = ({
    show,
    onNegative = () => {},
    onPositive = () => {}
}: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <div className={"text-lg mb-2"}>
                <Trans>NewUserWizard.Actives.DepositAddQuestion</Trans>
            </div>
            <div>
                <span className={"link"} onClick={onNegative}>
                    <Trans>NewUserWizard.Actives.DepositAddNegativeAnswer</Trans>
                </span>
                &nbsp;&nbsp;
                <Button size="sm" color={"primary"} onClick={onPositive}>
                    <Trans>NewUserWizard.Actives.DepositAddPositiveAnswer</Trans>
                </Button>
            </div>
        </Card>
    );
};

const NewUserActivesDepositStep3 = ({ show, onPositive = () => {} }: NewUserActivesMarketStepProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <>
            <DepositManage />
            <div className="text-end">
                <Button color={"success"} onClick={onPositive}>
                    <Trans>NewUserWizard.Actives.DepositCompleteSetup</Trans>
                </Button>
            </div>
        </>
    );
};
