import { ReactNode } from "react";

type DividerProps = {
    children?: ReactNode;
    className?: string;
};
export const Divider = ({ children, className }: DividerProps) => {
    return <div className={"divider" + (className ? " " + className : "")}>{children}</div>;
};
