import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import Link from "next/link";
import { Trans } from "react-i18next";
import { IoPerson } from "react-icons/io5";
import { usePathname } from "next/navigation";
import { Dropdown } from "../Common/Dropdown/Dropdown";

export const UserMenu = () => {
    const { lng } = useContext(ThemeContext);
    const pathname = usePathname();

    const handleClick = () => {
        const elem = document.activeElement as HTMLElement;
        if (elem) {
            elem?.blur();
        }
    };

    return (
        <Dropdown placement={"bottom"} align={"end"}>
            <Dropdown.Toggle className="btn btn-ghost btn-circle rounded-btn" button={false}>
                <IoPerson />
            </Dropdown.Toggle>
            <Dropdown.Menu className="w-52">
                <li className={"mb-1"}>
                    <Link
                        className={pathname === "/" + lng + "/user/market/credential" ? "focus" : ""}
                        onClick={handleClick}
                        href={makeLink("/user/market/credential", lng)}
                    >
                        <Trans>MainPage.MyAccounts</Trans>
                    </Link>
                </li>
                <li>
                    <Link
                        className={(pathname === "/" + lng + "/user/settings/interface" ? "focus " : "") + "mb-1"}
                        onClick={handleClick}
                        href={makeLink("/user/settings/interface", lng)}
                    >
                        <Trans>MainPage.InterfaceSettings</Trans>
                    </Link>
                </li>
                <li>
                    <a
                        className="mb-1"
                        onClick={handleClick}
                        href={process.env.NEXT_PUBLIC_LANDING_PAGE_URL + "/changelog"}
                        target={"_blank"}
                    >
                        <Trans>MainPage.Changelog</Trans>
                    </a>
                </li>
                <div className={"divider divider-neutral m-0"} />
                <li>
                    <Link className="mb-1" onClick={handleClick} href={makeLink("/logout", lng)}>
                        <Trans>MainPage.Logout</Trans>
                    </Link>
                </li>
            </Dropdown.Menu>
        </Dropdown>
    );
};
