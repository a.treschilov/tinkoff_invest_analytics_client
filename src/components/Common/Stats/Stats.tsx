import { FC, HTMLAttributes, ReactNode } from "react";
import { Stat } from "./Stat";

type StatsProps = {
    children?: ReactNode;
    className?: string;
    vertical?: boolean;
} & HTMLAttributes<HTMLDivElement>;

type StatsExtension = {
    Stat: typeof Stat;
};

export const Stats: FC<StatsProps> & StatsExtension = ({
    children,
    className,
    vertical = false,
    ...args
}: StatsProps) => {
    const classN = ["stats"];
    if (className) {
        classN.push(...className.split(" "));
    }
    if (vertical) {
        classN.push("stats-vertical");
    }
    return (
        <div className={classN.join(" ")} {...args}>
            {children}
        </div>
    );
};

Stats.Stat = Stat;
