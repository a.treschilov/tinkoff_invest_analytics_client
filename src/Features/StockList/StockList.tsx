import { useContext, useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { ApiService } from "../../services/ApiService";
import { Trans } from "react-i18next";
import { IStockFilters } from "../../../app/[lng]/user/market/stock/page";
import { Alert } from "../../components/Common/Alert/Alert";
import Link from "next/link";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import { SessionContext } from "../../providers/SessionProvider";
import { StockListSkeleton } from "./StockListSkeleton";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";
import { Table } from "../../components/Common/Table/Table";

export type MarketStockType = {
    id: number;
    itemId: number;
    ticker: string;
    isin: string;
    name: string;
    currency: string;
    country: string | null;
    sector: string | null;
    industry: string | null;
};

export type CountryType = {
    id: number;
    iso: string;
    name: string;
    continent: string;
};

export const StockList = ({ country, currency, marketSector }: IStockFilters) => {
    const [pageState, setPageState] = useState<string>(PageState.LOADING);
    const [initialState, setInitialState] = useState<PageState>(PageState.LOADING);
    const [stockList, setStockList] = useState<MarketStockType[]>([]);
    const [countryList, setCountryList] = useState<CountryType[]>([]);
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    useEffect(() => {
        setInitialState(PageState.LOADING);
        ApiService.fetch("/v1/dictionary/country").then(response => {
            setCountryList(response);
            setInitialState(PageState.SUCCESS);
        });
    }, []);

    useEffect(() => {
        setPageState(PageState.LOADING);

        const data = {
            country: country,
            currency: currency,
            sector: marketSector
        };
        ApiService.fetch("/v1/market/stock/list", data).then(response => {
            setStockList(response.stocks);
            setPageState(PageState.SUCCESS);
        });
    }, [country, currency, marketSector]);

    const getCountryByIso = (iso: string | null): CountryType | null => {
        if (iso === null) {
            return null;
        }
        const country = countryList.find(country => country.iso === iso);
        return country === undefined ? null : country;
    };

    const renderList = () => {
        return (
            <>
                {stockList.map((stock: MarketStockType) => {
                    return (
                        <tr key={"stock-" + stock.id}>
                            <td>{stock.ticker}</td>
                            <td>
                                <Link
                                    className={"link"}
                                    onClick={() => updatePreviousPage("stockList")}
                                    href={makeLink("/user/item/" + stock.itemId, lng)}
                                >
                                    {stock.name}
                                </Link>
                            </td>
                            <td className="hidden lg:table-cell">{getCountryByIso(stock.country)?.name}</td>
                            <td className="hidden lg:table-cell">{stock.currency}</td>
                            <td className="hidden lg:table-cell">{stock.sector}</td>
                        </tr>
                    );
                })}
            </>
        );
    };

    const renderEmptyList = () => {
        return (
            <tr>
                <td colSpan={5}>
                    <Alert status="info" dismissible={false}>
                        <Trans>StockList.EmptyListMessage</Trans>
                    </Alert>
                </td>
            </tr>
        );
    };

    const renderSuccess = () => {
        return stockList.length === 0 ? renderEmptyList() : renderList();
    };

    let content;
    if (pageState === PageState.LOADING || initialState === PageState.LOADING) {
        content = <StockListSkeleton />;
    } else {
        content = renderSuccess();
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>StockList.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="overflow-x-auto max-h-screen">
                <Table pinRows={true}>
                    <thead>
                        <tr>
                            <td>
                                <Trans>StockList.TableHeaderTicker</Trans>
                            </td>
                            <td>
                                <Trans>StockList.TableHeaderStock</Trans>
                            </td>
                            <td className="hidden lg:table-cell">
                                <Trans>StockList.TableHeaderCountry</Trans>
                            </td>
                            <td className="hidden lg:table-cell">
                                <Trans>StockList.TableHeaderCurrency</Trans>
                            </td>
                            <td className="hidden lg:table-cell">
                                <Trans>StockList.TableHeaderSector</Trans>
                            </td>
                        </tr>
                    </thead>
                    <tbody>{content}</tbody>
                </Table>
            </div>
        </Card>
    );
};
