import { Trans } from "react-i18next";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

type RealEstateEmptyListProps = {
    show: boolean;
    onAddDeposit: () => void;
};
export const RealEstateEmptyList = ({ show, onAddDeposit }: RealEstateEmptyListProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>RealEstateManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="text-center my-4">
                <Button size="lg" color={"primary"} onClick={onAddDeposit}>
                    <Trans>RealEstateManage.AddFirstRealEstateButton</Trans>
                </Button>
            </div>
        </Card>
    );
};
