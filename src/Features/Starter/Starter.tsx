"use client";

import { RealEstateManageWithBoundaryError } from "../RealEstate/RealEstateManage";
import { CrowdfundingManageWithBoundaryErrors } from "../Crowdlanding/CrowdfundingManage";
import { CredentialList } from "../Credential/CredentialList";
import { UserSettings } from "../UserSettings/UserSettings";
import { LoanManageWithBoundaryError } from "../Loan/LoanManage";
import { DepositManageWithBoundaryError } from "../Deposit/DepositManage";
import { useContext, useState } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { BsFillPencilFill } from "react-icons/bs";
import { RiLuggageDepositLine, RiStockFill } from "react-icons/ri";
import { AiFillCreditCard } from "react-icons/ai";
import { MdRealEstateAgent } from "react-icons/md";
import { SiCrowdsource } from "react-icons/si";
import { Tabs } from "../../components/Common/Tabs/Tabs";

type StarterProps = {
    onChange?: () => void;
};

type TabList = "personal" | "market" | "real_estate" | "crowdlanding" | "deposit" | "loan";

const starterTranslation = (lng: string) => {
    switch (lng) {
        case "ru":
            return {
                PersonalGoalsTitle: "Персональные цели",
                StockMarketTitle: "Фондовый рынок",
                CrowdLandingTitle: "Краудлэндинг",
                RealEstateTitle: "Недвижимость",
                LoanTitle: "Кредиты",
                DepositTitle: "Банковские вклады"
            };
        default:
            return {
                PersonalGoalsTitle: "Personal goals",
                StockMarketTitle: "Stock market",
                CrowdLandingTitle: "Crowdlanding",
                RealEstateTitle: "Real estate",
                LoanTitle: "Loans",
                DepositTitle: "Deposits"
            };
    }
};

export const Starter = ({ onChange }: StarterProps) => {
    const { lng } = useContext(ThemeContext);
    const [activeTab, setActiveTab] = useState<TabList>("personal");

    const handleChange = () => {
        if (onChange !== undefined) {
            onChange();
        }
    };

    const translation = starterTranslation(lng);

    return (
        <div className={"overflow-y-auto w-full"}>
            <Tabs className="mt-4 bg-base-100" decorate={"box"}>
                <Tabs.Tab
                    active={activeTab === "personal"}
                    onClick={() => {
                        setActiveTab("personal");
                    }}
                >
                    <BsFillPencilFill className={"md:hidden"} />
                    <span className={"hidden md:inline"}>{translation.PersonalGoalsTitle}</span>
                </Tabs.Tab>
                <Tabs.Tab
                    active={activeTab === "deposit"}
                    onClick={() => {
                        setActiveTab("deposit");
                    }}
                >
                    <RiLuggageDepositLine className={"md:hidden"} />
                    <span className={"hidden md:inline"}>{translation.DepositTitle}</span>
                </Tabs.Tab>
                <Tabs.Tab
                    active={activeTab === "loan"}
                    onClick={() => {
                        setActiveTab("loan");
                    }}
                >
                    <AiFillCreditCard className={"md:hidden"} />
                    <span className={"hidden md:inline"}>{translation.LoanTitle}</span>
                </Tabs.Tab>
                <Tabs.Tab
                    active={activeTab === "real_estate"}
                    onClick={() => {
                        setActiveTab("real_estate");
                    }}
                >
                    <MdRealEstateAgent className={"md:hidden"} />
                    <span className={"hidden md:inline"}>{translation.RealEstateTitle}</span>
                </Tabs.Tab>
                <Tabs.Tab
                    active={activeTab === "market"}
                    onClick={() => {
                        setActiveTab("market");
                    }}
                >
                    <RiStockFill className={"md:hidden"} />
                    <span className={"hidden md:inline"}>{translation.StockMarketTitle}</span>
                </Tabs.Tab>
                <Tabs.Tab
                    active={activeTab === "crowdlanding"}
                    onClick={() => {
                        setActiveTab("crowdlanding");
                    }}
                >
                    <SiCrowdsource className={"md:hidden"} />
                    <span className={"hidden md:inline"}>{translation.CrowdLandingTitle}</span>
                </Tabs.Tab>
            </Tabs>
            <div className={"tab-content p-6 bg-base-100" + (activeTab === "personal" ? " block" : "")}>
                <UserSettings onChange={handleChange} />
            </div>
            <div className={"tab-content p-6 bg-base-100" + (activeTab === "deposit" ? " block" : "")}>
                <DepositManageWithBoundaryError onChange={handleChange} />
            </div>
            <div className={"tab-content p-6 bg-base-100" + (activeTab === "loan" ? " block" : "")}>
                <LoanManageWithBoundaryError onChange={handleChange} />
            </div>
            <div className={"tab-content p-6 bg-base-100" + (activeTab === "real_estate" ? " block" : "")}>
                <RealEstateManageWithBoundaryError onChange={handleChange} />
            </div>
            <div className={"tab-content p-6 bg-base-100" + (activeTab === "market" ? " block" : "")}>
                <CredentialList />
            </div>
            <div className={"tab-content p-6 bg-base-100" + (activeTab === "crowdlanding" ? " block" : "")}>
                <CrowdfundingManageWithBoundaryErrors onChange={handleChange} />
            </div>
        </div>
    );
};
