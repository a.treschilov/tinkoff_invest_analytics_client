import { FC, HTMLAttributes, ReactNode } from "react";
import { MenuItem } from "./MenuItem";
import { MenuTitle } from "./MenuTitle";

type MenuProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLUListElement>;

type MenuExtension = {
    Title: typeof MenuTitle;
    Item: typeof MenuItem;
};

export const Menu: FC<MenuProps> & MenuExtension = ({ children, className, ...args }: MenuProps) => {
    const classes = ["menu"];
    if (className) {
        classes.push(...className.split(" "));
    }
    return (
        <ul className={classes.join(" ")} {...args}>
            {children}
        </ul>
    );
};

Menu.Title = MenuTitle;
Menu.Item = MenuItem;
