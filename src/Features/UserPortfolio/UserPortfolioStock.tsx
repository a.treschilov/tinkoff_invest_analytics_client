import { PortfolioItem } from "./UserPortfolio";
import { UserPortfolioStockStock } from "./UserPortfolioStockStock";
import { ComponentState } from "../../components/Common/Types";
import { CiFaceMeh } from "react-icons/ci";
import { EmptyContent } from "../../components/Common/EmptyContent/EmptyContent";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Collapse } from "../../components/Common/Collapse/Collapse";

type Props = {
    items: PortfolioItem[];
    state: ComponentState;
    show: boolean;
};

export const UserPortfolioStock = ({ items, state, show }: Props) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userPortfolio");

    if (!show) {
        return <></>;
    }
    if (state === "loading") {
        return <Skeleton className={"h-72 w-full"} />;
    } else if (items.length === 0) {
        return (
            <EmptyContent>
                {t("Stock.EmptyList")} <CiFaceMeh />
            </EmptyContent>
        );
    } else {
        const stocks = items.filter((item: PortfolioItem) => {
            return item.item.stock?.type === "Stock";
        });
        const bonds = items.filter((item: PortfolioItem) => {
            return item.item.stock?.type === "Bond";
        });
        const etf = items.filter((item: PortfolioItem) => {
            return item.item.stock?.type === "Etf";
        });
        const future = items.filter((item: PortfolioItem) => {
            return item.item.stock?.type === "Futures";
        });

        return (
            <>
                <Collapse icon={"arrow"} checkbox={true}>
                    <Collapse.Title>
                        {t("Stock.StockHeader")}&nbsp;&nbsp;&nbsp;
                        <small className={"text-primary"}>
                            <small>
                                {stocks.length} {t("Stock.StockSecondary")}
                            </small>
                        </small>
                    </Collapse.Title>
                    <Collapse.Content>
                        <UserPortfolioStockStock items={stocks} />
                    </Collapse.Content>
                </Collapse>
                <Collapse icon={"arrow"} checkbox={true}>
                    <Collapse.Title>
                        {t("Stock.BondHeader")}&nbsp;&nbsp;&nbsp;
                        <small className={"text-primary"}>
                            <small>
                                {bonds.length} {t("Stock.BondSecondary")}
                            </small>
                        </small>
                    </Collapse.Title>
                    <Collapse.Content>
                        <UserPortfolioStockStock items={bonds} />
                    </Collapse.Content>
                </Collapse>
                <Collapse icon={"arrow"} checkbox={true}>
                    <Collapse.Title>
                        {t("Stock.EtfHeader")}&nbsp;&nbsp;&nbsp;
                        <small className={"text-primary"}>
                            <small>
                                {etf.length} {t("Stock.EtfSecondary")}
                            </small>
                        </small>
                    </Collapse.Title>
                    <Collapse.Content>
                        <UserPortfolioStockStock items={etf} />
                    </Collapse.Content>
                </Collapse>
                <Collapse icon={"arrow"} checkbox={true}>
                    <Collapse.Title>
                        {t("Stock.FutureHeader")}&nbsp;&nbsp;&nbsp;
                        <small className={"text-primary"}>
                            <small>
                                {future.length} {t("Stock.FutureSecondary")}
                            </small>
                        </small>
                    </Collapse.Title>
                    <Collapse.Content>
                        <UserPortfolioStockStock items={future} />
                    </Collapse.Content>
                </Collapse>
            </>
        );
    }
};
