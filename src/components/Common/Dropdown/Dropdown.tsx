import { FC, HTMLAttributes, ReactNode } from "react";
import { DropdownToggle } from "./DropdownToggle";
import { DropdownItem } from "./DropdownItem";
import { DropdownMenu } from "./DropdownMenu";

type DropdownProps = {
    children?: ReactNode;
    placement?: "left" | "right" | "top" | "bottom";
    align?: "start" | "end" | "center";
} & HTMLAttributes<HTMLDivElement>;

type DropdownExtension = {
    Toggle: typeof DropdownToggle;
    Menu: typeof DropdownMenu;
    Item: typeof DropdownItem;
};

export const Dropdown: FC<DropdownProps> & DropdownExtension = ({
    children,
    placement = "left",
    align = "start",
    ...args
}: DropdownProps) => {
    let classN = "";
    switch (placement) {
        case "left":
            classN += " dropdown-left";
            break;
        case "right":
            classN += " dropdown-right";
            break;
        case "top":
            classN += " dropdown-top";
            break;
        case "bottom":
            classN += " dropdown-bottom";
            break;
    }
    switch (align) {
        case "start":
            classN += " dropdown-start";
            break;
        case "center":
            classN += " dropdown-center";
            break;
        case "end":
            classN += " dropdown-end";
            break;
    }
    return (
        <div className={"dropdown" + classN} {...args}>
            {children}
        </div>
    );
};

Dropdown.Toggle = DropdownToggle;
Dropdown.Menu = DropdownMenu;
Dropdown.Item = DropdownItem;
