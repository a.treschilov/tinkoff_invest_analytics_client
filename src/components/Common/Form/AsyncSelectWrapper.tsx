import AsyncSelect, { AsyncProps } from "react-select/async";
import { GroupBase } from "react-select/dist/declarations/src/types";

// @ts-expect-error unknown
type AsyncSelectWrapperProps = AsyncProps<Option, IsMulti, GroupBase<Option>>;

export const AsyncSelectWrapper = ({ ...args }: AsyncSelectWrapperProps) => {
    return (
        <AsyncSelect
            {...args}
            classNames={{
                control: () => "py-[5px] rounded-r-lg"
            }}
            theme={theme => ({
                ...theme,
                borderRadius: 0,
                colors: {
                    ...theme.colors,
                    neutral0: "var(--fallback-b1,oklch(var(--b1)/var(--tw-bg-opacity)))",
                    neutral10: "var(--fallback-b2,oklch(var(--b2)/var(--tw-bg-opacity)))",
                    neutral20: "var(--fallback-bc,oklch(var(--bc)/0.2))",
                    neutral80: "var(--fallback-bc,oklch(var(--bc)/1))",
                    primary: "var(--fallback-bc,oklch(var(--bc)/1))",
                    primary25: "var(--fallback-bc,oklch(var(--bc)/0.2))"
                }
            })}
        />
    );
};
