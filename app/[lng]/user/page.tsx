"use client";

import { useContext, useState } from "react";
import { Dashboard } from "../../../src/Features/Dashboard/Dashboard";
import { Starter } from "../../../src/Features/Starter/Starter";
import { AuthContext } from "../../../src/providers/AuthProvider";
import { NewUserSteps } from "../../../src/Features/NewUserSteps/NewUserSteps";

const Page = () => {
    const [timestamp, setTimestamp] = useState(new Date().getTime());
    const { isNewUser } = useContext(AuthContext);

    const handleChange = () => {
        setTimestamp(new Date().getTime());
    };

    if (isNewUser) {
        return <NewUserSteps />;
    }

    return (
        <>
            <Dashboard timestamp={timestamp} />
            <Starter onChange={handleChange} />
        </>
    );
};

export default Page;
