"use client";

import { useContext, useEffect, useState } from "react";
import { PageState } from "../../../components/Common/Types";
import { PageLoader } from "../../../components/Loader/Loaders";
import { AggregatedPeriod } from "./AnalyticsAccumulation";
import { ApiService } from "../../../services/ApiService";
import { AggregatePeriodOptions } from "./AnalyticsAccumulationFilter";
import { AnalyticsPayInDemoData } from "./AnalyticsPayInDemoData";
import { AnalyticsEarningDemoData } from "./AnalyticsEarningDemoData";
import { AuthContext } from "../../../providers/AuthProvider";

import dynamic from "next/dynamic";
import { mergeDeep } from "../../../utils/objects";
import { defaultChartOptions } from "../ChatOptions";
const Chart = dynamic(() => import("react-apexcharts"), {
    ssr: false
});

interface IAnalyticsAccumulationBarProps {
    dateFrom: string;
    dateTo: string;
    aggregationPeriod: AggregatedPeriod;
}

export interface IAggregateOperation {
    amount: number;
    date: string;
}

export const AnalyticsAccumulationBar = ({ dateFrom, dateTo, aggregationPeriod }: IAnalyticsAccumulationBarProps) => {
    const [pageStateEarning, setPageStateEarning] = useState(PageState.LOADING);
    const [pageStatePayIn, setPageStatePayIn] = useState(PageState.LOADING);
    const [earningData, setEarningData] = useState<IAggregateOperation[]>([]);
    const [payinData, setPayinData] = useState<IAggregateOperation[]>([]);
    const user = useContext(AuthContext);

    useEffect(() => {
        setPageStateEarning(PageState.LOADING);
        setPageStatePayIn(PageState.LOADING);
        if (user.isNewUser) {
            setPayinData(AnalyticsPayInDemoData);
            setEarningData(AnalyticsEarningDemoData);
            setPageStatePayIn(PageState.SUCCESS);
            setPageStateEarning(PageState.SUCCESS);
        } else {
            ApiService.fetch("/v1/operations/payin", {
                dateTo: dateTo,
                dateFrom: dateFrom,
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                aggregatePeriod: aggregationPeriod
            })
                .then(response => {
                    setPayinData(response.data);
                    setPageStatePayIn(PageState.SUCCESS);
                })
                .catch(error => {
                    console.log(error);
                });

            ApiService.fetch("/v1/operations/earning", {
                dateTo: dateTo,
                dateFrom: dateFrom,
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                aggregatePeriod: aggregationPeriod
            })
                .then(response => {
                    setEarningData(response.data);
                    setPageStateEarning(PageState.SUCCESS);
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }, [dateFrom, dateTo, aggregationPeriod, user.isNewUser]);

    const getBarDates = () => {
        const barDates = [];
        const finalDate = new Date(dateTo);
        const currentDate = new Date(dateFrom);

        switch (aggregationPeriod) {
            case "month":
                currentDate.setDate(1);
                break;
            case "year":
                currentDate.setMonth(0);
                currentDate.setDate(1);
                break;
        }

        do {
            barDates.push(new Date(currentDate));
            switch (aggregationPeriod) {
                case "month":
                    currentDate.setMonth(currentDate.getMonth() + 1);
                    break;
                case "year":
                    currentDate.setFullYear(currentDate.getFullYear() + 1);
                    break;
            }
        } while (currentDate < finalDate);

        return barDates;
    };

    const barData = () => {
        if (pageStatePayIn === PageState.SUCCESS && pageStateEarning === PageState.SUCCESS) {
            let dateFormatter = new Intl.DateTimeFormat("en-US", {
                year: "numeric",
                month: "long"
            });
            if (aggregationPeriod === AggregatePeriodOptions.YEAR) {
                dateFormatter = new Intl.DateTimeFormat("en-US", {
                    year: "numeric"
                });
            }

            const barDates = getBarDates();
            const labels = [];
            const datasetEarning = [];
            const datasetPayin = [];
            for (const i in barDates) {
                labels.push(dateFormatter.format(new Date(barDates[i])));
                const earningAmount = earningData.find(el => {
                    return new Date(el.date).getTime() === barDates[i].getTime();
                });
                datasetEarning.push(earningAmount !== undefined ? earningAmount.amount : 0);

                const payinAmount = payinData.find(el => {
                    return new Date(el.date).getTime() === barDates[i].getTime();
                });
                datasetPayin.push(payinAmount !== undefined ? payinAmount.amount : 0);
            }

            const chartData = [
                {
                    name: "Пополнения",
                    data: datasetPayin
                },
                {
                    name: "Доход",
                    data: datasetEarning
                }
            ];

            const options = {
                chart: {
                    id: "basic-bar",
                    background: "transparent"
                },
                xaxis: {
                    categories: labels
                },
                dataLabels: {
                    enabled: false
                }
            };

            const chartOptions = mergeDeep(defaultChartOptions, options);

            return <Chart options={chartOptions} type="bar" series={chartData} width="100%" height={300} />;
        }
    };

    return pageStateEarning === PageState.LOADING || pageStatePayIn === PageState.LOADING ? (
        <PageLoader height={"300px"} />
    ) : (
        <>{barData()}</>
    );
};
