import { HTMLAttributes, ReactNode } from "react";

type TabProps = {
    children: ReactNode;
    active?: boolean;
} & HTMLAttributes<HTMLDivElement>;

export const Tab = ({ children, active = false, className, ...args }: TabProps) => {
    const classes = ["tab"];
    if (className) {
        classes.push(...className.split(" "));
    }
    if (active) {
        classes.push("tab-active");
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
