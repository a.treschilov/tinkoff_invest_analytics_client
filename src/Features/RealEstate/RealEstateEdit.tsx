import { Trans } from "react-i18next";
import { Modal } from "../../components/Common/Modal/Modal";
import { RealEstateEditForm } from "./RealEstateEditForm";

interface IRealEstateEditProps {
    realEstateId: number;
    isShow: boolean;
    hideModal: () => void;
    changeDataHandler: () => void;
}

export const RealEstateEdit = ({ realEstateId, isShow, hideModal, changeDataHandler }: IRealEstateEditProps) => {
    return (
        <Modal show={isShow} onClose={hideModal}>
            <Modal.Header>
                <Trans>RealEstateEdit.ModalHeader</Trans>
            </Modal.Header>
            <Modal.Body>
                <RealEstateEditForm realEstateId={realEstateId} onSubmit={changeDataHandler} onDiscard={hideModal} />
            </Modal.Body>
        </Modal>
    );
};
