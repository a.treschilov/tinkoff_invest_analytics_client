import { ChangeEvent, useEffect, useState } from "react";
import { PageState } from "../../components/Common/Types";
import { Trans } from "react-i18next";
import { formatInputDayDate } from "../../utils/date";
import { DefaultRealEstateAsset } from "./RealEstateStaticData";
import { ApiService } from "../../services/ApiService";
import { Modal } from "../../components/Common/Modal/Modal";
import { InputText } from "../../components/Common/Form/InputText";
import { InputCurrencyAmount } from "../../components/Common/Form/InputCurrencyAmount";
import Select from "../../components/Common/Form/Select";
import { Toggle } from "../../components/Common/Form/Toggle";
import { PageLoader } from "../../components/Loader/Loaders";
import { Alert } from "../../components/Common/Alert/Alert";
import { Button } from "../../components/Common/Button/Button";
import { Join } from "../../components/Common/Join/Join";

interface IRealEstateAddProps {
    isShow: boolean;
    hideModal: () => void;
    showAlert: () => void;
    changeDataHandler: () => void;
}

export const RealEstateAdd = ({ isShow, hideModal, showAlert, changeDataHandler }: IRealEstateAddProps) => {
    const [pageState, setPageState] = useState(PageState.EDIT_FORM);
    const [errorMessage, setErrorMessage] = useState<string | null>(null);
    const [realEstateData, setRealEstateData] = useState(DefaultRealEstateAsset);
    const [rentBlockShow, setRentBlockShow] = useState(false);

    useEffect(() => {
        setRealEstateData(DefaultRealEstateAsset);
        setErrorMessage(null);
        setPageState(PageState.EDIT_FORM);
    }, [isShow]);

    const changeElement = (name: string, value: string, inputType: string) => {
        let realEstate = realEstateData;
        if (realEstate === null) {
            realEstate = DefaultRealEstateAsset;
        }

        let typedValue: number | string;
        switch (inputType) {
            case "number":
                typedValue = parseFloat(value);
                break;
            default:
                typedValue = value;
        }

        if (name === "purchaseCurrency") {
            realEstate.currency = typedValue + "";
        } else if (["amount", "currency"].indexOf(name) !== -1) {
            const price = realEstate.prices[0];

            if (name === "amount") {
                price.amount = parseFloat(value);
            }
            if (name === "currency") {
                price.currency = value + "";
            }
            realEstate.prices[0] = price;
        } else if (name === "purchaseDate") {
            realEstate.purchaseDate.date = typedValue.toString();
        } else {
            const data = {
                [name]: typedValue
            };
            realEstate = { ...realEstate, ...data };
        }

        setRealEstateData(realEstate);
    };

    const handleChangeFormData = (event: ChangeEvent<HTMLInputElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleChangeSelectData = (event: ChangeEvent<HTMLSelectElement>) => {
        changeElement(event.target.name, event.target.value, event.target.type);
    };

    const handleSubmitForm = (event: ChangeEvent<HTMLFormElement>) => {
        setPageState(PageState.LOADING);

        const data = {
            name: realEstateData?.name,
            payoutFrequency: rentBlockShow ? realEstateData?.payoutFrequency : 1,
            payoutFrequencyPeriod: rentBlockShow ? realEstateData?.payoutFrequencyPeriod : "month",
            interestAmount: rentBlockShow ? realEstateData?.interestAmount : 0,
            interestCurrency: rentBlockShow ? realEstateData?.interestCurrency : "RUB",
            currency: realEstateData?.currency,
            purchaseDate: realEstateData?.purchaseDate,
            purchaseAmount: realEstateData?.purchaseAmount,
            prices: realEstateData?.prices
        };

        ApiService.post("/v1/real_estate/item", data)
            .then(() => {
                changeDataHandler();
                hideModal();
                showAlert();
            })
            .catch(e => {
                setErrorMessage(e.message);
                setPageState(PageState.EDIT_FORM);
            });

        event.preventDefault();
    };

    const renderData = () => {
        const date =
            realEstateData.purchaseDate.date === undefined ? new Date() : new Date(realEstateData.purchaseDate.date);

        const formattedDate = formatInputDayDate(date) || "";

        return (
            <form onSubmit={handleSubmitForm}>
                <Modal.Header>
                    <Trans>RealEstateAdd.ModalHeader</Trans>
                </Modal.Header>
                <Modal.Body>
                    <Alert show={errorMessage !== null} onClose={() => setErrorMessage(null)}>
                        {errorMessage}
                    </Alert>
                    <div className={"grid grid-cols-2 gap-6 mb-6"}>
                        <InputText
                            containerClassName={"col-span-2"}
                            label={<Trans>RealEstateAdd.FormLabelName</Trans>}
                            type="text"
                            name="name"
                            onChange={handleChangeFormData}
                            value={realEstateData.name}
                        />
                        <InputCurrencyAmount
                            id={"real_estate_amount_0"}
                            amount={realEstateData.prices[0].amount}
                            currency={realEstateData.currency}
                            onChange={changeElement}
                            step={100}
                            containerClassName={"col-span-2"}
                            label={<Trans>RealEstateAdd.FormLabelAmount</Trans>}
                        />
                        <InputCurrencyAmount
                            containerClassName={"col-span-2 sm:col-span-1"}
                            label={<Trans>RealEstateAdd.FormLabelPurchaseAmount</Trans>}
                            id={"purchase_amount_0"}
                            amount={realEstateData.purchaseAmount || 0}
                            currency={realEstateData.currency || "RUB"}
                            onChange={changeElement}
                            step={100}
                            amountInputName={"purchaseAmount"}
                            currencyInputName={"purchaseCurrency"}
                        />
                        <InputText
                            containerClassName={"col-span-2 sm:col-span-1"}
                            label={<Trans>RealEstateAdd.FormLabelPurchaseDate</Trans>}
                            type="date"
                            name="purchaseDate"
                            onChange={handleChangeFormData}
                            defaultValue={formattedDate}
                        />
                        <div className="form-control col-span-2">
                            <Toggle>
                                <Toggle.Label>
                                    <Trans>RealEstateAdd.ForRent</Trans>
                                </Toggle.Label>
                                <Toggle.Checkbox
                                    checked={rentBlockShow}
                                    onChange={(event: ChangeEvent<HTMLInputElement>) => {
                                        setRentBlockShow(event.target.checked);
                                    }}
                                    color={"primary"}
                                />
                            </Toggle>
                        </div>
                        {rentBlockShow ? (
                            <>
                                <InputCurrencyAmount
                                    containerClassName={"col-span-2 sm:col-span-1"}
                                    label={<Trans>RealEstateAdd.FormLabelInterest</Trans>}
                                    amount={realEstateData.interestAmount}
                                    onChange={changeElement}
                                    currency={realEstateData.interestCurrency}
                                    id={"interest_currency_0"}
                                    step={100}
                                    currencyInputName={"interestCurrency"}
                                    amountInputName={"interestAmount"}
                                />
                                <div className={"col-span-2 sm:col-span-1"}>
                                    <label className="label">
                                        <span className={"label-text text-base-content"}>
                                            <Trans>RealEstateAdd.FormLabelPayoutFrequency</Trans>
                                        </span>
                                    </label>
                                    <Join>
                                        <InputText
                                            type="number"
                                            name="payoutFrequency"
                                            onChange={handleChangeFormData}
                                            value={realEstateData?.payoutFrequency}
                                            min={1}
                                            className={"rounded-r-none"}
                                        />
                                        <Select>
                                            <Select.Select
                                                name="payoutFrequencyPeriod"
                                                onChange={handleChangeSelectData}
                                                defaultValue={realEstateData?.payoutFrequencyPeriod}
                                                className={"rounded-l-none"}
                                            >
                                                <option value="day">
                                                    <Trans>RealEstateAdd.PeriodDay</Trans>
                                                </option>
                                                <option value="month">
                                                    <Trans>RealEstateAdd.PeriodMonth</Trans>
                                                </option>
                                                <option value="year">
                                                    <Trans>RealEstateAdd.PeriodYear</Trans>
                                                </option>
                                            </Select.Select>
                                        </Select>
                                    </Join>
                                </div>
                            </>
                        ) : (
                            <></>
                        )}
                    </div>
                </Modal.Body>
                <Modal.Actions>
                    <Button type="submit" color="primary">
                        <Trans>RealEstateAdd.ButtonTextSave</Trans>
                    </Button>
                </Modal.Actions>
            </form>
        );
    };

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = <PageLoader />;
            break;
        default:
            content = renderData();
            break;
    }
    return (
        <Modal show={isShow} onClose={hideModal}>
            {content}
        </Modal>
    );
};
