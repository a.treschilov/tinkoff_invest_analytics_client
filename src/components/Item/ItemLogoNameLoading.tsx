import { Skeleton } from "react-daisyui";

export const ItemLogoNameLoading = () => {
    return (
        <div className="flex">
            <Skeleton className={"w-6 h-6 rounded mr-2"} />
            <Skeleton className={"w-1/2 h-6 rounded"} />
        </div>
    );
};
