import React, { RefAttributes } from "react";
import { Color, Size } from "../Types";
import { daisyUIColorClass, daisyUISizeClass } from "../utils";

type FileInputProps = Omit<React.InputHTMLAttributes<HTMLInputElement>, "size"> & {
    color?: Color;
    size?: Size;
} & RefAttributes<HTMLInputElement>;

export const FileInput = ({ className, color, size, type = "file", ...args }: FileInputProps) => {
    const classes = ["file-input"];
    if (className) {
        classes.push(...className.split(" "));
    }
    classes.push(daisyUIColorClass("file-input", color));
    classes.push(daisyUISizeClass("file-input", size));

    return <input type={type} className={classes.join(" ")} {...args} />;
};
