import { PortfolioItem } from "./UserPortfolio";
import { ComponentState } from "../../components/Common/Types";
import { ItemLogoName } from "../../components/Item/ItemLogoName";
import { useContext, useMemo } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import Link from "next/link";
import { EmptyContent } from "../../components/Common/EmptyContent/EmptyContent";
import {
    ColumnDef,
    getCoreRowModel,
    getPaginationRowModel,
    getSortedRowModel,
    useReactTable
} from "@tanstack/react-table";
import { Table } from "../../components/Table/Table";
import { useTranslation } from "../../../app/i18n/client";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";

type Props = {
    items: PortfolioItem[];
    state: ComponentState;
    show: boolean;
};

export const UserPortfolioDeposit = ({ items, state, show }: Props) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userPortfolio");

    const columns = useMemo<ColumnDef<PortfolioItem>[]>(
        () => [
            {
                accessorKey: "name",
                header: () => t("Deposit.TableHeaderName"),
                cell: info => (
                    <Link href={makeLink("/user/item/" + info.row.original.itemId, lng)}>
                        <ItemLogoName
                            name={info.row.original.item.name}
                            type={info.row.original.item.type}
                            logo={info.row.original.item.logo}
                        />
                    </Link>
                ),
                sortingFn: (rowA, rowB) => {
                    return rowA.original.item.name > rowB.original.item.name ? 1 : -1;
                },
                sortDescFirst: false
            },
            {
                accessorKey: "amount",
                header: () => t("Deposit.TableHeaderAmount"),
                cell: info => (
                    <div>
                        {new Intl.NumberFormat("ru-RU", {
                            style: "currency",
                            currency: info.row.original.currencyIso
                        }).format(info.row.original.amount)}
                    </div>
                ),
                sortingFn: (rowA, rowB) => {
                    return rowA.original.amount - rowB.original.amount;
                }
            }
        ],
        [lng, t]
    );

    const table = useReactTable({
        columns: columns,
        data: items,
        getCoreRowModel: getCoreRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        autoResetPageIndex: false,
        initialState: {
            sorting: [
                {
                    id: "name",
                    desc: false
                }
            ],
            pagination: {
                pageIndex: 0,
                pageSize: 10000
            }
        }
    });

    if (!show) {
        return <></>;
    }

    if (state === "loading") {
        return <Skeleton className={"h-[300px] w-full"} />;
    } else if (items.length === 0) {
        return <EmptyContent size={"sm"}>{t("Deposit.EmptyList")}</EmptyContent>;
    } else {
        return <Table table={table} />;
    }
};
