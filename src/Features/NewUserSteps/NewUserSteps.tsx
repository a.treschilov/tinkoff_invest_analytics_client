import { Card, Divider, Steps } from "react-daisyui";
import { NewUserPersonalGoals } from "./NewUserPersonalGoals";
import { NewUserCapital } from "./NewUserCapital";
import { useContext, useEffect, useState } from "react";
import { NewUserActives } from "./NewUserActives";
import { NewUserPassives } from "./NewUserPassives";
import { NewUserSuccess } from "./NewUserSuccess";
import { Trans } from "react-i18next";
import { AuthContext } from "../../providers/AuthProvider";
import { ApiService } from "../../services/ApiService";
import { SessionContext } from "../../providers/SessionProvider";
import { ThemeContext } from "../../providers/ThemeProvider";

export type StepType = "personal_goals" | "capital" | "actives" | "loans";
export type StepComponentProps = {
    onComplete: () => void;
    active: boolean;
};
export type StepStatus = "success" | "active" | "not_started";
export type StepMap = Record<StepType, StepStatus>;

const DefaultStepMap: StepMap = {
    personal_goals: "active",
    capital: "not_started",
    actives: "not_started",
    loans: "not_started"
};

const getTranslation = (lng: string) => {
    switch (lng) {
        case "ru":
            return {
                pageTitle: "Начало работы"
            };
        default:
            return {
                pageTitle: "Getting started"
            };
    }
};

export const NewUserSteps = () => {
    const [wizard, setWizard] = useState(DefaultStepMap);
    const { refreshUser } = useContext(AuthContext);
    const { setPageTitle } = useContext(SessionContext);
    const { lng } = useContext(ThemeContext);

    useEffect(() => {
        setTimeout(() => {
            setPageTitle(getTranslation(lng).pageTitle);
        }, 0);
    }, [lng, setPageTitle]);

    const completeSettingUp = () => {
        refreshUser();
    };

    const updateUser = () => {
        ApiService.put("/v1/user/onboarding/complete");
    };

    const onStepComplete = (step: StepType) => {
        const steps: StepMap = structuredClone(wizard);
        switch (step) {
            case "personal_goals":
                steps.personal_goals = "success";
                steps.capital = "active";
                setWizard(steps);
                break;
            case "capital":
                steps.capital = "success";
                steps.actives = "active";
                setWizard(steps);
                break;
            case "actives":
                steps.actives = "success";
                steps.loans = "active";
                setWizard(steps);
                break;
            case "loans":
                steps.loans = "success";
                setWizard(steps);
                updateUser();
                break;
        }
    };

    const getStepColor = (step: StepStatus) => {
        switch (step) {
            case "active":
                return "primary";
            case "not_started":
                return "neutral";
            case "success":
                return "success";
        }
    };

    return (
        <div className={"lg:w-1/2 mx-auto"}>
            <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
                <div className="overflow-x-auto">
                    <Steps>
                        <Steps.Step
                            value={wizard.personal_goals === "success" ? "✓" : "1"}
                            color={getStepColor(wizard.personal_goals)}
                        >
                            <Trans>NewUserWizard.Steps.PersonalGoals</Trans>
                        </Steps.Step>
                        <Steps.Step
                            value={wizard.capital === "success" ? "✓" : "2"}
                            color={getStepColor(wizard.capital)}
                        >
                            <Trans>NewUserWizard.Steps.Capital</Trans>
                        </Steps.Step>
                        <Steps.Step
                            value={wizard.actives === "success" ? "✓" : "3"}
                            color={getStepColor(wizard.actives)}
                        >
                            <Trans>NewUserWizard.Steps.Actives</Trans>
                        </Steps.Step>
                        <Steps.Step value={wizard.loans === "success" ? "✓" : "4"} color={getStepColor(wizard.loans)}>
                            <Trans>NewUserWizard.Steps.Passives</Trans>
                        </Steps.Step>
                    </Steps>
                </div>
            </Card>
            <>
                <Divider />
                <NewUserPersonalGoals
                    active={wizard.personal_goals === "active"}
                    onComplete={() => onStepComplete("personal_goals")}
                />
                <NewUserCapital active={wizard.capital === "active"} onComplete={() => onStepComplete("capital")} />
                <NewUserActives onComplete={() => onStepComplete("actives")} active={wizard.actives === "active"} />
                <NewUserPassives onComplete={() => onStepComplete("loans")} active={wizard.loans === "active"} />
                <NewUserSuccess
                    active={wizard.loans === "success"}
                    onComplete={() => {
                        completeSettingUp();
                    }}
                />
            </>
        </div>
    );
};
