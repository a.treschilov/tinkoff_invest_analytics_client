import { useContext, useEffect, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { DateType, PageState } from "../../components/Common/Types";
import { toUserTimezone } from "../../utils/date";
import { useTranslation } from "../../../app/i18n/client";
import { ThemeContext } from "../../providers/ThemeProvider";
import { EmptyContent } from "../../components/Common/EmptyContent/EmptyContent";
import { Divider } from "../../components/Common/Divider/Divider";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Card } from "../../components/Common/Card/Card";
import { Table } from "../../components/Common/Table/Table";
import { Badge } from "../../components/Common/Badge/Badge";

type BrokerUploadReportProps = {
    date: number;
};

type BrokerReportStatusType = "success" | "fail";

type BrokerReportType = {
    brokerReportUploadId: number;
    date: DateType;
    brokerId: string;
    broker: {
        id: string;
        name: string;
        logo: string;
        isApi: boolean;
        isManual: boolean;
    };
    added: number;
    updated: number;
    skipped: number;
    status: BrokerReportStatusType;
};

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit"
});

export const BrokerUploadReport = ({ date }: BrokerUploadReportProps) => {
    const [reportList, setReportList] = useState<BrokerReportType[]>([]);
    const [pageState, setPageState] = useState(PageState.LOADING);
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "broker");

    useEffect(() => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/broker/brokerUploadReports").then((response: BrokerReportType[]) => {
            setReportList(response);
            setPageState(PageState.EDIT_FORM);
        });
    }, [date]);

    const emptyReportList = () => {
        return (
            <tbody>
                <tr>
                    <td colSpan={4}>
                        <EmptyContent size={"xs"}>{t("UploadReport.UploadReportListEmpty")}</EmptyContent>
                    </td>
                </tr>
            </tbody>
        );
    };
    const tableBody = () => {
        return (
            <tbody>
                {reportList.map(report => {
                    return (
                        <tr key={"broker_report_" + report.brokerReportUploadId}>
                            <td>{dateFormatter.format(toUserTimezone(report.date))}</td>
                            <td className={"hidden sm:table-cell"}>{report.broker.name}</td>
                            <td>
                                <span className={"sm:hidden"}>
                                    {report.broker.name}
                                    <br />
                                </span>
                                {report.added + "/" + report.skipped}
                            </td>
                            <td>
                                {report.status === "success" ? (
                                    <Badge color={"success"}>{t("UploadReport.UploadReportStatusSuccess")}</Badge>
                                ) : (
                                    <Badge color={"error"}>{t("UploadReport.UploadReportStatusFail")}</Badge>
                                )}
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        );
    };
    const tableBodyLoading = () => {
        return (
            <tbody>
                {[...Array(5)].map((value: number, key: number) => {
                    return (
                        <tr key={"broker_report_" + key}>
                            <td>
                                <Skeleton className={"w-5/6 h-3 sm:h-3.5"} />
                                <Skeleton className={"w-3/4 h-3 mt-1 sm:hidden"} />
                            </td>
                            <td className={"hidden sm:table-cell"}>
                                <Skeleton className={"w-3/4 h-3 sm:h-3.5"} />
                            </td>
                            <td>
                                <Skeleton className={"sm:hidden w-3/4 h-3 sm:h-3.5 mt-1"} />
                                <Skeleton className={"w-1/2 h-3 sm:h-3.5"} />
                            </td>
                            <td>
                                <Skeleton className={"w-1/2 h-3 sm:h-3.5"} />
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        );
    };

    let content;
    switch (pageState) {
        case PageState.LOADING:
            content = tableBodyLoading();
            break;
        default:
            if (reportList.length === 0) {
                content = emptyReportList();
            } else {
                content = tableBody();
            }
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6 max-w-full"} border={true}>
            <Card.Title>{t("UploadReport.Title")}</Card.Title>
            <Divider />
            <div className="overflow-x-auto max-h-80 w-full">
                <Table size={"xs"} zebra={true} className={"sm:table-md"}>
                    <thead>
                        <tr>
                            <th>{t("UploadReport.TableHeaderDate")}</th>
                            <th className={"hidden sm:table-cell"}>{t("UploadReport.TableHeaderBroker")}</th>
                            <th>{t("UploadReport.TableHeaderStatistic")}</th>
                            <th>{t("UploadReport.TableHeaderStatus")}</th>
                        </tr>
                    </thead>
                    {content}
                </Table>
            </div>
        </Card>
    );
};
