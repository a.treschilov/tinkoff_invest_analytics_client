"use client";

import { BsArrowLeft } from "react-icons/bs";
import { Trans } from "react-i18next";

export default function Error({ error }: { error: Error & { digest?: string }; reset: () => void }) {
    return (
        <div className="text-center mt-4">
            <h1 className="display-1">{error.name}</h1>
            <p className="lead">{error.message}</p>
            <a href={"/"}>
                <BsArrowLeft className="me-1 inline" />
                <span className={"link"}>
                    <Trans>ErrorContent.BackToDashboard</Trans>
                </span>
            </a>
        </div>
    );
}
