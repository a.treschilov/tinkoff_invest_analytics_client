import { useEffect, useState } from "react";
import { ApiService } from "../../../services/ApiService";
import { Item } from "../../../components/Item/Item";
import { PageState } from "../../../components/Common/Types";
import { UserItemDescriptionContent } from "./UserItemDescriptionContent";
import { UserItemDescriptionLoading } from "./UserItemDescriptionLoading";
import { Card } from "../../../components/Common/Card/Card";

type UserItemDescriptionProps = {
    itemId: number;
};

export const UserItemDescriptionManage = ({ itemId }: UserItemDescriptionProps) => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [item, setItem] = useState<Item | null>(null);
    const [isEditEnable, setIsEditEnable] = useState(false);

    useEffect(() => {
        ApiService.fetch("/v1/item/info/" + itemId)
            .then((response: Item) => {
                setItem(response);
                setIsEditEnable(["deposit", "loan", "real_estate"].indexOf(response.type) !== -1);
                setPageState(PageState.SUCCESS);
            })
            .catch(() => {});
    }, [itemId]);

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <UserItemDescriptionContent
                show={pageState === PageState.SUCCESS}
                item={item}
                isEditEnable={isEditEnable}
            />
            <UserItemDescriptionLoading show={pageState === PageState.LOADING} />
        </Card>
    );
};
