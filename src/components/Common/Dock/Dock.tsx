import { FC, HTMLAttributes, ReactNode } from "react";
import { Size } from "../Types";
import { daisyUISizeClass } from "../utils";
import { DockItem } from "./DockItem";

type DockProps = {
    children?: ReactNode;
    size?: Size;
} & HTMLAttributes<HTMLDivElement>;

type DockExtension = {
    Item: typeof DockItem;
};

export const Dock: FC<DockProps> & DockExtension = ({ children, size = "md", className, ...args }: DockProps) => {
    const classes = ["dock"];
    if (className) {
        classes.push(...className?.split(" "));
    }
    classes.push(daisyUISizeClass("dock", size));

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
Dock.Item = DockItem;
