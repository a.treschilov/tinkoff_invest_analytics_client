"use client";

import { ItemType, PageState } from "../../../../../src/components/Common/Types";
import { OperationType } from "../../../../../src/Features/OperationList/OperationTypeTranslation";
import { useEffect, useState } from "react";
import {
    getOperationInitialDateFrom,
    getOperationInitialDateTo,
    OperationFilters,
    OperationFilterValuesType
} from "../../../../../src/Features/OperationList/OperationFilters";
import { ApiService } from "../../../../../src/services/ApiService";
import { ErrorContent } from "../../../../../src/components/ErrorContent/ErrorContent";
import { OperationList } from "../../../../../src/Features/OperationList/OperationList";
import { convertDateToDateString } from "../../../../../src/utils/date";

export interface IOperationView {
    id: number;
    currency: string;
    date: {
        date: string;
        timezone: string;
    };
    itemId: number;
    itemName: string;
    itemType: ItemType | null;
    itemLogo: string | null;
    itemExternalId: string | null;
    broker: string;
    amount: number;
    price: number | null;
    quantity: number | null;
    status: string;
    type: OperationType;
}

interface IPrice {
    amount: number;
    currency: string;
}

const OperationsListPage = () => {
    const [operations, setOperations] = useState<IOperationView[]>([]);
    const [summary, setSummary] = useState<IPrice>({ amount: 0, currency: "RUB" });
    const [filterValues, setFilterValues] = useState<OperationFilterValuesType>({
        operationType: [],
        brokerId: [],
        operationStatus: []
    });
    const [pageState, setPageState] = useState(PageState.LOADING);

    useEffect(() => {
        getOperations(filterValues, getOperationInitialDateFrom(), getOperationInitialDateTo());
    }, [filterValues]);

    const getOperations = (filterValues: OperationFilterValuesType, dateFrom: Date, dateTo: Date) => {
        setPageState(PageState.LOADING);
        const dateToString = convertDateToDateString(dateTo);
        const dateFromString = convertDateToDateString(dateFrom);
        ApiService.fetch("/v1/operations/list", {
            dateTo: dateToString,
            dateFrom: dateFromString,
            operationType: filterValues.operationType,
            operationStatus: filterValues.operationStatus,
            brokerId: filterValues.brokerId,
            timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
        })
            .then(response => {
                setOperations(response.operations);
                setSummary(response.summary);
                setPageState(PageState.EDIT_FORM);
            })
            .catch(() => {
                setPageState(PageState.ERROR);
            });
    };

    const handleSubmitFilters = (filters: OperationFilterValuesType, dateFrom: Date, dateTo: Date) => {
        setFilterValues(filters);
        getOperations(filters, dateFrom, dateTo);
    };

    if (pageState === PageState.ERROR) {
        return <ErrorContent />;
    }

    return (
        <>
            <OperationFilters onFilterApply={handleSubmitFilters} />
            <OperationList
                state={pageState === PageState.LOADING ? "loading" : operations.length === 0 ? "empty" : "success"}
                operations={operations}
                summary={summary}
            />
        </>
    );
};

export default OperationsListPage;
