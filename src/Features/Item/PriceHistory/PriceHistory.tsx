"use client";

import { useEffect, useState } from "react";
import { ApiService } from "../../../services/ApiService";
import { DateType, PageState } from "../../../components/Common/Types";
import { PriceHistoryChart } from "./PriceHistoryChart";
import { PageLoader } from "../../../components/Loader/Loaders";

type PriceHistoryProps = {
    isin: string;
};

export type Candle = {
    open: number;
    close: number;
    date: DateType;
    currency: string;
};

export const PriceHistory = ({ isin }: PriceHistoryProps) => {
    const [candles, setCandles] = useState<Candle[]>([]);
    const [pageState, setPageState] = useState<PageState>(PageState.LOADING);

    useEffect(() => {
        ApiService.fetch("/v1/market/item/price/" + isin).then((response: Candle[]) => {
            setCandles(response);
            setPageState(PageState.SUCCESS);
        });
    }, [isin]);

    let content;
    switch (pageState) {
        case PageState.SUCCESS:
            content = <PriceHistoryChart candles={candles} />;
            break;
        default:
            content = <PageLoader height={"500px"} />;
    }

    return <div className={"h-[500px]"}>{content}</div>;
};
