import { HTMLAttributes, ReactNode } from "react";
import { Divider } from "../Divider/Divider";

type ModalHeaderProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const ModalHeader = ({ children, className, ...args }: ModalHeaderProps) => {
    const classes = ["text-xl"];
    if (className) {
        classes.push(...className?.split(" "));
    }

    return (
        <div className={classes.join(" ")} {...args}>
            {children}
            <Divider />
        </div>
    );
};
