import { Trans } from "react-i18next";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";

type LoanEmptyListProps = {
    show: boolean;
    onAddLoan: () => void;
};

export const LoanEmptyList = ({ show, onAddLoan }: LoanEmptyListProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>LoanManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="text-center my-4">
                <Button size="lg" color={"primary"} onClick={onAddLoan}>
                    <Trans>LoanManage.AddFirstLoanButton</Trans>
                </Button>
            </div>
        </Card>
    );
};
