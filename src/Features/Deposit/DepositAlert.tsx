import { Trans } from "react-i18next";
import { DepositAlertAction } from "./DepositManage";
import { Alert } from "../../components/Common/Alert/Alert";
import { ReactElement } from "react";

interface IDepositAlertProps {
    isShow: boolean;
    action: DepositAlertAction;
    handleOnClose: () => void;
    closeTimeout: number;
}

export const DepositAlert = ({ isShow, action, handleOnClose, closeTimeout }: IDepositAlertProps) => {
    let message: ReactElement;
    switch (action) {
        case "operationAdded":
            message = <Trans>DepositManage.OperationsAdded</Trans>;
            break;
        case "depositEdited":
            message = <Trans>DepositManage.EditComplete</Trans>;
            break;
        case "depositAdded":
            message = <Trans>DepositManage.AddComplete</Trans>;
            break;
        case "depositDeleted":
            message = <Trans>DepositManage.DepositDeleted</Trans>;
            break;
        case "depositClosed":
            message = <Trans>DepositManage.DepositClosed</Trans>;
            break;
        default:
            message = <></>;
    }

    return (
        <Alert
            className="mb-4 text-left"
            show={isShow}
            onClose={handleOnClose}
            status={"success"}
            closeTimeout={closeTimeout}
            dismissible
        >
            {message}
        </Alert>
    );
};
