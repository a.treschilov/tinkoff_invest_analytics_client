import { Trans } from "react-i18next";
import { StepComponentProps } from "./NewUserSteps";
import { Card } from "../../components/Common/Card/Card";
import { Button } from "../../components/Common/Button/Button";
import { Hero } from "../../components/Common/Hero/Hero";

export const NewUserSuccess = ({ active, onComplete }: StepComponentProps) => {
    if (!active) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Hero>
                <Hero.Content className="text-center">
                    <div className="max-w-md">
                        <h1 className="text-4xl font-bold">
                            <Trans>NewUserWizard.SuccessStep.Title</Trans>
                        </h1>
                        <p className="py-6">
                            <Trans>NewUserWizard.SuccessStep.MainMessage</Trans>
                        </p>
                        <Button
                            color="primary"
                            onClick={() => {
                                onComplete();
                            }}
                        >
                            <Trans>NewUserWizard.SuccessStep.StartButton</Trans>
                        </Button>
                    </div>
                </Hero.Content>
            </Hero>
        </Card>
    );
};
