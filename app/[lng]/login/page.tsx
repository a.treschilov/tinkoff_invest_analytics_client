"use client";
import { ReactNode, use, useEffect, useState } from "react";

import { AuthForm } from "../../../src/Features/Auth/AuthForm";
import { useTranslation } from "../../i18n/client";
import { LogoLoader } from "../../../src/components/Loader/LogoLoader";
import { useDidMount } from "../../../src/hooks/useDidMount";
import { tgInit } from "../../../src/services/TgService";
import { useClientOnce } from "../../../src/hooks/useClientOnce";
import { initData, initDataUser, useSignal, User } from "@telegram-apps/sdk-react";
import { PageLoader } from "../../../src/components/Loader/Loaders";
import { useRouter, useSearchParams } from "next/navigation";
import { Theme } from "../../../src/components/Common/Theme/Theme";

const Page = (props: { params: Promise<{ lng: string }> }) => {
    const params = use(props.params);
    const { t } = useTranslation(params.lng, "login");

    const didMount = useDidMount();

    if (!didMount) {
        return <LogoLoader />;
    }
    return (
        <TgLayout lng={params.lng}>
            <Theme theme="dark" className={"h-screen flex flex-col"}>
                <AuthForm t={t} lng={params.lng} />
            </Theme>
        </TgLayout>
    );
};

const TgLayout = ({ lng, children }: { lng: string; children: ReactNode }) => {
    const router = useRouter();
    const [user, setUser] = useState<User | undefined>(undefined);
    const [hash, setHash] = useState<string | undefined>(undefined);
    const [dataRaw, setDataRaw] = useState<string | undefined>(undefined);
    const [authDate, setAuthDate] = useState<Date | undefined>(undefined);

    const searchParams = useSearchParams();
    const authRedirect = searchParams.get("auth_redirect");

    useClientOnce(() => {
        tgInit().then(() => {});
    });

    const initUserData = useSignal(initData.user);
    const initHash = useSignal(initData.hash);
    const initAuthDate = useSignal(initData.authDate);
    const initDataRaw = useSignal(initData.raw);

    useEffect(() => {
        if (initDataUser !== undefined) {
            setUser(initUserData);
        }
    }, [initUserData]);

    useEffect(() => {
        if (initHash !== undefined) {
            setHash(initHash);
        }
    }, [initHash]);

    useEffect(() => {
        if (initAuthDate !== undefined) {
            setAuthDate(initAuthDate);
        }
    }, [initAuthDate]);

    useEffect(() => {
        if (initDataRaw !== undefined) {
            setDataRaw(initDataRaw);
        }
    }, [initDataRaw]);

    if (
        authRedirect != "1" &&
        user?.id !== undefined &&
        hash !== undefined &&
        authDate !== undefined &&
        dataRaw !== undefined
    ) {
        const jsonString = JSON.stringify(dataRaw);

        const param = btoa(jsonString);
        const url = "/" + lng + "/auth/redirect/telegram_web#tgAuthResult=" + param;

        router.push(url);

        return (
            <div className={"m-20"}>
                <PageLoader />
            </div>
        );
    }

    return <>{children}</>;
};

export default Page;
