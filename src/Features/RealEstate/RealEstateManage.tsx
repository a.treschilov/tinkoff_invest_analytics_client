import { useCallback, useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { RealEstateEdit } from "./RealEstateEdit";
import { RealEstateAdd } from "./RealEstateAdd";
import { RealEstateOperationManage } from "./RealEstateOperationManage";
import { ApiService } from "../../services/ApiService";
import { RealEstateList } from "./RealEstateList";
import { PageState } from "../../components/Common/Types";
import { RealEstateActionType } from "./RealEstateListActions";
import { RealEstateListSold } from "./RealEstateListSold";
import { RealEstateListLoading } from "./RealEstateListLoading";
import { useErrorBoundary, withErrorBoundary } from "react-error-boundary";
import { ErrorComponent } from "../../components/ErrorContent/ErrorComponent";
import { Alert } from "../../components/Common/Alert/Alert";
import { RealEstateEmptyList } from "./RealEstateEmptyList";
import { RealEstate } from "../../components/Item/Item";
import { Button } from "../../components/Common/Button/Button";

type RealEstateManageProps = {
    onChange?: () => void;
};

export const RealEstateManage = ({ onChange }: RealEstateManageProps) => {
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [isShowEditForm, setIsShowEditForm] = useState(false);
    const [isShowAddForm, setIsShowAddForm] = useState(false);
    const [isShowOperationForm, setIsShowOperationForm] = useState(false);
    const [isShowAlert, setIsShowAlert] = useState(false);
    const [realEstateList, setRealEstateList] = useState<RealEstate[]>([]);
    const [realEstateListSold, setRealEstateListSold] = useState<RealEstate[]>([]);
    const [editRealEstateId, setEditRealEstateId] = useState(0);
    const [editItemId, setEditItemId] = useState(0);
    const [alertMessage, setAlertMessage] = useState<null | string>(null);
    const { showBoundary } = useErrorBoundary();

    const updateRealEstateList = useCallback(() => {
        setPageState(PageState.LOADING);

        ApiService.fetch("/v1/real_estate/list")
            .then(response => {
                setPageState(PageState.SUCCESS);
                setRealEstateList(
                    response.filter((el: RealEstate) => {
                        return el.isActive;
                    })
                );
                setRealEstateListSold(
                    response.filter((el: RealEstate) => {
                        return !el.isActive;
                    })
                );
            })
            .catch(error => {
                showBoundary(error);
            });
    }, [showBoundary]);

    useEffect(() => {
        updateRealEstateList();
    }, [updateRealEstateList]);

    const handleChange = () => {
        updateRealEstateList();
        if (onChange !== undefined) {
            onChange();
        }
    };

    const handleHideAlert = () => {
        setIsShowAlert(false);
    };

    const handleOpenEditForm = (realEstateId: number) => {
        setIsShowEditForm(true);
        setEditRealEstateId(realEstateId);
    };

    const handleOpenOperationForm = (itemId: number) => {
        setIsShowOperationForm(true);
        setEditItemId(itemId);
    };

    const handleHideOperationForm = () => {
        setIsShowOperationForm(false);
    };

    const handleOpenAddForm = () => {
        setIsShowAddForm(true);
    };

    const handleHideEditForm = () => {
        setIsShowAddForm(false);
        setIsShowEditForm(false);
    };

    const handleActionComplete = (action: RealEstateActionType) => {
        switch (action) {
            case "deleteAsset":
                setAlertMessage("RealEstateManage.DeleteComplete");
                setIsShowAlert(true);
                handleChange();
                break;
            case "addAsset":
                setAlertMessage("RealEstateManage.AlertAddedObject");
                setIsShowAlert(true);
                handleChange();
                break;
            case "sellAsset":
                setAlertMessage("RealEstateManage.SellComplete");
                setIsShowAlert(true);
                handleChange();
                break;
        }
    };

    return (
        <div>
            <div
                className={
                    "text-end mt-4 mb-6" +
                    (realEstateList.length === 0 && realEstateListSold.length === 0 ? " hidden" : "")
                }
            >
                <Button onClick={() => handleOpenAddForm()} color={"primary"}>
                    <Trans>RealEstateManage.AddButtonText</Trans>
                </Button>
            </div>
            <Alert show={isShowAlert} status="success" onClose={handleHideAlert} dismissible>
                <Trans>{alertMessage}</Trans>
            </Alert>
            <RealEstateListLoading show={pageState === PageState.LOADING} />
            <RealEstateEmptyList
                show={pageState !== PageState.LOADING && realEstateList.length === 0 && realEstateListSold.length === 0}
                onAddDeposit={handleOpenAddForm}
            />
            <RealEstateList
                isShow={pageState !== PageState.LOADING && !!realEstateList.length}
                realEstateList={realEstateList}
                handleOpenEditForm={handleOpenEditForm}
                handleOpenOperationForm={handleOpenOperationForm}
                handleActionComplete={handleActionComplete}
            />
            <RealEstateListSold
                show={pageState !== PageState.LOADING && !!realEstateListSold.length}
                list={realEstateListSold}
            />
            <RealEstateAdd
                isShow={isShowAddForm}
                showAlert={() => {
                    handleActionComplete("addAsset");
                }}
                hideModal={handleHideEditForm}
                changeDataHandler={handleChange}
            ></RealEstateAdd>
            <RealEstateEdit
                isShow={isShowEditForm}
                hideModal={handleHideEditForm}
                changeDataHandler={handleChange}
                realEstateId={editRealEstateId}
            ></RealEstateEdit>
            <RealEstateOperationManage
                isShow={isShowOperationForm}
                itemType="real_estate"
                hideModal={handleHideOperationForm}
                itemId={editItemId}
                onChange={handleChange}
            />
        </div>
    );
};

export const RealEstateManageWithBoundaryError = withErrorBoundary(RealEstateManage, {
    FallbackComponent: ErrorComponent
});
