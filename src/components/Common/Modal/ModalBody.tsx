import { HTMLAttributes, ReactNode } from "react";

type ModalBodyProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const ModalBody = ({ children, ...args }: ModalBodyProps) => {
    return <div {...args}>{children}</div>;
};
