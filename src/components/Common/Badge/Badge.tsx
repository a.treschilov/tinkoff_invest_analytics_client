import { HTMLAttributes, ReactNode } from "react";
import { Color, Size } from "../Types";
import { daisyUIColorClass, daisyUISizeClass } from "../utils";

type BadgeProps = {
    children?: ReactNode;
    className?: string;
    size?: Size;
    color?: Color;
} & HTMLAttributes<HTMLScriptElement>;

export const Badge = ({ children, className, size = "md", color, ...args }: BadgeProps) => {
    const classes = ["badge"];
    if (className) {
        classes.push(...className?.split(" "));
    }

    classes.push(daisyUISizeClass("badge", size));
    classes.push(daisyUIColorClass("badge", color));

    return (
        <span className={classes.join(" ")} {...args}>
            {children}
        </span>
    );
};
