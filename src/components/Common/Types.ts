export enum PageState {
    LOADING = "LOADING",
    EDIT_FORM = "EDIT_FORM",
    EDIT_FORM_ERROR = "EDIT_FORM_ERROR",
    ERROR = "ERROR",
    SAVING_DATA = "SAVING_DATA",
    SUCCESS = "SUCCESS",
    EMPTY_CONTENT = "EMPTY"
}

export type ComponentState = "loading" | "empty" | "success";

export enum StrategyTypes {
    STOCK = "STOCK",
    CURRENCY = "CURRENCY",
    CURRENCY_BALANCE = "CURRENCY_BALANCE",
    COUNTRY = "COUNTRY",
    SECTOR = "SECTOR"
}

export type SelectOption = {
    id: string;
    name: string;
};

export type ItemType = "market" | "real_estate" | "loan" | "deposit" | "crowdfunding";

export type PeriodType = "day" | "month" | "year";

export type Price = {
    amount: number;
    currency: string;
};

export type DateType = {
    date: string;
    timezone: string;
    timezone_type: number | null;
};

export type DateTypeGo = {
    Time: string;
    Valid: boolean;
};

export type Color = "neutral" | "primary" | "secondary" | "success" | "accent" | "info" | "warning" | "error";
export type Size = "xs" | "sm" | "md" | "lg" | "xl";
