import { HTMLAttributes, ReactNode } from "react";

type StatValueProps = {
    children: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const StatValue = ({ children, className, ...args }: StatValueProps) => {
    const classN = ["stat-value"];
    if (className) {
        classN.push(...className.split(" "));
    }
    return (
        <div className={classN.join(" ")} {...args}>
            {children}
        </div>
    );
};
