import { Button, Card, Hero } from "react-daisyui";
import { Trans } from "react-i18next";
import { StepComponentProps } from "./NewUserSteps";

export const NewUserSuccess = ({ active, onComplete }: StepComponentProps) => {
    if (!active) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
            <Hero>
                <Hero.Content className="text-center">
                    <div className="max-w-md">
                        <h1 className="text-4xl font-bold">
                            <Trans>NewUserWizard.SuccessStep.Title</Trans>
                        </h1>
                        <p className="py-6">
                            <Trans>NewUserWizard.SuccessStep.MainMessage</Trans>
                        </p>
                        <Button
                            color="primary"
                            onClick={() => {
                                onComplete();
                            }}
                        >
                            <Trans>NewUserWizard.SuccessStep.StartButton</Trans>
                        </Button>
                    </div>
                </Hero.Content>
            </Hero>
        </Card>
    );
};
