import withSerwistInit from "@serwist/next";

const withSerwist = withSerwistInit({
    swSrc: "app/sw.ts",
    swDest: "public/sw.js",
    disable: false
});

export default withSerwist({
    // Your Next.js config
    output: "standalone",
    transpilePackages: ["react-daisyui"],
    reactStrictMode: true,
    eslint: {
        ignoreDuringBuilds: false
    }
});
