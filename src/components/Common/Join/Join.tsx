import { HTMLAttributes, ReactNode } from "react";

type JoinProps = {
    children: ReactNode;
    className?: string;
} & HTMLAttributes<HTMLDivElement>;

export const Join = ({ children, className, ...args }: JoinProps) => {
    const classes = ["join"];
    if (className) {
        classes.push(...className.split(" "));
    }
    return (
        <div className={classes.join(" ")} {...args}>
            {children}
        </div>
    );
};
