import { Popover } from "../../components/Common/Popover/Popover";
import { Trans } from "react-i18next";
import { BsQuestionCircle } from "react-icons/bs";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import Link from "next/link";
import { makeLink } from "../../utils/url";

interface IDashboardProps {
    wealthRate: number | null;
}

export const DashboardPopover = ({ wealthRate }: IDashboardProps) => {
    const { lng } = useContext(ThemeContext);
    const popover = (
        <div>
            <div>
                <Trans>Dashboard.PopoverTitle</Trans>
            </div>
            <div>
                <p>
                    <Trans>Dashboard.PopoverDescription</Trans>,&nbsp;
                    <Link href={makeLink("/user/deposit", lng)}>
                        <Trans>Dashboard.PopoverDescriptionDeposit</Trans>
                    </Link>
                    ,&nbsp;
                    <Link href={makeLink("/user/realEstate", lng)}>
                        <Trans>Dashboard.PopoverDescriptionRealEstate</Trans>
                    </Link>
                    ,&nbsp;
                    <Link href={makeLink("/user/loan", lng)}>
                        <Trans>Dashboard.PopoverDescriptionLoan</Trans>
                    </Link>
                    , <Trans>Dashboard.PopoverDescriptionAnother</Trans> <Trans>Dashboard.PopoverDescriptionAnd</Trans>
                    &nbsp;
                    <Link href={makeLink("/user/strategy/portfolio", lng)}>
                        <Trans>Dashboard.PopoverDescriptionExpenses</Trans>
                    </Link>
                    .<br />
                    <br />
                    <Trans>Dashboard.PopoverFillData</Trans>
                </p>
                <span className={"p-1" + (wealthRate !== null && wealthRate < 0 ? " bg-success" : "")}>
                    &lt; 0 - <Trans>Dashboard.PopoverDebtHole</Trans>
                </span>
                <br />
                <span
                    className={
                        "p-1" + (wealthRate !== null && wealthRate >= 0 && wealthRate < 0.25 ? " bg-success" : "")
                    }
                >
                    &gt; 0 - <Trans>Dashboard.PopoverPrecariousBalance</Trans>
                </span>
                <br />
                <span
                    className={
                        "p-1" + (wealthRate !== null && wealthRate >= 0.25 && wealthRate < 1 ? " bg-success" : "")
                    }
                >
                    &gt; 0.25 - <Trans>Dashboard.PopoverFinancialAirbag</Trans>
                </span>
                <br />
                <span
                    className={"p-1" + (wealthRate !== null && wealthRate >= 1 && wealthRate < 5 ? " bg-success" : "")}
                >
                    &gt; 1 - <Trans>Dashboard.PopoverRunway</Trans>
                </span>
                <br />
                <span
                    className={"p-1" + (wealthRate !== null && wealthRate >= 5 && wealthRate < 15 ? " bg-success" : "")}
                >
                    &gt; 5 - <Trans>Dashboard.PopoverClimb</Trans>
                </span>
                <br />
                <span
                    className={
                        "p-1" + (wealthRate !== null && wealthRate >= 15 && wealthRate < 30 ? " bg-success" : "")
                    }
                >
                    &gt; 15 - <Trans>Dashboard.PopoverFinancialStability</Trans>
                </span>
                <br />
                <span className={"p-1" + (wealthRate !== null && wealthRate >= 30 ? " bg-success" : "")}>
                    &gt; 30 - <Trans>Dashboard.PopoverFinancialIndependence</Trans>
                </span>
                <p className="text-end mb-0 mt-2">
                    <a href="https://hakkes.com/blog/wealthRate" target="_blank">
                        <Trans>Dashboard.PopoverLearnMore</Trans>
                    </a>
                </p>
            </div>
        </div>
    );

    return (
        <Popover content={popover}>
            <BsQuestionCircle style={{ width: "16px", cursor: "pointer" }} />
        </Popover>
    );
};
