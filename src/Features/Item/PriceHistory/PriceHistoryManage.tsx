"use client";

import { Item } from "../../../components/Item/Item";
import { UserDescriptionContentName } from "../UserItemDescription/UserDescriptionContentName";
import { PriceHistory } from "./PriceHistory";
import { useTranslation } from "../../../../app/i18n/client";
import { Card } from "../../../components/Common/Card/Card";

type PriceHistoryManageProps = {
    item: Item;
    lng: string;
};
export const PriceHistoryManage = ({ item, lng }: PriceHistoryManageProps) => {
    const { t } = useTranslation(lng, "public/translation");
    return (
        <>
            <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                <div className={"grid grid-cols-1 gap-2"}>
                    <div className={"flex gap-2 text-lg"}>
                        <UserDescriptionContentName item={item} />
                    </div>
                </div>
            </Card>
            <Card className={"bg-base-100 p-6 mb-6"} border={true}>
                <Card.Title>{t("Item.PriceDynamicHeader")}</Card.Title>
                <PriceHistory isin={item.externalId} />
            </Card>
        </>
    );
};
