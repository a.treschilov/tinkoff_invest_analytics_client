import { BondType } from "./BondListManage";
import Link from "next/link";
import { makeLink } from "../../utils/url";
import { toUserTimezone } from "../../utils/date";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { SessionContext } from "../../providers/SessionProvider";

const browserLocale = navigator.languages != undefined ? navigator.languages[0] : "en-US";
const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat(browserLocale, {
    year: "numeric",
    month: "short",
    day: "numeric"
});

type BonsListProps = {
    bonds: BondType[];
};

export const BondList = ({ bonds }: BonsListProps) => {
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    return (
        <>
            {bonds.map(bond => {
                const nominalCurrencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                    style: "currency",
                    currency: bond.nominal.currency
                });
                const initialNominalCurrencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                    style: "currency",
                    currency: bond.nominal.currency
                });

                return (
                    <tr key={"bond-item-" + bond.id}>
                        <td className={"hidden sm:table-cell"}>
                            <Link
                                onClick={() => updatePreviousPage("bondList")}
                                href={makeLink("/user/item/" + bond.itemId, lng)}
                                className={"link"}
                            >
                                {bond.name}
                            </Link>
                        </td>
                        <td className={"sm:hidden"}>
                            <Link
                                onClick={() => updatePreviousPage("bondList")}
                                href={makeLink("/user/item/" + bond.itemId, lng)}
                                className={"link"}
                            >
                                {bond.name}
                            </Link>
                            <br />
                            {nominalCurrencyFormatter.format(bond.nominal.amount)}
                            <br />
                            {bond.maturityDate?.date !== undefined ? (
                                dateFormatter.format(toUserTimezone(bond.maturityDate))
                            ) : (
                                <></>
                            )}
                        </td>
                        <td className={"hidden sm:table-cell"}>
                            {nominalCurrencyFormatter.format(bond.nominal.amount)}
                        </td>
                        <td className={"hidden lg:table-cell"}>
                            {initialNominalCurrencyFormatter.format(bond.nominal.amount)}
                        </td>
                        <td className={"hidden sm:table-cell"}>
                            {bond.maturityDate?.date !== undefined ? (
                                dateFormatter.format(toUserTimezone(bond.maturityDate))
                            ) : (
                                <></>
                            )}
                        </td>
                    </tr>
                );
            })}
        </>
    );
};
