"use client";

import { PageState } from "../../../components/Common/Types";
import { useEffect, useState } from "react";
import { IWealth } from "../../../../app/[lng]/user/analytics/page";

import dynamic from "next/dynamic";
import { ApexOptions } from "apexcharts";
import { PageLoader } from "../../../components/Loader/Loaders";
import { defaultChartOptions } from "../ChatOptions";
import { mergeDeep } from "../../../utils/objects";
const Chart = dynamic(() => import("react-apexcharts"), {
    ssr: false
});

const compactCurrencyFormatter = Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB",
    notation: "compact"
});

const currencyFormatter = Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB"
});

const numberFormatter = Intl.NumberFormat("ru-RU", {
    maximumFractionDigits: 2,
    minimumFractionDigits: 2
});

interface AnalyticsWealthDynamicChartProps {
    wealthHistory: IWealth[];
    dataState: string;
}

const maxPoints = 200;

export const AnalyticsWealthDynamicChart = ({ wealthHistory, dataState }: AnalyticsWealthDynamicChartProps) => {
    const [chartData, setChartData] = useState<ApexAxisChartSeries>();
    const [chartOptions, setChartOptions] = useState<ApexOptions>({});

    useEffect(() => {
        const dataWealthRate: number[][] = [];
        const dataActives: number[][] = [];
        const dataPassives: number[][] = [];
        let maxValue = 0;
        const ratePoints = Math.ceil(wealthHistory.length / maxPoints);

        wealthHistory.forEach((wealth, ind) => {
            if (ind % ratePoints === 0) {
                const actives = [
                    new Date(wealth.date.date).getTime(),
                    wealth.stock + wealth.realEstate + wealth.assets + wealth.deposit
                ];
                const passives = [
                    new Date(wealth.date.date).getTime(),
                    wealth.loan !== 0 ? -1 * wealth.loan : wealth.loan
                ];
                maxValue = Math.max(
                    wealth.stock + wealth.realEstate + wealth.assets + wealth.deposit,
                    -1 * wealth.loan
                );

                dataActives.push(actives);
                dataPassives.push(passives);

                if (wealth.wealthRate !== null) {
                    dataWealthRate.push([new Date(wealth.date.date).getTime(), wealth.wealthRate]);
                }
            }
        });

        const series: ApexAxisChartSeries = [
            {
                type: "area",
                data: dataWealthRate,
                name: "Уровень богатства",
                zIndex: 10
            },
            {
                type: "line",
                data: dataActives,
                name: "Активы",
                zIndex: 1
            },
            {
                type: "line",
                data: dataPassives,
                name: "Пассивы",
                zIndex: 0
            }
        ];

        const options = mergeDeep(defaultChartOptions, {
            fill: {
                opacity: [0.2, 1, 1, 1, 1]
            },
            xaxis: {
                type: "datetime",
                min: new Date("01 Mar 2019").getTime()
            },
            yaxis: [
                {
                    opposite: true,
                    decimalsInFloat: 0,
                    max: 30
                },
                {
                    opposite: false,
                    labels: {
                        formatter: (value: number) => {
                            return compactCurrencyFormatter.format(value);
                        }
                    },
                    max: maxValue * 1.1,
                    forceNiceScale: true
                },
                {
                    max: maxValue * 1.1,
                    show: false,
                    labels: {
                        formatter: (value: number) => {
                            return compactCurrencyFormatter.format(value);
                        }
                    }
                }
            ],
            tooltip: {
                y: {
                    formatter(value: number, { seriesIndex }: { seriesIndex: number }): string {
                        if ([1, 2].indexOf(seriesIndex) !== -1) {
                            return currencyFormatter.format(value);
                        }
                        return numberFormatter.format(value);
                    }
                }
            }
        });

        setChartData(series);
        setChartOptions(options);
    }, [dataState, wealthHistory]);

    const renderChart = () => {
        return (
            <Chart options={chartOptions} type="area" series={chartData} width="100%" height={500} group={"dynamic"} />
        );
    };

    let content;
    switch (dataState) {
        case PageState.LOADING:
            content = <PageLoader />;
            break;
        case PageState.EDIT_FORM:
            content = renderChart();
            break;
        default:
            content = <></>;
    }

    return content;
};
