import { ReactNode } from "react";
import { FooterComponent } from "../../../src/components/Footer/FooterComponent";
import { PublicHeader } from "../../../src/components/Header/PublicHeader";
import { ThemeProvider } from "../../../src/providers/ThemeProvider";

const Layout = async (props: { children: ReactNode; params: Promise<{ lng: string }> }) => {
    const params = await props.params;

    const { children } = props;

    return (
        <ThemeProvider lng={params.lng}>
            <div className={"pt-6 px-10"}>
                <PublicHeader />
                <main className={"flex-1 overflow-y-auto overflow-x-auto p-2 sm:p-6 bg-base-200"}>{children}</main>
            </div>
            <FooterComponent lng={params.lng} />
        </ThemeProvider>
    );
};

export default Layout;
