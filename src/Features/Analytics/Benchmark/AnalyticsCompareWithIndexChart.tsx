import dynamic from "next/dynamic";
import { ApexOptions } from "apexcharts";
import { useContext, useEffect, useState } from "react";
import { LineChartSeriesDataType } from "./AnalyticsCompareWithIndex";
import { defaultChartOptions } from "../ChatOptions";
import { mergeDeep } from "../../../utils/objects";
import { ThemeContext } from "../../../providers/ThemeProvider";
const Chart = dynamic(() => import("react-apexcharts"), {
    ssr: false
});

const AnalyticsCompareWithIndexTranslations = (lng: string) => {
    switch (lng) {
        case "ru":
            return {
                DataNotFound: "Данные за период не найдены"
            };
        default:
            return {
                DataNotFound: "Data not found"
            };
    }
};

type AnalyticsCompareWithIndexChartProps = {
    data: LineChartSeriesDataType[];
    show: boolean;
};

export const AnalyticsCompareWithIndexChart = ({ data, show }: AnalyticsCompareWithIndexChartProps) => {
    const [series, setSeries] = useState<ApexAxisChartSeries>([]);
    const [options, setOptions] = useState<ApexOptions>({});

    const { lng } = useContext(ThemeContext);
    const translations = AnalyticsCompareWithIndexTranslations(lng);

    useEffect(() => {
        const seriesData: ApexAxisChartSeries = [];
        data.map((item, index) => {
            const type = index === 0 ? "area" : "line";
            seriesData.push({
                type: type,
                data: item.data,
                name: item.name
            });
        });

        const optionsData: ApexOptions = mergeDeep(defaultChartOptions, {
            fill: {
                opacity: [0.2, 1, 1, 1, 1]
            },
            noData: {
                text: translations.DataNotFound
            }
        });

        setSeries(seriesData);
        setOptions(optionsData);
    }, [data, translations.DataNotFound]);

    if (!show) {
        return <></>;
    }

    return <Chart options={options} type="line" series={series} width="100%" height={500} group={"dynamic"} />;
};
