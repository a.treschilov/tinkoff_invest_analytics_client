import { Trans } from "react-i18next";
import { ICredentialPortfolioItem } from "./CredentialList";
import { CredentialListEmpty } from "./CredentialListEmpty";
import { BsFillPencilFill, BsListUl } from "react-icons/bs";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

interface ICredentialListTableProps {
    openUserAccounts: (userCredentialId: number) => void;
    openUserCredential: (userCredentialId: number) => void;
    onAddCredential: () => void;
    show: boolean;
    credentials: ICredentialPortfolioItem[];
}

export const CredentialListTable = ({
    openUserAccounts,
    openUserCredential,
    onAddCredential,
    show,
    credentials
}: ICredentialListTableProps) => {
    const renderCredentials = (credentials: ICredentialPortfolioItem[]) => {
        return credentials.map(credential => {
            return (
                <tr key={"user_credential_" + credential.userCredentialId}>
                    <td className="hidden lg:table-cell">{credential.brokerName}</td>
                    <td>
                        <a
                            href="#"
                            onClick={e => {
                                openUserAccounts(credential.userCredentialId);
                                e.preventDefault();
                            }}
                            className={"link"}
                        >
                            {credential.activeAccountNumber + "/" + credential.accountNumber}
                        </a>
                    </td>
                    <td>
                        <Trans>
                            CredentialList.
                            {credential.isActive ? "StatusActive" : "StatusDisabled"}
                        </Trans>
                    </td>
                    <td className="text-end">
                        <BsFillPencilFill
                            className="me-4 inline-block"
                            style={{ cursor: "pointer" }}
                            data-bs-toggle="modal"
                            data-bs-target="#userAccountEditModal"
                            onClick={() => openUserCredential(credential.userCredentialId)}
                        />
                        <BsListUl
                            className="me-2 inline-block"
                            style={{ cursor: "pointer" }}
                            data-bs-toggle="modal"
                            data-bs-target="#userCredentialEditModal"
                            onClick={() => openUserAccounts(credential.userCredentialId)}
                        />
                    </td>
                </tr>
            );
        });
    };

    const renderTable = () => {
        if (credentials.length === 0) {
            return <CredentialListEmpty onAddCredential={onAddCredential} />;
        }
        return (
            <table className="table w-full">
                <thead>
                    <tr>
                        <th className="hidden lg:table-cell">
                            <Trans>CredentialList.TableBroker</Trans>
                        </th>
                        <th>
                            <Trans>CredentialList.TableAccount</Trans>
                        </th>
                        <th>
                            <Trans>CredentialList.TableStatus</Trans>
                        </th>
                        <th />
                    </tr>
                </thead>
                <tbody>{renderCredentials(credentials)}</tbody>
            </table>
        );
    };

    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>CredentialList.PageHeader</Trans>
            </Card.Title>
            <Divider />
            {renderTable()}
        </Card>
    );
};
