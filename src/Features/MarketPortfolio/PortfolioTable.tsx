import { ReactElement } from "react";
import { Trans } from "react-i18next";
import { BsTable } from "react-icons/bs";
import { ComponentState } from "../../components/Common/Types";
import { IShare } from "../../../app/[lng]/user/market/portfolio/page";
import { Card } from "../../components/Common/Card/Card";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Divider } from "../../components/Common/Divider/Divider";

interface IPortfolioTableProps {
    name?: ReactElement;
    data: IShare[] | null;
    strategyLink?: string;
    state?: ComponentState;
}

const currencyFormatter = new Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB"
});

const percentFormatter = new Intl.NumberFormat("ru-RU", {
    style: "percent",
    maximumFractionDigits: 2
});

export const PortfolioTable = ({ name, data, strategyLink, state }: IPortfolioTableProps) => {
    const renderStrategyLink = (href: string) => {
        return (
            <a href={href} target="_blank" className={"link link-primary"} rel="noreferrer">
                <Trans>PortfolioTable.Strategy</Trans>
            </a>
        );
    };

    const renderLoading = () => {
        return <Skeleton className={"h-[350px] w-full"} />;
    };

    const renderData = () => {
        return (
            <table className="table w-full">
                <thead>
                    <tr>
                        <th scope="col">
                            <Trans>PortfolioTable.TableHeaderType</Trans>
                        </th>
                        <th scope="col" className="hidden sm:table-cell">
                            <Trans>PortfolioTable.TableHeaderInPortfolio</Trans>
                        </th>
                        <th scope="col" className="hidden sm:table-cell">
                            <Trans>PortfolioTable.TableHeaderStrategy</Trans>
                        </th>
                        <th scope="col">
                            <Trans>PortfolioTable.TableHeaderDiff</Trans>
                        </th>
                        <th scope="col">
                            <Trans>PortfolioTable.TableHeaderDiff</Trans>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {data !== null ? (
                        data.map((share: IShare) => {
                            return (
                                <tr key={share.name} className={share.diffPercent < 0 ? "table-primary" : ""}>
                                    <td>{share.name}</td>
                                    <td className="hidden sm:table-cell">{percentFormatter.format(share.share)}</td>
                                    <td className="hidden sm:table-cell">
                                        {percentFormatter.format(share.targetShare)}
                                    </td>
                                    <td>{percentFormatter.format(share.diffPercent)}</td>
                                    <td>{currencyFormatter.format(share.diffAmount)}</td>
                                </tr>
                            );
                        })
                    ) : (
                        <tr>
                            <td className="d-none">
                                <Trans>PortfolioTable.DataNotFound</Trans>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    };

    let content;
    switch (state) {
        case "loading":
            content = renderLoading();
            break;
        default:
            content = renderData();
    }

    return (
        <Card className={"bg-base-100 p-6"} border={true}>
            <Card.Title>
                <BsTable size={16} />
                &nbsp;
                {name} {strategyLink ? renderStrategyLink(strategyLink) : ""}
            </Card.Title>
            <Divider />
            {content}
        </Card>
    );
};
