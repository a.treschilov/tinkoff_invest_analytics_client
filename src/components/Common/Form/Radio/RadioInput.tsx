import { Color, Size } from "../../Types";
import { DetailedHTMLProps, InputHTMLAttributes } from "react";
import { daisyUIColorClass, daisyUISizeClass } from "../../utils";

type RadioInputProps = {
    color?: Color;
    size?: Size;
} & DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

export const RadioInput = ({ color, size, className, ...args }: RadioInputProps) => {
    const classes = ["radio"];
    if (className) {
        classes.push(...className.split(" "));
    }
    classes.push(daisyUIColorClass("radio", color));
    classes.push(daisyUISizeClass("radio", size));

    return <input type={"radio"} className={classes.join(" ")} {...args}></input>;
};
