import { FC, HTMLAttributes, ReactNode } from "react";
import { CollapseTitle } from "./CollapseTitle";
import { CollapseContent } from "./CollapseContent";

type CollapseProps = {
    children: ReactNode;
    icon?: "arrow" | "plus";
    open?: boolean;
    checkbox?: boolean;
} & HTMLAttributes<HTMLDivElement>;

type CollapseExtension = {
    Title: typeof CollapseTitle;
    Content: typeof CollapseContent;
};

export const Collapse: FC<CollapseProps> & CollapseExtension = ({
    children,
    className,
    icon,
    open = false,
    checkbox = false,
    ...args
}: CollapseProps) => {
    const classes = ["collapse"];
    if (className) {
        classes.push(...className?.split(" "));
    }

    switch (icon) {
        case "arrow":
            classes.push("collapse-arrow");
            break;
        case "plus":
            classes.push("collapse-plus");
            break;
    }
    if (open) {
        classes.push("collapse-open");
    }

    return (
        <div tabIndex={0} className={classes.join(" ")} {...args}>
            {checkbox ? <input type="checkbox" /> : <></>}
            {children}
        </div>
    );
};

Collapse.Title = CollapseTitle;
Collapse.Content = CollapseContent;
