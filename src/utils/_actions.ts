"use server";

import { redirect } from "next/navigation";
import { cookies } from "next/headers";

export async function clientRedirect(url: string) {
    redirect(url);
}

export async function getCookie(name: string) {
    return (await cookies()).get(name)?.value;
}

export async function deleteCookie(name: string) {
    return (await cookies()).delete(name);
}
