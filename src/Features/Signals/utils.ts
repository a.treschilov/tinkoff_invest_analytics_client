import { VolumeSignal } from "./VolumeSignalList";
import { DateTypeGo } from "../../components/Common/Types";

type VolumeSignalInstrumentResponseType = {
    item_id: number;
    name: string;
    isin: string;
    ticker: string;
};

export type VolumeSignalApiResponseType = {
    market_instrument: VolumeSignalInstrumentResponseType;
    start_date: DateTypeGo;
    end_date: DateTypeGo;
};

export const convertVolumeResponseToSignal = (response: VolumeSignalApiResponseType[]): VolumeSignal[] => {
    const formattedSignals: VolumeSignal[] = [];
    response.forEach(el => {
        formattedSignals.push({
            itemId: el.market_instrument.item_id,
            isin: el.market_instrument.isin,
            name: el.market_instrument.name,
            ticker: el.market_instrument.ticker,
            startDate: el.start_date !== null ? new Date(el.start_date.Time) : null,
            endDate: el.end_date !== null ? new Date(el.end_date.Time) : null
        });
    });

    return formattedSignals;
};
