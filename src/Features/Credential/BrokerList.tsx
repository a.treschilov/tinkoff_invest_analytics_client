import { BrokerType } from "./CredentialFormBody";
import { PageState } from "../../components/Common/Types";
import { ItemLogoName } from "../../components/Item/ItemLogoName";
import { ItemLogoNameLoading } from "../../components/Item/ItemLogoNameLoading";
import { Kbd } from "../../components/Common/Kbd/Kbd";

const BrokerLoadingItemCount = 4;

type BrokerListType = {
    onItemClick: (itemId: string) => void;
    brokerList: BrokerType[];
    currentBrokerId: string | null;
    pageState: PageState;
};

export const BrokerList = ({ onItemClick, brokerList, currentBrokerId, pageState }: BrokerListType) => {
    if (pageState === PageState.LOADING) {
        return (
            <div
                key={"broker-loading-list"}
                className={"grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-x-1 gap-y-1"}
            >
                {[...Array(BrokerLoadingItemCount)].map((value: number, key: number) => {
                    return (
                        <div className={"p-3"} key={"broker-loading-item" + key}>
                            <ItemLogoNameLoading />
                        </div>
                    );
                })}
            </div>
        );
    }

    return (
        <div className={"grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-1"}>
            {brokerList.map(broker => {
                if (!broker.isManual || ["jetlend", "money_friends", "potok"].indexOf(broker.id) !== -1) {
                    return <></>;
                }
                return (
                    <div
                        key={"brokerItem-" + broker.id}
                        className={
                            (broker.id === currentBrokerId ? "bg-accent-content " : "") +
                            "cursor-pointer flex p-3 hover:bg-accent-content"
                        }
                        onClick={() => {
                            onItemClick(broker.id);
                        }}
                    >
                        <ItemLogoName name={broker.name} type={"market"} logo={broker.logo} />
                        {broker.isApi ? (
                            <Kbd size={"xs"} className={"py-1 px-2 ml-1"}>
                                API
                            </Kbd>
                        ) : (
                            <></>
                        )}
                        <span className={"pl-1 align-super text-xs"}>&beta;</span>
                    </div>
                );
            })}
        </div>
    );
};
