import { HTMLAttributes, ReactNode } from "react";

type MenuItemProps = {
    children?: ReactNode;
} & HTMLAttributes<HTMLLIElement>;

export const MenuItem = ({ children, ...args }: MenuItemProps) => {
    return <li {...args}>{children}</li>;
};
