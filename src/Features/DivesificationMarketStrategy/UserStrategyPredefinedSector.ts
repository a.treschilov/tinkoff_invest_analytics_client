export const UserStrategyPredefinedSector = [
    {
        id: "snp500",
        name: "UserStrategySettings.Strategies.SectorSnPName",
        description: "UserStrategySettings.Strategies.SectorSnPDescription",
        strategy: [
            {
                value: 0.02,
                id: "basic_materials"
            },
            {
                value: 0.12,
                id: "communication_services"
            },
            {
                value: 0.13,
                id: "consumer_cyclical"
            },
            {
                value: 0.06,
                id: "consumer_defensive"
            },
            {
                value: 0.02,
                id: "energy"
            },
            {
                value: 0.13,
                id: "financial_services"
            },
            {
                value: 0.15,
                id: "healthcare"
            },
            {
                value: 0.07,
                id: "industrials"
            },
            {
                value: 0.03,
                id: "real_estate"
            },
            {
                value: 0.24,
                id: "technology"
            },
            {
                value: 0.03,
                id: "utilities"
            }
        ]
    }
];
