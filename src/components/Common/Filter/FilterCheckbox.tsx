import { Trans } from "react-i18next";
import { SelectOption } from "../Types";
import { Checkbox } from "../Form/Checkbox";

type Props = {
    name: string;
    options: SelectOption[];
    selected: string[];
    onChange: (label: string) => void;
};

export const FilterCheckbox = (props: Props) => {
    const { name, options, onChange, selected } = props;

    const createCheckbox = (option: SelectOption, selected: string[]) => {
        return (
            <Checkbox
                key={option.id}
                id={option.id}
                name={option.name}
                checked={selected.indexOf(option.id) != -1}
                handleCheckboxChange={onChange}
            />
        );
    };

    return (
        <div className="mt-2.5">
            <div className={"mb-1"}>
                <b>
                    <Trans>{name}</Trans>
                </b>
            </div>
            {options.map(option => createCheckbox(option, selected))}
        </div>
    );
};
