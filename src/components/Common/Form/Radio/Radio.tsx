import { RadioLabel } from "./RadioLabel";
import { FC, InputHTMLAttributes, ReactNode } from "react";
import { RadioInput } from "./RadioInput";

type RadioProps = {
    children: ReactNode;
} & InputHTMLAttributes<HTMLLabelElement>;

type RadioExtensions = {
    Label: typeof RadioLabel;
    Input: typeof RadioInput;
};

export const Radio: FC<RadioProps> & RadioExtensions = ({ children, className, ...args }: RadioProps) => {
    const classes = ["label"];
    if (className) {
        classes.push(...className.split(" "));
    }

    return (
        <label className={classes.join(" ")} {...args}>
            {children}
        </label>
    );
};

Radio.Label = RadioLabel;
Radio.Input = RadioInput;
