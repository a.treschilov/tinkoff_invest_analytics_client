import { PortfolioItem } from "./UserPortfolio";
import { Price } from "../../components/Common/Types";
import { ItemLogoName } from "../../components/Item/ItemLogoName";
import { useContext, useMemo } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import Link from "next/link";
import { EmptyContent } from "../../components/Common/EmptyContent/EmptyContent";
import {
    ColumnDef,
    getCoreRowModel,
    getPaginationRowModel,
    getSortedRowModel,
    useReactTable
} from "@tanstack/react-table";
import { Table } from "../../components/Table/Table";
import { useTranslation } from "../../../app/i18n/client";

type Props = {
    items: PortfolioItem[];
};

const numberFormatter = new Intl.NumberFormat("ru-RU");
const percentFormatter = new Intl.NumberFormat("ru-RU", {
    style: "percent",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

export const UserPortfolioStockStock = ({ items }: Props) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userPortfolio");

    const columns = useMemo<ColumnDef<PortfolioItem>[]>(
        () => [
            {
                accessorKey: "name",
                header: () => t("Stock.TableHeaderName"),
                cell: info => (
                    <Link href={makeLink("/user/item/" + info.row.original.itemId, lng)} className={"link"}>
                        <ItemLogoName
                            name={info.row.original.item.name}
                            type={info.row.original.item.type}
                            logo={info.row.original.item.logo}
                        />
                    </Link>
                ),
                sortingFn: (rowA, rowB) => {
                    const nameA = rowA.original.item.name;
                    const nameB = rowB.original.item.name;
                    return nameA > nameB ? 1 : -1;
                },
                sortDescFirst: false
            },
            {
                accessorKey: "quantity",
                header: () => <span className={"hidden sm:inline"}>{t("Stock.TableHeaderQuantity")}</span>,
                cell: info => (
                    <span className={"hidden sm:inline"}>{numberFormatter.format(info.row.original.quantity)}</span>
                )
            },
            {
                accessorKey: "portfolio",
                header: () => <span className={"sm:hidden"}>{t("Stock.TableHeaderPortfolio")}</span>,
                cell: info => (
                    <span className={"sm:hidden"}>
                        {numberFormatter.format(info.row.original.quantity)} {t("Stock.Pcs")}
                        <br />
                        {renderMultiCurrency(
                            { amount: info.row.original.amount, currency: info.row.original.currencyIso },
                            info.row.original.baseAmount
                        )}
                        <br />
                        {percentFormatter.format(info.row.original.itemTypeShare)}
                    </span>
                ),
                sortingFn: (rowA, rowB) => {
                    return rowA.original.itemTypeShare - rowB.original.itemTypeShare;
                }
            },
            {
                accessorKey: "price",
                header: () => <span className={"hidden lg:inline"}>{t("Stock.TableHeaderPrice")}</span>,
                cell: info => (
                    <span className={"hidden lg:inline"}>
                        {info.row.original.price === null
                            ? ""
                            : new Intl.NumberFormat("ru-RU", {
                                  style: "currency",
                                  currency: info.row.original.currencyIso
                              }).format(info.row.original.price.amount)}
                    </span>
                ),
                enableSorting: false
            },
            {
                accessorKey: "amount",
                header: () => <span className={"hidden sm:inline"}>{t("Stock.TableHeaderAmount")}</span>,
                cell: info => (
                    <span className={"hidden sm:inline"}>
                        {renderMultiCurrency(
                            {
                                amount: info.row.original.amount,
                                currency: info.row.original.currencyIso
                            },
                            info.row.original.baseAmount
                        )}
                    </span>
                ),
                sortingFn: (rowA, rowB) => {
                    return rowA.original.baseAmount.amount - rowB.original.baseAmount.amount;
                }
            },
            {
                accessorKey: "share",
                header: () => <span className={"hidden sm:inline"}>{t("Stock.TableHeaderShare")}</span>,
                cell: info => (
                    <span className={"hidden sm:inline"}>
                        {percentFormatter.format(info.row.original.itemTypeShare)}
                    </span>
                ),
                sortingFn: (rowA, rowB) => {
                    return rowA.original.itemTypeShare - rowB.original.itemTypeShare;
                }
            }
        ],
        [lng, t]
    );

    const table = useReactTable({
        columns: columns,
        data: items,
        getCoreRowModel: getCoreRowModel(),
        getSortedRowModel: getSortedRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        autoResetPageIndex: false,
        initialState: {
            sorting: [
                {
                    id: "share",
                    desc: true
                }
            ],
            pagination: {
                pageIndex: 0,
                pageSize: 1000
            }
        }
    });

    const renderMultiCurrency = (primary: Price, secondary: Price) => {
        const primaryCurrencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
            style: "currency",
            currency: primary.currency
        });
        const secondaryCurrencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
            style: "currency",
            currency: secondary.currency
        });

        if (primary.currency === secondary.currency) {
            return <>{primaryCurrencyFormatter.format(primary.amount)}</>;
        }

        return (
            <>
                {primaryCurrencyFormatter.format(primary.amount)}
                <br />
                <small className="text-primary">{secondaryCurrencyFormatter.format(secondary.amount)}</small>
            </>
        );
    };

    if (items.length === 0) {
        return <EmptyContent size={"sm"}>{t("Stock.EmptyList")}</EmptyContent>;
    } else {
        return <Table table={table} />;
    }
};
