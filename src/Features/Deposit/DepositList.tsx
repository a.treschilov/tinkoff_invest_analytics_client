import { useContext, useState } from "react";
import { IDepositPortfolioItem } from "./DepositManage";
import { Trans } from "react-i18next";
import { toUserTimezone } from "../../utils/date";
import { DepositListActions } from "./DepositListActions";
import { ItemName } from "../../components/Item/ItemName";
import Link from "next/link";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import { SessionContext } from "../../providers/SessionProvider";
import { ItemDelete } from "../Item/Delete/ItemDelete";
import { ItemClose } from "../Item/Close/ItemClose";
import { Divider } from "../../components/Common/Divider/Divider";
import { Card } from "../../components/Common/Card/Card";

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
    year: "numeric",
    month: "numeric",
    day: "numeric"
});

interface IDepositListProps {
    show: boolean;
    depositList: IDepositPortfolioItem[];
    depositLoadStatus: string;
    handleEditDeposit: (depositId: number) => void;
    handleOperationManage: (depositId: number) => void;
    handleDeleteDeposit: () => void;
    handleCloseDeposit: () => void;
}

export const DepositList = ({
    show,
    depositList,
    handleEditDeposit,
    handleOperationManage,
    handleDeleteDeposit,
    handleCloseDeposit
}: IDepositListProps) => {
    const [isDeleteWarningShow, setIsDeleteWarningShow] = useState(false);
    const [isCloseWarningShow, setIsCloseWarningShow] = useState(false);
    const [currentDepositId, setCurrentDepositId] = useState(0);
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    const renderDeleteConfirmation = () => {
        const currentDeposit = depositList.find(el => {
            return el.deposit.depositId === currentDepositId;
        });

        if (currentDeposit === undefined) {
            return;
        }

        return (
            <ItemDelete
                itemId={currentDeposit?.deposit.itemId || 0}
                show={isDeleteWarningShow}
                onSuccess={() => {
                    setIsDeleteWarningShow(false);
                    handleDeleteDeposit();
                }}
                onDiscard={() => {
                    setIsDeleteWarningShow(false);
                }}
            />
        );
    };

    const renderCloseConfirmation = () => {
        const currentDeposit = depositList.find(el => {
            return el.deposit.depositId === currentDepositId;
        });

        if (currentDeposit === undefined) {
            return;
        }

        return (
            <ItemClose
                itemId={currentDeposit.deposit.itemId ?? 0}
                show={isCloseWarningShow}
                onSuccess={() => {
                    handleCloseDeposit();
                    setIsCloseWarningShow(false);
                }}
                onDiscard={() => {
                    setIsCloseWarningShow(false);
                }}
            />
        );
    };

    const renderDepositList = () => {
        return depositList.map((deposit: IDepositPortfolioItem) => {
            const currencyBalanceFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                style: "currency",
                currency: deposit.currency
            });
            const incomeShare = deposit.inputAmount !== 0 ? deposit.income / deposit.inputAmount : 0;
            return (
                <tr key={"deposit-" + deposit.deposit.depositId}>
                    <td className={"sm:hidden"}>
                        <Link
                            href={makeLink("/user/item/" + deposit.deposit.itemId, lng)}
                            onClick={() => updatePreviousPage("deposit")}
                            className={"link"}
                        >
                            <ItemName name={deposit.deposit.name} type={"deposit"} />
                        </Link>
                        <br />
                        {currencyBalanceFormatter.format(deposit.amount)}&nbsp;
                        <span className="text-success">(+{currencyBalanceFormatter.format(deposit.income)})</span>
                    </td>
                    <td className={"hidden sm:table-cell"}>
                        <Link
                            href={makeLink("/user/item/" + deposit.deposit.itemId, lng)}
                            onClick={() => updatePreviousPage("deposit")}
                            className={"link"}
                        >
                            <ItemName name={deposit.deposit.name} type={"deposit"} />
                        </Link>
                    </td>
                    <td className="hidden md:table-cell">
                        {dateFormatter.format(toUserTimezone(deposit.deposit.dealDate))}
                    </td>
                    <td className={"hidden sm:table-cell"}>
                        {currencyBalanceFormatter.format(deposit.amount)}&nbsp;
                        <span className="text-success">(+{currencyBalanceFormatter.format(deposit.income)})</span>
                    </td>
                    <td className="hidden lg:table-cell">
                        {(deposit.deposit.interestPercent * 100).toFixed(2)}
                        %&nbsp;
                        <span className="text-success">(+{(incomeShare * 100).toFixed(2)}%)</span>
                    </td>
                    <td className="hidden lg:table-cell">
                        {deposit.deposit.duration + " " + deposit.deposit.durationPeriod}
                    </td>
                    <td className={"w-12"}>
                        <DepositListActions
                            deposit={deposit.deposit}
                            handleOperationManage={() =>
                                handleOperationManage(deposit.deposit.itemId === null ? 0 : deposit.deposit.itemId)
                            }
                            handleEditDeposit={() =>
                                handleEditDeposit(deposit.deposit.depositId === null ? 0 : deposit.deposit.depositId)
                            }
                            handleCloseDeposit={depositId => {
                                setCurrentDepositId(depositId);
                                setIsCloseWarningShow(true);
                            }}
                            handleDeleteDeposit={depositId => {
                                setCurrentDepositId(depositId);
                                setIsDeleteWarningShow(true);
                            }}
                        />
                    </td>
                </tr>
            );
        });
    };

    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>DepositManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="w-full">
                <table className="table w-full">
                    <thead>
                        <tr>
                            <th scope="col" className={"sm:hidden"}>
                                <Trans>DepositManage.TableHeaderDeposit</Trans>
                            </th>
                            <th scope="col" className={"hidden sm:table-cell"}>
                                <Trans>DepositManage.TableHeaderName</Trans>
                            </th>
                            <th scope="col" className="hidden md:table-cell">
                                <Trans>DepositManage.TableHeaderDate</Trans>
                            </th>
                            <th scope="col" className={"hidden sm:table-cell"}>
                                <Trans>DepositManage.TableHeaderBalance</Trans>
                            </th>
                            <th scope="col" className="hidden lg:table-cell">
                                <Trans>DepositManage.TableHeaderInterest</Trans>
                            </th>
                            <th scope="col" className="hidden lg:table-cell">
                                <Trans>DepositManage.TableHeaderDuration</Trans>
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>{renderDepositList()}</tbody>
                </table>
                {renderDeleteConfirmation()}
                {renderCloseConfirmation()}
            </div>
        </Card>
    );
};
