"use client";

import { useEffect, useState } from "react";
import { CredentialForm } from "./CredentialForm";
import { UserAccountList } from "./UserAccountList";
import { CredentialListManagePanel } from "./CredentialListManagePanel";
import { CredentialListTable } from "./CredentialListTable";
import { PageState } from "../../components/Common/Types";
import { ApiService } from "../../services/ApiService";
import { PageLoader } from "../../components/Loader/Loaders";
import { ErrorContent } from "../../components/ErrorContent/ErrorContent";

export interface ICredentialPortfolioItem {
    userCredentialId: number;
    broker: string;
    brokerName: string;
    isActive: number;
    apiKey: string;
    accountNumber: number;
    activeAccountNumber: number;
}

export const CredentialList = () => {
    const [editedCredentialId, setEditedCredentialId] = useState(0);
    const [editCredentialFormShow, setEditCredentialFormShow] = useState(false);
    const [editUserAccountFormShow, setEditUserAccountFormShow] = useState(false);
    const [credentials, setCredentials] = useState<ICredentialPortfolioItem[]>([]);
    const [pageState, setPageState] = useState(PageState.LOADING);

    useEffect(() => {
        updateData();
    }, []);

    const updateData = () => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/user/credential")
            .then(response => {
                setCredentials(response);
                setPageState(PageState.SUCCESS);
            })
            .catch(() => {
                setPageState(PageState.ERROR);
            });
    };

    const openUserCredential = (userCredentialId: number) => {
        setEditedCredentialId(userCredentialId);
        setEditUserAccountFormShow(false);
        setEditCredentialFormShow(true);
    };

    const openUserAccounts = (userCredentialId: number) => {
        setEditedCredentialId(userCredentialId);
        setEditCredentialFormShow(false);
        setEditUserAccountFormShow(true);
    };

    const openTab = (name: string, userCredentialId: number) => {
        switch (name) {
            case "account":
                openUserAccounts(userCredentialId);
                break;
            case "credential":
                openUserCredential(userCredentialId);
                break;
        }
    };

    const handleHideModal = (name: "credential" | "userAccount") => {
        switch (name) {
            case "credential":
                setEditCredentialFormShow(false);
                break;
            case "userAccount":
                setEditUserAccountFormShow(false);
                break;
        }
    };

    if (pageState === PageState.ERROR) {
        return <ErrorContent />;
    }

    return (
        <>
            <CredentialListManagePanel
                handleAddButton={openUserCredential}
                show={pageState !== PageState.LOADING && credentials.length > 0}
            />
            <PageLoader show={pageState === PageState.LOADING} />
            <CredentialListTable
                openUserAccounts={openUserAccounts}
                openUserCredential={openUserCredential}
                onAddCredential={() => {
                    openUserCredential(0);
                }}
                credentials={credentials}
                show={pageState === PageState.SUCCESS}
            />
            <CredentialForm
                userCredentialId={editedCredentialId}
                isShow={editCredentialFormShow}
                hideModal={() => handleHideModal("credential")}
                successAction={editedCredentialId => {
                    openUserAccounts(editedCredentialId);
                    updateData();
                }}
            />
            <UserAccountList
                isShow={editUserAccountFormShow}
                userCredentialId={editedCredentialId}
                hideModal={() => handleHideModal("userAccount")}
                handleChangeTab={(name, userCredentialId) => openTab(name, userCredentialId)}
            />
        </>
    );
};
