import { ReactNode, TableHTMLAttributes } from "react";
import { Size } from "../Types";
import { daisyUISizeClass } from "../utils";

type TableProps = {
    children: ReactNode;
    size?: Size;
    className?: string;
    pinRows?: boolean;
    pinCols?: boolean;
    zebra?: boolean;
} & TableHTMLAttributes<HTMLTableElement>;

export const Table = ({
    children,
    size,
    className,
    pinRows = false,
    pinCols = false,
    zebra = false,
    ...args
}: TableProps) => {
    const classes = ["table"];
    if (className) {
        classes.push(...className.split(" "));
    }
    classes.push(daisyUISizeClass("table", size));

    if (pinRows) {
        classes.push("table-pin-rows");
    }
    if (pinCols) {
        classes.push("table-pin-cols");
    }
    if (zebra) {
        classes.push("table-zebra");
    }

    return (
        <table className={classes.join(" ")} {...args}>
            {children}
        </table>
    );
};
