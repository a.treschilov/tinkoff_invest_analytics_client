import { CardTitle } from "./CardTitle";
import { FC, ReactNode } from "react";

type CardBodyProps = {
    children?: ReactNode;
};

type CardBodyExtension = {
    Title: typeof CardTitle;
};

export const CardBody: FC<CardBodyProps> & CardBodyExtension = ({ children }: CardBodyProps) => {
    return <div className={"card-body"}>{children}</div>;
};

CardBody.Title = CardTitle;
