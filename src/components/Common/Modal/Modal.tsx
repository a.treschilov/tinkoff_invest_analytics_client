import { FC, ReactNode, useEffect, useRef } from "react";
import { Button } from "../Button/Button";
import { ModalAction } from "./ModalAction";
import { ModalBody } from "./ModalBody";
import { ModalHeader } from "./ModalHeader";

type ModalExtensions = {
    Header: typeof ModalHeader;
    Body: typeof ModalBody;
    Actions: typeof ModalAction;
};

type ModalProps = {
    children: ReactNode;
    backdrop?: boolean;
    closeButton?: boolean;
    onClose?: () => void;
    show: boolean;
    className?: string;
    responsive?: boolean;
};
export const Modal: FC<ModalProps> & ModalExtensions = ({
    children,
    closeButton = true,
    backdrop = true,
    onClose = () => {},
    show = false,
    className = "",
    responsive = false
}: ModalProps) => {
    const classes = ["modal"];
    if (className) {
        classes.push(...className?.split(" "));
    }
    if (responsive) {
        classes.push("modal-bottom");
        classes.push("sm:modal-middle");
    }

    const refModal = useRef<HTMLDialogElement>(null);

    useEffect(() => {
        refModal.current?.removeEventListener("close", () => onClose());
        refModal.current?.addEventListener("close", () => onClose());
    }, [onClose, refModal]);

    useEffect(() => {
        if (show) {
            refModal.current?.showModal();
        } else {
            refModal.current?.close();
        }
    }, [show]);

    const closeButtonContent = (
        <form method={"dialog"}>
            <Button size="sm" ghost={true} shape="circle" className="absolute right-2 top-2">
                x
            </Button>
        </form>
    );

    const backdropElement = (
        <form method="dialog" className="modal-backdrop">
            <button>close</button>
        </form>
    );

    return (
        <dialog className={classes.join(" ")} ref={refModal}>
            <div className={"modal-box"}>
                {closeButton ? closeButtonContent : <></>}
                {children}
            </div>
            {backdrop ? backdropElement : <></>}
        </dialog>
    );
};

Modal.Header = ModalHeader;
Modal.Body = ModalBody;
Modal.Actions = ModalAction;
