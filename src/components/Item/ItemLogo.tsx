"use client";

import { useState } from "react";

type ItemLogoProps = {
    src: string | null;
    name: string;
    width?: string;
    height?: string;
};
export const ItemLogo = ({ src, name, width = "24px", height = "24px" }: ItemLogoProps) => {
    const [showImage, setShowImage] = useState(true);

    const defaultIcon = () => {
        setShowImage(false);
    };

    const defaultImage = () => {
        return (
            <span
                className={
                    "bg-neutral-content text-primary-content item-logo-name-default text-center inline-block rounded-sm"
                }
                style={{ width: width, height: height, lineHeight: height }}
            >
                {name.substring(0, 3)}
            </span>
        );
    };

    const renderLogo = () => {
        if (src === null) {
            return defaultImage();
        } else {
            return (
                <picture>
                    <img
                        className={"rounded-sm" + (showImage ? "" : " hidden")}
                        src={src}
                        onError={defaultIcon}
                        alt={name}
                        style={{ width: width, height: height }}
                    />
                </picture>
            );
        }
    };

    return <>{showImage ? renderLogo() : defaultImage()}</>;
};
