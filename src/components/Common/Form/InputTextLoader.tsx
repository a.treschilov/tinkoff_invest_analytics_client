import { Skeleton } from "../Skeleton/Skeleton";

export const InputTextLoader = () => {
    return (
        <div className={`form-control w-full`}>
            <label className="label">
                <Skeleton className={"w-2/5 h-5"} />
            </label>
            <Skeleton className={"w-full h-12"} />
        </div>
    );
};
