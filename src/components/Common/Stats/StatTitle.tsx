import { HTMLAttributes, ReactNode } from "react";

type StatsTitleProps = {
    children: ReactNode;
} & HTMLAttributes<HTMLDivElement>;

export const StatTitle = ({ children, className, ...args }: StatsTitleProps) => {
    const classN = ["stat-title"];
    if (className) {
        classN.push(...className.split(" "));
    }
    return (
        <div className={classN.join(" ")} {...args}>
            {children}
        </div>
    );
};
