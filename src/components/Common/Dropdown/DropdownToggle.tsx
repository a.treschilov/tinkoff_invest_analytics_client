import { ButtonHTMLAttributes, ReactNode } from "react";

type DropdownToggleProps = {
    children: ReactNode;
    button?: boolean;
    className?: string;
} & ButtonHTMLAttributes<HTMLButtonElement>;

export const DropdownToggle = ({ children, className, button = true, ...args }: DropdownToggleProps) => {
    return (
        <div tabIndex={0} role="button">
            <button type={"button"} className={(button ? "btn" : "") + (className ? " " + className : "")} {...args}>
                {children}
            </button>
        </div>
    );
};
