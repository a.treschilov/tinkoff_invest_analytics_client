import { IOperation } from "../OperationEdit";
import { Fragment } from "react";
import { InputText } from "../../../components/Common/Form/InputText";
import { formatInputDayDate, toUserTimezone } from "../../../utils/date";
import { CurrencyAmount } from "../../../components/Common/CurrencyAmount/CurrencyAmount";
import { BsFillTrashFill } from "react-icons/bs";
import { PageState } from "../../../components/Common/Types";
import { Skeleton } from "../../../components/Common/Skeleton/Skeleton";

type OperationSuggestionProps = {
    operations: IOperation[];
    onChange: (operations: IOperation[]) => void;
    state: PageState;
};

export const OperationSuggestion = ({ operations, onChange, state }: OperationSuggestionProps) => {
    const skeletonTableRow = (id: string | null, isShow: boolean) => {
        if (!isShow) {
            return <></>;
        }
        return (
            <Fragment key={"depositSuggestionRowSkeleton-" + id}>
                <div className={"col-span-12 sm:col-span-3 flex"}>
                    <Skeleton className={"w-3/5 h-6 my-auto"} />
                </div>
                <div className={"col-span-12 sm:col-span-9"}>
                    <Skeleton className={"w-40 h-12 inline-block mr-4"} />
                    <Skeleton className={"w-56 h-12 inline-block"} />
                </div>
                <div className={"col-span-12 h-0.25 divider m-0"}></div>
            </Fragment>
        );
    };

    const operationRow = (operation: IOperation, isShow: boolean) => {
        if (!isShow) {
            return <></>;
        }
        return (
            <>
                <div className={"col-span-12 sm:col-span-3 max-h-12 flex"}>
                    <div className={"my-auto"}>{operation.itemName}</div>
                </div>
                <div className={"col-span-12 sm:col-span-9 gap-2 flex flex-wrap"}>
                    <div className={"w-40"}>
                        <InputText
                            type={"date"}
                            value={formatInputDayDate(toUserTimezone(operation.date))}
                            name={"date-" + operation.externalId}
                            onChange={e => {
                                updateOperationDate(e.target.name, e.target.value);
                            }}
                        />
                    </div>
                    <div className={"w-56"}>
                        <CurrencyAmount
                            id={"deposit_" + operation.externalId}
                            amountInputName={"amount-" + operation.externalId}
                            currencyInputName={"currency-" + operation.externalId}
                            amount={operation.amount}
                            currency={operation.currencyIso}
                            onChange={(name, value) => {
                                updateOperationAmount(name, value);
                            }}
                        />
                    </div>
                    <div className={"my-auto ml-4"}>
                        <BsFillTrashFill
                            style={{ cursor: "pointer" }}
                            title="Remove"
                            onClick={() => {
                                removeSuggestion(operation.externalId);
                            }}
                        />
                    </div>
                </div>
            </>
        );
    };

    const updateOperationDate = (name: string, value: string) => {
        if (!name.startsWith("date-")) {
            return;
        }

        const externalId = name.replace("date-", "");

        const updatedOperations: IOperation[] = operations.map((operation: IOperation) => {
            if (operation.externalId === externalId) {
                operation.date = {
                    date: value + " 00:00:00",
                    timezone: "UTC"
                };
            }
            return operation;
        });
        onChange(updatedOperations);
    };

    const updateOperationAmount = (name: string, value: string) => {
        const type = name.startsWith("amount-") ? "amount" : name.startsWith("currency-") ? "currency" : null;

        if (type === null) {
            return;
        }

        let externalId: string;
        switch (type) {
            case "amount":
                externalId = name.replace("amount-", "");
                break;
            case "currency":
                externalId = name.replace("currency-", "");
                break;
            default:
                return;
        }

        const updatedOperations: IOperation[] = operations.map((operation: IOperation) => {
            if (operation.externalId === externalId) {
                switch (type) {
                    case "amount":
                        operation.amount = parseFloat(value);
                        break;
                    case "currency":
                        operation.currencyIso = value;
                        break;
                }
            }
            return operation;
        });
        onChange(updatedOperations);
    };

    const removeSuggestion = (externalId: string | null) => {
        const updatedOperations: IOperation[] = operations.filter((operation: IOperation) => {
            return operation.externalId !== externalId;
        });
        onChange(updatedOperations);
    };

    if (operations.length === 0) {
        return <></>;
    }

    return (
        <>
            {operations.map((op: IOperation) => {
                return (
                    <Fragment key={"depositSuggestionRow-" + op.externalId}>
                        {operationRow(op, state !== PageState.SAVING_DATA)}
                        {skeletonTableRow(op.externalId, state === PageState.SAVING_DATA)}
                        <div className={"col-span-12 h-0.25 divider m-0"}></div>
                    </Fragment>
                );
            })}
        </>
    );
};
