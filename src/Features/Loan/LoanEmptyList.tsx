import { Button, Card, Divider } from "react-daisyui";
import { Trans } from "react-i18next";

type LoanEmptyListProps = {
    show: boolean;
    onAddLoan: () => void;
};

export const LoanEmptyList = ({ show, onAddLoan }: LoanEmptyListProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} bordered={true}>
            <Card.Title tag="h2">
                <Trans>LoanManage.PageHeader</Trans>
            </Card.Title>
            <Divider />
            <div className="text-center my-4">
                <Button size="lg" color={"primary"} onClick={onAddLoan}>
                    <Trans>LoanManage.AddFirstLoanButton</Trans>
                </Button>
            </div>
        </Card>
    );
};
