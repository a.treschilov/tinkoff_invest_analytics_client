import { Trans } from "react-i18next";
import { ChangeEvent, Fragment, useEffect, useState } from "react";
import { BsQuestionCircle } from "react-icons/bs";
import { UserCredentialType } from "./CredentialForm";
import { Select } from "../../components/Common/Form/Select";
import { Toggle } from "../../components/Common/Form/Toggle";
import { Textarea } from "../../components/Common/Form/Textarea";
import { Popover } from "../../components/Common/Popover/Popover";
import { DefaultBrokerList } from "./CredentialStaticData";
import { ApiService } from "../../services/ApiService";
import Link from "next/link";

type CredentialFormBodyProps = {
    show: boolean;
    credential: UserCredentialType;
    changeForm: (userCredential: UserCredentialType) => void;
};

export type BrokerType = {
    id: string;
    name: string;
    logo: string;
    isApi: boolean;
    isManual: boolean;
    instructionLink: string | null;
};

const popover = (
    <ul className={"list-disc mt-4 mx-4"}>
        <li>
            <Trans>CredentialForm.ApiKeyPopoverReadonly</Trans>
        </li>
        <li>
            <Trans>CredentialForm.ApiKeyPopoverProtect</Trans>
        </li>
    </ul>
);

export const CredentialFormBody = ({ show, credential, changeForm }: CredentialFormBodyProps) => {
    const [brokerList, setBrokerList] = useState(DefaultBrokerList);
    useEffect(() => {
        ApiService.fetch("/v1/broker/list").then((response: BrokerType[]) => {
            setBrokerList(response);
        });
    }, []);

    const handleApiKeyChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
        const newUserCredential: UserCredentialType = credential;
        newUserCredential.apiKey = event.target.value;
        changeForm(newUserCredential);
    };

    const handleBrokerChange = (event: ChangeEvent<HTMLSelectElement>) => {
        const newUserCredential: UserCredentialType = credential;
        newUserCredential.brokerId = event.target.value;
        changeForm(newUserCredential);
    };

    const handleIsActiveChange = () => {
        const newUserCredential: UserCredentialType = credential;
        newUserCredential.isActive = newUserCredential.isActive ? 0 : 1;
        changeForm(newUserCredential);
    };

    if (!show) {
        return <></>;
    }

    return (
        <>
            <Toggle>
                <Toggle.Label>
                    <Trans>CredentialForm.FormActive</Trans>
                </Toggle.Label>
                <Toggle.Checkbox
                    id="userCredentialIsActiveCheckbox"
                    defaultChecked={!!credential.isActive}
                    onChange={() => {
                        handleIsActiveChange();
                    }}
                    defaultValue={credential.isActive}
                    color={"primary"}
                />
            </Toggle>
            <Select>
                <Select.Label>
                    <Trans>CredentialForm.FormBroker</Trans>
                </Select.Label>
                <Select.Select
                    id="userCredentialBrokerInput"
                    onChange={(event: ChangeEvent<HTMLSelectElement>) => handleBrokerChange(event)}
                    defaultValue={credential.brokerId}
                >
                    {brokerList.map(broker => {
                        if (!broker.isApi) {
                            return <Fragment key={"credentialBrokerOption-" + broker.id}></Fragment>;
                        }
                        return (
                            <option key={"credentialBrokerOption-" + broker.id} value={broker.id}>
                                {broker.name}
                            </option>
                        );
                    })}
                </Select.Select>
            </Select>
            <Textarea>
                <Textarea.Label>
                    <div className={"flex"}>
                        <Trans>CredentialForm.FormApiKey</Trans>&nbsp;
                        <Popover content={popover} trigger={"hover"}>
                            <BsQuestionCircle style={{ cursor: "pointer" }} />
                        </Popover>
                    </div>
                </Textarea.Label>
                <Textarea.Textarea
                    rows={3}
                    className="form-control"
                    id="userCredentialApiKeyInput"
                    defaultValue={credential.apiKey}
                    onChange={(event: ChangeEvent<HTMLTextAreaElement>) => handleApiKeyChange(event)}
                />
                <Textarea.Description>
                    <Link
                        href="https://hakkes.com/help/article/integration/t-invest"
                        target={"_blank"}
                        className={"text-primary"}
                    >
                        <Trans>CredentialForm.FormApiKeyDescription</Trans>
                    </Link>{" "}
                </Textarea.Description>
            </Textarea>
        </>
    );
};
