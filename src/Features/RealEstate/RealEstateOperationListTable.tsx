import { Trans } from "react-i18next";
import { toUserTimezone } from "../../utils/date";
import { OperationTypeTranslation } from "../OperationList/OperationTypeTranslation";
import { ItemType } from "../../components/Common/Types";
import { BsFillPencilFill } from "react-icons/bs";
import { IOperationView } from "../../../app/[lng]/user/operations/list/page";

interface IRealEstateOperationListTableProps {
    itemType: ItemType;
    operations: IOperationView[];
    handleEditOperation: (operationId: number) => void;
}

const typeColumn = (
    <th scope="col">
        <Trans>RealEstateOperationsList.TableHeaderOperationType</Trans>
    </th>
);

export const RealEstateOperationListTable = ({
    itemType,
    operations,
    handleEditOperation
}: IRealEstateOperationListTableProps) => {
    const renderOperationList = (operations: IOperationView[]) => {
        return operations.map((operation: IOperationView) => {
            let locale;
            switch (operation.currency) {
                case "RUB":
                    locale = "ru-RU";
                    break;
                case "USD":
                case "EUR":
                    locale = "en-US";
                    break;
                default:
                    locale = "ru-RU";
            }
            const currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat(locale, {
                style: "currency",
                currency: operation.currency
            });
            const date = toUserTimezone(operation.date);
            const formattedDate = new Intl.DateTimeFormat("ru-RU", {
                year: "numeric",
                month: "numeric",
                day: "numeric",
                timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
            }).format(date);
            const operationType = (
                <td>
                    <OperationTypeTranslation operationType={operation.type} itemType={itemType} />
                </td>
            );
            return (
                <tr key={"operation-" + operation.id}>
                    <td className={"md:hidden"}>
                        {currencyFormatter.format(operation.amount)}
                        <br />
                        {formattedDate}
                    </td>
                    <td className={"hidden md:table-cell"}>{formattedDate}</td>
                    <td className={"hidden md:table-cell"}>{currencyFormatter.format(operation.amount)}</td>
                    {operationType}
                    <td>
                        <BsFillPencilFill
                            className="me-4"
                            title="Edit"
                            style={{ cursor: "pointer" }}
                            onClick={() => handleEditOperation(operation.id)}
                        />
                    </td>
                </tr>
            );
        });
    };

    return (
        <table className="table w-full">
            <thead>
                <tr>
                    <th className={"md:hidden"}>
                        <Trans>RealEstateOperationsList.TableHeaderOperation</Trans>
                    </th>
                    <th className={"hidden md:table-cell"}>
                        <Trans>RealEstateOperationsList.TableHeaderDate</Trans>
                    </th>
                    <th className={"hidden md:table-cell"}>
                        <Trans>RealEstateOperationsList.TableHeaderAmount</Trans>
                    </th>
                    {typeColumn}
                    <th></th>
                </tr>
            </thead>
            <tbody>{renderOperationList(operations)}</tbody>
        </table>
    );
};
