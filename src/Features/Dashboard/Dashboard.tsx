"use client";

import { useContext, useEffect, useState } from "react";
import { Trans } from "react-i18next";
import { PageState } from "../../components/Common/Types";
import { DashboardDefaultData, DashboardDemoData } from "./DashboardStaticData";
import { DashboardDemoMessage } from "./DashboardDemoMessage";
import {
    DashboardCardMoneyPeriodRow,
    DashboardCardMoneyRow,
    DashboardCardNumberRow,
    DashboardCardPercentRow
} from "./DashboardCardRow";
import { ApiService } from "../../services/ApiService";
import { AuthContext } from "../../providers/AuthProvider";
import { CapitalChangeType, DashboardCapitalDifference } from "./DashboardCapitalDifference";
import { Skeleton } from "../../components/Common/Skeleton/Skeleton";
import { Card } from "../../components/Common/Card/Card";

export interface IDashboardData {
    targetCapital: number | null;
    actives: number;
    passives: number;
    shouldIncrease: number;
    saveSpeed: number | null;
    pension: number;
    futurePension: number;
    capital: number;
    predictInterest: number;
    currentInterest: number;
    wealthRate: number | null;
    safeRatio: number | null;
    capitalBefore: CapitalChangeType;
}

interface IDashBoardProps {
    timestamp: number;
}

export const Dashboard = ({ timestamp }: IDashBoardProps) => {
    const user = useContext(AuthContext);
    const [pageState, setPageState] = useState(PageState.LOADING);
    const [dashboardData, setDashboardData] = useState<IDashboardData>(DashboardDefaultData);

    useEffect(() => {
        if (user.isNewUser) {
            getDemoData();
        } else {
            getDashboardData();
        }
    }, [timestamp, user.isNewUser]);

    const getDemoData = () => {
        setPageState(PageState.SUCCESS);
        setDashboardData(DashboardDemoData);
    };

    const getDashboardData = () => {
        setPageState(PageState.LOADING);
        ApiService.fetch("/v1/analytics/dashboard")
            .then(response => {
                const data: IDashboardData = {
                    targetCapital: response.targetCapital,
                    actives: response.actives,
                    passives: response.passives,
                    shouldIncrease: response.shouldIncrease,
                    saveSpeed: response.saveSpeed,
                    pension: response.pension,
                    futurePension: response.futurePension,
                    capital: response.capital,
                    predictInterest: response.predictInterest,
                    currentInterest: response.lastYearInterest,
                    wealthRate: response.wealthRate,
                    safeRatio: response.safeRatio,
                    capitalBefore: {
                        day:
                            response.capitalBefore.day === null
                                ? null
                                : {
                                      amount: response.capitalBefore.day.amount,
                                      currency: response.capitalBefore.day.currency
                                  },
                        month:
                            response.capitalBefore.month === null
                                ? null
                                : {
                                      amount: response.capitalBefore.month.amount,
                                      currency: response.capitalBefore.month.currency
                                  },
                        year:
                            response.capitalBefore.year === null
                                ? null
                                : {
                                      amount: response.capitalBefore.year.amount,
                                      currency: response.capitalBefore.year.currency
                                  }
                    }
                };
                setDashboardData(data);
            })
            .catch(() => {
                setDashboardData(DashboardDefaultData);
            })
            .finally(() => {
                setPageState(PageState.SUCCESS);
            });
    };

    const wealthRate = dashboardData.wealthRate;
    const isLoaded = pageState === PageState.SUCCESS;

    return (
        <div>
            <DashboardDemoMessage isShow={user.isNewUser} />
            <div className="mb-2">
                <DashboardCapitalDifference isLoaded={isLoaded} capital={dashboardData.capitalBefore} />
            </div>
            <div className={"grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 xxl:grid-cols-4 gap-4"}>
                <div>
                    <Card className="mb-4 bg-base-100 shadow-xl">
                        <Card.Body>
                            <DashboardCardNumberRow
                                label="Dashboard.CardPensionWealthRate"
                                amount={wealthRate}
                                isLoaded={isLoaded}
                                helperValue={wealthRate}
                                isTitle={true}
                            />

                            <DashboardCardMoneyPeriodRow
                                label="Dashboard.CardPensionTitle"
                                amount={dashboardData.pension}
                                isLoaded={isLoaded}
                            />
                            <DashboardCardMoneyPeriodRow
                                label="Dashboard.CardPensionYearLater"
                                amount={dashboardData.futurePension}
                                isLoaded={isLoaded}
                            />
                        </Card.Body>
                    </Card>
                </div>
                <div>
                    <Card className="mb-4 bg-base-100">
                        <Card.Body>
                            <DashboardCardMoneyRow
                                label="Dashboard.CardCapitalTitle"
                                amount={dashboardData.capital}
                                isLoaded={isLoaded}
                                isTitle={true}
                            />
                            <DashboardCardMoneyRow
                                label="Dashboard.CardCapitalActives"
                                amount={dashboardData.actives}
                                isLoaded={isLoaded}
                            />
                            <DashboardCardMoneyRow
                                label="Dashboard.CardCapitalPassives"
                                amount={dashboardData.passives}
                                isLoaded={isLoaded}
                            />
                        </Card.Body>
                    </Card>
                </div>
                <div>
                    <Card className="mb-4 bg-base-100">
                        <Card.Body>
                            {isLoaded ? (
                                <span className="small">
                                    <Trans>Dashboard.CardIncomeTitle</Trans>:
                                </span>
                            ) : (
                                <Skeleton className={"h-4"} />
                            )}
                            <DashboardCardMoneyRow
                                label="Dashboard.CardIncomeLastYear"
                                amount={dashboardData.currentInterest}
                                isLoaded={isLoaded}
                            />
                            <DashboardCardMoneyRow
                                label="Dashboard.CardIncomeNextYear"
                                amount={dashboardData.predictInterest}
                                isLoaded={isLoaded}
                            />
                        </Card.Body>
                    </Card>
                </div>
                <div>
                    <Card className="mb-4 bg-base-100">
                        <Card.Body>
                            <DashboardCardPercentRow
                                label="Dashboard.CardNavigatorTitle"
                                amount={dashboardData.safeRatio}
                                isLoaded={isLoaded}
                                popoverBody={"Dashboard.PopoverSafeRationDescription"}
                                isTitle={true}
                                emptyValueDescription={{
                                    body: "Dashboard.PopoverSafeRatioEmptyValueBody"
                                }}
                            />
                            <DashboardCardMoneyRow
                                label={"Dashboard.CardNavigatorShouldIncrease"}
                                amount={dashboardData.shouldIncrease}
                                isLoaded={isLoaded}
                            />
                            <DashboardCardMoneyPeriodRow
                                label="Dashboard.CardNavigatorSaveSpeed"
                                amount={dashboardData.saveSpeed}
                                isLoaded={isLoaded}
                            />
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </div>
    );
};
