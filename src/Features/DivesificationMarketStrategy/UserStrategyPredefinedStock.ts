export const UserStrategyPredefinedStock = [
    {
        id: "aggressive",
        name: "UserStrategySettings.Strategies.StockAggressiveName",
        description: "UserStrategySettings.Strategies.StockAggressiveDescription",
        strategy: [
            {
                id: "Stock",
                value: 0.5
            },
            {
                id: "Bond",
                value: 0.05
            },
            {
                id: "FederalBond",
                value: 0.05
            },
            {
                id: "Etf",
                value: 0.1
            },
            {
                id: "Gold",
                value: 0.2
            },
            {
                id: "Currency",
                value: 0.1
            },
            {
                id: "Futures",
                value: 0
            }
        ]
    },
    {
        id: "moderate",
        name: "UserStrategySettings.Strategies.StockModerateName",
        description: "UserStrategySettings.Strategies.StockModerateDescription",
        strategy: [
            {
                id: "Stock",
                value: 0.3
            },
            {
                id: "Bond",
                value: 0.15
            },
            {
                id: "FederalBond",
                value: 0.15
            },
            {
                id: "Etf",
                value: 0.1
            },
            {
                id: "Gold",
                value: 0.2
            },
            {
                id: "Currency",
                value: 0.1
            },
            {
                id: "Futures",
                value: 0
            }
        ]
    },
    {
        id: "conservative",
        name: "UserStrategySettings.Strategies.StockConservativeName",
        description: "UserStrategySettings.Strategies.StockConservativeDescription",
        strategy: [
            {
                id: "Stock",
                value: 0.2
            },
            {
                id: "Bond",
                value: 0.3
            },
            {
                id: "FederalBond",
                value: 0.2
            },
            {
                id: "Etf",
                value: 0.1
            },
            {
                id: "Gold",
                value: 0.1
            },
            {
                id: "Currency",
                value: 0.1
            },
            {
                id: "Futures",
                value: 0
            }
        ]
    }
];
