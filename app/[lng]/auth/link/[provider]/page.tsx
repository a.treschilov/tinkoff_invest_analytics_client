"use client";

import { LogoLoader } from "../../../../../src/components/Loader/LogoLoader";
import { use, useEffect } from "react";
import { usePathname } from "next/navigation";
import { parseToken } from "../../../../../src/Features/Auth/socialAccounts";
import { ApiService } from "../../../../../src/services/ApiService";
import { clientRedirect } from "../../../../../src/utils/_actions";

const Page = (props: { params: Promise<{ provider: string }> }) => {
    const params = use(props.params);
    const pathname = usePathname();

    useEffect(() => {
        const hash = window.location.href.split("#")[1];
        const socialCode = params.provider.toUpperCase();

        const token = parseToken(socialCode, hash);

        const data = {
            auth_type: socialCode,
            access_token: token
        };

        ApiService.post("/v1/auth/link", data)
            .then(() => {
                clientRedirect("/user/settings/interface?link=success").then();
            })
            .catch(error => {
                if (error.status === 422) {
                    clientRedirect("/user/settings/interface?link=exist").then();
                } else {
                    clientRedirect("/user/settings/interface?link=error").then();
                }
            });
    }, [params.provider, pathname]);

    return <LogoLoader />;
};

export default Page;
