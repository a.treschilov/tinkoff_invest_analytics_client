"use client";

import { FutureItem } from "./FutureList";
import { toUserTimezone } from "../../utils/date";
import Link from "next/link";
import { makeLink } from "../../utils/url";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { SessionContext } from "../../providers/SessionProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Card } from "../../components/Common/Card/Card";
import { Table } from "../../components/Common/Table/Table";

type FutureTableProps = {
    futures: FutureItem[];
};

const browserLocale = navigator.languages != undefined ? navigator.languages[0] : "en-US";
const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat(browserLocale, {
    year: "numeric",
    month: "short",
    day: "numeric"
});

export const FutureTable = ({ futures }: FutureTableProps) => {
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);
    const { t } = useTranslation(lng, "features/futureList");

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <div className="overflow-x-auto max-h-screen">
                <Table pinRows={true}>
                    <thead>
                        <tr>
                            <th className={"hidden sm:table-cell"}>{t("TitleIsin")}</th>
                            <th className={"hidden sm:table-cell"}>{t("TitleName")}</th>
                            <th className={"sm:hidden"}>{t("TitleFuture")}</th>
                            <th>{t("TitleExpirationDate")}</th>
                        </tr>
                    </thead>
                    {futures.map(future => {
                        return (
                            <tr key={"future-row_" + future.isin}>
                                <td className={"hidden sm:table-cell"}>{future.isin}</td>
                                <td className={"hidden sm:table-cell"}>
                                    <Link
                                        href={makeLink("/user/item/" + future.itemId, lng)}
                                        onClick={() => updatePreviousPage("futureList")}
                                        className={"link"}
                                    >
                                        {future.name}
                                    </Link>
                                </td>
                                <td className={"sm:hidden"}>
                                    <Link
                                        href={makeLink("/user/item/" + future.itemId, lng)}
                                        onClick={() => updatePreviousPage("futureList")}
                                        className={"link"}
                                    >
                                        {future.name}
                                    </Link>
                                    <br />
                                    <small className={"text-primary"}>{future.isin}</small>
                                </td>
                                <td>
                                    {future.expirationDate === null
                                        ? "—"
                                        : dateFormatter.format(toUserTimezone(future.expirationDate))}
                                </td>
                            </tr>
                        );
                    })}
                </Table>
            </div>
        </Card>
    );
};
