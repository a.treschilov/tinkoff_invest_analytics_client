import { Skeleton } from "../../../components/Common/Skeleton/Skeleton";

type ItemDescriptionLoadingProps = {
    show?: boolean;
};
export const UserItemDescriptionLoading = ({ show = true }: ItemDescriptionLoadingProps) => {
    if (!show) {
        return <></>;
    }

    return (
        <div className={"flex gap-2 text-lg col-span-4 sm:col-span-3"}>
            <div>
                <Skeleton className={"w-[60px] h-[60px]"} />
            </div>
            <div className="flex flex-col gap-3">
                <Skeleton className={"w-52 h-5"} />
                <Skeleton className={"w-32 h-5"} />
            </div>
        </div>
    );
};
