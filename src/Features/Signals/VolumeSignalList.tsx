import Link from "next/link";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { makeLink } from "../../utils/url";
import { useTranslation } from "../../../app/i18n/client";
import { VolumeSignalLoading } from "./VolumeSignalLoading";
import { Table } from "../../components/Common/Table/Table";

export type VolumeSignal = {
    itemId: number;
    name: string;
    isin: string;
    ticker: string;
    startDate: Date | null;
    endDate: Date | null;
};

type VolumeSignalListProps = {
    loading: boolean;
    signals: VolumeSignal[];
};

const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat("ru-RU", {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit"
});

export const VolumeSignalList = ({ loading, signals }: VolumeSignalListProps) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/signals");

    const emptySignalList = () => {
        return (
            <tr>
                <td colSpan={3} className={"text-center py-6 text-lg"}>
                    {t("VolumeSignal.NoSignals")}
                </td>
            </tr>
        );
    };

    const signalContent = () => {
        return (
            <>
                {signals.map(signal => {
                    return (
                        <tr key={"signal-row-key_" + signal.isin + "_" + signal.startDate?.valueOf()}>
                            <td className={"hidden sm:table-cell"}>{signal.name}</td>
                            <td className={"hidden sm:table-cell"}>
                                <Link href={makeLink("/user/item/" + signal.itemId, lng)} className={"link"}>
                                    {signal.ticker}
                                </Link>
                            </td>
                            <td className={"hidden sm:table-cell"}>
                                {signal.startDate !== null ? dateFormatter.format(new Date(signal.startDate)) : "—"}

                                {signal.endDate !== null ? " — " + dateFormatter.format(signal.endDate) : ""}
                            </td>
                            <td className={"sm:hidden"}>
                                {signal.name}
                                <br />
                                <Link href={makeLink("/user/item/" + signal.itemId, lng)} className={"link"}>
                                    {signal.ticker}
                                </Link>
                                <br />
                                {signal.startDate !== null ? dateFormatter.format(signal.startDate) : "—"}

                                {signal.endDate !== null ? " — " + dateFormatter.format(signal.endDate) : ""}
                            </td>
                        </tr>
                    );
                })}
            </>
        );
    };

    return (
        <Table className={"w-full"}>
            <thead>
                <tr>
                    <th className={"hidden sm:table-cell"}>{t("VolumeSignal.TableHeaderName")}</th>
                    <th className={"hidden sm:table-cell"}>{t("VolumeSignal.TableHeaderTicker")}</th>
                    <th className={"hidden sm:table-cell"}>{t("VolumeSignal.TableHeaderStartDate")}</th>
                    <th className={"sm:hidden"}></th>
                </tr>
            </thead>
            <tbody>
                {loading ? (
                    <VolumeSignalLoading show={loading} />
                ) : signals.length === 0 ? (
                    emptySignalList()
                ) : (
                    signalContent()
                )}
            </tbody>
        </Table>
    );
};
