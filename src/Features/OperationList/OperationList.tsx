import { Trans } from "react-i18next";
import { toUserTimezone } from "../../utils/date";
import { IPrice } from "../../interfaces/IPrice";
import { ComponentState } from "../../components/Common/Types";
import { OperationTypeTranslation } from "./OperationTypeTranslation";
import { OperationListLoading } from "./OperationListLoading";
import { ItemLogoName } from "../../components/Item/ItemLogoName";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import Link from "next/link";
import { makeLink } from "../../utils/url";
import { IOperationView } from "../../../app/[lng]/user/operations/list/page";
import { EmptyContent } from "../../components/Common/EmptyContent/EmptyContent";
import { SessionContext } from "../../providers/SessionProvider";
import { Card } from "../../components/Common/Card/Card";
import { Divider } from "../../components/Common/Divider/Divider";
import { Table } from "../../components/Common/Table/Table";
import { Badge } from "../../components/Common/Badge/Badge";

type OperationListProps = {
    state: ComponentState;
    operations: IOperationView[];
    summary: IPrice;
};
export const OperationList = ({ state, operations, summary }: OperationListProps) => {
    const { lng } = useContext(ThemeContext);
    const { updatePreviousPage } = useContext(SessionContext);

    const renderOperationRows = (operations: IOperationView[]) => {
        const browserLocale = navigator.languages != undefined ? navigator.languages[0] : "en-US";
        const dateFormatter: Intl.DateTimeFormat = new Intl.DateTimeFormat(browserLocale, {
            year: "numeric",
            month: "short",
            day: "numeric"
        });

        return operations.map(operation => {
            let classRow = "";
            if (["Tax", "Fee"].indexOf(operation.type) !== -1) classRow = "table-info";
            if (["Dividend", "DividendCard"].indexOf(operation.type) !== -1) classRow = "table-success";

            const currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
                style: "currency",
                currency: operation.currency
            });
            let status:
                | "neutral"
                | "primary"
                | "secondary"
                | "accent"
                | "ghost"
                | "info"
                | "success"
                | "warning"
                | "error"
                | undefined;
            switch (operation.status) {
                case "Done":
                    status = "accent";
                    break;
                case "Decline":
                    status = "error";
                    break;
                default:
                    status = "primary";
            }
            return (
                <tr className={classRow} key={"operation_" + operation.id}>
                    <td>
                        {operation.itemId !== null ? (
                            <Link
                                onClick={() => updatePreviousPage("operations")}
                                href={makeLink("/user/item/" + operation.itemId, lng)}
                                className={"link"}
                            >
                                <ItemLogoName
                                    name={operation.itemName}
                                    type={operation.itemType}
                                    logo={operation.itemLogo}
                                />
                            </Link>
                        ) : (
                            <></>
                        )}
                    </td>
                    <td className="hidden xl:table-cell">{operation.broker}</td>
                    <td className="hidden xl:table-cell">
                        <OperationTypeTranslation operationType={operation.type} itemType={operation.itemType} />
                    </td>
                    <td className="hidden xl:table-cell">
                        {operation.quantity === null
                            ? null
                            : currencyFormatter.format(operation.amount / operation.quantity)}
                    </td>
                    <td className="hidden sm:table-cell">{operation.quantity}</td>
                    <td className="hidden sm:table-cell">{currencyFormatter.format(operation.amount)}</td>
                    <td className="hidden sm:table-cell">{dateFormatter.format(toUserTimezone(operation.date))}</td>
                    <td className="hidden sm:table-cell">
                        <Badge color={status}>{operation.status}</Badge>
                    </td>
                    <td className="sm:hidden ps-0.5 pe-0">
                        {currencyFormatter.format(operation.amount)}
                        <br />
                        <small className="text-accent">{dateFormatter.format(toUserTimezone(operation.date))}</small>
                        <br />
                        <small>
                            <Badge color={status}>{operation.status}</Badge>
                        </small>
                    </td>
                </tr>
            );
        });
    };

    const currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: summary.currency
    });

    if (state === "loading") {
        return <OperationListLoading show={true} />;
    }

    if (state === "empty") {
        return (
            <EmptyContent>
                <Trans>Operations.EmptyList</Trans>
            </EmptyContent>
        );
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>
                <Trans>Operations.PageHeader</Trans>
            </Card.Title>
            <Divider />

            <div className="overflow-x-auto max-h-[48rem]">
                <Table pinRows={true} className="w-full">
                    <thead>
                        <tr>
                            <th>
                                <Trans>Operations.TableHeaderLot</Trans>
                            </th>
                            <th className="hidden xl:table-cell">
                                <Trans>Operations.TableHeaderBroker</Trans>
                            </th>
                            <th className="hidden xl:table-cell">
                                <Trans>Operations.TableHeaderOperationType</Trans>
                            </th>
                            <th className="hidden xl:table-cell">
                                <Trans>Operations.TableHeaderCostShare</Trans>
                            </th>
                            <th className="hidden sm:table-cell">
                                <Trans>Operations.TableHeaderCountLots</Trans>
                            </th>
                            <th className="hidden sm:table-cell">
                                <Trans>Operations.TableHeaderAmount</Trans>
                            </th>
                            <th className="hidden sm:table-cell">
                                <Trans>Operations.TableHeaderDate</Trans>
                            </th>
                            <th className="hidden sm:table-cell">
                                <Trans>Operations.TableHeaderStatus</Trans>
                            </th>
                            <th className="sm:hidden">
                                <Trans>Operations.TableHeaderInfo</Trans>
                            </th>
                        </tr>
                    </thead>
                    <tbody>{renderOperationRows(operations)}</tbody>
                    <tfoot>
                        <tr>
                            <th>
                                <Trans>Operations.TableSummary</Trans>
                            </th>
                            <th className="hidden lg:table-cell" colSpan={4} />
                            <th colSpan={3}>{currencyFormatter.format(summary.amount)}</th>
                        </tr>
                    </tfoot>
                </Table>
            </div>
        </Card>
    );
};
