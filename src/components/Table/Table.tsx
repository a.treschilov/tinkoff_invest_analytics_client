import { flexRender, Table as TableType } from "@tanstack/react-table";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TableProps<T = any> = {
    table: TableType<T>;
};
export const Table = ({ table }: TableProps) => {
    return (
        <table className="table w-full">
            <thead>
                {table.getHeaderGroups().map(headerGroup => (
                    <tr key={headerGroup.id}>
                        {headerGroup.headers.map(header => {
                            return (
                                <th key={header.id} colSpan={header.colSpan}>
                                    {header.isPlaceholder ? null : (
                                        <div
                                            className={
                                                header.column.getCanSort()
                                                    ? "whitespace-nowrap cursor-pointer select-none"
                                                    : ""
                                            }
                                            onClick={header.column.getToggleSortingHandler()}
                                            title={
                                                header.column.getCanSort()
                                                    ? header.column.getNextSortingOrder() === "asc"
                                                        ? "Sort ascending"
                                                        : header.column.getNextSortingOrder() === "desc"
                                                          ? "Sort descending"
                                                          : "Clear sort"
                                                    : undefined
                                            }
                                        >
                                            <span className={header.column.getIsSorted() === false ? "pr-4" : ""}>
                                                {flexRender(header.column.columnDef.header, header.getContext())}
                                            </span>
                                            {{
                                                asc: <IoIosArrowUp className={"ml-1 inline"} />,
                                                desc: <IoIosArrowDown className={"ml-1 inline"} />
                                            }[header.column.getIsSorted() as string] ?? null}
                                        </div>
                                    )}
                                </th>
                            );
                        })}
                    </tr>
                ))}
            </thead>
            <tbody>
                {table.getRowModel().rows.map(row => {
                    return (
                        <tr key={row.id}>
                            {row.getVisibleCells().map(cell => {
                                return (
                                    <td key={cell.id}>{flexRender(cell.column.columnDef.cell, cell.getContext())}</td>
                                );
                            })}
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
};
