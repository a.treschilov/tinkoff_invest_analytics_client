import { useContext, useEffect, useState } from "react";
import { ThemeContext } from "../../../providers/ThemeProvider";
import Datepicker, { DateValueType } from "react-tailwindcss-datepicker";

type DateRangeProps = {
    dateFrom: Date;
    dateTo: Date;
    onChange: (dateFrom: Date, dateEnd: Date) => void;
    type?: "date" | "month";
    className?: string;
};

const defaultDateRangeProps: DateRangeProps = {
    dateFrom: new Date(),
    dateTo: new Date(),
    onChange: () => {},
    type: "date",
    className: ""
};

export const DateRange = (props: DateRangeProps = defaultDateRangeProps) => {
    const { onChange, className } = props;
    const { lng } = useContext(ThemeContext);

    const [dateFrom, setDateFrom] = useState(props.dateFrom);
    const [dateTo, setDateTo] = useState(props.dateTo);

    useEffect(() => {
        setDateFrom(props.dateFrom);
        setDateTo(props.dateTo);
    }, [props.dateFrom, props.dateTo]);

    const handleChange = (value: DateValueType) => {
        let startDate = new Date();
        if (value !== null) {
            switch (typeof value.startDate) {
                case "string":
                    startDate = new Date(value.startDate);
                    break;
                case "object":
                    startDate = value.startDate === null ? new Date() : value.startDate;
                    break;
            }
        }

        let endDate = new Date();
        if (value !== null) {
            switch (typeof value.endDate) {
                case "string":
                    endDate = new Date(value.endDate);
                    break;
                case "object":
                    endDate = value.endDate === null ? new Date() : value.endDate;
                    break;
            }
        }

        setDateFrom(startDate);
        setDateTo(endDate);
        onChange(startDate, endDate);
    };
    return (
        <div className={"dateRange" + (className !== undefined ? " " + className : "")}>
            <Datepicker
                primaryColor={"blue"}
                startWeekOn="mon"
                i18n={lng}
                showShortcuts={true}
                value={{ startDate: dateFrom, endDate: dateTo }}
                onChange={handleChange}
                inputClassName={"input input-bordered w-full text-neutral-content"}
                displayFormat={"DD.MM.YYYY"}
                containerClassName={"w-64 relative"}
            />
        </div>
    );
};
