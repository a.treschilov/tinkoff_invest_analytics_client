import { useContext, useEffect, useState } from "react";
import { ApiService } from "../../services/ApiService";
import { IPrice } from "../../interfaces/IPrice";
import { DateType, PageState } from "../../components/Common/Types";
import { BondListSkeleton } from "./BondListSkeleton";
import { BondList } from "./BondList";
import { useTranslation } from "../../../app/i18n/client";
import { ThemeContext } from "../../providers/ThemeProvider";
import { Card } from "../../components/Common/Card/Card";
import { Table } from "../../components/Common/Table/Table";

export type BondType = {
    id: number;
    itemId: number;
    name: string;
    initialNominal: IPrice;
    nominal: IPrice;
    maturityDate: DateType | null;
};

export const BondListManage = () => {
    const [bonds, setBonds] = useState<BondType[]>([]);
    const [pageState, setPageState] = useState<PageState>(PageState.LOADING);
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/bondList");

    useEffect(() => {
        ApiService.fetch("/v1/market/bond/list").then((response: BondType[]) => {
            setBonds(response);
            setPageState(PageState.SUCCESS);
        });
    }, []);

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <div className="overflow-x-auto max-h-screen">
                <Table pinRows={true}>
                    <thead>
                        <tr>
                            <th className={"hidden sm:table-cell"}>{t("TitleName")}</th>
                            <th className={"sm:hidden"}>{t("TitleInfo")}</th>
                            <th className={"hidden sm:table-cell"}>{t("TitleNominal")}</th>
                            <th className={"hidden lg:table-cell"}>{t("TitleInitialNominal")}</th>
                            <th className={"hidden sm:table-cell"}>{t("TitleMaturityDate")}</th>
                        </tr>
                    </thead>
                    {pageState === PageState.LOADING ? <BondListSkeleton /> : <BondList bonds={bonds} />}
                </Table>
            </div>
        </Card>
    );
};
