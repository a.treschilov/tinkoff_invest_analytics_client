"use client";

import { ReactNode, use, useEffect, useState } from "react";
import { redirect } from "next/navigation";
import { ErrorBoundary } from "react-error-boundary";
import { ErrorContent } from "../../../src/components/ErrorContent/ErrorContent";
import { FooterComponent } from "../../../src/components/Footer/FooterComponent";
import { DefaultUser, UserService, UserType } from "../../../src/services/UserSerice";
import { AuthProvider } from "../../../src/providers/AuthProvider";
import { clientRedirect } from "../../../src/utils/_actions";
import { ThemeProvider } from "../../../src/providers/ThemeProvider";
import { SessionProvider } from "../../../src/providers/SessionProvider";
import { useTranslation } from "../../i18n/client";
import { UserMenu } from "../../../src/components/Menu/UserMenu";
import { LayoutHeader } from "../../../src/components/Menu/LayoutHeader";
import { LogoLoader } from "../../../src/components/Loader/LogoLoader";
import { MenuLayout } from "../../../src/components/Menu/MenuLayout";
import { ApiService } from "../../../src/services/ApiService";
import { Trans } from "react-i18next";
import { Alert } from "../../../src/components/Common/Alert/Alert";
import { formatInputDateTime } from "../../../src/utils/date";
import { Toast } from "../../../src/components/Common/Toast/Toast";
import { Navbar } from "../../../src/components/Common/Navbar/Navbar";
import { Theme } from "../../../src/components/Common/Theme/Theme";

const UserLayout = (props: { children: ReactNode; params: Promise<{ lng: string }> }) => {
    const params = use(props.params);

    const { children } = props;

    const [showContent, setShowContent] = useState(false);
    const [importedOperations, setImportedOperations] = useState(0);
    const [user, setUser] = useState<UserType>(DefaultUser);

    const { t } = useTranslation(params.lng, "headerTitle");

    useEffect(() => {
        const userPromise = UserService.getUser();
        userPromise.then(response => {
            if (response.userId === null) {
                clientRedirect("/" + params.lng);
            } else {
                setShowContent(true);
                setUser(response);
                importOperations(response);
            }
        });
    }, [params.lng]);

    const importOperations = (user: UserType) => {
        if (!user.isNewUser) {
            const dateFrom = new Date();
            dateFrom.setDate(dateFrom.getDate() - 7);
            const dateTo = new Date();
            dateTo.setDate(dateTo.getDate() + 1);
            ApiService.post("/v1/operations/import", {
                dateFrom: formatInputDateTime(dateFrom),
                dateTo: formatInputDateTime(dateTo)
            }).then(response => {
                setImportedOperations(response.added);
            });
        }
    };

    const goToDashboard = () => {
        redirect("/user");
    };

    const toast = () => {
        if (importedOperations > 0) {
            return (
                <Toast horizontal={"end"} vertical={"top"} className={"z-50"}>
                    <Alert dismissible={true} status={"info"} onClose={() => setImportedOperations(0)}>
                        <div className="w-full flex-row justify-between gap-2">
                            <h3>
                                <Trans
                                    i18nKey="LayoutAlert.OperationImportedAlert"
                                    values={{ added: importedOperations }}
                                />
                            </h3>
                        </div>
                    </Alert>
                </Toast>
            );
        }

        return <></>;
    };

    if (!showContent) {
        return <LogoLoader />;
    }
    return (
        <AuthProvider user={user}>
            <ThemeProvider lng={params.lng}>
                <SessionProvider t={t}>
                    <Theme theme="dark" className={"h-screen flex flex-col"}>
                        <div className={"content pb-16 lg:pb-0 overflow-y-auto max-w-[100vw]"}>
                            {toast()}
                            <div className={"flex flex-col col-start-2 row-start-1 overflow-x-auto"}>
                                <Navbar className={"sticky top-0 bg-base-100 z-10 shadow-md"}>
                                    <div className={"flex-1"}>
                                        <div className={"prose"}>
                                            <LayoutHeader />
                                        </div>
                                    </div>
                                    <div className={"flex-none"}>
                                        <UserMenu />
                                    </div>
                                </Navbar>
                                <ErrorBoundary
                                    FallbackComponent={ErrorContent}
                                    onReset={() => {
                                        goToDashboard();
                                    }}
                                >
                                    <main className={"flex-1 overflow-y-auto overflow-x-auto p-2 sm:p-6 bg-base-200"}>
                                        {children}
                                    </main>
                                    <FooterComponent lng={params.lng} />
                                </ErrorBoundary>
                            </div>
                            <MenuLayout />
                        </div>
                    </Theme>
                </SessionProvider>
            </ThemeProvider>
        </AuthProvider>
    );
};

export default UserLayout;
