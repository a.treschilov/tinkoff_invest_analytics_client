import { ItemTypeRatio } from "./UserPortfolio";
import { ComponentState } from "../../components/Common/Types";
import { UserPortfolioItemTypeLoading } from "./UserPortfolioItemTypeLoading";
import { useContext } from "react";
import { ThemeContext } from "../../providers/ThemeProvider";
import { useTranslation } from "../../../app/i18n/client";
import { Card } from "../../components/Common/Card/Card";
import { Divider } from "../../components/Common/Divider/Divider";

type Props = {
    ratio: ItemTypeRatio;
    state: ComponentState;
};

const percentFormatter = new Intl.NumberFormat("ru-RU", {
    style: "percent",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
});

export const UserPortfolioItemType = ({ ratio, state }: Props) => {
    const { lng } = useContext(ThemeContext);
    const { t } = useTranslation(lng, "features/userPortfolio");

    const currencyFormatter: Intl.NumberFormat = new Intl.NumberFormat("ru-RU", {
        style: "currency",
        currency: ratio.market.amount.currency
    });

    const renderLoadingState = () => {
        return <UserPortfolioItemTypeLoading />;
    };

    const renderDataState = () => {
        return (
            <table className={"table w-full"}>
                <thead>
                    <tr>
                        <th></th>
                        <th className={"sm:hidden whitespace-normal"}>
                            {t("ItemTypes.TableHeaderAmount")} / {t("ItemTypes.TableHeaderShare")}
                        </th>
                        <th className={"hidden sm:table-cell"}>{t("ItemTypes.TableHeaderAmount")}</th>
                        <th className={"hidden sm:table-cell"}>{t("ItemTypes.TableHeaderShare")}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{t("ItemTypes.StockMarket")}</td>
                        <td className={"sm:hidden"}>
                            {currencyFormatter.format(ratio.market.amount.amount)}&nbsp; (
                            {percentFormatter.format(ratio.market.ratio)})
                        </td>
                        <td className={"hidden sm:table-cell"}>
                            {currencyFormatter.format(ratio.market.amount.amount)}
                        </td>
                        <td className={"hidden sm:table-cell"}>{percentFormatter.format(ratio.market.ratio)}</td>
                    </tr>
                    <tr>
                        <td>{t("ItemTypes.Deposit")}</td>
                        <td className={"sm:hidden"}>
                            {currencyFormatter.format(ratio.deposit.amount.amount)}
                            &nbsp; ({percentFormatter.format(ratio.deposit.ratio)})
                        </td>
                        <td className={"hidden sm:table-cell"}>
                            {currencyFormatter.format(ratio.deposit.amount.amount)}
                        </td>
                        <td className={"hidden sm:table-cell"}>{percentFormatter.format(ratio.deposit.ratio)}</td>
                    </tr>
                    <tr>
                        <td>{t("ItemTypes.RealEstate")}</td>
                        <td className={"sm:hidden"}>
                            {currencyFormatter.format(ratio.realEstate.amount.amount)}
                            &nbsp; ({percentFormatter.format(ratio.realEstate.ratio)})
                        </td>
                        <td className={"hidden sm:table-cell"}>
                            {currencyFormatter.format(ratio.realEstate.amount.amount)}
                        </td>
                        <td className={"hidden sm:table-cell"}>{percentFormatter.format(ratio.realEstate.ratio)}</td>
                    </tr>
                    <tr>
                        <td>{t("ItemTypes.Crowdlanding")}</td>
                        <td className={"sm:hidden"}>
                            {currencyFormatter.format(ratio.crowdfunding.amount.amount)}
                            &nbsp; ({percentFormatter.format(ratio.crowdfunding.ratio)})
                        </td>
                        <td className={"hidden sm:table-cell"}>
                            {currencyFormatter.format(ratio.crowdfunding.amount.amount)}
                        </td>
                        <td className={"hidden sm:table-cell"}>{percentFormatter.format(ratio.crowdfunding.ratio)}</td>
                    </tr>
                    <tr>
                        <td>{t("ItemTypes.Loan")}</td>
                        <td className={"sm:hidden"}>
                            {currencyFormatter.format(ratio.loan.amount.amount)}
                            &nbsp; ({percentFormatter.format(ratio.loan.ratio + 0)})
                        </td>
                        <td className={"hidden sm:table-cell"}>{currencyFormatter.format(ratio.loan.amount.amount)}</td>
                        <td className={"hidden sm:table-cell"}>{percentFormatter.format(ratio.loan.ratio + 0)}</td>
                    </tr>
                </tbody>
            </table>
        );
    };

    let content;
    switch (state) {
        case "loading":
            content = renderLoadingState();
            break;
        default:
            content = renderDataState();
    }

    return (
        <Card className={"bg-base-100 p-6 mb-6"} border={true}>
            <Card.Title>{t("ItemTypes.Header")}</Card.Title>
            <Divider />
            {content}
        </Card>
    );
};
